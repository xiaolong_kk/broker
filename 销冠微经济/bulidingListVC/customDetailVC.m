//
//  customDetailVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/21.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "customDetailVC.h"
#import "QRCodeGenerator.h"

@interface customDetailVC ()

@end

@implementation customDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationLabel.text = @"客户详情";
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;
    
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,220)];
    //headView.backgroundColor= [UIColor whiteColor];
    
    UIImageView *headViewBg = [[UIImageView alloc] init];
    headViewBg.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10,0,10,0);
    headViewBg.image = [headViewBg.image resizableImageWithCapInsets:insets];
    headViewBg.frame = CGRectMake(15,15,294,160);
    [headView addSubview:headViewBg];
    
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,20,240,20)];
//    titleLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
//    titleLabel.textAlignment = NSTextAlignmentLeft;
//    titleLabel.backgroundColor = [UIColor clearColor];
//    titleLabel.text = @"客户名称";
//    titleLabel.textColor = [AppSession colorWithHexString:@"666666"];
//    [headViewBg addSubview:titleLabel];
//
//    UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(130,20,30,20)];
//    sexLabel.tag=103;
//    sexLabel.text = @"先生";
//    sexLabel.textAlignment=NSTextAlignmentCenter;
//    sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
//    sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
//    sexLabel.backgroundColor=[UIColor clearColor];
//    [headViewBg addSubview:sexLabel];
//
//    UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
//    lineImg.frame = CGRectMake(0,60,290,1);
//    [headViewBg addSubview:lineImg];
//
//    UIView *detailView = [[UIView alloc] initWithFrame:CGRectMake(0,60,290,80)];
//    [headViewBg addSubview:detailView];

    
    for(int i =0;i<4;i++)
    {
        UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(5,i*40,40,40)];
        logoBtn.tag = 300+i;
        [logoBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+2]] forState:UIControlStateNormal];
        //logoBtn.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+1]];
        [headViewBg addSubview:logoBtn];
        UIButton *touchBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,i*40, 290, 40)];
        //[touchBtn addTarget:self action:@selector(nextView:) forControlEvents:UIControlEventTouchUpInside];
        [headViewBg addSubview:touchBtn];
      
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(50,40*i,100,40)];
        titleLabel.tag = 400+i;
        titleLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [headViewBg addSubview:titleLabel];
        
        if(i==0)
        {
            [logoBtn setImage:[UIImage imageNamed:@"custom_face.png"] forState:UIControlStateNormal];
         
            UILabel *sexLabel = [[UILabel alloc] initWithFrame:CGRectMake(130,10,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [headViewBg addSubview:sexLabel];
        }
       if(i==1)
       {
           [logoBtn setImage:[UIImage imageNamed:@"custom_phone.png"] forState:UIControlStateNormal];

       }
        if(i==2)
        {
            [logoBtn setImage:[UIImage imageNamed:@"custom_phone.png"] forState:UIControlStateNormal];

        }
        if(i==3)
      {
          [logoBtn setImage:[UIImage imageNamed:@"custom_pen.png"] forState:UIControlStateNormal];
      }

        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,i*40,290,1);
        [headViewBg addSubview:lineImg];
    }
    
    
    UIImageView *lineImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg2.frame = CGRectMake(0,headViewBg.frame.origin.y+headViewBg.frame.size.height+15,320,1);
    [headView addSubview:lineImg2];
    
    UILabel *taglabel = [[UILabel alloc] init];
    taglabel.tag = 444;
    taglabel.frame=CGRectMake(20,lineImg2.frame.origin.y+lineImg2.frame.size.height+10,120,20);
    taglabel.textColor = [AppSession colorWithHexString:@"666666"];
    taglabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    [headView addSubview:taglabel];

    
    UIImageView *lineImg3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg3.frame = CGRectMake(0,taglabel.frame.size.height+taglabel.frame.origin.y+10,320,1);
    [headView addSubview:lineImg3];
    
    myTable = [[HVTableView alloc] initWithFrame:CGRectMake(0,addHight+44,320,self.view.bounds.size.height-65) expandOnlyOneCell:YES enableAutoScroll:YES];
    myTable.tableHeaderView = headView;
    myTable.HVTableViewDelegate = self;
    myTable.HVTableViewDataSource = self;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    [myTable reloadData];
    [self.view addSubview:myTable];
    [self getData];
    
}
-(void)getData
{
    
    UILabel *titleLabel1 = (UILabel *)[self.view viewWithTag:400];
    UILabel *titleLabel2 = (UILabel *)[self.view viewWithTag:401];
    UILabel *titleLabel3 = (UILabel *)[self.view viewWithTag:402];
    UILabel *titleLabel4 = (UILabel *)[self.view viewWithTag:403];
    UILabel *sexLabel  = (UILabel *)[self.view viewWithTag:103];
    
    UILabel *tagLabel = (UILabel *)[self.view viewWithTag:444];

    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.weixincustomer.getcustomerdetail&Customerkid=%@&BrokerKid=%@&PageIndex=1",self.kid_str,theApp.brokerKid];
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....." didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         // NSLog(@"%@",request.responseString);
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSDictionary *dict2 = [dict objectForKey:@"data"];
             NSLog(@"%@",dict2);
             NSString *name_str = [dict2 objectForKey:@"F_Title"];
             titleLabel1.text = name_str;
             titleLabel2.text = [dict2 objectForKey:@"F_Phone1"];
             titleLabel3.text = [dict2 objectForKey:@"F_Phone2"];
             titleLabel4.text = [dict2 objectForKey:@"F_Remark"];
             sexLabel.text = [dict2 objectForKey:@"F_Sex"];
             sexLabel.center = CGPointMake((name_str.length*15+titleLabel1.frame.origin.x+10), 22);
             tagLabel.text = [NSString stringWithFormat:@"推荐楼盘 %@",[dict2 objectForKey:@"allCount"]];
             NSArray *arr = [dict2 objectForKey:@"list_itme"];
             dataSource = [NSMutableArray arrayWithArray:arr];
             
         }
         
         //NSLog(@"%@",self.dataSource);
         
         if(dataSource.count>0)
         
         [myTable reloadData];
         
     }
        didFailedRequest:^(ASIHTTPRequest *request){
                     
    }];
}

-(void)encodeBtn:(UIButton *)btn
{
    
    
    sortView=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, theApp.window.bounds.size.height)];
    shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    [sortView addSubview:shadowView];
    sortView.hidden = YES;
    [[[UIApplication sharedApplication] keyWindow] addSubview:sortView];
    
    NSInteger i  = btn.tag-2222;
    NSDictionary *dict = dataSource[i];
    
    
    NSDictionary *qr_dict = [dict objectForKey:@"QRCode"];
    
    NSMutableString *jsonString = [[NSMutableString alloc] init];
    
    NSString *str1 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"F_BuildingKid"]];
    NSString *str2 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"F_WeiXinBrokerKid"]];
    NSString *str3 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"Kid"]];
    NSString *str4 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"Type"]];
    
    //        timeStr  = [timeStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$€^•'@#$%^&*()_+'\""];
    //
    //        NSString *trimmedString = [timeStr stringByTrimmingCharactersInSet:set];
    //        trimmedString  = [trimmedString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *string  = [NSString stringWithFormat:
                         @"{\"F_BuildingKid\":\"%@\",\"F_WeiXinBrokerKid\":\"%@\",\"Kid\":\"%@\",\"Type\":\"%@\"}",str1,str2,str3,str4];
    
    [jsonString appendString:string];
    
    
    //    NSUInteger location = [jsonString length]-1;
    //
    //    NSRange range       = NSMakeRange(location, 1);
    
    // 4. 将末尾逗号换成结束的]}
    // [jsonString replaceCharactersInRange:range withString:@"]"];
    
    // NSLog(@"%@",jsonString);
    CGFloat qrSize = self.view.bounds.size.width - 10 * 2;
    
    UIView *codeBg =[[UIView alloc] initWithFrame:CGRectMake(10, (self.view.bounds.size.height - qrSize) / 2,
                                                             qrSize, qrSize)];
    codeBg.backgroundColor = [UIColor whiteColor];
    [sortView addSubview:codeBg];
    UIImage* image = [QRCodeGenerator qrImageForString:jsonString imageSize:300];
    
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0,0,qrSize, qrSize);
    [codeBg addSubview:imageView];
    [imageView layer].magnificationFilter = kCAFilterNearest;
    
    UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,44,320,self.view.bounds.size.height)];
    [bgBtn addTarget:self action:@selector(ViewHidden:) forControlEvents:UIControlEventTouchUpInside];
    [sortView addSubview:bgBtn];
    
    [AppSession fadeIn:sortView];
    
    
}

-(void)ViewHidden:(id)sender
{
    [AppSession fadeOut:sortView];
}



#pragma mark - dataSource
//perform your expand stuff (may include animation) for cell here. It will be called when the user touches a cell
-(void)tableView:(UITableView *)tableView expandCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    // 展开
    NSDictionary *dic = dataSource[indexPath.row];
    NSArray *logList = [dic objectForKey:@"LogList"];
    NSString *strLogList = [NSString stringWithFormat:@"%@",logList];
    if (![strLogList isEqualToString:@"<null>"]) {
        [self addLabel:cell logList:logList];
        [UIView animateWithDuration:0.3 animations:^{
            UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
            imageView.frame = CGRectMake(imageView.frame.origin.x, 105 + 20 * logList.count ,imageView.frame.size.width, imageView.frame.size.height);
        }];
    }
}

-(void)addLabel:(UITableViewCell *)cell logList:(NSArray *)logList
{
    for (int n = 0; n < logList.count; n ++)
    {
        [[cell viewWithTag: n + 1001] removeFromSuperview];
        [[cell viewWithTag: n + 2002] removeFromSuperview];
        [[cell viewWithTag: n + 3003] removeFromSuperview];
        [[cell viewWithTag: n + 5005] removeFromSuperview];
        [[cell viewWithTag: n + 4004] removeFromSuperview];
        
        
        //        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10, 100 + 20 * n, 200, 16)];
        //        lable.tag = 100 + n;
        //        NSDictionary *dict = logList[n];
        //        lable.text = [NSString stringWithFormat:@"%@   %@",[dict objectForKey:@"F_BuildingKid"],[dict objectForKey:@"F_Remark"]];
        //        [cell addSubview:lable];
        
        float cell_hight = 110;
        UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,cell_hight+20*n,12,12)];
        tagImg.tag = 1001+n;
        tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
        [cell addSubview:tagImg];
        
        UIImageView *process_lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3+20*n+cell_hight,7,20)];
        process_lineImg.tag=4004+n;
        process_lineImg.image = [UIImage imageNamed:@"process_line.png"];
        [cell addSubview:process_lineImg];
        
        UIImageView *process_pointImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3+20*n+cell_hight,7,7)];
        process_pointImg.image = [UIImage imageNamed:@"process_point.png"];
        process_pointImg.tag = 5005+n;
        [cell addSubview:process_pointImg];
        
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(45,-15+20*n+cell_hight,150,50)];
        tagLabel.tag = 2002+n;
        tagLabel.numberOfLines = 0;
        tagLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        tagLabel.textAlignment = NSTextAlignmentLeft;
        tagLabel.backgroundColor = [UIColor clearColor];
        tagLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [cell addSubview:tagLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(320-115,-3+20*n+cell_hight,100,20)];
        timeLabel.tag = 3003+n;
        timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        timeLabel.textAlignment = NSTextAlignmentRight;
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [cell addSubview:timeLabel];
        
        NSDictionary *dict = logList[n];
        tagLabel.text = [dict objectForKey:@"F_Remark"];
        timeLabel.text = [dict objectForKey:@"F_AddTime"];
        timeLabel.text = [timeLabel.text stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        timeLabel.text = [timeLabel.text substringToIndex:16];
        
        
    }
}

//perform your collapse stuff (may include animation) for cell here. It will be called when the user touches an expanded cell so it gets collapsed or the table is in the expandOnlyOneCell satate and the user touches another item, So the last expanded item has to collapse
-(void)tableView:(UITableView *)tableView collapseCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    //关闭
    [UIView animateWithDuration:0.3 animations:^{
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
        imageView.frame = CGRectMake(imageView.frame.origin.x,105 ,imageView.frame.size.width, imageView.frame.size.height);
        imageView.image = [UIImage imageNamed:@"line_show.png"];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    //you can define different heights for each cell. (then you probably have to calculate the height or e.g. read pre-calculated heights from an array
    if (isexpanded){
        NSDictionary *dic = dataSource[indexPath.row];
        NSMutableArray *logList = [dic objectForKey:@"LogList"];
        NSString *strLogList = [NSString stringWithFormat:@"%@",logList];
        if (![strLogList isEqualToString:@"<null>"]) {
            return 105+20*logList.count;
        }
    }
    return 105;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    //static NSString *CellIdentifier = @"CustomCellIdentifier";
    NSString *CellIdentifier = [NSString stringWithFormat:@"cell%d",indexPath.row];//以indexPath来唯一确定cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
        // headView.backgroundColor= [UIColor whiteColor];
        [cell addSubview:headView];
        
        UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,20,275,20)];
        budildingLabel.tag = 105;
        budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        budildingLabel.textAlignment = NSTextAlignmentLeft;
        budildingLabel.backgroundColor = [UIColor clearColor];
        budildingLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [headView addSubview:budildingLabel];
        
        UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(240,20,67,20)];
        clickBtn.tag = indexPath.row+2222;
        [clickBtn setBackgroundImage:[UIImage imageNamed:@"btn_code.png"] forState:UIControlStateNormal];
        [clickBtn addTarget:self action:@selector(encodeBtn:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:clickBtn];
        
        NSArray *arr = @[@"推荐有效",@"已到访",@"已认筹",@"已预定",@"已成交"];
        for(int i=0;i<5;i++)
        {
            UIView *lineBg = [[UIView alloc] initWithFrame:CGRectMake(10,50,300,60)];
            [headView addSubview:lineBg];
            
            UIImageView *pointlineImg = [[UIImageView alloc] initWithFrame:CGRectMake(i*60,0,60,10)];
            pointlineImg.tag = 222+i;
            pointlineImg.image = [UIImage imageNamed:@"pointline_gray.png"];
            [lineBg addSubview:pointlineImg];
            
            UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,15,60,20)];
            stateLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            stateLabel.textAlignment = NSTextAlignmentCenter;
            stateLabel.text =arr[i];
            stateLabel.tag = 333+i;
            stateLabel.backgroundColor = [UIColor clearColor];
            stateLabel.textColor = [AppSession colorWithHexString:@"000000"];
            [lineBg addSubview:stateLabel];
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,35,60,20)];
            timeLabel.tag = 555+i;
            timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            timeLabel.textAlignment = NSTextAlignmentCenter;
            timeLabel.backgroundColor = [UIColor clearColor];
            timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [lineBg addSubview:timeLabel];
        }
        
        UIImageView *exImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,105,320,9)];
        exImg.tag = 7;
        exImg.image = [UIImage imageNamed:@"line_show.png"];
        [cell.contentView addSubview:exImg];
    }
    
    NSDictionary *dict = dataSource[indexPath.row];
    UILabel *budildingLabel = (UILabel *)[cell viewWithTag:105];
    
    budildingLabel.text = [dict objectForKey:@"F_Title"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    UILabel *stateLabel = (UILabel *)[cell viewWithTag:334];
    UILabel *timeLabel1 = (UILabel *)[cell viewWithTag:555];
    UILabel *timeLabel2 = (UILabel *)[cell viewWithTag:556];
    UILabel *timeLabel3 = (UILabel *)[cell viewWithTag:557];
    UILabel *timeLabel4 = (UILabel *)[cell viewWithTag:558];
    UILabel *timeLabel5 = (UILabel *)[cell viewWithTag:559];
    
    UIImageView *pointlineImg1 = (UIImageView *)[cell viewWithTag:222];
    UIImageView *pointlineImg2 = (UIImageView *)[cell viewWithTag:223];
    UIImageView *pointlineImg3 = (UIImageView *)[cell viewWithTag:224];
    UIImageView *pointlineImg4 = (UIImageView *)[cell viewWithTag:225];
    UIImageView *pointlineImg5 = (UIImageView *)[cell viewWithTag:226];
    
    NSDictionary *dict_valid = [dict objectForKey:@"Valid"];
    NSDictionary *dict_come = [dict objectForKey:@"Come"];
    NSDictionary *dict_ticket = [dict objectForKey:@"Ticket"];
    NSDictionary *dict_preordain = [dict objectForKey:@"Preordain"];
    NSDictionary *dict_business = [dict objectForKey:@"Business"];
    
    if([[dict_valid objectForKey:@"values"] intValue]==1)
    {
        pointlineImg1.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel1.text = [dict_valid objectForKey:@"Times"];
    }
    if([[dict_come objectForKey:@"values"] intValue]==1)
    {
        pointlineImg2.image = [UIImage imageNamed:@"pointline_orange.png"];
        stateLabel.text = [dict_come objectForKey:@"Names"];
        timeLabel2.text = [dict_come objectForKey:@"Times"];
        
    }
    if([[dict_ticket objectForKey:@"values"] intValue]==1)
    {
        pointlineImg3.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel3.text = [dict_ticket objectForKey:@"Times"];
    }
    if([[dict_preordain objectForKey:@"values"] intValue]==1)
    {
        pointlineImg4.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel4.text = [dict_preordain objectForKey:@"Times"];
    }
    if([[dict_business objectForKey:@"values"] intValue]==1)
    {
        pointlineImg5.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel5.text = [dict_business objectForKey:@"Times"];
    }
    
    
    if (dataSource.count > 0)
    {
        NSString *strLogList = [NSString stringWithFormat:@"%@",[dict objectForKey:@"LogList"]];
        
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
        
        [cell.contentView viewWithTag:7].hidden = [strLogList isEqualToString:@"<null>"]?YES:NO;
        NSArray *logList = [dict objectForKey:@"LogList"];
        
        
        if (![strLogList isEqualToString:@"<null>"])
        {
            
            for (int n = 0; n < logList.count; n ++)
            {
                [[cell viewWithTag: n + 1001] removeFromSuperview];
                [[cell viewWithTag: n + 2002] removeFromSuperview];
                [[cell viewWithTag: n + 3003] removeFromSuperview];
                [[cell viewWithTag: n + 5005] removeFromSuperview];
                [[cell viewWithTag: n + 4004] removeFromSuperview];
            }

            
            imageView.frame = CGRectMake( [cell.contentView viewWithTag:7].frame.origin.x,
                                         isExpanded ? (105 + 20*logList.count):105,
                                         [cell.contentView viewWithTag:7].frame.size.width,
                                         [cell.contentView viewWithTag:7].frame.size.height);
            imageView.image = [UIImage imageNamed:@"line_hide.png"];
            if (isExpanded)
            {
                [self addLabel:cell logList:logList];
            }
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)buttonClicked:(UIButton *)btn
{
    // NSLog(@"%ld",btn.tag);
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
