//
//  CustomAvatarImageView.m
//  greentea
//
//  Created by xu shun wang on 13-6-17.
//
//

#import "CustomAvatarImageView.h"

@implementation CustomAvatarImageView
{
    float r;
}
@synthesize image = _image;
-(void)dealloc
{
    self.image=nil;
    [super dealloc];
}
- (id)initWithPosition:(CGPoint)position radius:(float)radius
{
    self = [super initWithFrame:CGRectMake(position.x,position.y, 2*radius, 2*radius)];
    if (self) {
        // Initialization code
        r=radius;
        self.backgroundColor=[UIColor clearColor];
    }
    return self;
}
-(void)setImage:(UIImage *)image
{
    [_image release];
    _image=[image retain];
    
    [self setNeedsDisplay];
}
- (void)drawRect:(CGRect)rect
{
//    [super drawRect:rect];
    
    CGContextRef context=UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    
    CGRect bounds=self.bounds;
    float dt=bounds.size.width/100*2.5f;
    
    bounds.origin.x=dt;
    bounds.origin.y=dt;
    bounds.size.width=bounds.size.width-2*dt;
    bounds.size.height=bounds.size.height-2*dt;
    
    
//
//    CGContextClearRect(context, bounds);
    
//    UIRectFillUsingBlendMode(bounds, kCGBlendModeClear);
////    CGContextSetBlendMode(context, kCGBlendModeClear);
//    [[UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] set];
////
//    UIRectFill(bounds);
    
    [[UIImage imageNamed:@"head_picbg.png"] drawInRect:rect];
    
    
    
    [[UIBezierPath bezierPathWithRoundedRect:bounds cornerRadius:r-dt] addClip];
    
    
    
    [self.image drawInRect:bounds];
}

- (void) setImageWithURL:(NSString *)url
{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    // Remove in progress downloader from queue
    [manager cancelForDelegate:self];
    
//    self.image = [UIImage imageNamed:@"cm_user_avatar.png"];
    
    if (url)
    {
        [manager downloadWithURL:[NSURL URLWithString:url] delegate:self options:0];
    }
}

- (void)setWebImage:(NSString *)url placeholderImage:(UIImage *)placeholder
{
    [self setWebImage:[NSURL URLWithString:url] placeholderImage:placeholder];
}

#pragma mark - SDWebImageManagerDelegate回调


- (void)webImageManager:(SDWebImageManager *)imageManager didProgressWithPartialImage:(UIImage *)image forURL:(NSURL *)url
{
    self.image = image;
}

- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image
{
    self.image = image;
}
@end