//
//  MainVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/15.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VRGCalendarView.h"
#import "MJRefresh.h"
@interface MainVC : BaseViewController<UIScrollViewDelegate>
{
    UIView *sortView;
    UIView *countdownView;
    NSMutableArray *bulidingListArr;
    NSMutableArray *tellBookArr;
    UITableView *TellBooktableView;
    UITableView *budliingListTableView;
    UIImageView *menuImg;
    
    int num;
    BOOL upOrdown;
    NSTimer * timer;
    MJRefreshHeaderView *_header;
    
    BOOL isYesOrNoInView;
    
    NSInteger select_length;
    
    UIScrollView *_scrollView;
    
}

@property(nonatomic,strong) VRGCalendarView *calendar;
@property(nonatomic,strong)  UIView *calenderViewBg;
@property(nonatomic,retain) NSString *dayDateString;
@property(nonatomic,strong) NSDate *selectDate;
@property (nonatomic, strong) UIImageView * line;
@property (nonatomic,retain) NSString *F_IsRealTimeStr;
@property (nonatomic,retain) NSString *isDefine_str;
@property (nonatomic,retain) NSString *isDefine_time;

@end
