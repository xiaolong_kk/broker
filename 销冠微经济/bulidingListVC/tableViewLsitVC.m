//
//  tableViewLsitVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/30.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "tableViewLsitVC.h"

@implementation tableViewLsitVC


- (id) initWithFrame:(CGRect)frame
{
    self=[super init];
    if(self)
    {
        _tableView1 = [[UITableView alloc] initWithFrame:CGRectMake(0,0,160,250)];
        _tableView1.delegate = self;
        _tableView1.dataSource = self;
        // _tableView1.hidden = YES;
        _tableView1.showsVerticalScrollIndicator = NO;
        _tableView1.separatorStyle=UITableViewCellSeparatorStyleNone;
        //[self.view addSubview:_tableView1];
        _tableView2 = [[UITableView alloc] initWithFrame:CGRectMake(160,0,160,250)];
        _tableView2.delegate = self;
        _tableView2.dataSource = self;
        // _tableView2.hidden = YES;
        _tableView2.showsVerticalScrollIndicator = NO;
        _tableView2.separatorStyle=UITableViewCellSeparatorStyleNone;
       // [self.view addSubview:_tableView2];

    }
    return self;
}
- (void)viewDidLoad
{
    
    [self setData];
     [self.view addSubview:_tableView1];
    [self.view addSubview:_tableView2];


}
-(void)setData
{
    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];

    self.tableArray = [userDefault objectForKey:@"wuyeData"];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView==_tableView1)
    {
        return  self.tableArray.count;
    }
    if(tableView==_tableView2)
    {
        return  self.tableArray2.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==_tableView1)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,39,320,1);
            [cell addSubview:lineImg];
            
        }
        NSInteger selected_cell = 0;
        
//        if(self.selectInt==1)
//        {
//            selected_cell = cell_index1;
//            dict = self.tableArray[indexPath.row];
//            cell.textLabel.text = [dict objectForKey:@"SortName"];
//        }
//        if(self.selectInt==2)
//        {
//            selected_cell = cell_index2;
//            dict = self.tableArray2[indexPath.row];
//            cell.textLabel.text = [dict objectForKey:@"ParameterValue"];
//        }
        
        if(selected_cell==indexPath.row)
        {
            cell.textLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
        }
        else
        {
            cell.textLabel.textColor = [AppSession colorWithHexString:@"000000"];
            
        }
        
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    if(tableView==_tableView2)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,39,320,1);
            [cell addSubview:lineImg];
            
        }
        
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    
    return nil;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_tableView1)
    {
   
        
        [_tableView1 reloadData];
    }
    if(tableView==_tableView2)
    {
        
    }
    
}

@end
