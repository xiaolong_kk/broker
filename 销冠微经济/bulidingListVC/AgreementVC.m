//
//  AgreementVC.m
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-6.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import "AgreementVC.h"

@interface AgreementVC ()

@end

@implementation AgreementVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;

   // NSString *boolStr = [NSString stringWithFormat:@"%@",self.Agreement?@"YES":@"NO"];

//    float temp_H=0;
//    if([boolStr isEqualToString:@"YES"])
//        temp_H=130;
    
    self.navigationLabel.text = @"分销协议";

    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,addHight+44,320,self.view.bounds.size.height-addHight-130)];
    webView.delegate = self;
    webView.backgroundColor = [UIColor whiteColor];
    webView.scalesPageToFit = YES;
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/BuildingAgreement.aspx?BuildingKid=%@&CKBrokerKid=%@",self.buildingKid_str,theApp.brokerKid];
    NSURL *url = [NSURL URLWithString:str];
    NSURLRequest *request =[NSURLRequest requestWithURL:url];
    [webView loadRequest:request];
    [self.view addSubview:webView];
    
    
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, webView.frame.origin.y+webView.frame.size.height, 320,130)];
    bottomView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bottomView];
    
    //if([boolStr isEqualToString:@"YES"])
        //bottomView.hidden = YES;
    
    UIButton *agreementBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    agreementBtn.frame=CGRectMake(80,0,50,30);
    [agreementBtn addTarget:self action:@selector(agree:) forControlEvents:UIControlEventTouchUpInside];
    [agreementBtn setImage:[UIImage imageNamed:@"check_box.png"] forState:UIControlStateNormal];
    agreementBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-10,0,0);
   // agreementBtn.backgroundColor = [AppSession colorWithHexString:@"f98156"];
    [agreementBtn setTitle:@"同意"forState:UIControlStateNormal];
    [agreementBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    agreementBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
    agreementBtn.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    [bottomView addSubview:agreementBtn];

    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(135,6,220,20)];
    tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel.textAlignment = NSTextAlignmentLeft;
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.text = @"《经纪人分销约定》";
    tempLabel.textColor = [AppSession colorWithHexString:@"f98156"];
    [bottomView addSubview:tempLabel];
    
    UIButton *add_customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    add_customBtn.frame = CGRectMake(15,35,290,35);
    [add_customBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [add_customBtn setTitle:@"签订协议" forState:UIControlStateNormal];
    add_customBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
    [add_customBtn addTarget:self action:@selector(AgreementSuccess) forControlEvents:UIControlEventTouchUpInside];
    [add_customBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [bottomView addSubview:add_customBtn];

    self.view.backgroundColor = [UIColor whiteColor];

}

-(void)agree:(UIButton *)btn
{
    if(!isYseOrNo)
    {
        [btn setImage:[UIImage imageNamed:@"check_box_hl.png"] forState:UIControlStateNormal];
        agree_int=1;
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"check_box.png"] forState:UIControlStateNormal];
        agree_int=0;
    }
    
    isYseOrNo =!isYseOrNo;
}

-(void)AgreementSuccess
{
    
    if(agree_int==0)
    {
        [self showMBAlertView:self.view message:@"请先同意经纪人分销协议" duration:1.5];
        
        return;

    }
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinagreement.getweixinagreementaffix&BrokerKid=%@&AgreementKid=%@&BuildingKid=%@",theApp.brokerKid,self.AgreementKid_str,self.buildingKid_str];
    // NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         NSString *str = [dict objectForKey:@"msg"];
         [self showMBAlertView:self.view message:str duration:1];

     }
                 didFailedRequest:^(ASIHTTPRequest *request)
     {
         
     }];

}


//-(void)getData
//{
//    
//    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinagreement.getweixinagreementbykid&CKBrokerKid=%@&AgreementKid=%@&BuildingKid=%@",theApp.brokerKid,self.AgreementKid_str,self.buildingKid_str];
//     NSLog(@"%@",str);
//    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
//     {
//         
//         NSDictionary *dict= [request.responseString objectFromJSONString];
//         if([[dict objectForKey:@"code"] intValue] ==0)
//         {
//             NSString *F_Content  =[[dict objectForKey:@"data"] objectForKey:@"F_Content"];
//             [webView  loadHTMLString:F_Content baseURL:nil];
//         }
//         else
//         {
//             [self showMBAlertView:self.view message:[dict objectForKey:@"msg"] duration:1.5];
//
//         }
//     }
//        didFailedRequest:^(ASIHTTPRequest *request)
//    {
//        
//    }];
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
