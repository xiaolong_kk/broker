//
//  QDCheck.h
//  TopSales
//
//  Created by zen huang on 14/7/31.
//
//

#import <UIKit/UIKit.h>
@protocol QDCheckDelegate;

@interface QDCheck : UIButton{
    NSString                        *_groupId;
    BOOL                            _checked;
    __unsafe_unretained id<QDCheckDelegate>       _delegate;
}

@property(nonatomic, assign)id<QDCheckDelegate>   delegate;
@property(nonatomic, copy, readonly)NSString            *groupId;
@property(nonatomic, assign)BOOL checked;


- (id)initWithDelegate:(id)delegate groupId:(NSString*)groupId;

@end

@protocol QDCheckDelegate <NSObject>

@optional

- (void)didSelectedRadioButton:(QDCheck *)radio groupId:(NSString *)groupId;


@end
