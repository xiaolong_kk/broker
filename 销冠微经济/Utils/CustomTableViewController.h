//
//  CustomTableViewController.h
//  baseUtil
//
//  Created by Chai Xue Liang on 12-10-15.
//  Copyright (c) 2012年 xu shun wang. All rights reserved.
//

#import "BaseViewController.h"
#import "PullingRefreshTableView.h"
@interface CustomTableViewController : BaseViewController<UITableViewDataSource,UITableViewDelegate,PullingRefreshTableViewDelegate>
@property (nonatomic,retain) PullingRefreshTableView *table;//table对象
@property (nonatomic,retain) NSMutableArray *tableDataArray;//当前显示的列表数据,注意当替换tableDataArray的时候，会调用setTableDataArray方法，将会把当前tableDataArray数据缓存。
@property (nonatomic,retain) NSString *refreshAllURLStr;//列表刷新全部数据的url字符串
@property (nonatomic,retain) NSString *refreshBehindURLStr;//列表加载后面一页url字符串，如果是get方法，这个值不用设，会在回调里面变化。如果是post方式，这个值需要设置
@property (nonatomic,retain) NSString *noResultStr;//没有结果显示的文字。默认显示“无数据”
@property BOOL requestInPost;//是否是以post方式调用连接，默认否，以get方式调用
@property (nonatomic,retain) NSDictionary *refreshAllPostDic;//如果以post方式调用方法，则这个dic中的值将会在调用refreshAll方法的时候被附加上去。
@property BOOL needFirstCellCustom;//是否需要列表第一项变为自定义，自定义后可以在此放入图片浏览器之类的东西，默认否
@property float customFirstCellHeight;//如果需要第一个cell自定义，此单元格高度通过此值设定
@property BOOL isRefreshing;//是否正在刷新数据中
@property int currrentPage;//当前显示的数据是第几页
@property float customRowHeight;//默认90高度，想要改变其他的自己设置
@property BOOL isRowHeightVariable;//设置行高是否可以变，默认否，行高根据customRowHeight的值，如果yes，行高可变，根据cell.frame.size.height值调整，默认NO
@property BOOL isOddList;//列表显示的时候是否单行与双行背景显示不一样，默认否,即都显示一样
@property BOOL isTableDataCached;//是否缓存列表数据，默认否。如果要缓存列表数据，则表格将会把最后一次列表展示的数据缓存在本地。设置此属性后，一定要设置cachedTableDataName。缓存数据是以此名字保存的。缓存数据在本地是在此tableView被释放的时候才会缓存，因此要注意释放此视图。当缓存数据为空的时候，将自动调用一次refreshAll方法。
@property (nonatomic,retain) NSString *cachedTableDataName;//默认nil，如果设定isTableDataCached为YES，但是此值为nil，也不会有缓存效果。
@property (nonatomic,assign) id delegate;//delegate不要retain父类
@property (nonatomic,assign) UIView *target;//显示弹出框之类的视图
@property(nonatomic) int AllItemCount;

- (id) initWithFrame:(CGRect)frame needRefreshBehind:(BOOL)needRefreshBehind;
- (id) initWithFrame:(CGRect)frame needRefreshBehind:(BOOL)needRefreshBehind needHeader:(BOOL) needHeader;//当设置needHeader为no的时候，则不会出现头，并且列表自带刷新活动将不会发生。如缓存数据为空的时候，needHeader为NO，则不会自动刷新。
- (void) finishRefresh;//停止刷新数据，第一个参数YES：是刷新全部数据，NO：获取后面数据 done，YES：数据获取成功，NO：数据获取失败
- (void) refreshAll;
- (void) reloadTableData;//刷新列表，根据当前tableDataArray刷新
- (void) reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation NS_AVAILABLE_IOS(3_0);
- (void) reloadTableDataFromCache;//刷新列表，从缓存中取出名字为cachedTableDataName的数据，要开启table cache，并设置好cachedTableDataName

- (NSMutableArray *)tableDataArrayFromCacheWithName:(NSString *)name;//从缓存中根据名字取出列表数组
- (void) saveTableDataArrayToCache:(NSString *)name array:(NSArray *)array;//将数组存入缓存，根据名字
@end

#pragma mark 协议
@protocol CustomTableViewDelegate <NSObject>
@required
- (UITableViewCell *) tableView:(UITableView *)tableView rowNum:(NSInteger) rowNum dataDictionary:(NSDictionary *) dataDictionary;//如果是可变列表，需要在此方法返回计算后的列表高度cell.frame.size.height;
@optional
- (void) didStartRefreshAll;//开始更新数据，调用此方法
- (void) didSucceedRefreshAll;//刷新全部成功，调用此方法
- (void) didFailedRefreshAll;//刷新全部失败，调用此方法

- (void) didStartRefreshBehind;//开始获取后面的列表，调用此方法
- (void) didSucceedRefreshBehind;//刷新后面成功，调用此方法
- (void) didFailedRefreshBehind;//刷新后面失败，调用此方法

- (void) didSelectRow:(UITableView *)tableView cell:(UITableViewCell *) cell rowNum:(NSInteger) rowNum dataDictionary:(NSDictionary *) dataDictionary;
- (NSString *) refreshBehindStr:(int) currentPage currentDataArray:(NSArray *)currentDataArray;//get方式上拉更新后面的数据时候，调用此方法得到url地址
- (NSDictionary *) refreshBehindDictonary:(int) currentPage currentDataArray:(NSArray *)currentDataArray;//post方式上拉更新后面的数据时候，调用此方法得到url地址

- (UIView *) customFirstCell:(UITableView *)tableView;//如果needFirstCellCustom为yes，则第一个cell由用户自己定义，此方法返回自定义的view,

- (void)tableDidScroll:(UIScrollView *)scrollView;
- (void)tableDidEndScrollingAnimation:(UIScrollView *)scrollView;
- (void)tableDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (void)customTableView:(UITableView *)tableView willShowCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
//-(void)showCustomAlertView:(UIView *)currentView message:(NSString *) message image:(UIImage *) image duration:(float) duration;
@end