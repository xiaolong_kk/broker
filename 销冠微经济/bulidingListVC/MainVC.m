//
//  MainVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/15.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "MainVC.h"
#import "addCustomVC.h"
#import "groomCustomVC.h"
#import "SMPageControl.h"
#import "AttributedLabel.h"
#import "superMarkVC.h"
#import "bulidingListVC.h"
#import "applyVC.h"
#import "definition_timoutVC.h"
#import "myCustomVC.h"
#import "youxiaoCustomVC.h"
#import "messageVC.h"
#import "planViewController.h"

#import "definitioningVC.h"

@interface MainVC ()

@end

@implementation MainVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationSubLabel.hidden = YES;
    self.logoImg.hidden = YES;
    self.navigationLabel.text = @"首页";
    self.navigationLeftTitleBtn.hidden  = YES;

    //右边按钮
    self.navigationRightTitleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    self.navigationRightTitleBtn.frame=CGRectMake(271,3,40,44);
    [self.navigationRightTitleBtn setImage:[UIImage imageNamed:@"mian_rightBtn.png"] forState:UIControlStateNormal];
    //[self.navigationRightTitleBtn addTarget:self action:@selector(setupCamera) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBg addSubview:self.navigationRightTitleBtn];
    //左边按钮
    self.navigationLeftTitleBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    self.navigationLeftTitleBtn .frame=CGRectMake(0,-6,60,60);
    [self.navigationLeftTitleBtn setImage:[UIImage imageNamed:@"main_leftBtn.png"] forState:UIControlStateNormal];
    [self.navigationLeftTitleBtn addTarget:self action:@selector(msgVC) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBg addSubview:self.navigationLeftTitleBtn];

    [self initMainView];
    // Do any additional setup after loading the view.
}

-(void)msgVC
{
    messageVC *vc = [[messageVC alloc] initWithNavigationBar:YES];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)initMainView
{
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;
    [theApp.userDefault setObject:@"1" forKey:@"FirstUse"];

     _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,self.view.bounds.size.height)];
    _scrollView.delegate =self;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(320,570);
    [self.view addSubview:_scrollView];
    
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,0,320,153)];
    [scrollView setContentSize:CGSizeMake(320*4,153)];
    [scrollView setPagingEnabled:YES];
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    for(int i=0;i<4;i++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320*i,0,320,153)];
         imageView.tag = 5004+i;
        [scrollView addSubview:imageView];
    }
    scrollView.delegate = self;
    
    [_scrollView addSubview:scrollView];

    [self addHeader];
    
    
    UIImageView *index_line = [[UIImageView alloc] initWithFrame:CGRectMake(0,153,320,4)];
    index_line.image = [UIImage imageNamed:@"index_line.png"];
    [_scrollView addSubview:index_line];
    
    UIImageView *index_line_hl = [[UIImageView alloc] initWithFrame:CGRectMake(0,153,80,4)];
    index_line_hl.tag = 10001;
    index_line_hl.image = [UIImage imageNamed:@"index_line_hl.png"];
    [_scrollView addSubview:index_line_hl];
    
    
    UIView *midView = [[UIView alloc] initWithFrame:CGRectMake(0,163, 320,293)];
    midView.userInteractionEnabled = YES;
    [_scrollView addSubview:midView];
    
    UIImageView *midViewImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0,320,293)];
    midViewImg.image = [UIImage imageNamed:@"main_index.png"];
   // midViewImg.userInteractionEnabled = YES;
    [midView addSubview:midViewImg];
    
    NSArray *arr = @[@"个超积分",@"个楼盘",@"个新楼盘",@"个客户",@"个有效客户",@"个超级客户",@"个正在界定",@"个超时客户",@"个申请确认"];
    for(int i=0;i<9;i++)
    {
        
//        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake((i % 3) * 106, 293/3*(i/3)+60,60,30)];
//        tempLabel.backgroundColor = [UIColor clearColor];
//        tempLabel.textAlignment = NSTextAlignmentCenter;
//        tempLabel.tag = 2005+i;
//        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
//        tempLabel.textColor = [AppSession colorWithHexString:@"999999"];
//        [midView addSubview:tempLabel];
      
        UILabel *tempLabel1 = [[UILabel alloc] initWithFrame:CGRectMake((i % 3)*106,293/3*(i/3)+60,40,30)];
        tempLabel1.backgroundColor = [UIColor clearColor];
        tempLabel1.textAlignment = NSTextAlignmentRight;
        tempLabel1.tag = 2005+i;
        tempLabel1.font=[UIFont fontWithName:stringFontOfSize size:15];
        tempLabel1.textColor = [AppSession colorWithHexString:@"75a7be"];
        [midView addSubview:tempLabel1];

        UILabel *tempLabel2 = [[UILabel alloc] initWithFrame:CGRectMake((i % 3)*106+30, 293/3*(i/3)+60,80,30)];
        tempLabel2.backgroundColor = [UIColor clearColor];
        tempLabel2.textAlignment = NSTextAlignmentCenter;
        tempLabel2.text = arr[i];
        tempLabel2.tag = 3005+i;
        tempLabel2.font=[UIFont fontWithName:stringFontOfSize size:11];
        tempLabel2.textColor = [AppSession colorWithHexString:@"999999"];
        [midView addSubview:tempLabel2];
        
        UIButton *clickBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        clickBtn.tag = 1005+i;
       // clickBtn.backgroundColor = [UIColor blackColor];
        clickBtn.frame=CGRectMake(5+(i % 3) * 101, 86 * (i / 3),105,85);
        [clickBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        clickBtn.titleLabel.font = [UIFont systemFontOfSize:13];//title字体大小
        clickBtn.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
        clickBtn.titleEdgeInsets = UIEdgeInsetsMake(70,0,0,0);//设置title在b
        [clickBtn addTarget:self action:@selector(onClick:) forControlEvents:UIControlEventTouchUpInside];
        [midView addSubview:clickBtn];
    }
    
    
}

-(void)popToGroomCustomView
{
    groomCustomVC *vc = [[groomCustomVC alloc] initWithNavigationBar:YES];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)onClick:(UIButton *)btn
{
    if(btn.tag==1005)
    {
        superMarkVC *vc = [[superMarkVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController  pushViewController:vc animated:YES];
    }
    if(btn.tag==1006)
    {
        bulidingListVC *vc = [[bulidingListVC alloc] initWithNavigationBar:YES];
        vc.type_str = @"my";
        [theApp.tabBarController.navigationController  pushViewController:vc animated:YES];
    }
    if(btn.tag==1007)
    {
        bulidingListVC *vc = [[bulidingListVC alloc] initWithNavigationBar:YES];
        vc.type_str = @"new";
        [theApp.tabBarController.navigationController  pushViewController:vc animated:YES];
    }
    if(btn.tag==1008)
    {
        myCustomVC *vc = [[myCustomVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    if(btn.tag==1009)
    {
        youxiaoCustomVC *vc = [[youxiaoCustomVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    if(btn.tag==1010)
    {
        
    }
    if(btn.tag==1011)
    {
        definitioningVC *vc = [[definitioningVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];

    }
    if(btn.tag==1012)
    {
        definition_timoutVC *vc = [[definition_timoutVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    if(btn.tag==1013)
    {
        applyVC *vc = [[applyVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController  pushViewController:vc animated:YES];

    }
}



#pragma mark - 下拉刷新回调方法

- (void)addHeader
{
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = _scrollView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        [self performSelector:@selector(refreshALLData) withObject:refreshView afterDelay:0.0];
        
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:0.5];
        
        // NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        //NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                // NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
                //NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
                // NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    
    [header beginRefreshing];
    _header = header;
}
- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}


-(void)refreshALLData
{
    
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.broker.index&BrokerKid=%@",theApp.brokerKid];
    
    UILabel *superMoneyLabel = (UILabel *)[self.view viewWithTag:2005];
    UILabel *myBuildingLabel = (UILabel *)[self.view viewWithTag:2006];
    UILabel *willOnLineLabel = (UILabel *)[self.view viewWithTag:2007];
    UILabel *myCustomLabel = (UILabel *)[self.view viewWithTag:2008];
    UILabel *youxiaoCusotmLabel = (UILabel *)[self.view viewWithTag:2009];
    UILabel *superCustomLabel = (UILabel *)[self.view viewWithTag:2010];
    UILabel *jiedingLabel = (UILabel *)[self.view viewWithTag:2011];
    UILabel *chaoshiLabel = (UILabel *)[self.view viewWithTag:2012];
    UILabel *myApplyLabel = (UILabel *)[self.view viewWithTag:2013];

    
    [self startSynchronousRequest:str didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict =[[request responseString]objectFromJSONString];
         if([[dict objectForKey:@"code"] intValue]==0)
         {
         NSDictionary *dict2 = [dict objectForKey:@"data"];
             
             superMoneyLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"MyFound"]];
             myBuildingLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"MyBuilding"]];
             willOnLineLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"WillLineBuilding"]];
             myCustomLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"MyCustomerCount"]];
             youxiaoCusotmLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"MyCustomervail"]];
             superCustomLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"MyCustomerSupperCount"]];
             jiedingLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"WaitIng"]];
             chaoshiLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"OutTime"]];
             myApplyLabel.text =[NSString stringWithFormat:@"%@",[dict2 objectForKey:@"MyApply_Count"]];
             
             NSArray *arr = [dict2 objectForKey:@"titleImages"];
             
             for(int i=0;i<arr.count;i++)
             {
                 UIImageView *imageView = (UIImageView *)[self.view viewWithTag:5004+i];
                 
                 [imageView setWebImageWithFade:[arr[i] objectForKey:@"url"] placeholderImage:nil];

             }
         }
       
         
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
     }];
    
    
}

- (void)fadeIn:(UIView *)view
{
    view.transform = CGAffineTransformMakeScale(1, 1);
    view.alpha = 0;
    [UIView animateWithDuration:.3 animations:^{
        view.alpha = 1;
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.hidden = NO;
    }];
    
}

- (void)fadeOut:(UIView *)view
{
    [UIView animateWithDuration:.3 animations:^{
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.alpha = 0.0;
        // [view removeFromSuperview];
        
    } completion:^(BOOL finished) {
        if (finished) {
            view.hidden = YES;
        }
    }];
}



-(void)ViewHidden:(id)sender
{
    
    [self fadeOut:sender];
    
}

#pragma mark --UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    UIImageView *index_line_hl = (UIImageView *)[self.view viewWithTag:10001];
    
    int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
    
    index_line_hl.frame = CGRectMake(index*80,index_line_hl.frame.origin.y, 80, 4);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
