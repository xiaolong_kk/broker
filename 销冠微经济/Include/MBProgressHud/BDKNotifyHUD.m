#import "BDKNotifyHUD.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#define kBDKNotifyHUDDefaultRoundness    0.0f
#define kBDKNotifyHUDDefaultOpacity      0.75f
#define kBDKNotifyHUDDefaultPadding      20.0f
#define kBDKNotifyHUDDefaultInnerPadding 15.0f

@implementation NSObject (PerformBlockAfterDelay)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay {
    block = [block copy];
    [self performSelector:@selector(fireBlockAfterDelay:) withObject:block afterDelay:delay];
}

- (void)fireBlockAfterDelay:(void (^)(void))block {
    block();
}

@end

@interface BDKNotifyHUD ()

@property (strong, nonatomic) UIView *backgroundView;
@property (strong, nonatomic) UIImageView *imageView;
@property (strong, nonatomic) UILabel *textLabel;
@property (strong,nonatomic) UIImageView *bg_imageView;

- (void)recalculateHeight;
- (void)adjustTextLabel:(UILabel *)label;
- (void)fadeAfter:(CGFloat)duration speed:(CGFloat)speed completion:(void (^)(void))completion;

@end

@implementation BDKNotifyHUD

#pragma mark - Lifecycle

+ (id)notifyHUDWithImage:(UIImage *)image bgImage:(UIImage *)bgImage text:(NSString *)text
{
    
    return [[[self alloc] initWithImage:image bgImage:bgImage text:text] autorelease];

}

+ (CGRect)defaultFrame {
    return CGRectMake(0, 0, 0, 0);
}

- (id)initWithImage:(UIImage *)image bgImage:(UIImage *)bgImage text:(NSString *)text {
    if ((self = [self initWithFrame:[self.class defaultFrame]]))
    {
        
        [self addSubview:self.backgroundView];

        self.bgImage = bgImage;
        self.image = image;
        self.text = text;
        self.roundness = kBDKNotifyHUDDefaultRoundness;
        self.borderColor = [UIColor clearColor];
        self.destinationOpacity = kBDKNotifyHUDDefaultOpacity;
        self.currentOpacity = 0.0f;
        [self addSubview:self.bg_imageView];
        [self addSubview:self.imageView];
        [self addSubview:self.textLabel];
        [self recalculateHeight];
    }
    return self;
}

- (void)presentWithDuration:(CGFloat)duration speed:(CGFloat)speed inView:(UIView *)view completion:(void (^)(void))completion {
    self.isAnimating = YES;
    [UIView animateWithDuration:speed animations:^{
        [self setCurrentOpacity:0.5];
    } completion:^(BOOL finished) {
        if (finished) [self fadeAfter:duration speed:speed completion:completion];
    }];
}

- (void)fadeAfter:(CGFloat)duration speed:(CGFloat)speed completion:(void (^)(void))completion {
    [self performBlock:^{
        [UIView animateWithDuration:speed animations:^{
            [self setCurrentOpacity:0];
        } completion:^(BOOL finished) {
            if (finished) {
                self.isAnimating = NO;
                if (completion != nil) completion();
            }
        }];
    } afterDelay:duration];
}

#pragma mark - Setters

- (void)setRoundness:(CGFloat)roundness {
    if (_backgroundView != nil) self.backgroundView.layer.cornerRadius = roundness;
    _roundness = roundness;
}

- (void)setBorderColor:(UIColor *)borderColor {
    if (_backgroundView != nil) self.backgroundView.layer.borderColor = [borderColor CGColor];
    _borderColor = borderColor;
}

- (void)setText:(NSString *)text {
    if (_textLabel != nil) {
        self.textLabel.text = text;
        [self adjustTextLabel:self.textLabel];
    }
    _text = text;
}


-(void)setBgImage:(UIImage *)bgImage
{
    if (_bg_imageView != nil) self.bg_imageView.image = bgImage;
    _bgImage = bgImage;
}
- (void)setImage:(UIImage *)image {
    if (_imageView != nil) self.imageView.image = image;
    _image = image;
    
}

- (void)setCurrentOpacity:(CGFloat)currentOpacity {
    self.imageView.alpha = currentOpacity > 0 ? 1.0f : 0.0f;
    self.textLabel.alpha = currentOpacity > 0 ? 1.0f : 0.0f;
    self.bg_imageView.alpha = currentOpacity > 0 ? 0.85f : 0.0f;
    self.backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:currentOpacity];
    _currentOpacity = currentOpacity;
}

#pragma mark - Getters

- (UIView *)backgroundView {
    if (_backgroundView != nil) return _backgroundView;
    
    _backgroundView = [[UIView alloc] initWithFrame:self.bounds];
    _backgroundView.layer.cornerRadius = self.roundness;
    _backgroundView.layer.borderWidth = 1.0f;
    _backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
    _backgroundView.layer.borderColor = [self.borderColor CGColor];
    
    return _backgroundView;
}

-(UIImageView *)bg_imageView
{
    if (_bg_imageView != nil) return _bg_imageView;
    
    _bg_imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _bg_imageView.backgroundColor = [UIColor clearColor];
    _bg_imageView.contentMode = UIViewContentModeCenter;
    if (self.bgImage != nil) {
        _bg_imageView.image = self.bgImage;
        CGRect frame = _bg_imageView.frame;
        frame.size = self.bgImage.size;
        frame.origin = CGPointMake(95,200);
        _bg_imageView.frame = frame;
        _bg_imageView.alpha = 0.0f;
    }
    
    return _bg_imageView;
}

- (UIImageView *)imageView {
    if (_imageView != nil) return _imageView;
    
    _imageView = [[UIImageView alloc] initWithFrame:CGRectZero];
    _imageView.backgroundColor = [UIColor clearColor];
    _imageView.contentMode = UIViewContentModeCenter;
    if (self.image != nil)
    {
        _imageView.image = self.image;
        CGRect frame = _imageView.frame;
        frame.size = self.image.size;
        frame.origin = CGPointMake(95+60,200+15);
        _imageView.frame = frame;
        _imageView.alpha = 0.0f;
    }
    
    return _imageView;
}

- (UILabel *)textLabel {
    if (_textLabel != nil) return _textLabel;
    
   // CGRect frame = CGRectMake(0, floorf(CGRectGetMaxY(self.imageView.frame) +kBDKNotifyHUDDefaultInnerPadding),floorf(self.backgroundView.frame.size.width),floorf(self.backgroundView.frame.size.height / 2.0f));
    _textLabel = [[UILabel alloc] initWithFrame:CGRectMake(95+10,200+35,120, 50)];
    _textLabel.font=[UIFont fontWithName:@"Helvetica"size:13];
    _textLabel.textColor = [UIColor blackColor];
    _textLabel.alpha = 0.0f;
    _textLabel.backgroundColor = [UIColor clearColor];
    _textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel.numberOfLines = 0;
    if (self.text != nil) _textLabel.text = self.text;
    //[self adjustTextLabel:_textLabel];
    [self recalculateHeight];
    
    return _textLabel;
}

#pragma mark - UIView

- (void)layoutSubviews {
    [self recalculateHeight];
}

- (void)adjustTextLabel:(UILabel *)label {
    CGRect frame = _textLabel.frame;
    frame.size.width = self.backgroundView.frame.size.width-20;
    _textLabel.frame = frame;
    [label sizeToFit];
    frame = _textLabel.frame;
    frame.origin.x = floorf((self.backgroundView.frame.size.width - _textLabel.frame.size.width) / 2);
    _textLabel.frame = frame;
}

- (void)recalculateHeight {
    CGRect frame = self.backgroundView.frame;
    frame.size.height = CGRectGetMaxY(self.textLabel.frame) + kBDKNotifyHUDDefaultPadding;
    self.backgroundView.frame = theApp.window.frame;
}

@end
