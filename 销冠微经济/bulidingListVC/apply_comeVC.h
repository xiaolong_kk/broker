//
//  apply_comeVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/24.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QRadioButton.h"
#import "VRGCalendarView.h"
#import "SBPickerSelector.h"

@interface apply_comeVC : BaseViewController<UITextFieldDelegate,UIScrollViewDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,QRadioButtonDelegate,VRGCalendarViewDelegate,SBPickerSelectorDelegate>
{
    UIScrollView *_scrollView;
    UITextView *_textView;
    UIButton *faceBtn;
    UITextField *name_text;
    UIImage *faceImg;
    
    UIView *mid_view;
    UIView *mid_view2;
    NSString *sex_str;
    NSMutableArray *brokerArr;
    NSMutableArray *allotArr;

}

@property(nonatomic,strong)  UIView *calenderViewBg;
@property(nonatomic,strong)NSDictionary *user_dict;
@property(nonatomic,strong) VRGCalendarView *calendar;
@property(nonatomic,strong)NSString *dateString;
@property(nonatomic,strong)NSString *NowApply;
@property(nonatomic,strong)NSString *ApplyType;
@property(nonatomic,strong) NSString *brokerStr;

@end
