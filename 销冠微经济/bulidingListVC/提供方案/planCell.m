//
//  planCell.m
//  HRVTableView
//
//  Created by liu on 14-10-9.
//  Copyright (c) 2014年 Hamidreza Vakilian. All rights reserved.
//

#import "planCell.h"
#import "tagLableView.h"

@implementation planCell

#define maxMake 400
#define BORDER  45/2

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.backgroundColor = [AppSession colorWithHexString:@"f5f5f5"];
        _labelTitle = [[UILabel alloc]initWithFrame:CGRectMake(BORDER + 10, 35/2 - 10, 160, 20)];
        _labelTitle.font = [UIFont boldSystemFontOfSize:15];
        _labelTitle.textColor = [AppSession colorWithHexString:@"333333"];
        
        _labelContent = [[UILabel alloc] initWithFrame:CGRectMake(120, 35/2 - 10, 160, 20)];
        _labelContent.font = [UIFont systemFontOfSize:14];
        _labelContent.textColor = [AppSession colorWithHexString:@"333333"];
        
        _labelDetial = [[UILabel alloc]initWithFrame:CGRectMake(400, 35/2 - 10, 160, 20)];
        _labelDetial.font = [UIFont systemFontOfSize:16];
        _labelDetial.textColor = [AppSession colorWithHexString:@"fd710a"];
        
        _expandGlyph = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH - 49, 20, 10, 19)];
        _expandGlyph.image = [UIImage imageNamed:@"arrow_go2@2x.png"];
        
        _bgdImage = [[UIImageView alloc]init];
        insets = UIEdgeInsetsMake(10, 100, 10, 100);
        
        [self addSubview:_bgdImage];
        
        _starView = [[starView alloc]init];
        _lineView = [[UIImageView alloc]init];
        [self addSubview:_lineView];
        
        [self addSubview:_expandGlyph];
        [self addSubview:_labelTitle];
        [self addSubview:_labelContent];
        [self addSubview:_labelDetial];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)content:(NSDictionary *)dataDic cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (0 == indexPath.section) {
        if (0 == indexPath.row) {
            UIImageView *picView = [[UIImageView alloc]initWithFrame:CGRectMake(BORDER + 10, 10, 35, 35)];
            [self addSubview:picView];
            
            // 勋章 星
            UIImageView *medalView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 25, 10, 10)];
            medalView.image = [UIImage imageNamed:@"prove06@2x"];
            [picView addSubview:medalView];
            
            UILabel *labelName = [[UILabel alloc]initWithFrame:CGRectMake(picView.frame.origin.x + picView.frame.size.width + 25/2, 10, 20, 20)];
            labelName.font = [UIFont systemFontOfSize:18];
            labelName.textColor = [AppSession colorWithHexString:@"333333"];
            [self addSubview:labelName];
            
            UILabel *labelPrefixal = [[UILabel alloc]initWithFrame:CGRectMake(labelName.frame.origin.x + labelName.frame.size.width + 6, 10, 40, 20)];
            labelPrefixal.font = [UIFont systemFontOfSize:14];
            labelPrefixal.textColor = [AppSession colorWithHexString:@"999999"];
            [self addSubview:labelPrefixal];
            
            UILabel *labelTime = [[UILabel alloc]initWithFrame:CGRectMake(labelPrefixal.frame.origin.x + labelPrefixal.frame.size.width , 10, 100, 20)];
            labelTime.font = [UIFont systemFontOfSize:14];
            labelTime.textColor = [AppSession colorWithHexString:@"999999"];
            [self addSubview:labelTime];
            
            UILabel *labelPhone = [[UILabel alloc]initWithFrame:CGRectMake(labelName.frame.origin.x, 30, 200, 20)];
            labelPhone.font = [UIFont systemFontOfSize:14];
            labelPhone.textColor = [AppSession colorWithHexString:@"333333"];
            [self addSubview:labelPhone];
            
            _lineView.image = [UIImage imageNamed:@"dotted_line@2x.png"];
            _lineView.frame = CGRectMake(BORDER, 53, SCREEN_WIDTH - BORDER*2-1, 1);
            
            
            UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd_header@2x.png"] resizableImageWithCapInsets:insets];
            _bgdImage.image = bgdImage;
            _bgdImage.frame = CGRectMake(BORDER, 0, SCREEN_WIDTH - BORDER*2 , 53);
            
            NSDictionary *clientDetail = [dataDic objectForKey:@"ClientDetail"];
             picView.image = [UIImage imageNamed:@"user_0.jpg"];
            labelName.text = [clientDetail objectForKey:@"F_Title"];
            labelPrefixal.text = [clientDetail objectForKey:@"F_Sex"];
            NSString *timeStr = [clientDetail objectForKey:@"F_LastActiveTime"];
            
            labelTime.text = [self time:timeStr];
            labelPhone.text = [clientDetail objectForKey:@"F_Phone"];
            
            _labelContent.hidden = YES;
            _labelDetial.hidden = YES;
            _labelTitle.hidden = YES;
            _expandGlyph.hidden = NO;
        } else {
            _expandGlyph.hidden = YES;
            _labelContent.hidden = NO;
            _labelDetial.hidden = YES;
            _labelTitle.hidden = NO;
            _lineView.image = [UIImage imageNamed:@"mainview_line_long@2x.png"];
            _lineView.frame = CGRectMake(BORDER, 34, SCREEN_WIDTH - BORDER * 2-1, 1);
            UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd@2x.png"] resizableImageWithCapInsets:insets];
            _bgdImage.image = bgdImage;
            _bgdImage.frame = CGRectMake(BORDER, 0, SCREEN_WIDTH - BORDER*2 - 0.5 , 34);
            switch (indexPath.row) {
                case 1:{
                    _labelTitle.text = @"物业需求";
                    NSArray *area = [dataDic objectForKey:@"area"];
                    _labelContent.text = nil;
                    for (NSString *atrArea  in area) {
                        if (_labelContent.text == nil) {
                            _labelContent.text = [NSString stringWithFormat:@"%@",atrArea];
                        } else {
                            _labelContent.text = [NSString stringWithFormat:@"%@/%@",_labelContent.text,atrArea];
                        }
                    }
                }  break;
                case 2:{
                    _labelTitle.text = @"意向区域";
                    _labelContent.text = @"意向区域内容";
                    }  break;
                case 3:{
                    _labelTitle.text = @"总价预算";
                }  break;
                case 4:{
                    UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd_footer@2x.png"] resizableImageWithCapInsets:insets];
                    _bgdImage.image = bgdImage;
                    _bgdImage.frame = CGRectMake(BORDER - 0.5, 0, SCREEN_WIDTH - BORDER*2 + 0.2 , 34);
                    _lineView.image = nil;
                    _labelTitle.text = @"意向楼盘";
                    
                }  break;
                default:
                    break;
            }
        }
        
    } else if (1 == indexPath.section) {
        _expandGlyph.hidden = YES;
        if (indexPath.row == 0) {
            _bgdImage.frame = CGRectMake(BORDER, 33/2, 320 - BORDER * 2, 75);
            _bgdImage.image = [UIImage imageNamed:@"prove01@2x.png"];
            NSDictionary *dicBroker = [dataDic objectForKey:@"Broker"];
           
            UIImageView *picView = [[UIImageView alloc]initWithFrame:CGRectMake(BORDER + 10, 17+33/2, 38, 38)];
            [self addSubview:picView];
            
            // 勋章 超级V
            UIImageView *medalView = [[UIImageView alloc]initWithFrame:CGRectMake(14, 28, 24, 10)];
            medalView.image = [UIImage imageNamed:@"prove07@2x"];
            [picView addSubview:medalView];
            
            UILabel *labelName = [[UILabel alloc]initWithFrame:CGRectMake(picView.frame.origin.x + picView.frame.size.width + 25/2, 20+33/2, 60, 20)];
            labelName.font = [UIFont boldSystemFontOfSize:17];
            labelName.textColor = [UIColor whiteColor];
            [self addSubview:labelName];
            
            UILabel *labelPhone = [[UILabel alloc]initWithFrame:CGRectMake(136, 20+33/2, 200, 20)];
            labelPhone.font = [UIFont systemFontOfSize:15];
            labelPhone.textColor = [UIColor whiteColor];
            [self addSubview:labelPhone];
            
            // 星级
            _starView.frame = CGRectMake(labelName.frame.origin.x, 38+33/2, 100, 30);
            [self addSubview:_starView];
            
            UILabel *labelLikeNum = [[UILabel alloc]initWithFrame:CGRectMake(156, 40+33/2, 100, 20)];
            labelLikeNum.font = [UIFont boldSystemFontOfSize:12];
            labelLikeNum.textColor = [UIColor whiteColor];
            [self addSubview:labelLikeNum];
            
            _expandGlyph.hidden = NO;
            _expandGlyph.image = [UIImage imageNamed:@"circle@2x.png"];
            _expandGlyph.frame = CGRectMake(_expandGlyph.frame.origin.x, 150/4 - 8 + 33/2, 21, 20) ;
            
            _lineView.image = [UIImage imageNamed:@"mainview_line_long@2x.png"];
            _lineView.frame = CGRectMake(BORDER, (150 + 32)/2, SCREEN_WIDTH - BORDER*2, 1);
            
            picView.image = [UIImage imageNamed:@"user_1.jpg"];
            // 获得❤️数
            [_starView starNumble:3];
            labelName.text = [dicBroker objectForKey:@"F_Title"];
            labelPhone.text = [dicBroker objectForKey:@"F_Phone"];
            labelLikeNum.text = @"好评 05";

        } else if (1 == indexPath.row){
            // 标签云
            NSString *strTag = [dataDic objectForKey:@"TagItem"];
            NSArray *tagArray = [strTag componentsSeparatedByString:@","];
            tagLableView *tagLabel = [tagLableView new];
            float yyy = [tagLabel tagLabel:tagArray withView:self];
            _lineView.image = [UIImage imageNamed:@"mainview_line_long@2x.png"];
            _lineView.frame = CGRectMake(BORDER + 1, yyy + 6, SCREEN_WIDTH - BORDER * 2 - 1, 1);
            UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd@2x.png"] resizableImageWithCapInsets:insets];
            _bgdImage.image = bgdImage;
            _bgdImage.frame = CGRectMake(BORDER , 0, SCREEN_WIDTH - BORDER*2 , yyy + 6 );
        } else if (2 == indexPath.row){
            _labelContent.hidden = NO;
            _labelTitle.hidden = NO;
            _labelTitle.text = @"带看时间";
            _labelContent.text = @"08/12 上午，08/15 下午";
            _lineView.image = [UIImage imageNamed:@"mainview_line_long@2x.png"];
            _lineView.frame = CGRectMake(BORDER + 1, 75/2-1, SCREEN_WIDTH - BORDER * 2 -1, 1);
            UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd@2x.png"] resizableImageWithCapInsets:insets];
            _bgdImage.image = bgdImage;
            _bgdImage.frame = CGRectMake(BORDER , 0, SCREEN_WIDTH - BORDER*2 , 75/2-1 );
        } else {
            _labelContent.hidden = NO;
            _labelTitle.hidden = NO;
            _labelContent.lineBreakMode = NSLineBreakByWordWrapping;
            _labelContent.numberOfLines = 0;
            _labelContent.textColor = [AppSession colorWithHexString:@"999999"];
            _labelTitle.text = @"备注";
            _labelContent.text = @"另有其他可看另有其他可看另有其他可看另有其他可,看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可。看另有其他可看另有其他可看";
            CGSize size = [_labelContent.text sizeWithFont:_labelContent.font constrainedToSize:
                           CGSizeMake(160, 2000)];
            CGRect rect = _labelContent.frame;
            _labelContent.frame = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, size.height);
            UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd_footer@2x.png"] resizableImageWithCapInsets:insets];
            _bgdImage.image = bgdImage;
            _bgdImage.frame = CGRectMake(BORDER -0.5, 0, SCREEN_WIDTH - BORDER*2 +0.5, size.height + 20);
        }
    }
    
    /* 推荐楼盘
    else {
        
        UIImage *bgdImage = [[UIImage imageNamed:@"cellBgd"] resizableImageWithCapInsets:insets];
        _bgdImage.image = bgdImage;
        _bgdImage.frame = CGRectMake(BORDER , 0, SCREEN_WIDTH - BORDER*2 , 75/2);
        _lineView.image = [UIImage imageNamed:@"mainview_line_long@2x.png"];
        _lineView.frame = CGRectMake(BORDER, 75/2-1, SCREEN_WIDTH - BORDER * 2, 1);
        
        if (indexPath.row == 0) {
            UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-50, 10, 100, 20)];
            title.font = [UIFont boldSystemFontOfSize:16];
            title.textAlignment =  NSTextAlignmentCenter;
            title.text = @"推荐楼盘";
            _expandGlyph.hidden = YES;
            [self addSubview:title];
            _bgdImage.frame = CGRectMake(BORDER, 0, SCREEN_WIDTH - BORDER * 2, 70);
            _bgdImage.backgroundColor = [UIColor grayColor];
        }else {
            _expandGlyph.hidden = NO;
            _labelContent.hidden = NO;
            _labelTitle.hidden = NO;
            _labelDetial.hidden = NO;
            _expandGlyph.frame = CGRectMake(_expandGlyph.frame.origin.x, 75/4 - 8, 8, 16) ;
            _labelTitle.text = @"太和广场";
            _labelContent.text = @"150W起";
            _labelDetial.text = @"存一低五";
        }
    }
    */
}

-(NSString *)time:(NSString *)timeStr
{
    if (timeStr.length  > 10 ) {
        timeStr = [timeStr substringToIndex:10];
    }
    return timeStr;
}

@end
