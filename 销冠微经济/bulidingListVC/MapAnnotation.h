#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "MapAnnotation.h"
#import "B_CalloutAnnotation.h"
@interface MapAnnotation : NSObject <MKAnnotation>
{
@private
    NSString *title_;
    CLLocationCoordinate2D coordinate_;
    B_CalloutAnnotation *calloutAnnotation_;
}
@property (nonatomic, retain) NSString *title;
@property (nonatomic,retain) NSString *shopKid;
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic,retain) NSString *sub_title;
@property (nonatomic,retain)NSString *img_str;
@property (nonatomic,retain)NSString *_kid;
@property (nonatomic, retain) B_CalloutAnnotation *calloutAnnotation;

@end
