//
//  starView.m
//  HRVTableView
//
//  Created by liu on 14-10-10.
//  Copyright (c) 2014年 Hamidreza Vakilian. All rights reserved.
//

#import "starView.h"

@implementation starView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        [self ret:frame];
    }
    return self;
}

-(void)ret:(CGRect)rect
{
    for (int n = 0; n < 5; n ++) {
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(0 + 14*n, 5, 12, 12)];
        imageView.image = [UIImage imageNamed:@"star2@2x.png"];
        imageView.tag = 10+n;
        [self addSubview:imageView];
    }
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
}

-(void)starNumble:(NSUInteger)numble
{
    for (UIImageView *view in self.subviews) {
        if (view.tag < numble + 10) {
            view.image = [UIImage imageNamed:@"star1@2x.png"];
        }
    }
}


@end
