//
//  verifyResultVC.m
//  销冠微经济
//
//  Created by zen huang on 14/10/9.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "verifyResultVC.h"

@interface verifyResultVC ()

@end

@implementation verifyResultVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    float addHeight = 0;
    if(IOS_7)
        addHeight = 20;
    float addHeight2 = 0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight2 = 100;
    self.navigationLabel.text = @"审核状态";

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,44+addHeight,320,self.view.bounds.size.height-44-addHeight) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];

   [self getData];

}

-(void)getData
{
    NSString *str;
    
    if([self.rtype_str isEqualToString:@"2"])
    str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.topbrokerapplying.getapplystutaslist&BrokerKid=%@&rtype=%@&type=%@&PageIndex=1",theApp.brokerKid,self.rtype_str,self.type_str];
      else
    str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.topbrokerapplying.getapplystutaslist&BrokerKid=%@&type=%@&PageIndex=1",theApp.brokerKid,self.type_str];
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSArray *arr = [[dict objectForKey:@"data"] objectForKey:@"PushList"];
             dataSource = [NSMutableArray arrayWithArray:arr];
             
            // NSLog(@"%@",dataSource);
             [_tableView reloadData];
             
         }
         
     }
        didFailedRequest:^(ASIHTTPRequest *request){
                     
    }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
        // headView.backgroundColor= [UIColor whiteColor];
        [cell addSubview:headView];
        
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
        logoImg.tag = 101;
        //            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
        [headView addSubview:logoImg];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,240,20)];
        titleLabel.tag = 102;
        titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [headView addSubview:titleLabel];
        
        UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
        sexLabel.tag=103;
        sexLabel.textAlignment=NSTextAlignmentCenter;
        sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
        sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        sexLabel.backgroundColor=[UIColor clearColor];
        [headView addSubview:sexLabel];
        
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
        phoneLabel.tag = 104;
        phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.backgroundColor = [UIColor clearColor];
        phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
        [headView addSubview:phoneLabel];
        
        UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
        budildingLabel.tag = 105;
        budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
        budildingLabel.textAlignment = NSTextAlignmentLeft;
        budildingLabel.backgroundColor = [UIColor clearColor];
        budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [headView addSubview:budildingLabel];
        
        
        //            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(230,15,12,12)];
        //            tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
        //            [headView addSubview:tagImg];
        //
        //
        //            UILabel *tageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250,11,150,20)];
        //            tageLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        //            tageLabel.textAlignment = NSTextAlignmentLeft;
        //            tageLabel.text = @"无效客户";
        //            tageLabel.backgroundColor = [UIColor clearColor];
        //            tageLabel.textColor = [AppSession colorWithHexString:@"666666"];
        //            [headView addSubview:tageLabel];
        //
        
        UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(240,11,60,20)];
        clickBtn.tag = 106;
        [clickBtn setTitleColor:[AppSession colorWithHexString:@"75a7be"] forState:UIControlStateNormal];
        clickBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:13];
        //[clickBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:clickBtn];
        
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,64,320,1);
        [cell addSubview:lineImg];
        
        
    }
    
    NSDictionary *dict = dataSource[indexPath.row];
    UIImageView *logoImg = (UIImageView *)[cell viewWithTag:101];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
    UILabel *phoneLabel = (UILabel *)[cell viewWithTag:104];
    UILabel *budildingLabel = (UILabel *)[cell viewWithTag:105];
    UIButton *clickBtn = (UIButton *)[cell viewWithTag:106];
    
    if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
        logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
    else
        logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
    
    titleLabel.text = [dict objectForKey:@"F_Title"];
    sexLabel.text = [dict objectForKey:@"F_Sex"];
    phoneLabel.text = [dict objectForKey:@"F_Phone"];
    budildingLabel.text = [NSString stringWithFormat:@"推荐楼盘  %@",[dict objectForKey:@"BuildingName"]];
    
    [clickBtn setTitle:[dict objectForKey:@"Pass"] forState:UIControlStateNormal];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
