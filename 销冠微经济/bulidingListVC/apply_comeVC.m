//
//  apply_comeVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/24.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "apply_comeVC.h"

@interface apply_comeVC ()

@end

@implementation apply_comeVC

@synthesize calendar;
@synthesize calenderViewBg;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;
    
    self.navigationLabel.text = @"到访客户";
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,self.view.bounds.size.height)];
    _scrollView.delegate =self;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(320,590);
    [self.view addSubview:_scrollView];
    
    [self setUpMainView];
    [self setUpCalendarView];
}


-(void)setUpMainView

{
//    faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    faceBtn.frame=CGRectMake(17.5,17.5,57,57);
//    [faceBtn setImage:[UIImage imageNamed:@"custom_photo.png"] forState:UIControlStateNormal];
//    [faceBtn addTarget:self action:@selector(pickImg) forControlEvents:UIControlEventTouchUpInside];
//    [_scrollView addSubview:faceBtn];
//    
//    
//    UIImageView *textBg1 = [[UIImageView alloc] init];
//    textBg1.userInteractionEnabled = YES;
//    textBg1.image = [UIImage imageNamed:@"kuang.png"];
//    UIEdgeInsets insets1 = UIEdgeInsetsMake(10,10,10,10);
//    textBg1.image = [textBg1.image resizableImageWithCapInsets:insets1];
//    textBg1.frame = CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width+12.5, 17.5,215,36);
//    [_scrollView addSubview:textBg1];
//    
//    UIImageView *tagImg1 = [[UIImageView alloc]initWithFrame:CGRectMake(10,10,15,16)];
//    tagImg1.image = [UIImage imageNamed:@"custom_face.png"];
//    [textBg1 addSubview:tagImg1];
//    
//    name_text = [[UITextField alloc] initWithFrame:CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width-40,0,215,36)];
//    name_text.placeholder = @"客户姓名";
//    name_text.font=[UIFont fontWithName:strinBoldFontOfSize size:14];
//    name_text.textColor = [UIColor blackColor];
//    [textBg1 addSubview:name_text];
//    
//    
//    QRadioButton *manBtn = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"2"]];
//    manBtn.frame = CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width+15,57,100, 30);
//    manBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
//    [manBtn setTitle:@"先生"forState:UIControlStateNormal];
//    [manBtn setTitleColor:[AppSession colorWithHexString:@"333333"] forState:UIControlStateNormal];
//    [manBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
//    [_scrollView addSubview:manBtn];
//    
//    QRadioButton *ladyBtn = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"2"]];
//    ladyBtn.frame = CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width+100,57,100, 30);
//    ladyBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
//    [ladyBtn setTitle:@"女士"forState:UIControlStateNormal];
//    [ladyBtn setTitleColor:[AppSession colorWithHexString:@"333333"] forState:UIControlStateNormal];
//    [ladyBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
//    [_scrollView addSubview:ladyBtn];
    
    
    
    UIImageView *textBg1 = [[UIImageView alloc] init];
    textBg1.userInteractionEnabled = YES;
    textBg1.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets1 = UIEdgeInsetsMake(10,0,10,0);
    textBg1.image = [textBg1.image resizableImageWithCapInsets:insets1];
    textBg1.frame = CGRectMake(15,17.5,294,144);
    [_scrollView addSubview:textBg1];
    
    NSArray *arr = [NSArray arrayWithObjects:@"客户姓名",@"联系电话",@"推荐楼盘",@"到访类型", nil];
    for(int i =0;i<4;i++)
    {
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,36*i,150,36)];
        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.text= arr[i];
        tempLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [textBg1 addSubview:tempLabel];
        
        if(i<3)
        {
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,36*(i+1),290,1);
            [textBg1 addSubview:lineImg];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(130,36*i,150,36)];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textAlignment = NSTextAlignmentRight;
            titleLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [textBg1 addSubview:titleLabel];
            
            if(i==0)
            {
                titleLabel.text= [self.user_dict objectForKey:@"F_Title"];

            }
            if(i==1)
            {
                titleLabel.text= [self.user_dict objectForKey:@"F_Phone"];

            }
            if(i==2)
            {
                titleLabel.text= [NSString stringWithFormat:@"%@",[self.user_dict objectForKey:@"BuildingName"]];

            }

        }
        
        
    }
    QRadioButton *comeLookBtn = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"2"]];
    comeLookBtn.frame = CGRectMake(102,144-36,140,36);
    comeLookBtn.tag = 101;
    [comeLookBtn setChecked:YES];
    comeLookBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [comeLookBtn setTitle:@"带看到方"forState:UIControlStateNormal];
    [comeLookBtn setTitleColor:[AppSession colorWithHexString:@"666666"] forState:UIControlStateNormal];
    [comeLookBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [textBg1 addSubview:comeLookBtn];

    
    QRadioButton *comeBtn = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"2"]];
    comeBtn.frame = CGRectMake(200,144-36,140,36);
    comeBtn.tag = 202;
    comeBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [comeBtn setTitle:@"自然到访" forState:UIControlStateNormal];
    [comeBtn setTitleColor:[AppSession colorWithHexString:@"666666"] forState:UIControlStateNormal];
    [comeBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [textBg1 addSubview:comeBtn];
    
    UIImageView *textBg2 = [[UIImageView alloc] init];
    textBg2.userInteractionEnabled = YES;
    textBg2.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets2 = UIEdgeInsetsMake(10,10,10,10);
    textBg2.image = [textBg2.image resizableImageWithCapInsets:insets2];
    textBg2.frame = CGRectMake(15,textBg1.frame.origin.y+textBg1.frame.size.height+10,294,36);
    [_scrollView addSubview:textBg2];
    
    UIButton *tagBtn2=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn2.frame=CGRectMake(0,0,320,36);
    [tagBtn2 setImage:[UIImage imageNamed:@"date_logo.png"] forState:UIControlStateNormal];
    tagBtn2.imageEdgeInsets = UIEdgeInsetsMake(0,-220,0,0);
    [tagBtn2 setTitle:@"到访时间"forState:UIControlStateNormal];
    [tagBtn2 setTitleColor:[AppSession colorWithHexString:@"999999"] forState:UIControlStateNormal];
    [tagBtn2 addTarget:self action:@selector(showCalendarView) forControlEvents:UIControlEventTouchUpInside];
    tagBtn2.titleLabel.font = [UIFont systemFontOfSize:14];
    tagBtn2.titleEdgeInsets = UIEdgeInsetsMake(0,-200,0,0);
    [textBg2 addSubview:tagBtn2];

    
    mid_view = [[UIView alloc] initWithFrame:CGRectMake(0,textBg2.frame.origin.y+textBg2.frame.size.height+10,320,145)];
    mid_view.userInteractionEnabled = YES;
    [_scrollView addSubview:mid_view];
    
    UIImageView *textBg3 = [[UIImageView alloc] init];
    textBg3.userInteractionEnabled = YES;
    textBg3.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets3 = UIEdgeInsetsMake(10,10,10,10);
    textBg3.image = [textBg3.image resizableImageWithCapInsets:insets3];
    textBg3.frame = CGRectMake(15,0,294,36);
    [mid_view addSubview:textBg3];
    
    UILabel *tempLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(10,0,150,36)];
    tempLabel3.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel3.backgroundColor = [UIColor clearColor];
    tempLabel3.text= @"是否在现场";
    tempLabel3.textColor = [AppSession colorWithHexString:@"000000"];
    [textBg3 addSubview:tempLabel3];
   
    QRadioButton *isYes = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"3"]];
    isYes.tag = 103;
    isYes.frame = CGRectMake(102,0,140,36);
    isYes.titleLabel.textAlignment = NSTextAlignmentLeft;
    [isYes setTitle:@"我在现场" forState:UIControlStateNormal];
    [isYes setTitleColor:[AppSession colorWithHexString:@"666666"] forState:UIControlStateNormal];
    [isYes.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [textBg3 addSubview:isYes];
    
    QRadioButton *isNO = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"3"]];
    isNO.tag = 203;
    isNO.frame = CGRectMake(200,0,140,36);
    isNO.titleLabel.textAlignment = NSTextAlignmentLeft;
    [isNO setTitle:@"我已离开"forState:UIControlStateNormal];
    [isNO setTitleColor:[AppSession colorWithHexString:@"666666"] forState:UIControlStateNormal];
    [isNO.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [textBg3 addSubview:isNO];

    
    UIImageView *textBg4 = [[UIImageView alloc] init];
    textBg4.userInteractionEnabled = YES;
    textBg4.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets4 = UIEdgeInsetsMake(10,10,10,10);
    textBg4.image = [textBg4.image resizableImageWithCapInsets:insets4];
    textBg4.frame = CGRectMake(15,textBg3.frame.origin.y+textBg3.frame.size.height+10,294,36);
    [mid_view addSubview:textBg4];
    
    UIButton *tagBtn4=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn4.frame=CGRectMake(10,0,320,36);
    [tagBtn4 setImage:[UIImage imageNamed:@"custom_face.png"] forState:UIControlStateNormal];
    tagBtn4.imageEdgeInsets = UIEdgeInsetsMake(0,-220,0,0);
    [tagBtn4 setTitle:@"选择置业顾问"forState:UIControlStateNormal];
    [tagBtn4 addTarget:self action:@selector(showPicker:) forControlEvents:UIControlEventTouchUpInside];
    [tagBtn4 setTitleColor:[AppSession colorWithHexString:@"999999"] forState:UIControlStateNormal];
    tagBtn4.titleLabel.font = [UIFont systemFontOfSize:14];
    tagBtn4.titleEdgeInsets = UIEdgeInsetsMake(0,-200,0,0);
    [textBg4 addSubview:tagBtn4];

    UIImageView *textBg5 = [[UIImageView alloc] init];
    textBg5.userInteractionEnabled = YES;
    textBg5.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets5 = UIEdgeInsetsMake(10,10,10,10);
    textBg5.image = [textBg5.image resizableImageWithCapInsets:insets5];
    textBg5.frame = CGRectMake(15,textBg4.frame.origin.y+textBg4.frame.size.height+10,294,50);
    [mid_view addSubview:textBg5];
    
    
    UIButton *tagBtn5=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn5.frame=CGRectMake(10,0,294,60);
    [tagBtn5 setImage:[UIImage imageNamed:@"apply_come_photo.png"] forState:UIControlStateNormal];
    tagBtn5.imageEdgeInsets = UIEdgeInsetsMake(-10,-150,0,0);
    [tagBtn5 setTitle:@"上传带看图"forState:UIControlStateNormal];
    [tagBtn5 setTitleColor:[AppSession colorWithHexString:@"75a7be"] forState:UIControlStateNormal];
    tagBtn5.titleLabel.font = [UIFont fontWithName:strinBoldFontOfSize size:15];
    tagBtn5.titleEdgeInsets = UIEdgeInsetsMake(-10,-50,0,0);
    [textBg5 addSubview:tagBtn5];

    mid_view2 = [[UIView alloc] initWithFrame:CGRectMake(0,mid_view.frame.origin.y+mid_view.frame.size.height+10,320,145)];
    mid_view2.userInteractionEnabled = YES;
    [_scrollView addSubview:mid_view2];

    UIImageView *textBg6 = [[UIImageView alloc] init];
    textBg6.userInteractionEnabled = YES;
    textBg6.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets6 = UIEdgeInsetsMake(10,10,10,10);
    textBg6.image = [textBg6.image resizableImageWithCapInsets:insets6];
    textBg6.frame = CGRectMake(15,0,294,91);
    [mid_view2 addSubview:textBg6];

    _textView = [[UITextView alloc] initWithFrame:CGRectMake(0,0,290,91)];
    _textView.delegate = self;
    _textView.backgroundColor = [UIColor clearColor];
    [textBg6 addSubview:_textView];
    
    UIView *placeholderView = [[UIView alloc] initWithFrame:CGRectMake(0,5,320,20)];
    //placeholderView.userInteractionEnabled = YES;
   // placeholderView.backgroundColor = [UIColor blackColor];
    [_textView addSubview:placeholderView];
    
    UIImageView *tagImg3 = [[UIImageView alloc] initWithFrame:CGRectMake(10,0,15,16)];
    tagImg3.image = [UIImage imageNamed:@"custom_pen.png"];
    [placeholderView addSubview:tagImg3];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,0,250,20)];
    tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.text= @"填写备注";
    tempLabel.textColor = [AppSession colorWithHexString:@"999999"];
    [placeholderView addSubview:tempLabel];
    
    UIButton *add_customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    add_customBtn.frame = CGRectMake(10,textBg6.frame.size.height+textBg6.frame.origin.y+15,300,40);
    [add_customBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [add_customBtn setTitle:@"确认申请" forState:UIControlStateNormal];
    add_customBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
    [add_customBtn addTarget:self action:@selector(subCustomInfo) forControlEvents:UIControlEventTouchUpInside];
    [add_customBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [mid_view2 addSubview:add_customBtn];
    
    
}

- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId;
{
    if(radio.tag==202)
    {
        mid_view.hidden = YES;
        mid_view2.frame = CGRectMake(0,220,320,145);
        self.ApplyType = @"1";
    }
    if(radio.tag==101)
    {
        mid_view2.frame = CGRectMake(0,mid_view.frame.origin.y+mid_view.frame.size.height+10,320,145);
        mid_view.hidden = NO;
        self.ApplyType = @"2";

    }
    if(radio.tag==103)
    {
        self.NowApply = @"1";

    }
    if(radio.tag==203)
    {
        self.NowApply = @"2";

    }

}


-(void)subCustomInfo
{
   // NSLog(@"%@",self.user_dict);
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:10];
    [param setValue:theApp.brokerKid forKey:@"BrokerKid"];
    [param setValue:[self.user_dict objectForKey:@"Kid"] forKey:@"Kid"];
    [param setValue:[self.user_dict objectForKey:@"F_BuildingKid"] forKey:@"Bid"];
    [param setValue:self.NowApply  forKey:@"NowApply"];
    [param setValue:self.ApplyType forKey:@"ApplyType"];
    [param setValue:self.dateString forKey:@"HandelTime"];
    [param setValue:self.brokerStr forKey:@"ConsultantKid"];
    [param setValue:@"2" forKey:@"type"];
    [param setValue:_textView.text forKey:@"F_Remark"];

    [self startSynchronousPostRequest:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinagreement.xxweixinbrokerapply" postData:param target:self.view alertText:@"客户信息提交中..." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
            [self.navigationController popViewControllerAnimated:YES];
      
         [self showMBAlertView:self.view message:[dict objectForKey:@"msg"] duration:1.5];

     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
         
     }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [self resignKeyBoardInView:self.view];
    }
}

#pragma mark 回调
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self takephoto];
            break;
            
        case 1:
            [self takealbum];
            break;
    }
}
//从相机获取，拍照
-(void)takephoto{
    //打开摄像头
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        NSArray *temp_MediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        picker.mediaTypes = temp_MediaTypes;
        picker.delegate = self;
        [picker setAllowsEditing:YES];
    }
    
    [self presentViewController:picker animated:YES completion:^{
    }];
}
//从相册获取
-(void)takealbum{
    //打开相册
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSArray *temp_MediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        picker.mediaTypes = temp_MediaTypes;
        picker.delegate = self;
        [picker setAllowsEditing:YES];
    }
    
    [self presentViewController:picker animated:YES completion:^{
    }];
}
//回调，设置头像
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    [picker dismissViewControllerAnimated:YES completion:^{
    }];
    //UIImage *faceImage=[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage *editImage=[info objectForKey:@"UIImagePickerControllerEditedImage"];
    //editImage = [AppSession scaleImage:editImage toScale:0.5];
    [faceBtn setImage:editImage forState:UIControlStateNormal];
    [AppSession scaleImage:editImage];
    faceImg = editImage;
    
}

-(void)pickImg
{
    [self resignKeyBoardInView:self.view];
    
    UIActionSheet *pick = [[UIActionSheet alloc ]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册获取", nil];
    
    [pick showInView:self.view];
}

- (void)resignKeyBoardInView:(UIView *)view
{
    for (UIView *v in view.subviews) {
        if ([v.subviews count] > 0) {
            [self resignKeyBoardInView:v];
        }
        
        if ([v isKindOfClass:[UITextView class]] || [v isKindOfClass:[UITextField class]]) {
            [v resignFirstResponder];
        }
    }
}

- (void) showPicker:(id)sender{

    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.topbuildingconsultantlist.getbuildingconsultantlist&BrokerKid=%@&BuildingKid=%@",theApp.brokerKid,[self.user_dict objectForKey:@"F_BuildingKid"]];

    allotArr = [[NSMutableArray alloc] init];
    brokerArr= [[NSMutableArray alloc] init];
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中" didFinishRequest:^(ASIHTTPRequest *request)
     {
         if([self isRequestJsonSucceed:[request.responseString objectFromJSONString]])
         {
             NSDictionary *dict= [request.responseString objectFromJSONString];
             
             if([[dict objectForKey:@"code"] intValue] ==0)
             {
                NSArray *arr = [dict objectForKey:@"data"];
                 for(NSDictionary *dict3 in arr)
                 {
                     NSString *name = [dict3 objectForKey:@"F_Title"];
                     
                     NSString *kid = [dict3 objectForKey:@"Kid"];
                     
                     [brokerArr addObject:name];
                     
                     [allotArr addObject:kid];


                 }
                 
                // NSLog(@"%@",brokerArr);
                 
                 SBPickerSelector *picker = [SBPickerSelector picker];
                 picker.title_str = @"置业顾问";
                 picker.pickerData = [brokerArr mutableCopy];
                 picker.delegate = self;
                 picker.pickerType = SBPickerSelectorTypeText;
                 picker.doneButtonTitle = @"确定";
                 picker.cancelButtonTitle = @"取消";
                 
                 
                 CGPoint point = [self.view convertPoint:[sender frame].origin fromView:[sender superview]];
                 CGRect frame = [sender frame];
                 frame.origin = point;
                 [picker showPickerIpadFromRect:frame inView:self.view];
             }
         }
         
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
     }];
    
}


-(void)setUpCalendarView
{
    calenderViewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    calenderViewBg.userInteractionEnabled = YES;
    calenderViewBg.hidden = YES;
    
    calenderViewBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    UIView *calenderView = [[UIView alloc] initWithFrame:CGRectMake(0,100,320, 300)];
    calendar.userInteractionEnabled  =YES;
    calenderView.userInteractionEnabled  =YES;
    calendar = [[VRGCalendarView alloc] init];
    calendar.delegate=self;
    
    UIButton *selectBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    selectBtn.frame=CGRectMake(0,0,320,500);
    [selectBtn addTarget:self action:@selector(hiddenView) forControlEvents:UIControlEventTouchUpInside];
    [calenderViewBg addSubview:selectBtn];
    
    [calenderViewBg addSubview:calenderView];
    [calenderView addSubview:calendar];
    [theApp.window addSubview:calenderViewBg];
}


-(void)showCalendarView
{
    [AppSession fadeIn:calenderViewBg];
    
}

-(void)hiddenView
{
    [AppSession fadeOut:calenderViewBg];
    
}
#pragma mark - SBPickerSelectorDelegate
-(void) SBPickerSelector:(SBPickerSelector *)selector selectedValue:(NSString *)value index:(NSInteger)idx
{
    
    self.brokerStr = allotArr[idx];
    
}

#pragma mark-- VRGCalendarViewDelegate

-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated
{
    
    NSArray *dates = [NSArray arrayWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:5], nil];
    [calendarView markDates:dates];
}

-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date
{
    NSDateFormatter *formatter3 =[[NSDateFormatter alloc]init];
    [formatter3 setDateFormat:@"yyyy-MM-dd"];
    self.dateString = [formatter3 stringFromDate:date];
    [self hiddenView];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
