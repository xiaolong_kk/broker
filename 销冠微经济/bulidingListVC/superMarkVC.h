//
//  superMarkVC.h
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-8.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@interface superMarkVC : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *dataSource;

    MJRefreshHeaderView *_header;
    
    UITableView *superableView;
}

@end
