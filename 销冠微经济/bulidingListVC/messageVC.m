//
//  messageVC.m
//  往来
//
//  Created by zen huang on 14/6/13.
//
//

#import "messageVC.h"
#import "planViewController.h"
@interface messageVC ()

@end

@implementation messageVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];    
    self.navigationLabel.text = @"消 息";
    dataSource = [[NSMutableArray alloc] init];
    [self createTableView];
    [self getData];
    
    self.view.backgroundColor = [UIColor whiteColor];
}


-(void)getData
{
    //    NSDate *datenow = [NSDate date];
    //    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[datenow timeIntervalSince1970]];
    NSString *str = nil;
    
    str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinbroker.smspushlist&BrokerKid=%@",theApp.brokerKid];
    
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中..." didFinishRequest:^(ASIHTTPRequest *request) {
        
        NSDictionary * dict = [request.responseString objectFromJSONString];
        if([[dict objectForKey:@"code"] intValue] ==0)
        {
        
            dataSource = [dict objectForKey:@"data"];
     
        [tableViewController reloadData];
        }
        
    } didFailedRequest:^(ASIHTTPRequest *request) {
        
    }];
}

- (void)createTableView
{
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;

    tableViewController=[[UITableView alloc] initWithFrame:CGRectMake(0,44+addHight,320,self.view.bounds.size.height-44-addHight)];
    tableViewController.delegate=self;
    tableViewController.dataSource = self;
    tableViewController.showsVerticalScrollIndicator = NO;
    tableViewController.separatorStyle=UITableViewCellSeparatorStyleNone;
    tableViewController.backgroundColor = [UIColor clearColor];
    [self.view addSubview:tableViewController];
}



#pragma mark - 分类列表
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
    
    UITableViewCell *cell =[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil)
    {
        cell =[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.tag = 1000;
        lineImg.frame = CGRectMake(0,59,320,1);
        [cell addSubview:lineImg];
        
        UIImageView *logImg = [[UIImageView alloc] init];
        //logImg.backgroundColor = [UIColor blackColor];
        logImg.tag = 1111;
        logImg.frame = CGRectMake(15,10,40,40);
        [cell addSubview:logImg];
        
        
        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,8,150,20)];
        titleLabel.tag=2222;
        titleLabel.textAlignment=NSTextAlignmentLeft;
        titleLabel.textColor=[UIColor blackColor];
        titleLabel.font=[UIFont fontWithName:@"Helvetica-Bold"size:15];
        titleLabel.backgroundColor=[UIColor clearColor];
        [cell addSubview:titleLabel];
        
        UILabel *subTitleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,33,260,20)];
        subTitleLabel.tag=3333;
        subTitleLabel.numberOfLines = 0;
        subTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        subTitleLabel.textAlignment=NSTextAlignmentLeft;
        subTitleLabel.textColor=[UIColor grayColor];
        subTitleLabel.font=[UIFont systemFontOfSize:12.5];
        subTitleLabel.backgroundColor=[UIColor clearColor];
        [cell addSubview:subTitleLabel];
        
        UILabel *timeLabel=[[UILabel alloc] initWithFrame:CGRectMake(210,10,100,20)];
        timeLabel.tag = 104;
        timeLabel.textAlignment=NSTextAlignmentRight;
        timeLabel.textColor=[UIColor grayColor];
        timeLabel.font=[UIFont systemFontOfSize:10];;
        timeLabel.backgroundColor=[UIColor clearColor];
        [cell addSubview:timeLabel];
        
    }
    UIImageView *lineImg = (UIImageView *)[cell viewWithTag:1000];
    UIImageView *logoImg = (UIImageView *)[cell viewWithTag:1111];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:2222];
    UILabel *subTitleLabel = (UILabel *)[cell viewWithTag:3333];
    UILabel *timeLabel = (UILabel *)[cell viewWithTag:104];
   
    
    NSDictionary *dict;
    dict = dataSource[indexPath.row];

    NSLog(@"%@",dict);
    titleLabel.text = [dict objectForKey:@"F_Title"];
    subTitleLabel.text = [dict objectForKey:@"F_Content"];
    timeLabel.text = [dict objectForKey:@"F_PushTime"];
    timeLabel.text = [timeLabel.text substringToIndex:10];
    
    CGSize size;
 
    size=[subTitleLabel.text sizeWithFont:[UIFont fontWithName:@"Helvetica"size:13] constrainedToSize:CGSizeMake(230, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    subTitleLabel.frame = CGRectMake(70,33,260,size.height);
    lineImg.frame = CGRectMake(0, 50+size.height,320,1);

    if([[dict objectForKey:@"F_Type"] isEqualToString:@"ProgrammeSuccess"])
    {
        logoImg.image = [UIImage imageNamed:@"msg_ic01.png"];
    }
    if([[dict objectForKey:@"F_Type"] isEqualToString:@"Invalid"])
    {
        logoImg.image = [UIImage imageNamed:@"msg_ic02.png"];

    }
    if([[dict objectForKey:@"F_Type"] isEqualToString:@"EveInvalid"])
    {
        logoImg.image = [UIImage imageNamed:@"msg_ic03.png"];

    }
    if([[dict objectForKey:@"F_Type"] isEqualToString:@"PushSuccess"])
    {
        logoImg.image = [UIImage imageNamed:@"msg_ic04.png"];

    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    return cell;
    

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *hightStr = [[dataSource objectAtIndex:indexPath.row] objectForKey:@"F_Content"];
    CGSize size;
    size=[hightStr sizeWithFont:[UIFont fontWithName:@"Helvetica"size:13] constrainedToSize:CGSizeMake(230, MAXFLOAT) lineBreakMode:NSLineBreakByWordWrapping];
    
    return size.height+50;

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    planViewController *vc = [[planViewController alloc] initWithNavigationBar:YES];
    [theApp.tabBarController.navigationController pushViewController:vc animated:YES];

   
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [AppSession resignKeyBoardInView:self.view];
}
#pragma mark - 内存警告

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
