//
//  loginVC.m
//  往来
//
//  Created by 朱晓龙 on 14-8-4.
//
//

#import "loginVC.h"
#import "registerVC.h"
#import "bulidingListVC.h"
#import "MainVC.h"
#import "customListVC.h"
#import "settingVC.h"

@interface loginVC ()

@end

@implementation loginVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
//    UIImageView *logImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_launcher.png"]];
//    logImg.frame = CGRectMake(110,70,101,101);
//    [self.view addSubview:logImg];
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height)];
    bgImg.image = [UIImage imageNamed:@"loginBg.png"];
    [self.view addSubview:bgImg];

    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,70,320,40)];
    tempLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:30];
    tempLabel.textAlignment = NSTextAlignmentCenter;
    tempLabel.text = @"销冠经纪人";
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.textColor = [AppSession colorWithHexString:@"ffffff"];
    [self.view addSubview:tempLabel];

    
    UIImageView *textBgImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lg101.png"]];
    textBgImg1.userInteractionEnabled = YES;
    textBgImg1.frame = CGRectMake(35,160,250,48);
    [self.view addSubview:textBgImg1];
    
   
   
    UIImageView *textBgImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lg203.png"]];
    textBgImg2.userInteractionEnabled = YES;
    textBgImg2.frame = CGRectMake(35,225,250,48);
    [self.view addSubview:textBgImg2];
 
    text1=[[UITextField alloc] init];
    text1.placeholder = @"用户名(手机号)";
    text1.font=[UIFont systemFontOfSize:15];
    text1.keyboardType = UIKeyboardTypePhonePad;
    text1.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    text1.borderStyle=UITextBorderStyleNone;
    text1.clearButtonMode = UITextFieldViewModeWhileEditing;
    text1.frame=CGRectMake(60,7,280,30);
    text1.delegate=self;
    [textBgImg1 addSubview:text1];
    
    text2=[[UITextField alloc] init];
    text2.placeholder = @"请填写密码";
    text2.font=[UIFont systemFontOfSize:15];
    text2.keyboardType = UIKeyboardTypeDefault;
    text2.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    text2.borderStyle=UITextBorderStyleNone;
    text2.clearButtonMode = UITextFieldViewModeWhileEditing;
    text2.frame=CGRectMake(60,7,280,30);
    text2.secureTextEntry=YES;
    text2.delegate=self;
    [textBgImg2 addSubview:text2];
    
    
    UIButton *logInBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logInBtn.frame =CGRectMake(35,305,250,48);
    [logInBtn setTitle:@"登 录" forState:UIControlStateNormal];
    [logInBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logInBtn.titleLabel.font = [UIFont systemFontOfSize:16];//title字体大
    [logInBtn setBackgroundImage:[UIImage imageNamed:@"login_kuang2.png"] forState:UIControlStateNormal];
    [logInBtn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:logInBtn];
    
    
    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerBtn.frame =CGRectMake(35,370,250,48);
    [registerBtn setTitle:@"注 册" forState:UIControlStateNormal];
    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:14];//title字体大
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"login_kuang3.png"] forState:UIControlStateNormal];
    [registerBtn addTarget:self action:@selector(registerView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBtn];
    

    UIButton *f_passwordBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    f_passwordBtn.frame =CGRectMake(120,415,80,40);
    [f_passwordBtn setTitle:@"忘记密码?" forState:UIControlStateNormal];
    [f_passwordBtn setTitleColor:[AppSession colorWithHexString:@"999999"] forState:UIControlStateNormal];
    f_passwordBtn.titleLabel.font = [UIFont systemFontOfSize:13];//title字体大
   // [f_passwordBtn addTarget:self action:@selector(forgetpassword) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:f_passwordBtn];
    
    //添加手势事件
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    
    self.view.backgroundColor = [UIColor grayColor];

}



#pragma mark - 触摸事件
-(void)tap
{
    
    [AppSession resignKeyBoardInView:self.view];
    
}


-(void)login
{

    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:10];
    [param setValue:text1.text forKey:@"UserName"];
    [param setValue:text2.text forKey:@"PassWord"];
    
    if([text1.text isEqualToString:@""])
    {
        [self showMBAlertView:self.view message:@"请输入手机号" duration:1];
        return;
    }
    if([text2.text isEqualToString:@""])
    {
        [self showMBAlertView:self.view message:@"请输入密码" duration:1];
        return;
    }
    
    //NSLog(@"%@",param);
    [self startSynchronousPostRequest:@"http://apiweixin.tops001.com/api.aspx?api=api.broker.brokerlogin" postData:param target:self.view alertText:@"登录中..." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict = [request.responseString objectFromJSONString];
                  
         if([[dict objectForKey:@"code"] intValue]==0)
         {
             NSDictionary *dict2 = [dict objectForKey:@"data"];
             
             theApp.brokerKid = [dict2 objectForKey:@"Kid"];
             
             [theApp.userDefault setObject:theApp.brokerKid forKey:@"brokerKid"];

             [self MainTabBarView];

//             UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:theApp.mainTabBarController];
//             [self presentViewController:nav animated:YES completion:nil];

         }
         else
         {
             [self showMBAlertView:self.view message:[dict objectForKey:@"msg"] duration:1.5];
         }
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
         
     }];
    
}


-(void)MainTabBarView
{
    theApp.tabBarController = [[MainTabBarViewController alloc] init];
    self.tabBarController.delegate = self;
    
    
    MainVC *vc1 = [[MainVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav1 = [[MLNavigationController alloc] initWithRootViewController:vc1];
    nav1.navigationBar.hidden= YES;
    //
    bulidingListVC *vc2 = [[bulidingListVC alloc] initWithNavigationBar:YES];
    vc2.type_str = @"all";
    MLNavigationController *nav2 = [[MLNavigationController alloc] initWithRootViewController:vc2];
    [nav2.navigationBar setHidden:YES];
    //
    customListVC *vc3 = [[customListVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav3 = [[MLNavigationController alloc] initWithRootViewController:vc3];
    nav3.navigationBar.hidden = YES;
    
    settingVC *vc4 = [[settingVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav4 = [[MLNavigationController alloc] initWithRootViewController:vc4];
    nav4.navigationBar.hidden = YES;
    
    NSArray *arrVC = [[NSArray alloc] initWithObjects:nav1,nav2,nav3,nav4,nil];
    [theApp.tabBarController setViewControllers:arrVC animated:YES];
    theApp.MLNav = [[MLNavigationController alloc] initWithRootViewController:theApp.tabBarController];
    theApp.MLNav.navigationBar.hidden = YES;
    if (IOS_7)
    {
        theApp.MLNav.interactivePopGestureRecognizer.enabled = NO;
    }
    [self presentViewController:theApp.MLNav animated:YES completion:nil];

    
}


-(void)registerView
{
    registerVC *vc = [[registerVC alloc] initWithNavigationBar:NO];
    [self.navigationController pushViewController:vc animated:YES];
}




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
