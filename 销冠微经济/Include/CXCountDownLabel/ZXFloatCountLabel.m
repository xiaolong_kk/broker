//
//  ZXFloatCountLabel.m
//  TopSales
//
//  Created by 朱晓龙 on 14-1-9.
//
//

#import "ZXFloatCountLabel.h"

@interface ZXFloatCountLabel()

{
    BOOL _ascending;
}

@property (nonatomic, assign) float currentNumber;
@property (nonatomic, copy) ZXFloatCountHandler floatCountHandeler;

- (void)countDown;

@end

@implementation ZXFloatCountLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _countInterval = 0;
        self.font = [UIFont systemFontOfSize:20.];
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _countInterval = 0;
        self.font = [UIFont systemFontOfSize:20.];
        self.textAlignment = NSTextAlignmentCenter;
    }
    return self;
}

- (void)setStartNumber:(float)startNumber endNumber:(float)endNumber floatcountHandeler:(ZXFloatCountHandler)floatcountHandeler;
{
    NSParameterAssert(startNumber != endNumber);
    self.startNumber = startNumber;
    self.endNumber = endNumber;
    self.floatCountHandeler = floatcountHandeler;
}

#pragma mark - setter / getter
- (void)setStartNumber:(float)startNumber
{
    self.currentNumber = startNumber;
    if (_startNumber == startNumber) {
        return;
    }
    _startNumber = startNumber;
}

- (void)setEndNumber:(float)endNumber
{
    if (_endNumber == endNumber) {
        return;
    }
    _endNumber = endNumber;
}

- (void)setCurrentNumber:(float)currentNumber
{
    if (_currentNumber == currentNumber) {
        return;
    }
    _currentNumber = currentNumber;
}

- (void)setCountInterval:(float)countInterval
{
    if (_countInterval == countInterval) {
        return;
    }
    
    _countInterval = countInterval;
}
#pragma mark - PB
- (void)start
{
    if (_currentNumber == _endNumber) {
        if (self.floatCountHandeler) {
            self.floatCountHandeler(self,_currentNumber,YES);
        }
        return;
    }
    
    if (!_countDownTimer) {
        _countDownTimer = [CADisplayLink displayLinkWithTarget:self selector:@selector(countDown)];
        _countDownTimer.frameInterval = 0;
    }
    
    [_countDownTimer addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)pause
{
    _countDownTimer.paused = YES;
    if (self.floatCountHandeler) {
        self.floatCountHandeler(self,_currentNumber,YES);
    }
}

- (void)resume
{
    _countDownTimer.paused = NO;
}
#pragma mark - PV
- (void)countDown
{
    _ascending = (_endNumber > _currentNumber);
    float interval = sqrtf(_currentNumber - _endNumber);
    float c = 0;
    if (_countInterval > interval) {
        c = interval;
    }
    else {
        c = _countInterval > 0 ? _countInterval : (float)sqrtf(interval);
    }
    
    self.currentNumber = _ascending ? _currentNumber + c : _currentNumber - c;
    
    self.text = [NSString stringWithFormat:@"%0.2f",_currentNumber];
    
    if (self.floatCountHandeler)
    {
        self.floatCountHandeler(self,_currentNumber,(_currentNumber == _endNumber));
    }
    
    if (_currentNumber >( _endNumber-0.01))
    {
        
        [_countDownTimer invalidate];
        self.text = [NSString stringWithFormat:@"%0.2f",_endNumber];
        _countDownTimer = nil;
    }
}
@end
