#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface B_CalloutAnnotation : NSObject <MKAnnotation> 
    {
    @private
        NSString *title_;
        CLLocationCoordinate2D coordinate_;
    }
    @property (nonatomic, retain) NSString *title;
    @property (nonatomic,retain) NSString *shopKid;
@property(nonatomic,retain) NSString *subTitle;
@property(nonatomic,retain) NSString *shop_name;
    @property (nonatomic) CLLocationCoordinate2D coordinate;

@end
