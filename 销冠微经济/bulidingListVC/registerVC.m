//
//  registerVC.m
//  往来
//
//  Created by 朱晓龙 on 14-8-4.
//
//

#import "registerVC.h"
#import "register2VC.h"
@interface registerVC ()

@end

@implementation registerVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.navigationController.navigationBar.hidden = NO;
//    self.navigationController.navigationBar.hidden = NO;
//    UIButton *btn1=[UIButton buttonWithType:UIButtonTypeCustom];
//    btn1.frame=CGRectMake(0,0,40,40);
//    [btn1 addTarget:self action:@selector(popToRootView) forControlEvents:UIControlEventTouchUpInside];
//    btn1.imageEdgeInsets = UIEdgeInsetsMake(0,-10,0,0);
//    [btn1 setTitle:@"取消" forState:UIControlStateNormal];
//    [btn1 setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
//    btn1.titleLabel.font = [UIFont systemFontOfSize:15];
//    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
//    btn1.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
//    
//    UIBarButtonItem *BarBtn1 = [[UIBarButtonItem alloc] initWithCustomView:btn1];
//    self.navigationItem.leftBarButtonItem=BarBtn1;
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height)];
    bgImg.image = [UIImage imageNamed:@"loginBg.png"];
    [self.view addSubview:bgImg];

    
    UILabel *titleLabel1=[[UILabel alloc] initWithFrame:CGRectMake(0,70,320,50)];
    titleLabel1.textAlignment=NSTextAlignmentCenter;
    titleLabel1.textColor=[UIColor whiteColor];
    titleLabel1.text = @"注 册";
    titleLabel1.font=[UIFont fontWithName:strinBoldFontOfSize size:48];
    titleLabel1.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel1];
    
    UILabel *titleLabel2=[[UILabel alloc] initWithFrame:CGRectMake(0,140,320,40)];
    titleLabel2.textAlignment=NSTextAlignmentCenter;
    titleLabel2.textColor=[UIColor whiteColor];
    titleLabel2.text = @"第一步,输入您的手机号";
    titleLabel1.font=[UIFont fontWithName:strinBoldFontOfSize size:36];
    titleLabel2.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel2];

    UIImageView *textBgImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lg101.png"]];
    textBgImg1.userInteractionEnabled = YES;
    textBgImg1.frame = CGRectMake(35,200,250,48);
    [self.view addSubview:textBgImg1];
    
    text1=[[UITextField alloc] init];
    text1.placeholder = @"手机号";
    text1.font=[UIFont systemFontOfSize:15];
    text1.keyboardType = UIKeyboardTypePhonePad;
    text1.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    text1.borderStyle=UITextBorderStyleNone;
    text1.clearButtonMode = UITextFieldViewModeWhileEditing;
    text1.frame=CGRectMake(60,7,280,30);
    text1.delegate=self;
    [textBgImg1 addSubview:text1];
    
     getVerifyCodeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    getVerifyCodeBtn.frame =CGRectMake(167,textBgImg1.frame.origin.y+textBgImg1.frame.size.height+20,117,48);
   // getVerifyCodeBtn.layer.cornerRadius = 5;
   // getVerifyCodeBtn.layer.masksToBounds = YES;
    [getVerifyCodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [getVerifyCodeBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [getVerifyCodeBtn setBackgroundImage:[UIImage imageNamed:@"lg103.png"] forState:UIControlStateNormal];
    getVerifyCodeBtn.titleLabel.font = [UIFont systemFontOfSize:14];//title字体大
    [getVerifyCodeBtn addTarget:self action:@selector(checkVerifyCode)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:getVerifyCodeBtn];
    
    UIImageView *textBg2 = [[UIImageView alloc] initWithFrame:CGRectMake(35,textBgImg1.frame.origin.y+textBgImg1.frame.size.height+20,117,48)];
    textBg2.userInteractionEnabled = YES;
    textBg2.image = [UIImage imageNamed:@"lg101_02.png"];
    [self.view addSubview:textBg2];
    
    text2=[[UITextField alloc] init];
    text2.placeholder = @"输入验证码";
    text2.font=[UIFont systemFontOfSize:15];
    text2.keyboardType = UIKeyboardTypePhonePad;
    text2.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    text2.borderStyle=UITextBorderStyleNone;
    text2.clearButtonMode = UITextFieldViewModeWhileEditing;
    text2.frame=CGRectMake(15,10,270,30);
    text2.delegate=self;
    [textBg2 addSubview:text2];

   
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame =CGRectMake(35,textBg2.frame.size.height+textBg2.frame.origin.y+20,48,48);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"lg201.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(popToRootView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerBtn.frame =CGRectMake(35+58,textBg2.frame.size.height+textBg2.frame.origin.y+20,191,48);
    [registerBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:16];//title字体大
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"lg202.png"] forState:UIControlStateNormal];
    [registerBtn addTarget:self action:@selector(nextStep) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBtn];

    self.view.backgroundColor = [UIColor whiteColor];
    
    //添加手势事件
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    
    self.view.backgroundColor = [UIColor grayColor];


}

-(void)checkVerifyCode
{
    if([AppSession isBlankString:text1.text]==YES)
    {
        [self showMBAlertView:theApp.window message:@"请输入手机号" duration:1];
        return;
    }
    
    [self startTime];

    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.broker.getmobilecheckcode&Phone=%@",text1.text];
    
//    NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             verifyCode =[dict objectForKey:@"data"];
             
             [self showMBAlertView:theApp.window message:@"验证码获取成功" duration:1];
         }
         else
         {
            [self showMBAlertView:theApp.window message:[dict objectForKey:@"msg"] duration:1];

         }
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];

}

-(void)nextStep
{
    if([AppSession isBlankString:text1.text]==YES)
    {
        [self showMBAlertView:theApp.window message:@"请输入验证码" duration:1];
         return;
    }
    if(![verifyCode isEqualToString:text2.text])
    {
        [self showMBAlertView:theApp.window message:@"请输入正确的验证码" duration:1];
        return;
    }
    
    register2VC *vc = [[register2VC alloc] initWithNavigationBar:NO];
    vc.phone_str = text1.text;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - 触摸事件
-(void)tap
{
    
    [AppSession resignKeyBoardInView:self.view];
    
}

-(void)startTime{
    __block int timeout=59; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [getVerifyCodeBtn setTitle:@"发送验证码" forState:UIControlStateNormal];
                getVerifyCodeBtn.userInteractionEnabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 60;
            NSString *strTime = [NSString stringWithFormat:@"%d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [getVerifyCodeBtn setTitle:[NSString stringWithFormat:@"%@秒",strTime] forState:UIControlStateNormal];
                getVerifyCodeBtn.userInteractionEnabled = NO;
                
            });
            timeout--;
            
        }
    });
    dispatch_resume(_timer);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(void)popToRootView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
