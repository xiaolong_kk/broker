//
//  Swizzling.h
//  TopSales
//
//  Created by zen huang on 14-4-29.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Common)
+(UIImage*)myImageNamed: (NSString*)imageName;
@end
