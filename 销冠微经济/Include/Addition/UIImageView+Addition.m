//
//  UIImageView+Addition.m
//  vanke
//
//  Created by xu shun wang on 13-5-31.
//
//

#import "UIImageView+Addition.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIImageView (Addition)
- (void)setWebImage:(NSString *)url
{
    [self setImageWithURL:[NSURL URLWithString:url]];
}

- (void)setWebImage:(NSString *)url placeholderImage:(UIImage *)placeholder
{
    [self setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder];
}
- (void)setWebImageWithFade:(NSString *)url
{
    [self setImageWithURL:[NSURL URLWithString:url] success:^(UIImage *image, BOOL cached) {
        if(!cached)
            [self fadeInLayer:self.layer];
    } failure:^(NSError *error) {
        
    }];
}
- (void)setWebImageWithFade:(NSString *)url placeholderImage:(UIImage *)placeholder
{
    [self setImageWithURL:[NSURL URLWithString:url] placeholderImage:placeholder success:^(UIImage *image, BOOL cached) {
        if(!cached)
            [self fadeInLayer:self.layer];
    } failure:^(NSError *error) {
        
    }];
}


- (void)fadeInLayer:(CALayer *)layer
{
    CABasicAnimation *fadeInAnimate   = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeInAnimate.duration            = 0.75f;
    fadeInAnimate.repeatCount         = 1;
    fadeInAnimate.autoreverses        = NO;
    fadeInAnimate.fromValue           = [NSNumber numberWithFloat:0.0];
    fadeInAnimate.toValue             = [NSNumber numberWithFloat:1.0];
    fadeInAnimate.removedOnCompletion = YES;
    [layer addAnimation:fadeInAnimate forKey:@"animateOpacity"];
    return;
}

@end
