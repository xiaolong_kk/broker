//
//  addCell.h
//  销冠微经济
//
//  Created by zen huang on 14/9/26.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface addCell : UITableViewCell<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tableView_;
    
    unsigned long arr_count;
}
@property(nonatomic,strong) NSMutableArray *temp_arr;
-(void)setData:(NSMutableArray *)arr;

@end
