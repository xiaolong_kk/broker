//
//  myCustomVC.h
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-11.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@interface myCustomVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    UITableView *_tableView;
    UITableView *resultTableView;
    //UIButton *btn;
    UIView *topView;
    //NSMutableArray *dataSource;
    MJRefreshHeaderView *_header;
    UISearchBar *mySearchBar;
    
}

@property (nonatomic, retain) UISearchBar*mSearchBar;
@property (nonatomic, retain) NSMutableArray *resultDataArr;
@property (nonatomic, retain) NSMutableArray *sortedArrForArrays;
@property (nonatomic, retain) NSMutableArray *sectionHeadsKeys;
@property (nonatomic, retain) NSMutableArray *dataSource;


@end
