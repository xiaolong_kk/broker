//
//  CustomSegmentControlBar.h
//  BaseLibrary
//
//  Created by Chai Xue Liang on 12-12-24.
//  Copyright (c) 2012年 xu shun wang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomSegmentControlBar : UIView

@property (nonatomic) int selectedIndex;//当选选中的index，从0开始

@property (nonatomic) int totalCount;//总的按钮数

@property (nonatomic,assign) id delegate;

@property (nonatomic,retain) NSMutableArray *segmentButtons;//返回此组件包含的按钮

@property (nonatomic,retain) NSArray *imageArray;//按钮普通状态下的图片

@property (nonatomic,retain) NSArray *highlightImageArray;//按钮选中状态下的图片

@property (nonatomic,retain) NSArray *titleArray;//按钮标题数组

@property (nonatomic,retain) NSArray *highlightTitleArray;//按钮被选择标题数组

- (id) initWithDelegate:(id)delegate totalCount:(int)totalCount frame:(CGRect)frame tag:(int)tag;

@end

@protocol CustomSegmentControlBarDelegate <NSObject>

@optional

- (void)initSegmentButton:(CustomSegmentControlBar *)sender btn:(UIButton *)btn index:(int)index;//根据totalCount值和frame自动设定好各个button的大小

- (void)selectButton:(CustomSegmentControlBar *)sender btn:(UIButton *)btn index:(int)index;


@end