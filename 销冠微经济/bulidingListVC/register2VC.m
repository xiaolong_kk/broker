//
//  register2VC.m
//  往来
//
//  Created by 朱晓龙 on 14-8-4.
//
//

#import "register2VC.h"

#import "bulidingListVC.h"
#import "MainVC.h"
#import "customListVC.h"
#import "settingVC.h"
#import "registerSuccessedVC.h"

@interface register2VC ()

@end

@implementation register2VC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height)];
    bgImg.image = [UIImage imageNamed:@"loginBg.png"];
    [self.view addSubview:bgImg];

    UILabel *titleLabel1=[[UILabel alloc] initWithFrame:CGRectMake(0,70,320,50)];
    titleLabel1.textAlignment=NSTextAlignmentCenter;
    titleLabel1.textColor=[UIColor whiteColor];
    titleLabel1.text = @"注 册";
    titleLabel1.font=[UIFont fontWithName:strinBoldFontOfSize size:48];
    titleLabel1.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel1];
    
    UILabel *titleLabel2=[[UILabel alloc] initWithFrame:CGRectMake(0,140,320,40)];
    titleLabel2.textAlignment=NSTextAlignmentCenter;
    titleLabel2.textColor=[UIColor whiteColor];
    titleLabel2.text = @"第二步,输入您的公司名称";
    titleLabel1.font=[UIFont fontWithName:strinBoldFontOfSize size:36];
    titleLabel2.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel2];
    
    UIImageView *textBgImg1 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lg101.png"]];
    textBgImg1.userInteractionEnabled = YES;
    textBgImg1.frame = CGRectMake(35,200,250,48);
    [self.view addSubview:textBgImg1];
    
    
    
    UIImageView *textBgImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"lg203.png"]];
    textBgImg2.userInteractionEnabled = YES;
    textBgImg2.frame = CGRectMake(35,textBgImg1.frame.origin.y+textBgImg1.frame.size.height+20,250,48);
    [self.view addSubview:textBgImg2];
    
    text1=[[UITextField alloc] init];
    text1.placeholder = @"请输入公司名称";
    text1.font=[UIFont systemFontOfSize:15];
    //text1.keyboardType = UIKeyboardTypePhonePad;
    text1.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    text1.borderStyle=UITextBorderStyleNone;
    text1.clearButtonMode = UITextFieldViewModeWhileEditing;
    text1.frame=CGRectMake(60,7,280,30);
    text1.delegate=self;
    [textBgImg1 addSubview:text1];
    
    text2=[[UITextField alloc] init];
    text2.placeholder = @"请填写密码";
    text2.font=[UIFont systemFontOfSize:15];
    text2.keyboardType = UIKeyboardTypeDefault;
    text2.contentVerticalAlignment=UIControlContentVerticalAlignmentCenter;
    text2.borderStyle=UITextBorderStyleNone;
    text2.clearButtonMode = UITextFieldViewModeWhileEditing;
    text2.frame=CGRectMake(60,7,280,30);
    text2.secureTextEntry=YES;
    text2.delegate=self;
    [textBgImg2 addSubview:text2];

    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, textBgImg2.frame.origin.y+textBgImg2.frame.size.height+10,320,40)];
    [self.view addSubview:bottomView];
    
    
    UIButton *agreementBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    agreementBtn.frame=CGRectMake(38,0,50,30);
    [agreementBtn addTarget:self action:@selector(agree:) forControlEvents:UIControlEventTouchUpInside];
    [agreementBtn setImage:[UIImage imageNamed:@"lgbox.png"] forState:UIControlStateNormal];
    agreementBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-10,0,0);
    // agreementBtn.backgroundColor = [AppSession colorWithHexString:@"f98156"];
    [agreementBtn setTitle:@"同意"forState:UIControlStateNormal];
    [agreementBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    agreementBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
    agreementBtn.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    [bottomView addSubview:agreementBtn];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(90,6,220,20)];
    tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel.textAlignment = NSTextAlignmentLeft;
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.text = @"《销冠微经纪系统服务协议》";
    tempLabel.textColor = [AppSession colorWithHexString:@"f98156"];
    [bottomView addSubview:tempLabel];

    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame =CGRectMake(35,bottomView.frame.size.height+bottomView.frame.origin.y+10,48,48);
    [backBtn setBackgroundImage:[UIImage imageNamed:@"lg201.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(popToRootView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];

    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerBtn.frame =CGRectMake(35+58,bottomView.frame.size.height+bottomView.frame.origin.y+10,191,48);
    [registerBtn setTitle:@"下一步" forState:UIControlStateNormal];
    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:16];//title字体大
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"lg202.png"] forState:UIControlStateNormal];
    [registerBtn addTarget:self action:@selector(registerSuccessed) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBtn];
    
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //添加手势事件
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tap)];
    tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:tap];
    
}

-(void)agree:(UIButton *)btn
{
    if(!isYseOrNo)
    {
        [btn setImage:[UIImage imageNamed:@"lgbox2.png"] forState:UIControlStateNormal];
        agree_int=1;
    }
    else
    {
        [btn setImage:[UIImage imageNamed:@"lgbox.png"] forState:UIControlStateNormal];
        agree_int=0;
    }
    
    isYseOrNo =!isYseOrNo;
}


#pragma mark - 触摸事件
-(void)tap
{
    
    [AppSession resignKeyBoardInView:self.view];
    
}

-(void)registerSuccessed
{
    
    if([text1.text isEqual:@""])
    {
        [self showMBAlertView:self.view message:@"请输入公司名称" duration:1];
        return;

    }
    if([text2.text isEqual:@""])
    {
        [self showMBAlertView:self.view message:@"请输入密码" duration:1];
        return;
    }
    if(agree_int==0)
    {
        [self showMBAlertView:self.view message:@"请同意《销冠微经纪系统服务协议》" duration:1];
        return;
    }
    
    
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:10];
    [param setValue:self.phone_str forKey:@"Phone"];
    [param setValue:text2.text forKey:@"password"];
    [param setValue:text1.text forKey:@"CompanyName"];
    [param setValue:@"1" forKey:@"Type"];
        
    [self startSynchronousPostRequest:@"http://apiweixin.tops001.com/api.aspx?api=api.broker.brokerregister" postData:param target:self.view alertText:@"注册中..." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict = [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue]==0)
         {
             NSDictionary *dict2 = [dict objectForKey:@"data"];
             theApp.brokerKid = [dict2 objectForKey:@"Kid"];
             [theApp.userDefault setObject:theApp.brokerKid forKey:@"brokerKid"];
             registerSuccessedVC *vc = [[registerSuccessedVC alloc] initWithNavigationBar:NO];
             [self.navigationController pushViewController:vc animated:YES];
        }
         else
         {
             [self showMBAlertView:self.view message:[dict objectForKey:@"msg"] duration:1];

         }
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
     }];
}


-(void)findPassword
{
    
    if([text1.text isEqual:@""]||[text2.text isEqual:@""])
    {
        [self showMBAlertView:self.view message:@"请输入密码" duration:1];
        return;
        
    }
    
    
    if(![text2.text isEqualToString:text1.text])
    {
        [self showMBAlertView:self.view message:@"请确认密码一致" duration:1];
        return;


    }
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:10];
    [param setValue:self.phone_str forKey:@"mobile"];
    [param setValue:text2.text forKey:@"password"];
    
    NSLog(@"%@",param);
    
    [self startSynchronousPostRequest:@"http://release.api.wanglai.cc:80/v1/login/reset" postData:param target:self.view alertText:@"登录中..." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict = [request.responseString objectFromJSONString];
         if([[dict objectForKey:@"status"] intValue]==1&&[[dict objectForKey:@"msg"] isEqualToString:@"success"])
         {
             [self showMBAlertView:theApp.window message:[dict objectForKey:@"msg"] duration:1];
             self.navigationController.navigationBarHidden = YES;
             [self MainTabBarView];
         }
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
     }];
}

-(void)MainTabBarView
{
    theApp.tabBarController = [[MainTabBarViewController alloc] init];
    self.tabBarController.delegate = self;
    
    
    MainVC *vc1 = [[MainVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav1 = [[MLNavigationController alloc] initWithRootViewController:vc1];
    nav1.navigationBar.hidden= YES;
    //
    bulidingListVC *vc2 = [[bulidingListVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav2 = [[MLNavigationController alloc] initWithRootViewController:vc2];
    [nav2.navigationBar setHidden:YES];
    //
    customListVC *vc3 = [[customListVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav3 = [[MLNavigationController alloc] initWithRootViewController:vc3];
    nav3.navigationBar.hidden = YES;
    
    settingVC *vc4 = [[settingVC alloc] initWithNavigationBar:NO];
    MLNavigationController *nav4 = [[MLNavigationController alloc] initWithRootViewController:vc4];
    nav4.navigationBar.hidden = YES;
    
    NSArray *arrVC = [[NSArray alloc] initWithObjects:nav1,nav2,nav3,nav4,nil];
    [theApp.tabBarController setViewControllers:arrVC animated:YES];
    //UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:theApp.mainTabBarController];
    //nav.navigationBarHidden = YES;
    [self presentViewController:theApp.tabBarController animated:YES completion:nil];
    
}

-(void)popToRootView
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
