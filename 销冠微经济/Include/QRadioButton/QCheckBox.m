//
//  EICheckBox.m
//  EInsure
//
//  Created by ivan on 13-7-9.
//  Copyright (c) 2013年 ivan. All rights reserved.
//

#import "QCheckBox.h"

#define Q_CHECK_ICON_WH                    (15.0)
#define Q_ICON_TITLE_MARGIN                (5.0)

@implementation QCheckBox

@synthesize delegate = _delegate;
@synthesize checked = _checked;
@synthesize userInfo = _userInfo;

- (id)initWithDelegate:(id)delegate groupId:(int)groupId _image:(UIImage *)_image _selectedImage:(UIImage *)_selectedImage
{
    self = [super init];
    if (self) {
        _delegate = delegate;
        self.groupId=groupId;
        self.exclusiveTouch = YES;
        [self setImage:_image forState:UIControlStateNormal];
        [self setImage:_selectedImage forState:UIControlStateSelected];
        [self addTarget:self action:@selector(checkboxBtnChecked) forControlEvents:UIControlEventTouchUpInside];
        
        self.dict = [NSMutableDictionary dictionary];
        self._imageName = _image;
        self._selectedImageName = _selectedImage;
    }
    return self;
}


-(void)getId_Str :(NSString *)id_Str
{
    
    self.id_Str = id_Str;
}
- (void)setChecked:(BOOL)checked
{
    
    NSLog(@"%@",checked?@"YES":@"NO");
    
    if (_checked == checked)
    {
        return;
    }
    
    _checked = checked;
    self.selected = checked;
    [self setImage:self._selectedImageName forState:UIControlStateNormal];
    [self setImage:self._imageName forState:UIControlStateSelected];
    
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedCheckBox:checked:)]) {
        [_delegate didSelectedCheckBox:self checked:self.selected];
    }
}

- (void)checkboxBtnChecked {
    self.selected = !self.selected;
    _checked = self.selected;
    
    if (_delegate && [_delegate respondsToSelector:@selector(didSelectedCheckBox:checked:)]) {
        [_delegate didSelectedCheckBox:self checked:self.selected];
    }
}

//- (CGRect)imageRectForContentRect:(CGRect)contentRect {
//    return CGRectMake(5,(CGRectGetHeight(contentRect) - 14)/2.0, 23,23);
//}
//
//- (CGRect)titleRectForContentRect:(CGRect)contentRect {
//    return CGRectMake(20 +6, 0,
//                      CGRectGetWidth(contentRect) - 20 - 10,
//                      CGRectGetHeight(contentRect));
//}



@end
