//
//  Verification.h
//  topsales
//
//  Created by adam_yan on 13-11-02.
//  Copyright (c) 2013年 topsales. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Verification : NSObject{
    
}

+ (NSString *)getCurrentTime;
+ (NSString *)getDeviceId;
+ (NSURL *)auth;
+ (NSURL *)post:(NSString *)authCode apiCode:(NSString *)apiCode apiParams:(NSString *)apiParams requestTime:(NSString *)requestTime;


@end
