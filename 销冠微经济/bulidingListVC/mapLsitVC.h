//
//  mapLsitVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/20.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "B_CalloutAnnotation.h"
#import "B_CalloutAnnotationView.h"
#import "MapAnnotation.h"
@interface mapLsitVC : BaseViewController<MKMapViewDelegate,CLLocationManagerDelegate,UISearchBarDelegate,UISearchDisplayDelegate,B_CalloutAnnotationViewDelegate>
{
    B_CalloutAnnotation *buildingAnnotation;
    
    B_CalloutAnnotationView *buildingAnnotationView;
    
    MapAnnotation *_mapAnnotation;



}
@property (nonatomic,retain) MKMapView *mapView;
@property(nonatomic,retain) NSMutableArray *dataSource;

@end
