//
//  EICheckBox.h
//  EInsure
//
//  Created by ivan on 13-7-9.
//  Copyright (c) 2013年 ivan. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QCheckBoxDelegate;

@interface QCheckBox : UIButton {
    // id<QCheckBoxDelegate> _delegate;
    BOOL _checked;
    id _userInfo;
}

@property(nonatomic, assign)id<QCheckBoxDelegate> delegate;
@property(nonatomic, assign)BOOL checked;
@property(nonatomic, retain)id userInfo;
@property(nonatomic)int groupId;

@property(nonatomic, strong) NSMutableDictionary *dict;
@property(nonatomic, retain)NSString *id_Str;
@property(nonatomic, retain)UIImage *_imageName;
@property(nonatomic, retain)UIImage *_selectedImageName;

-(void)getId_Str :(NSString *)id_Str;

- (id)initWithDelegate:(id)delegate groupId:(int)groupId _image:(UIImage *)_image _selectedImage:(UIImage *)_selectedImage;

@end

@protocol QCheckBoxDelegate <NSObject>

@optional

- (void)didSelectedCheckBox:(QCheckBox *)checkbox checked:(BOOL)checked;

@end
