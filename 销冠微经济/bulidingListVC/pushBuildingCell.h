//
//  pushBuildingCell.h
//  销冠微经济
//
//  Created by zen huang on 14/9/25.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol pushBuildingCellViewDelegate;

@interface pushBuildingCell : UITableViewCell
{
    UIView *sortView;
}
@property (nonatomic, assign) id<pushBuildingCellViewDelegate> delegate;
@property(nonatomic) NSString *hide_str;
@property(nonatomic) int cell_index;
@property(nonatomic) int cell_select;
@property(nonatomic,retain) NSString *cuostomid_str;

-(void)setData:(NSDictionary *)dict;
@end


@protocol pushBuildingCellViewDelegate
@required
- (void)buttonClicked:(int)cell_index;
@end
