//
//  planViewController.m
//  HRVTableView
//
//  Created by liu on 14-10-9.
//  Copyright (c) 2014年 Hamidreza Vakilian. All rights reserved.
//

#import "planViewController.h"
#import "planCell.h"
#import "tagLableView.h"
#import "selectTimeView.h"




@interface planViewController ()<selectTimeDelegate>
{
    NSDictionary *dicData;
    selectTimeView *selectTime;
    selectTimeView *selectLabel;
}

@end

@implementation planViewController



- (void)viewDidLoad
{
    [super viewDidLoad];

    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHight1=44;
    self.navigationLabel.text = @"即将失效";
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,44+addHight,320,self.view.bounds.size.height-44-addHight) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [self headerView];
    [self footer];
    [self getData];
    selectTime = [[selectTimeView alloc]initWithFrame:_tableView.frame];
    selectTime.delegate = self;
    [selectTime theCalendar:[NSArray arrayWithObjects:@"2014/10/15 9:00:00",@"2014/10/12 9:00:00",@"2014/10/13 16:00:00",@"2014/10/14 20:00:00", nil]];
    selectLabel = [[selectTimeView alloc]initWithFrame:_tableView.frame];
    selectLabel.delegate = self;
    self.view.backgroundColor = [AppSession colorWithHexString:@"f5f5f5"];
}

-(void)selectDayAndHours:(NSArray *)Hours
{
    for (NSDictionary *dic in Hours) {
        NSLog(@"%@,%@",[dic objectForKey:@"day"],[dic objectForKey:@"hours"]);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)headerView
{
    UIView *headerView =[[ UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
    headerView.backgroundColor = [AppSession colorWithHexString:@"f5f5f5"];
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(12, 10, 12, 12)];
    imageView.image = [UIImage imageNamed:@"index_tip@2x.png"];
    [headerView addSubview:imageView];
    UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(30, 6, 300, 20)];
    lable.font = [UIFont systemFontOfSize:12];
    lable.textColor = [AppSession colorWithHexString:@"999999"];
    lable.text = @"您要采纳方案后才能获得超级经纪人的联系方式哦!";
    [headerView addSubview:lable];
    UIImageView *lineView =[[UIImageView alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, 1)];
    lineView.image = [UIImage imageNamed:@"mainview_line_long@2x.png"];
    [headerView addSubview:lineView];
    _tableView.tableHeaderView = headerView;
}

-(void)footer
{
    UIView *footView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
    footView.backgroundColor = [AppSession colorWithHexString:@"f5f5f5"];
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-135 , 20, 270, 34)];
    UIEdgeInsets insets = UIEdgeInsetsMake(15, 50, 10, 50);
    UIImage *bgdImage = [[UIImage imageNamed:@"lg103"] resizableImageWithCapInsets:insets];
    [button setBackgroundImage:bgdImage forState:UIControlStateNormal];
    [button addTarget:self action:@selector(touchFootButton) forControlEvents:UIControlEventTouchUpInside];
    [footView addSubview:button];
    button.titleLabel.font  =[UIFont boldSystemFontOfSize:16];
    [button setTitle:@"已提供方案" forState:UIControlStateNormal];
    _tableView.tableFooterView = footView;
}

-(void)touchFootButton
{
    
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (0 == section) {
        return 5;
    }
  
    return 4;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return (indexPath.row == 0)?54:35;
    } else if (indexPath.section == 1)
    {
        if (indexPath.row == 0) {
            return (150 + 33)/2;
        }
        if (indexPath.row == 1) {
            NSString *strTag = [dicData objectForKey:@"TagItem"];
            NSArray *tagArray = [strTag componentsSeparatedByString:@","];
            tagLableView *tagLabel = [tagLableView new];
            return [tagLabel tagLableHight:tagArray] + 7;
        }
        
        if (indexPath.row == 2) {
            
        }
        if (indexPath.row == 3) {
            NSString *str = @"另有其他可看另有其他可看另有其他可看另有其他，可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看另有其他可看。另有其他可看另有其他可看另有其他可看";
            CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:
                           CGSizeMake(160, 2000)];
            return size.height + 20;
        }
    }
    
    return 75/2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier =@"cell";
    planCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
		cell = [[planCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    // Configure the cell...
    [cell content:dicData cellForRowAtIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
        switch (indexPath.row) {
            case 0:  break;
            case 1: [selectLabel nowAppear]; break;
            case 2:[selectTime nowAppear];  break;
            default:
                break;
        }
    }
    
}

-(void)getData
{
    
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.weixinbroker.clientdemandshow&BrokerKid=%@&DemandKid=84",theApp.brokerKid];

    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             dicData = [dict objectForKey:@"data"];
             NSString *str = [dicData objectForKey:@"TagItem"];
             NSArray *tagArray = [str componentsSeparatedByString:@","];
             [selectLabel classLabel:tagArray];
             
             [_tableView reloadData];

         }
         
     }
        didFailedRequest:^(ASIHTTPRequest *request){
                     
    }];
}

@end
