//
//  groomCustomVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/25.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "groomCustomVC.h"
#import "pinyin.h"
#import "ChineseString.h"
#import "simpleCustomPushBuildingVC.h"
#import "groom_resultVC.h"
@implementation groomCustomVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createTableView];
}


- (void)createTableView
{
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHight1=44;
    self.navigationLabel.text = @"推荐客户";
    topView = [[UIView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,45)];
    [self.view addSubview:topView];

    //初始化搜索条
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,44)];
    [mySearchBar setPlaceholder:@"请输入客户姓名或手机号"];
    mySearchBar.barTintColor=[AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"s_search_input.png"] forState:UIControlStateNormal];
    //mySearchBar.userInteractionEnabled = NO;
    mySearchBar.tintColor=[AppSession colorWithHexString:@"84c445"];
    mySearchBar.delegate = self;
    [topView addSubview:mySearchBar];
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,5)];
    tempView.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView];
    
    UIView *tempView1 = [[UIView alloc]initWithFrame:CGRectMake(0,40, 320,5)];
    tempView1.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView1];
    
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,150)];
    
    for(int i=0;i<3;i++)
    {
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0,i*50,320,50);
    [btn addTarget:self action:@selector(popToNextView:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:[UIImage imageNamed:@"icon_add.png"] forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0,-200,0,0);
    [btn setTitle:@"新增客户"forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
    btn.titleEdgeInsets = UIEdgeInsetsMake(0,-150,0,0);
    [headView addSubview:btn];
        
        
                UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
                lineImg.frame = CGRectMake(0,49,320,1);
               [btn addSubview:lineImg];
    }
    
    //        UIImageView *logImg = [[UIImageView alloc] init];
    //        logImg.frame = CGRectMake(15,45,36,36);
    //        logImg.image = [UIImage imageNamed:@"icon_add.png"];
    //        [headView addSubview:logImg];
    
    //        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_short.png"]];
    //        lineImg.frame = CGRectMake(15,59,290,1);
    //        [btn addSubview:lineImg];
    
    //    for(int i=1;i<arr.count+1;i++)
    //    {
    //
    //        UIImageView *logImg = [[UIImageView alloc] init];
    //        logImg.frame = CGRectMake(15,(i-1)*59+10,40,40);
    //        logImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"tel_list%d.png",i]];
    //        [headView addSubview:logImg];
    //
    //        UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,(i-1)*59+18,150,20)];
    //        titleLabel.textAlignment=NSTextAlignmentLeft;
    //        titleLabel.textColor=[UIColor blackColor];
    //        titleLabel.text = [arr objectAtIndex:i-1];
    //        titleLabel.font=[UIFont fontWithName:@"Helvetica-Bold"size:12.5];
    //        titleLabel.backgroundColor=[UIColor clearColor];
    //        [headView addSubview:titleLabel];
    //
    //        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    //        btn.tag = 111+i;
    //        btn.frame=CGRectMake(0,(i-1)*60,320,60);
    //        [btn addTarget:self action:@selector(popToNextView:) forControlEvents:UIControlEventTouchUpInside];
    //        [headView addSubview:btn];
    //
    //        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_short.png"]];
    //        lineImg.frame = CGRectMake(15,59,290,1);
    //        [btn addSubview:lineImg];
    //
    //    }
    
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height+5,320,self.view.bounds.size.height-150) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = headView;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    resultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,100,320,self.view.bounds.size.height-150) style:UITableViewStylePlain];
    resultTableView.dataSource = self;
    resultTableView.delegate = self;
    resultTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:resultTableView];
    resultTableView.hidden = YES;
    
    [self addHeader];
    
    UIButton *add_customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    add_customBtn.frame = CGRectMake(10,self.view.bounds.size.height-50,300,40);
    [add_customBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [add_customBtn setTitle:@"推荐客户" forState:UIControlStateNormal];
    add_customBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
    [add_customBtn addTarget:self action:@selector(popToNextView) forControlEvents:UIControlEventTouchUpInside];
    [add_customBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [self.view addSubview:add_customBtn];

    
}

-(void)popToNextView
{
    groom_resultVC *vc = [[groom_resultVC alloc ]initWithNavigationBar:YES];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addHeader
{
    
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = _tableView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        [self performSelector:@selector(getData) withObject:refreshView afterDelay:0.0];
        
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:0.5];
        
        //NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        //NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                //NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
                // NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
                // NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    [header beginRefreshing];
    _header = header;
}
- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_tableView reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}


-(void)getData
{
    self.sortedArrForArrays = [[NSMutableArray alloc] init];
    self.sectionHeadsKeys = [[NSMutableArray alloc] init];
    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.mycustomer&BrokerKid=%@",theApp.brokerKid];
    [self startSynchronousRequest:str  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSArray *arr = [dict objectForKey:@"data"];
             self.dataSource = [NSMutableArray arrayWithArray:arr];
         }
         
         if(self.dataSource.count>0)
             self.sortedArrForArrays = [self getChineseStringArr:self.dataSource];
         
         [_tableView reloadData];
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
}

-(void)popToNextView:(UIButton *)sender
{
    if(sender.tag==114)
    {
        //        weiAddressListVC *vc = [[weiAddressListVC alloc] init];
        //        vc.back_name = @"通讯录";
        //        [self.navigationController pushViewController:vc animated:YES];
    }
    if(sender.tag==113)
    {
        //        groupListVC *vc = [[groupListVC alloc] init];
        //        vc.back_name = @"通讯录";
        //        [self.navigationController pushViewController:vc animated:YES];
    }
    if(sender.tag==112)
    {
        //        newFriendsVC *vc = [[newFriendsVC alloc] init];
        //        vc.back_name = @"通讯录";
        //        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return  [[self.sortedArrForArrays objectAtIndex:section] count];
    }
    if(tableView==resultTableView)
    {
        return [self.resultDataArr count];
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==_tableView)
    {
        return [self.sortedArrForArrays count];
    }
    if(tableView==resultTableView)
    {
        return [self.resultDataArr count];
        
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return [_sectionHeadsKeys objectAtIndex:section];
    }
    if(tableView==resultTableView)
    {
        return 0;
    }
    
    return 0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(tableView==_tableView)
    {
        return self.sectionHeadsKeys;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tempStr = nil;
    if(tableView==_tableView)
    {
        if ([self.sortedArrForArrays count] > indexPath.section)
        {
            
            NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
            if ([arr count] > indexPath.row)
            {
                
                ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
                tempStr = str.string;
                
                for(NSDictionary *dict in _dataSource)
                {
                    if([[dict objectForKey:@"F_Title"] isEqualToString:str.string])
                    {
                        tempStr  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]];
                    }
                }
            }
        }
    }
    if(tableView==resultTableView)
    {
        tempStr  =[NSString stringWithFormat:@"%@",[[self.resultDataArr objectAtIndex:indexPath.row] objectForKey:@"mid"]];
        
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==_tableView)
    {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImageView *logImg = [[UIImageView alloc] init];
            logImg.tag = 100;
            logImg.backgroundColor = [UIColor grayColor];
            logImg.frame = CGRectMake(15,7.5,40,40);
            [cell addSubview:logImg];
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,54,320,1);
            [cell addSubview:lineImg];
            
            UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,15,150,20)];
            titleLabel.tag=101;
            titleLabel.textAlignment=NSTextAlignmentLeft;
            titleLabel.textColor=[UIColor blackColor];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(120,15,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:sexLabel];
            
            UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(140,15,120,20)];
            phoneLabel.tag=102;
            phoneLabel.textAlignment=NSTextAlignmentCenter;
            phoneLabel.textColor=[AppSession colorWithHexString:@"75a7be"];
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            phoneLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:phoneLabel];
            
            QCheckBox *selectBtn = [[QCheckBox alloc] initWithDelegate:self groupId:12 _image:(UIImage *)[UIImage imageNamed:@"check_box.png"] _selectedImage:(UIImage *)[UIImage imageNamed:@"check_box_hl.png"]];
            selectBtn.tag = 5555;
            selectBtn.frame = CGRectMake(255,15,43,25);
            // [destroyBtn addTarget:self action:@selector(unFollow:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:selectBtn];
            
            
        }
        
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UIImageView *logImg = (UIImageView *)[cell viewWithTag:100];
        QCheckBox *selectBtn = (QCheckBox *)[cell viewWithTag:5555];
        if ([self.sortedArrForArrays count] > indexPath.section)
        {
            
            NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
            if ([arr count] > indexPath.row)
            {
                
                ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
                titleLabel.text = str.string;
                
                for(NSDictionary *dict in _dataSource)
                {
                    if([[dict objectForKey:@"F_Title"] isEqualToString:str.string])
                    {
                        titleLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Title"]];
                        
                        phoneLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Phone"]];
                        sexLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Sex"]];
                        
                        [logImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];
                        
                        //                    self.id_str = [[_dataSource objectAtIndex:destroyBtn.tag-5555] objectForKey:@"mid"];
                        [selectBtn setTitle:[dict objectForKey:@"mid"] forState:UIControlStateNormal];
                        
                        
                    }
                }
            }
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    if(tableView==resultTableView)
    {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImageView *logImg = [[UIImageView alloc] init];
            logImg.tag = 100;
            logImg.frame = CGRectMake(15,10,40,40);
            [cell addSubview:logImg];
            
            
            //        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_short.png"]];
            //        lineImg.frame = CGRectMake(15,0,290,1);
            //        [cell addSubview:lineImg];
            
            UIImageView *lineImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_short.png"]];
            lineImg2.frame = CGRectMake(15,59,290,1);
            [cell addSubview:lineImg2];
            
            
            UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,15,150,20)];
            titleLabel.tag=101;
            titleLabel.textAlignment=NSTextAlignmentLeft;
            titleLabel.textColor=[UIColor blackColor];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(120,15,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:sexLabel];
            
            UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(140,15,120,20)];
            phoneLabel.tag=102;
            phoneLabel.textAlignment=NSTextAlignmentCenter;
            phoneLabel.textColor=[AppSession colorWithHexString:@"75a7be"];
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            phoneLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:phoneLabel];
            
            
            QCheckBox *selectBtn = [[QCheckBox alloc] initWithDelegate:self groupId:12 _image:(UIImage *)[UIImage imageNamed:@"check_box.png"] _selectedImage:(UIImage *)[UIImage imageNamed:@"check_box_hl.png"]];
            selectBtn.tag = 5555;
            selectBtn.frame = CGRectMake(255,13,43,25);
            // [destroyBtn addTarget:self action:@selector(unFollow:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:selectBtn];
            
        }
        
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UIImageView *logImg = (UIImageView *)[cell viewWithTag:100];
        QCheckBox *selectBtn = (QCheckBox *)[cell viewWithTag:5555];
        NSDictionary *dict = [self.resultDataArr objectAtIndex:indexPath.row];
        titleLabel.text = [dict objectForKey:@"F_Title"];
        
        titleLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Title"]];
        
        phoneLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Phone"]];
        sexLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Sex"]];
        
        [logImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];
        
        //                    self.id_str = [[_dataSource objectAtIndex:destroyBtn.tag-5555] objectForKey:@"mid"];
        [selectBtn setTitle:[dict objectForKey:@"mid"] forState:UIControlStateNormal];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    return nil;
}

- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort {
    
    NSMutableArray *chineseStringsArray = [NSMutableArray array];
    
    for(int i = 0; i < [arrToSort count]; i++) {
        ChineseString *chineseString=[[ChineseString alloc]init];
        chineseString.string=[NSString stringWithString:[[arrToSort objectAtIndex:i]  objectForKey:@"F_Title"]];
        if(chineseString.string==nil){
            chineseString.string=@"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            //join the pinYin
            NSString *pinYinResult = [NSString string];
            for(int j = 0;j < chineseString.string.length; j++) {
                NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                 pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            chineseString.pinYin = pinYinResult;
        } else {
            chineseString.pinYin = @"";
        }
        [chineseStringsArray addObject:chineseString];
        
    }
    
    //sort the ChineseStringArr by pinYin
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *arrayForArrays = [NSMutableArray array];
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [chineseStringsArray count]; index++)
    {
        ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
        NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
        NSString *sr= [strchar substringToIndex:1];
        //sr containing here the first character of each string
        if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
        {
            [_sectionHeadsKeys addObject:[sr uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
        {
            [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
            
            if(checkValueAtIndex == NO)
            {
                [arrayForArrays addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
                
            }
        }
    }
    return arrayForArrays;
}

- (void)didSelectedCheckBox:(QCheckBox *)checkbox checked:(BOOL)checked;
{

}

@end
