//
//  settingVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/21.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "settingVC.h"
#import "applyVC.h"
#import "complainViewController.h"
#import "loginVC.h"
@interface settingVC ()

@end

@implementation settingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;
    self.navigationLeftTitleBtn.hidden = YES;
    self.navigationLabel.text = @"我 的";
    //右边按钮
    self.navigationRightTitleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    self.navigationRightTitleBtn.frame=CGRectMake(271,2,40,44);
    [self.navigationRightTitleBtn setTitle:@"修改" forState:UIControlStateNormal];
    [self.navigationRightTitleBtn addTarget:self action:@selector(editBtn) forControlEvents:UIControlEventTouchUpInside];
    self.navigationRightTitleBtn.titleLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:15];

    [self.navigationBg addSubview:self.navigationRightTitleBtn];

    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,44+addHight,320,self.view.bounds.size.height-44-addHight)];
    _scrollView.delegate =self;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_scrollView];
    
    [self setUpMainView];
    
    [self getData];
}

-(void)editBtn
{
    
    if(edit_int==0)
    {
        [self.navigationRightTitleBtn setTitle:@"提交" forState:UIControlStateNormal];
        edit_int=1;
        cellBgImg.userInteractionEnabled = YES;
        UITextField *nameLabel = (UITextField *)[self.view viewWithTag:102];
        [nameLabel becomeFirstResponder];
        return;
    }


    UITextField *nameLabel = (UITextField *)[self.view viewWithTag:102];
    UITextField *phoneLabel = (UITextField *)[self.view viewWithTag:103];
    UITextField *companyLabel = (UITextField *)[self.view viewWithTag:104];
    UITextField *idNumLabel = (UITextField *)[self.view viewWithTag:105];
    UITextField *bankAccountLabel = (UITextField *)[self.view viewWithTag:106];
 
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:20];
    
    [param setValue:theApp.brokerKid forKey:@"BrokerKid"];
    [param setValue:nameLabel.text forKey:@"F_Title"];
    [param setValue:phoneLabel.text forKey:@"F_Phone"];
    [param setValue:companyLabel.text forKey:@"F_CompanyName"];
    [param setValue:idNumLabel.text forKey:@"F_IDNumber"];
    [param setValue:bankAccountLabel.text forKey:@"F_BankAccount"];
   
    [self startSynchronousPostRequest:@"http://apiweixin.tops001.com/api.aspx?api=api.broker.editbroker" postData:param target:self.view alertText:@"个人资料修改中..." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict = [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             
             [self showMBAlertView:self.view message:@"个人资料修改成功" duration:1.5];
             [self.navigationRightTitleBtn setTitle:@"修改" forState:UIControlStateNormal];
             edit_int=0;
             [self getData];

         }
         
         
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         
     }];

}

-(void)setUpMainView
{
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,320, 190)];
    headView.userInteractionEnabled = NO;
    [_scrollView addSubview:headView];
    
   UIImageView *headImg = [[UIImageView alloc] initWithFrame:headView.frame];
    headImg.image = [UIImage imageNamed:@"head_pbg.png"];
    [headView addSubview:headImg];
    
    avatar = [AppSession getRoundImageView:CGPointMake(127.5,45) radius:36];
    [headView addSubview:avatar];
   // avatar=[[AppSession getRoundImageView:CGPointMake(127.5,94.5) radius:32 isMan:[[[currentUser objectForKey:@"gender"] objectForKey:@"id"] intValue]] retain];

//    UIImageView *faceImg = [[UIImageView alloc] initWithFrame:CGRectMake(124,45,72,72)];
//    faceImg.image = [UIImage imageNamed:@"head_picbg.png"];
//    [headView addSubview:faceImg];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(110,130,100,20)];
    nameLabel.tag=999;
    nameLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:15];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = [AppSession colorWithHexString:@"ffffff"];
    [headView addSubview:nameLabel];

    for(int i =0;i<2;i++)
    {
        UIImageView *lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(105*(i+1),160,1,25)];
        lineImg.backgroundColor = [UIColor whiteColor];
        [headView addSubview:lineImg];
    }
    
    for(int i =0;i<3;i++)
    {
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(108*i,160,100,20)];
        tempLabel.tag = 202+i;
        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
        tempLabel.textAlignment = NSTextAlignmentCenter;
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.textColor = [AppSession colorWithHexString:@"ffffff"];
        [headView addSubview:tempLabel];

    }
    
    UIView *cellBgView = [[UIView alloc] initWithFrame:CGRectMake(0,215,290,400)];
    [_scrollView addSubview:cellBgView];
    
     cellBgImg = [[UIImageView alloc] init];
    cellBgImg.userInteractionEnabled = NO;
    cellBgImg.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets = UIEdgeInsetsMake(10,0,10,0);
    cellBgImg.image = [cellBgImg.image resizableImageWithCapInsets:insets];
    cellBgImg.frame = CGRectMake(15,0,294,220);
    [cellBgView addSubview:cellBgImg];
    

    for(int i =0;i<5;i++)
    {
        
        UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(25,i*45,45,45)];
        [logoBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+1]] forState:UIControlStateNormal];
        //logoBtn.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+1]];
        [cellBgView addSubview:logoBtn];
        
        UILabel *tempLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(45,45*i,100,45)];
        tempLabel1.font=[UIFont fontWithName:stringFontOfSize size:13];
        tempLabel1.textAlignment = NSTextAlignmentCenter;
        tempLabel1.backgroundColor = [UIColor clearColor];
        tempLabel1.textColor = [AppSession colorWithHexString:@"000000"];
        [cellBgView addSubview:tempLabel1];
        
        
        UITextField *tempLabel2 = [[UITextField alloc] initWithFrame:CGRectMake(145,45*i,150,45)];
        tempLabel2.font=[UIFont fontWithName:stringFontOfSize size:13];
        tempLabel2.textAlignment = NSTextAlignmentRight;
        tempLabel2.tag = 102+i;
        tempLabel2.delegate = self;
        tempLabel2.backgroundColor = [UIColor clearColor];
        tempLabel2.textColor = [AppSession colorWithHexString:@"000000"];
        [cellBgView addSubview:tempLabel2];
        
        if(i<4)
        {
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(15,44*(i+1),290,1);
        [cellBgView addSubview:lineImg];
        }

        
    }
    
    UIView *cellBgView1 = [[UIView alloc] initWithFrame:CGRectMake(0,235+220,290,90)];
    [_scrollView addSubview:cellBgView1];
    
    UIImageView *cellBgImg1 = [[UIImageView alloc] init];
    cellBgImg1.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets1 = UIEdgeInsetsMake(10,0,10,0);
    cellBgImg1.image = [cellBgImg1.image resizableImageWithCapInsets:insets1];
    cellBgImg1.frame = CGRectMake(15,0,294,90);
    [cellBgView1 addSubview:cellBgImg1];
    
    NSArray *arr2 = [[NSArray alloc] initWithObjects:@"我的申请",@"意见反馈",nil];

    for(int i =0;i<2;i++)
    {
        UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(25,i*45,45,45)];
        [logoBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+6]] forState:UIControlStateNormal];
        //logoBtn.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+1]];
        [cellBgView1 addSubview:logoBtn];
        
        UIButton *touchBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,i*45, 290, 45)];
        [touchBtn addTarget:self action:@selector(nextView:) forControlEvents:UIControlEventTouchUpInside];
        touchBtn.tag = 333+i;
        [cellBgView1 addSubview:touchBtn];
        UILabel *tempLabel1 = [[UILabel alloc] initWithFrame:CGRectMake(45,45*i,100,45)];
        tempLabel1.font=[UIFont fontWithName:stringFontOfSize size:13];
        tempLabel1.textAlignment = NSTextAlignmentCenter;
        tempLabel1.text = arr2[i];
        tempLabel1.backgroundColor = [UIColor clearColor];
        tempLabel1.textColor = [AppSession colorWithHexString:@"000000"];
        [cellBgView1 addSubview:tempLabel1];
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(15,44,290,1);
        [cellBgView1 addSubview:lineImg];
        
    }
    UIButton *logoffBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoffBtn.frame =CGRectMake(35,cellBgView1.frame.size.height+cellBgView1.frame.origin.y+20,250,48);
    [logoffBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    [logoffBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    logoffBtn.titleLabel.font = [UIFont systemFontOfSize:16];//title字体大
    [logoffBtn setBackgroundImage:[UIImage imageNamed:@"lg104.png"] forState:UIControlStateNormal];
    [logoffBtn addTarget:self action:@selector(logOffSheet) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:logoffBtn];

    _scrollView.contentSize = CGSizeMake(320,cellBgView.frame.origin.y+cellBgView.frame.size.height+80);

    
}

-(void)logOffSheet
{
	UIAlertView *baseAlert = [[UIAlertView alloc]
							  initWithTitle: nil message:@"您确定注销此帐号？"
                              delegate: self cancelButtonTitle:@"取消"
							  otherButtonTitles:@"确定",nil];
	[baseAlert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;
{
    
    if(buttonIndex==1)
        [self logOff];
}

-(void)logOff
{
    [theApp.userDefault removeObjectForKey:@"FirstUse"];
    [theApp.userDefault removeObjectForKey:@"brokerKid"];
    
    loginVC *vc = [[loginVC alloc] initWithNavigationBar:NO];
    UINavigationController *nav = [[UINavigationController alloc ] initWithRootViewController:vc];
    nav.navigationBar.hidden = YES;
    //    NSArray *subViewArray = [self.view subviews];
    //    for (id obj in subViewArray)
    //    {
    //        [obj removeFromSuperview];
    //    }
    
    [self presentViewController:nav animated:YES completion:nil];
    

}


-(void)getData
{
    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.broker.getbrokerinfo&BrokerKid=%@",theApp.brokerKid];
    
    UITextField *nameLabel = (UITextField *)[self.view viewWithTag:102];
    UITextField *phoneLabel = (UITextField *)[self.view viewWithTag:103];
    UITextField *companyLabel = (UITextField *)[self.view viewWithTag:104];
    UITextField *idNumLabel = (UITextField *)[self.view viewWithTag:105];
    UITextField *bankAccountLabel = (UITextField *)[self.view viewWithTag:106];
    UITextField *logoNameLabel = (UITextField *)[self.view viewWithTag:999];
    
    UILabel *renzhengLabel = (UILabel *)[self.view viewWithTag:202];
    UILabel *signLabel = (UILabel *)[self.view viewWithTag:203];
    UILabel *pointLabel = (UILabel *)[self.view viewWithTag:204];

    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
            NSDictionary *dict2 = [dict objectForKey:@"data"];
             
            // NSLog(@"%@",dict2);
             nameLabel.text = [dict2 objectForKey:@"F_Name"];
             phoneLabel.text = [dict2 objectForKey:@"F_Phone"];
             companyLabel.text = [dict2 objectForKey:@"company"];
             
             if([AppSession isBlankString:[dict2 objectForKey:@"F_IDNumber"]]==YES)
             idNumLabel.text = @"未填写";
             else
             idNumLabel.text = [dict2 objectForKey:@"F_IDNumber"];
             
             
             if([AppSession isBlankString:[dict2 objectForKey:@"F_BankAccount"]]==YES)
            bankAccountLabel.text = @"未填写";
             else
             bankAccountLabel.text = [dict2 objectForKey:@"F_BankAccount"];
             
            logoNameLabel.text = [dict2 objectForKey:@"F_Name"];
             
             renzhengLabel.text = @"认证";
             signLabel.text = [NSString stringWithFormat:@"签到 %@",[dict2 objectForKey:@"Sign"]];
             pointLabel.text = [NSString stringWithFormat:@"超积金 %@",[dict2 objectForKey:@"F_Point"]];
             
             
             if([AppSession isBlankString:[dict2 objectForKey:@"F_PicUrl"]]==YES)
                 NSLog(@"ddddd");
             else
             [avatar setImageWithURL:[dict2 objectForKey:@"F_PicUrl"]];



         }
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
    
}


-(void)nextView:(UIButton *)sender
{
    if(sender.tag==333)
    {
    applyVC *vc = [[applyVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    if(sender.tag==334)

    {
        complainViewController *vc = [[complainViewController alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    

}


- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
{
    
    UITextField *nameTextField = (UITextField *)[self.view viewWithTag:106];

    if(textField==nameTextField)
    {
    if(IPHONE_5)
        [_scrollView setContentOffset:CGPointMake(0,200) animated:YES];
    else
        [_scrollView setContentOffset:CGPointMake(0,220) animated:YES];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [AppSession resignKeyBoardInView:self.view];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
