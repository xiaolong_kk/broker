//
//  complainViewController.m
//  TopSales
//
//  Created by 朱晓龙 on 13-10-31.
//
//

#import "complainViewController.h"
#import "AppDelegate.h"
@interface complainViewController ()

@end

@implementation complainViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    theApp.tabBarController.customTabBarView.hidden = NO;
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    theApp.tabBarController.customTabBarView.hidden = YES;

    
    self.navigationSubLabel.hidden = YES;
    self.logoImg.hidden = YES;
    
    self.navigationLabel.text = @"意见反馈";
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;
    
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,self.view.bounds.size.height)];
    _scrollView.delegate =self;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(320,570);
    [self.view addSubview:_scrollView];

    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,10,320,40)];
    tempLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:20];
    tempLabel.textAlignment = NSTextAlignmentCenter;
    tempLabel.text = @"有意见,遇问题?找客服!";
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.textColor = [AppSession colorWithHexString:@"000000"];
    [_scrollView addSubview:tempLabel];
    
    UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(16,tempLabel.frame.origin.y+tempLabel.frame.size.height+10,287,130)];
    tagImg.image  =[UIImage imageNamed:@"feedback_pic.png"];
    [_scrollView addSubview:tagImg];

    UIButton *phoneBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    phoneBtn.frame=CGRectMake(0,tagImg.frame.origin.y+tagImg.frame.size.height,320,45);
    [phoneBtn addTarget:self action:@selector(callPhone) forControlEvents:UIControlEventTouchUpInside];
    [phoneBtn setTitle:@"客服电话: 4008-125-121"forState:UIControlStateNormal];
    [phoneBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    phoneBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    phoneBtn.titleLabel.textAlignment = NSTextAlignmentLeft;//设置title的字体居中
    phoneBtn.titleEdgeInsets = UIEdgeInsetsMake(0,-130,0,0);
    [_scrollView addSubview:phoneBtn];

    UILabel *tempLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(16,phoneBtn.frame.origin.y+phoneBtn.frame.size.height-5,290,20)];
    tempLabel2.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel2.textAlignment = NSTextAlignmentLeft;
    tempLabel2.text = @"你也可以在下面填写反馈意见哦";
    tempLabel2.backgroundColor = [UIColor clearColor];
    tempLabel2.textColor = [AppSession colorWithHexString:@"000000"];
    [_scrollView addSubview:tempLabel2];

    UIImageView *bg = [[UIImageView alloc] initWithFrame:CGRectMake(15,tempLabel2.frame.origin.y+tempLabel2.frame.size.height+5,290,120)];
    bg.userInteractionEnabled = YES;
    bg.image  =[UIImage imageNamed:@"other_idea_input.png"];
    [_scrollView addSubview:bg];
    
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(2,10,288,110)];
    _textView.Delegate = self;
//    _textView.text.font=[UIFont fontWithName:@"HiraKakuProN-W3"size:15];
   // _textView.textView.textColor = [AppSession colorWithHexString:@"989691"];
   // _textView.placeHolder = @"请输入你想说的话";
    [bg addSubview:_textView];
    
    UIButton *subBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    subBtn.frame = CGRectMake(20,bg.frame.origin.y+bg.frame.size.height+20,280,40);
    [subBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [subBtn setTitle:@"提交" forState:UIControlStateNormal];
    subBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:14];
    [subBtn addTarget:self action:@selector(onNavigationRightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [subBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [_scrollView addSubview:subBtn];

    [self.navigationRightTitleBtn setBackgroundImage:[UIImage imageNamed:@"top_btn_ok.png"] forState:UIControlStateNormal];

}


- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    if(IPHONE_5)
        [_scrollView setContentOffset:CGPointMake(0,200) animated:YES];
    else
        [_scrollView setContentOffset:CGPointMake(0,220) animated:YES];
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [AppSession resignKeyBoardInView:self.view];
    }
}

-(void)callPhone
{
    [AppSession makePhoneTo:@"4008125121" name:nil target:self needSave:NO];
}

- (void) onNavigationRightBtnClick:(UIButton *)sender
{
    
 if([_textView.text isEqualToString:@""])
 {
     [self showMBAlertView:self.view message:@"请填写您的反馈意见" duration:1.5];

     return;
 }
    NSString *text_str =  [_textView.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinbroker.feedback&BrokerKid=%@&comment=%@",theApp.brokerKid,text_str];
    
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             [self showMBAlertView:self.view message:@"意见反馈成功" duration:1.0];
             
             [self performSelector:@selector(popView) withObject:nil afterDelay:0.5f];
             
         }
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];

}



-(void)popView
{
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
