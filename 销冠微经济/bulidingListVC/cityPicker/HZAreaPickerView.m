//
//  HZAreaPickerView.m
//  areapicker
//
//  Created by Cloud Dai on 12-9-9.
//  Copyright (c) 2012年 clouddai.com. All rights reserved.
//

#import "HZAreaPickerView.h"
#import <QuartzCore/QuartzCore.h>

#define kDuration 0.3
#define kItemsIDArray @"kItemsIDArray"
@interface HZAreaPickerView ()
{
    NSMutableArray *provinces, *cities, *areas;
    NSInteger selectIndexInCol1;
    NSInteger selectIndexInCol2;
    NSInteger selectIndexInCol3;

}

@end

@implementation HZAreaPickerView

@synthesize delegate=_delegate;
@synthesize pickerStyle=_pickerStyle;
@synthesize locate=_locate;
@synthesize locatePicker = _locatePicker;



-(HZLocation *)locate
{
    if (_locate == nil) {
        _locate = [[HZLocation alloc] init];
    }
    
    return _locate;
}

- (id)initWithStyle:(HZAreaPickerStyle)pickerStyle delegate:(id<HZAreaPickerDelegate>)delegate
{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"HZAreaPickerView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.delegate = delegate;
        self.pickerStyle = pickerStyle;
        self.locatePicker.dataSource = self;
        self.locatePicker.delegate = self;
        provinces = [NSMutableArray array];
        cities = [NSMutableArray array];
        

        //加载数据
//        if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
//            provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"area.plist" ofType:nil]];
//            cities = [[provinces objectAtIndex:0] objectForKey:@"cities"];
//            
//            self.locate.state = [[provinces objectAtIndex:0] objectForKey:@"state"];
//            self.locate.city = [[cities objectAtIndex:0] objectForKey:@"city"];
//            
//            areas = [[cities objectAtIndex:0] objectForKey:@"areas"];
//            if (areas.count > 0) {
//                self.locate.district = [areas objectAtIndex:0];
//            } else{
//                self.locate.district = @"";
//            }
//            
//        } else{
        
//        NSMutableArray * arr1 = [NSMutableArray arrayWithArray:self.allDic[kItemsIDArray]];
//     //  NSMutableArray * arr2 = [NSMutableArray arrayWithArray:<#(NSArray *)#>]
//        for (int i =0; i<arr1.count; i++) {
//            //NSMutableArray * provincesArr = [NSMutableArray  arrayWithObjects:self.allDic[@"i"][@"name"], nil];
//            [ provinces addObject:self.allDic[@"i"][@"name"] ];
//        }
//        
//          //  provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"city.plist" ofType:nil]];
//            cities = [[provinces objectAtIndex:0] objectForKey:@"cities"];
//           // self.locate.state = [[provinces objectAtIndex:0] objectForKey:@"state"];
//          //  self.locate.city = [cities objectAtIndex:0];
        
            NSString *path = [[NSBundle mainBundle] pathForResource:@"district" ofType:@"csv"];
            
            NSString *contents = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
            //解析出的大数组
            NSArray *contentsArray = [contents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            //大字典，所有数据和关系
            self.allDic = [NSMutableDictionary dictionary];
            //存储父级关系
            NSMutableDictionary * pDic = [NSMutableDictionary dictionary];
            int idxID = 0;
            int idxName = 1;
            int idxLevel = 2;
            int idxPID = 3;
            NSInteger idx;
            for (idx = 0; idx < contentsArray.count; idx++) {
                //每行记录
                NSString* currentContent = [contentsArray objectAtIndex:idx];
                //每行记录转换成内容数组
                NSArray* lineDataArr = [currentContent componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\t"]];
                if (lineDataArr == nil ||lineDataArr.count == 0|| lineDataArr.count != 4) {
                    //空行处理
                    break;
                }
                NSNumber* itemID = lineDataArr[idxID];
                NSNumber* pID = lineDataArr[idxPID];
                NSString *itemName = lineDataArr [idxName];
                pDic[itemID] = pID;
                if ([lineDataArr[idxLevel] intValue] == 1) {
                    //第一级：省，直辖市
                    NSMutableDictionary * dataDic = self.allDic[itemID];
                    if(dataDic == nil){
                        //新字典的name元素
                        dataDic = [NSMutableDictionary dictionary];
                        [dataDic setObject:itemName forKey:@"name"];
                        //新字典的items元素
                        NSMutableDictionary * itemsDic = [NSMutableDictionary dictionary];
                        [dataDic setObject:itemsDic forKey:@"items"];
                        //itemID对应的新字典
                        [self.allDic setObject:dataDic forKey:itemID];
                        //第一级ID加入到nsarray中避免后面需要排序
                        NSMutableArray *itemsIDArray = self.allDic[kItemsIDArray];
                        if (itemsIDArray == nil) {
                            itemsIDArray = [NSMutableArray array];
                            self.allDic[kItemsIDArray] = itemsIDArray;
                        }
                        [itemsIDArray addObject:itemID];
                    }
                }
                else if ([lineDataArr[idxLevel] intValue] == 2) {
                    //第二级：市，直辖市的区
                    NSMutableDictionary * secondLevelDic = self.allDic[pID][@"items"];
                    NSMutableDictionary *  dataDic = secondLevelDic[itemID];
                    if (dataDic == nil) {
                        dataDic = [NSMutableDictionary dictionary];
                        //新字典的name元素
                        [dataDic setObject:itemName forKey:@"name"];
                        NSMutableDictionary * itemsDic = [NSMutableDictionary dictionary];
                        //新字典的items元素
                        [dataDic setObject:itemsDic forKey:@"items"];
                        //itemID对应的新字典
                        [secondLevelDic setObject:dataDic forKey:itemID];
                    }
                    //第二级ID加入到nsarray中避免后面需要排序
                    NSMutableArray *itemsIDArray = self.allDic[pID][kItemsIDArray];
                    if (itemsIDArray == nil) {
                        itemsIDArray = [NSMutableArray array];
                        self.allDic[pID][kItemsIDArray] = itemsIDArray;
                    }
                    [itemsIDArray addObject:itemID];

              }
              else if ([lineDataArr[idxLevel] intValue] == 3) {
                  //第三级：区县
                  id ppID = pDic[pID];
                  //叶子节点为id:name的key:value对
                  NSMutableDictionary * thirdLevelDic = self.allDic[ppID][@"items"][pID][@"items"];
                  NSMutableDictionary *  dataDic = thirdLevelDic[itemID];
                  if (dataDic == nil) {
                      dataDic = [NSMutableDictionary dictionary];
                      //新字典的name元素
                      [dataDic setObject:itemName forKey:@"name"];
                      NSMutableDictionary * itemsDic = [NSMutableDictionary dictionary];
                      //新字典的items元素
                      [dataDic setObject:itemsDic forKey:@"items"];
                      //itemID对应的新字典
                      [thirdLevelDic setObject:dataDic forKey:itemID];
                  }
                  //第三级ID加入到nsarray中避免后面需要排序
                  NSMutableArray *itemsIDArray = self.allDic[ppID][@"items"][pID][kItemsIDArray];
                  if (itemsIDArray == nil) {
                      itemsIDArray = [NSMutableArray array];
                      self.allDic[ppID][@"items"][pID][kItemsIDArray] = itemsIDArray;
                  }
                  [itemsIDArray addObject:itemID];

              }
            }
        }
    NSMutableArray * arr = [NSMutableArray array];
    NSMutableArray * arr1 = [NSMutableArray arrayWithArray:self.allDic[kItemsIDArray]];
   
   // NSMutableArray * arr2 = [NSMutableArray array];
   
    for (int i =0; i<arr1.count; i++) {
       

        [ arr addObject:self.allDic[[arr1 objectAtIndex: i]] ];
//        [arr2 addObject:self.allDic[[arr1 objectAtIndex: i]][kItemsIDArray]];
    }
    [provinces addObjectsFromArray:arr];
    // cities = [[provinces objectAtIndex:0] objectForKey:@"cities"];

    return self;
    
}



#pragma mark - PickerView lifecycle

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
        return 3;
    } else{
        return 2;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    int count = 0;
    switch (component) {
        case 0:
            count =  [self.allDic[kItemsIDArray] count];
            break;
        case 1:{
            NSNumber *selectPid = [self.allDic[kItemsIDArray] objectAtIndex:selectIndexInCol1];
            count = [self.allDic[selectPid][kItemsIDArray] count];
            }
            break;
            
        case 2:
            if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
                return [areas count];
                break;
            }
        default:
            count =  0;
            break;
    }
    return count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
//    if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
//        switch (component) {
//            case 0:
//                return [[provinces objectAtIndex:row] objectForKey:@"state"];
//                break;
//            case 1:
//                return [[cities objectAtIndex:row] objectForKey:@"city"];
//                break;
//            case 2:
//                if ([areas count] > 0) {
//                    return [areas objectAtIndex:row];
//                    break;
//                }
//            default:
//                return  @"";
//                break;
//        }
//    }
//    else{
    
              // NSMutableArray * arr2 = [NSMutableArray arrayWithArray:self.allDic[]]
    NSString *strName = nil;
        switch (component) {
            case 0:
            {
                NSNumber *itemID = [self.allDic[kItemsIDArray] objectAtIndex:row];
                strName = self.allDic[itemID][@"name"];
            }
                break;
            case 1:
            {
                NSNumber *selectPid = [self.allDic[kItemsIDArray] objectAtIndex:selectIndexInCol1];
                NSNumber *itemID = [self.allDic[selectPid][kItemsIDArray] objectAtIndex:row];
                strName = self.allDic[selectPid][@"items"][itemID][@"name"];
            }
                break;
            default:
                strName = @"";
                break;
    }
    return strName;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
//    if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
//        switch (component) {
//            case 0:
//                cities = [[provinces objectAtIndex:row] objectForKey:@"cities"];
//                [self.locatePicker selectRow:0 inComponent:1 animated:YES];
//                [self.locatePicker reloadComponent:1];
//                
//                areas = [[cities objectAtIndex:0] objectForKey:@"areas"];
//                [self.locatePicker selectRow:0 inComponent:2 animated:YES];
//                [self.locatePicker reloadComponent:2];
//                
//                self.locate.state = [[provinces objectAtIndex:row] objectForKey:@"state"];
//                self.locate.city = [[cities objectAtIndex:0] objectForKey:@"city"];
//                if ([areas count] > 0) {
//                    self.locate.district = [areas objectAtIndex:0];
//                } else{
//                    self.locate.district = @"";
//                }
//                break;
//            case 1:
//                areas = [[cities objectAtIndex:row] objectForKey:@"areas"];
//                [self.locatePicker selectRow:0 inComponent:2 animated:YES];
//                [self.locatePicker reloadComponent:2];
//                
//                self.locate.city = [[cities objectAtIndex:row] objectForKey:@"city"];
//                if ([areas count] > 0) {
//                    self.locate.district = [areas objectAtIndex:0];
//                } else{
//                    self.locate.district = @"";
//                }
//                break;
//            case 2:
//                if ([areas count] > 0) {
//                    self.locate.district = [areas objectAtIndex:row];
//                } else{
//                    self.locate.district = @"";
//                }
//                break;
//            default:
//                break;
//        }
//    } else{
        switch (component) {
            case 0:
                selectIndexInCol1 = row;
                [self.locatePicker reloadComponent:1];
                
                /*
            {
                NSLog(@"%@", [[self.allDic objectForKey:[provinces objectAtIndex:row]]objectForKey:kItemsIDArray]);
                cities =[[[[provinces objectAtIndex:row] objectForKey:@"items"] objectForKey:@"37"] objectForKey:@"name"];
                [self.locatePicker selectRow:0 inComponent:1 animated:YES];
                [self.locatePicker reloadComponent:1];
                
                ;
            }
                 */
                break;
            case 1:
                selectIndexInCol2 = row;
//                NSNumber *selectPid = [self.allDic[kItemsIDArray] objectAtIndex:selectIndexInCol1];
                //NSNumber *itemID = [self.allDic[selectPid][kItemsIDArray] objectAtIndex:row];
//                NSString *pName = self.allDic[selectPid][@"name"];
               // NSString *itemName = self.allDic[selectPid][@"items"][itemID][@"name"];
                
                
            break;
            default:
            break;
        }
//    }
    NSNumber *selectPid = [self.allDic[kItemsIDArray] objectAtIndex:selectIndexInCol1];
    NSString *pName = self.allDic[selectPid][@"name"];
   // NSLog(@"%d,%d,%d",[self.allDic count],[self.allDic[selectPid][kItemsIDArray] count],row);
    
    if(row>[self.allDic[selectPid][kItemsIDArray] count])
    return;
    
    NSNumber *itemID = [self.allDic[selectPid][kItemsIDArray] objectAtIndex:row];
      NSString *itemName = self.allDic[selectPid][@"items"][itemID][@"name"];
    self.locate.city = itemName;
    self.locate.selectPid = selectPid;
    self.locate.itemID = itemID;
    self.locate.state = pName;
    if([self.delegate respondsToSelector:@selector(pickerDidChaneStatus:)]) {
        [self.delegate pickerDidChaneStatus:self];
        
        
        
        
   }

}


#pragma mark - animation

- (void)showInView:(UIView *) view
{
    self.frame = CGRectMake(0, view.frame.size.height, self.frame.size.width, self.frame.size.height);
    [view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, view.frame.size.height - self.frame.size.height, self.frame.size.width, self.frame.size.height);
    }];
    
}

-(IBAction)cancelPicker:(id)sender
{
    [self cancelPicker];
}

-(IBAction)quePicker:(id)sender;
{
    [self cancelPicker];
}

- (void)cancelPicker
{
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.frame = CGRectMake(0, self.frame.origin.y+self.frame.size.height, self.frame.size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         
                     }];
    
}

@end
