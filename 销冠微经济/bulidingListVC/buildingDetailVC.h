//
//  buildingDetailVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/21.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HVTableView.h"
@interface buildingDetailVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,UIWebViewDelegate>
{
	UITableView* myTable;
    UIWebView *webView;
    
    NSString *google_lng;
    NSString *google_lat;
}

@property(nonatomic,strong)NSString *buildingKid_str;
@property(nonatomic,strong)NSString *AgreementKid_str;
@property(nonatomic)BOOL Agreement;
@property(nonatomic,strong)NSDictionary *buildingDict;

@property(nonatomic,strong)NSString *address;
@end
