//
//  customDetailVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/21.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HVTableView.h"

@interface customDetailVC : BaseViewController<HVTableViewDelegate, HVTableViewDataSource>
{
	HVTableView* myTable;
    NSMutableArray *dataSource;
    
    UIView *sortView;
}

@property(nonatomic,strong)NSString *kid_str;



@end
