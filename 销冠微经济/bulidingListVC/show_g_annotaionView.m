//
//  show_g_annotaionView.m
//  销冠微经济
//
//  Created by zen huang on 14/9/22.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "show_g_annotaionView.h"

@implementation show_g_annotaionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0,0,64,22);
        UIImageView *tempImg =[[UIImageView alloc] init];
        tempImg.tag=222;
        tempImg.frame = self.frame;
        tempImg.image = [UIImage imageNamed:@"map_tip2.png"];
        self.centerOffset = CGPointMake(0,0);
        [self addSubview:tempImg];
        
        self.backgroundColor = [UIColor clearColor];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,64,15)];
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont fontWithName:stringFontOfSize size:10];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        [tempImg addSubview:self.titleLabel];
        
//       UIButton *button_ = [UIButton buttonWithType:UIButtonTypeCustom];
//        button_.frame = CGRectMake(0, 0, 240, 65);
//        [tempImg addSubview:button_];
//        [button_ addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchDown];
//        [self addSubview:button_];

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
