//
//  groomCustomVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/23.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "addCustomVC.h"
#import "pickerViewController.h"
@interface addCustomVC ()

@end

@implementation addCustomVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;
    
    self.navigationLabel.text = @"添加客户";
    

    UIButton *tagBtn2=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn2.frame=CGRectMake(245,3,80,44);
    [tagBtn2 setTitle:@"本地导入"forState:UIControlStateNormal];
    [tagBtn2 setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [tagBtn2 addTarget:self action:@selector(contact) forControlEvents:UIControlEventTouchUpInside];
    tagBtn2.titleLabel.font = [UIFont systemFontOfSize:15];
    tagBtn2.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    [self.navigationBg addSubview:tagBtn2];

    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,self.view.bounds.size.height)];
    _scrollView.delegate =self;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(320,570);
    [self.view addSubview:_scrollView];
    
    [self setUpMainView];
}

-(void)contact
{
    pickerViewController *vc = [[pickerViewController alloc] initWithNavigationBar:YES];
    [self.navigationController pushViewController:vc animated:YES];

}

-(void)setUpMainView

{
    faceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    faceBtn.frame=CGRectMake(17.5,17.5,57,57);
    [faceBtn setImage:[UIImage imageNamed:@"custom_photo.png"] forState:UIControlStateNormal];
    [faceBtn addTarget:self action:@selector(pickImg) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:faceBtn];
    
    
    UIImageView *textBg1 = [[UIImageView alloc] init];
    textBg1.userInteractionEnabled = YES;
    textBg1.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets1 = UIEdgeInsetsMake(10,10,10,10);
    textBg1.image = [textBg1.image resizableImageWithCapInsets:insets1];
    textBg1.frame = CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width+12.5, 17.5,215,36);
    [_scrollView addSubview:textBg1];
    
    UIImageView *tagImg1 = [[UIImageView alloc]initWithFrame:CGRectMake(10,10,15,16)];
    tagImg1.image = [UIImage imageNamed:@"custom_face.png"];
    [textBg1 addSubview:tagImg1];
    
    name_text = [[UITextField alloc] initWithFrame:CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width-40,0,215,36)];
    name_text.placeholder = @"客户姓名";
    name_text.font=[UIFont fontWithName:strinBoldFontOfSize size:14];
    name_text.textColor = [UIColor blackColor];
    [textBg1 addSubview:name_text];
    
    
    QRadioButton *manBtn = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"2"]];
    manBtn.frame = CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width+15,57,100, 30);
    manBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [manBtn setTitle:@"先生"forState:UIControlStateNormal];
    [manBtn setTitleColor:[AppSession colorWithHexString:@"333333"] forState:UIControlStateNormal];
    [manBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
    [_scrollView addSubview:manBtn];
    
    QRadioButton *ladyBtn = [[QRadioButton alloc] initWithDelegate:self groupId:[NSString stringWithFormat:@"2"]];
    ladyBtn.frame = CGRectMake(faceBtn.frame.origin.x+faceBtn.frame.size.width+100,57,100, 30);
    ladyBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    [ladyBtn setTitle:@"女士"forState:UIControlStateNormal];
    [ladyBtn setTitleColor:[AppSession colorWithHexString:@"333333"] forState:UIControlStateNormal];
    [ladyBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:13.0f]];
     [_scrollView addSubview:ladyBtn];
    
    
    
    UIImageView *textBg2 = [[UIImageView alloc] init];
    textBg2.userInteractionEnabled = YES;
    textBg2.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets2 = UIEdgeInsetsMake(10,0,10,0);
    textBg2.image = [textBg2.image resizableImageWithCapInsets:insets2];
    textBg2.frame = CGRectMake(15,100,294,108);
    [_scrollView addSubview:textBg2];
    
    
    for(int i =0;i<3;i++)
    {
        UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,i*36+10,15,16)];
        tagImg.image = [UIImage imageNamed:@"custom_phone.png"];
        [textBg2 addSubview:tagImg];
      
         UITextField *phone_text = [[UITextField alloc] initWithFrame:CGRectMake(35,i*36+2,250,36)];
        phone_text.tag = 222+i;
        phone_text.keyboardType = UIKeyboardTypePhonePad;
        phone_text.placeholder = [NSString stringWithFormat:@"联系电话%d",i+1];
        phone_text.font=[UIFont fontWithName:stringFontOfSize size:14];
        phone_text.textColor = [UIColor blackColor];
        [textBg2 addSubview:phone_text];
        
        if(i<2)
        {
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,36*(i+1),290,1);
        [textBg2 addSubview:lineImg];
        }
        
    }
    
    
    UIImageView *textBg3 = [[UIImageView alloc] init];
    textBg3.userInteractionEnabled = YES;
    textBg3.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets3 = UIEdgeInsetsMake(10,10,10,10);
    textBg3.image = [textBg3.image resizableImageWithCapInsets:insets3];
    textBg3.frame = CGRectMake(15,textBg2.frame.origin.y+textBg2.frame.size.height+20,294,91);


    _textView = [[UITextView alloc] initWithFrame:CGRectMake(0,0,290,91)];
    _textView.delegate = self;
    _textView.backgroundColor = [UIColor clearColor];
    [textBg3 addSubview:_textView];

    UIView *placeholderView = [[UIView alloc] initWithFrame:CGRectMake(0,8,320,40)];
    placeholderView.userInteractionEnabled = YES;
    [_textView addSubview:placeholderView];
    [_scrollView addSubview:textBg3];
    
    
    UIImageView *tagImg3 = [[UIImageView alloc] initWithFrame:CGRectMake(10,0,15,16)];
    tagImg3.image = [UIImage imageNamed:@"custom_pen.png"];
    [placeholderView addSubview:tagImg3];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,0,250,20)];
    tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.text= @"填写备注";
    tempLabel.textColor = [AppSession colorWithHexString:@"999999"];
    [placeholderView addSubview:tempLabel];

    UIButton *add_customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    add_customBtn.frame = CGRectMake(10,textBg3.frame.size.height+textBg3.frame.origin.y+15,300,40);
    [add_customBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [add_customBtn setTitle:@"提  交" forState:UIControlStateNormal];
    add_customBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
    [add_customBtn addTarget:self action:@selector(subCustomInfo) forControlEvents:UIControlEventTouchUpInside];
    [add_customBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [_scrollView addSubview:add_customBtn];


}

-(void)subCustomInfo
{
    
    UITextField *phone_text1 = (UITextField *)[self.view viewWithTag:222];
    UITextField *phone_text2 = (UITextField *)[self.view viewWithTag:223];
    UITextField *phone_text3 = (UITextField *)[self.view viewWithTag:224];
    
   NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:10];
   [param setValue:theApp.brokerKid forKey:@"BrokerKid"];
    [param setValue:sex_str forKey:@"F_Sex"];
    [param setValue:name_text.text forKey:@"F_Title"];
    [param setValue:phone_text1.text forKey:@"F_Phone"];
    [param setValue:phone_text2.text forKey:@"F_Phone2"];
    [param setValue:phone_text3.text forKey:@"F_Phone3"];
    [param setValue:_textView.text forKey:@"F_Remark"];
   [param setValue:faceImg forKey:@"F_PicUrl"];
   // NSLog(@"%@",param);
   [self startSynchronousPostRequest:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.addcustomer" postData:param target:self.view alertText:@"客户信息提交中..." didFinishRequest:^(ASIHTTPRequest *request)
  {
    // NSLog(@"%@",request.responseString);
      [self showMBAlertView:self.view message:@"信息提交成功" duration:1.5];

      
 } didFailedRequest:^(ASIHTTPRequest *request)
 {
     [self baseDidFailedRequest:request];
     
 }];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [self resignKeyBoardInView:self.view];
    }
}

- (void)textViewDidBeginEditing:(UITextView *)textView;
{
    if(IPHONE_5)
        [_scrollView setContentOffset:CGPointMake(0,200) animated:YES];
    else
        [_scrollView setContentOffset:CGPointMake(0,220) animated:YES];
    
}

#pragma mark 回调
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self takephoto];
            break;
            
        case 1:
            [self takealbum];
            break;
    }
}
//从相机获取，拍照
-(void)takephoto{
    //打开摄像头
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        NSArray *temp_MediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        picker.mediaTypes = temp_MediaTypes;
        picker.delegate = self;
        [picker setAllowsEditing:YES];
    }
    
    [self presentViewController:picker animated:YES completion:^{
    }];
}
//从相册获取
-(void)takealbum{
    //打开相册
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        NSArray *temp_MediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        picker.mediaTypes = temp_MediaTypes;
        picker.delegate = self;
        [picker setAllowsEditing:YES];
    }
    
    [self presentViewController:picker animated:YES completion:^{
    }];
}
//回调，设置头像
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
       [picker dismissViewControllerAnimated:YES completion:^{
    }];
    //UIImage *faceImage=[info objectForKey:@"UIImagePickerControllerOriginalImage"];
    UIImage *editImage=[info objectForKey:@"UIImagePickerControllerEditedImage"];
    //editImage = [AppSession scaleImage:editImage toScale:0.5];
    [faceBtn setImage:editImage forState:UIControlStateNormal];
    [AppSession scaleImage:editImage];
    faceImg = editImage;
    
}

-(void)pickImg
{
    [self resignKeyBoardInView:self.view];
    
    UIActionSheet *pick = [[UIActionSheet alloc ]initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册获取", nil];
    
    [pick showInView:self.view];
}

- (void)resignKeyBoardInView:(UIView *)view
{
    for (UIView *v in view.subviews) {
        if ([v.subviews count] > 0) {
            [self resignKeyBoardInView:v];
        }
        
        if ([v isKindOfClass:[UITextView class]] || [v isKindOfClass:[UITextField class]]) {
            [v resignFirstResponder];
        }
    }
}

- (void)didSelectedRadioButton:(QRadioButton *)radio groupId:(NSString *)groupId;
{
    sex_str = radio.titleLabel.text;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
