//
//  tagLableView.m
//  test_1
//
//  Created by liu on 14-10-10.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import "tagLableView.h"

@implementation tagLableView

#define maxMake 400
#define BORDER  45/2

-(float)tagLableHight:(NSArray *)tagArray
{
    return [self tagLabel:tagArray withView:nil];
}
//
// 生成标签
-(float)tagLabel:(NSArray *)tagArray withView:(UIView *)view
{
    float GAP_X   = 13.0f;
    float GAP_Y   = 26.0f;
    float addup_x = BORDER + 10;
    float addup_Y = 8.0f;
    for (int n = 0; n < tagArray.count; n ++) {
        
        UILabel *tagLabel = [[UILabel alloc]init];
        tagLabel.tag = 100 + n;
        if (n == tagArray.count) {
            tagLabel = [self addTagLabl:tagLabel];
        } else {
            tagLabel.text = tagArray[n];
            tagLabel = [self labelStyle:tagLabel];
        }
        if (tagLabel.text.length > 1) {
            CGSize sizeX;
            if (n != 0) {
                sizeX = [tagArray[n - 1] sizeWithFont:tagLabel.font constrainedToSize:
                         CGSizeMake(maxMake, 2000)];
            }
            float priorX = sizeX.width;
            float addupMax = addup_x + priorX + tagLabel.frame.size.width + GAP_X;
            if (addupMax > (SCREEN_WIDTH - ( BORDER ) * 2)) {
                addup_Y = addup_Y + GAP_Y;
                addup_x = BORDER + 10;
            } else {
                if (n == 0) {
                    addup_x = BORDER + 10;
                } else {
                    addup_x = addup_x + priorX + GAP_X;
                }
            }
            //NSLog(@"n=%i,%f,%f,%@",n,addup_x,addup_Y,tagLabel.text);
            tagLabel.frame = CGRectMake(addup_x, addup_Y, tagLabel.frame.size.width, tagLabel.frame.size.height);
            [view addSubview:tagLabel];
        }
    }
    return addup_Y + 20;
}

-(UILabel *)labelStyle:(UILabel *)label
{
    label.font = [UIFont systemFontOfSize:14];
    label.textColor = [AppSession colorWithHexString:@"fd710a"];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment =  NSTextAlignmentCenter;
    [[label layer] setBorderWidth:0.5];
    [[label layer] setBorderColor:label.textColor.CGColor];
    CGSize size = [tagLableView lableSize:label];
    label.frame = CGRectMake(label.frame.origin.x,label.frame.origin.y, size.width + 6, 20);
    return label;
}

-(UILabel *)addTagLabl:(UILabel *)label
{
    label = [self labelStyle:label];
    label.font = [UIFont boldSystemFontOfSize:14];
    label.textColor = [UIColor grayColor];
    label.userInteractionEnabled = YES;
    [label addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BtnClick)]];
    return label;
}

-(void)BtnClick
{
    
}

+(CGSize)lableSize:(UILabel *)label
{
    return [label.text sizeWithFont:label.font constrainedToSize:
            CGSizeMake(maxMake, 2000)];
}
@end
