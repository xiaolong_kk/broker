//
//  loadingVC.m
//  往来
//
//  Created by zen huang on 14/8/7.
//
//

#import "loadingVC.h"
#import "SMPageControl.h"
#import "registerVC.h"
#import "loginVC.h"

@interface loadingVC ()

@end

@implementation loadingVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,-20, 320, self.view.bounds.size.height)];
    [scrollView setContentSize:CGSizeMake(320*4,self.view.frame.size.height-50)];
    [scrollView setPagingEnabled:YES];
    scrollView.bounces = NO;
    scrollView.tag = 10001;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    

    for(int i=0;i<3;i++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320*i, 0, 320,self.view.bounds.size.height)];
        
        NSString *imgName;
        if(IPHONE_5)
        imgName = [NSString stringWithFormat:@"loading%d.png",i+1];
        else
        imgName = [NSString stringWithFormat:@"loading%d_i4.png",i+1];

        
        imageView.image = [UIImage imageNamed:imgName];
        
        [scrollView addSubview:imageView];
    }
    scrollView.delegate = self;
    
    [self.view addSubview:scrollView];
    
    SMPageControl *tempPageControl =[[SMPageControl alloc]initWithFrame:CGRectMake(100,self.view.frame.size.height-120,120,36)];
    tempPageControl.tag=10011;
    tempPageControl.backgroundColor = [UIColor clearColor];
    tempPageControl.numberOfPages =3;
    [tempPageControl setCurrentPage:0];
    [tempPageControl setPageIndicatorImage:[UIImage imageNamed:@"dian.png"]];
    [tempPageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"dian_hl.png"]];
    [self.view addSubview:tempPageControl];
    
    UIButton *registerBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [registerBtn setImage:[UIImage imageNamed:@"register.png"] forState:UIControlStateNormal];
    registerBtn.frame=CGRectMake(20,self.view.frame.size.height-70,125,40);
    [registerBtn addTarget:self action:@selector(registerView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBtn];
    
    UIButton *loginBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    [loginBtn setImage:[UIImage imageNamed:@"login.png"] forState:UIControlStateNormal];
    loginBtn.frame=CGRectMake(175,self.view.frame.size.height-70,125,40);
    [loginBtn addTarget:self action:@selector(loginView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:loginBtn];
    
}

-(void)loginView
{
    loginVC *vc = [[loginVC alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)registerView
{
    registerVC *vc = [[registerVC alloc] init];
    vc.type_str = @"register";
    vc.title = @"注 册";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;

}

#pragma mark --UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([scrollView isKindOfClass:[UITableView class]]) {
        return;
    }
    
    SMPageControl *tempPageControl = (SMPageControl *)[self.view viewWithTag:10011];
    int index = fabs(scrollView.contentOffset.x) / scrollView.frame.size.width;
    tempPageControl.currentPage = index;

}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int currentPostion = scrollView.contentOffset.x;
    
    NSLog(@"%d",currentPostion);
    if (currentPostion> 640)
    {
        [self loginView];
      
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
