//
//  simpleCustomPushBuildingVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/25.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "simpleCustomPushBuildingVC.h"
#import "buildingDetailVC.h"
@implementation simpleCustomPushBuildingVC


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationLeftTitleBtn.hidden = YES;
    self.navigationLabel.text = @"推荐楼盘";
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;
    
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,110)];
    headView.backgroundColor= [UIColor whiteColor];
    
    UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,15,36,36)];
    logoImg.tag = 101;
    [headView addSubview:logoImg];
    
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,14,240,20)];
    nameLabel.tag = 102;
    nameLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textColor = [AppSession colorWithHexString:@"000000"];
    [headView addSubview:nameLabel];
    
    UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(130,14,30,20)];
    sexLabel.tag=103;
    sexLabel.textAlignment=NSTextAlignmentCenter;
    sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
    sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
    sexLabel.backgroundColor=[UIColor clearColor];
    [headView addSubview:sexLabel];
    
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(165,14,150,20)];
    phoneLabel.tag = 104;
    phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    phoneLabel.textAlignment = NSTextAlignmentLeft;
    phoneLabel.backgroundColor = [UIColor clearColor];
    phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
    [headView addSubview:phoneLabel];

    UILabel *remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,35,275,20)];
    remarkLabel.tag = 105;
    remarkLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    remarkLabel.textAlignment = NSTextAlignmentLeft;
    remarkLabel.backgroundColor = [UIColor clearColor];
    remarkLabel.textColor = [AppSession colorWithHexString:@"666666"];
    [headView addSubview:remarkLabel];

    UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg.frame = CGRectMake(0,75,320,1);
    [headView addSubview:lineImg];
    
    UILabel *budingCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(15,81,200,20)];
    budingCountLabel.tag = 106;
    budingCountLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
    budingCountLabel.textAlignment = NSTextAlignmentLeft;
    budingCountLabel.backgroundColor = [UIColor clearColor];
    budingCountLabel.textColor = [AppSession colorWithHexString:@"000000"];
    [headView addSubview:budingCountLabel];

    UIImageView *lineImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg2.frame = CGRectMake(0,110,320,1);
    [headView addSubview:lineImg2];

    
    htTableView = [[HVTableView alloc] initWithFrame:CGRectMake(0,addHight+44,320,self.view.bounds.size.height-65) expandOnlyOneCell:YES enableAutoScroll:NO];
    htTableView.HVTableViewDelegate = self;
    htTableView.HVTableViewDataSource = self;
    htTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    htTableView.tableHeaderView = headView;
    [htTableView reloadData];
    [self.view addSubview:htTableView];

    [self getData];
    
}

-(void)getData
{
    
    UIImageView *logoImg = (UIImageView *)[self.view viewWithTag:101];
    UILabel *nameLabel = (UILabel *)[self.view viewWithTag:102];
    UILabel *sexLabel=(UILabel *)[self.view viewWithTag:103];
    UILabel *phoneLabel = (UILabel *)[self.view viewWithTag:104];
    UILabel *remarkLabel = (UILabel *)[self.view viewWithTag:105];
    UILabel *budingCountLabel = (UILabel *)[self.view viewWithTag:106];

    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.customerpushofbuilderlist&BrokerKid=%@&CustomerID=%@",theApp.brokerKid,self.CustomerID_str];
    
   // NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         NSDictionary *dict2;

         if([[dict objectForKey:@"code"] intValue] ==0)
         {
            dict2 = [dict objectForKey:@"data"];
         }
         nameLabel.text = [dict2 objectForKey:@"F_Title"];
         sexLabel.text = [dict2 objectForKey:@"F_Sex"];
         phoneLabel.text = [dict2 objectForKey:@"F_Phone"];
         remarkLabel.text = [dict2 objectForKey:@"F_Remark"];
         budingCountLabel.text =[NSString stringWithFormat:@"选择楼盘(%@)",[dict2 objectForKey:@"F_B_Count"]];
         
         NSString *sex_type;
         if([sexLabel.text isEqualToString:@"女士"])
             sex_type = @"user_0.jpg";
         else
             sex_type = @"user_1.jpg";
             
         [logoImg setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[dict2 objectForKey:@"F_Pic"]] placeholderImage:[UIImage imageNamed:sex_type]];
         
         dataSource = [NSMutableArray arrayWithArray:[dict2 objectForKey:@"list_itme"]];
         [htTableView reloadData];

     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                     
    }];
}


#pragma mark - dataSource
//perform your expand stuff (may include animation) for cell here. It will be called when the user touches a cell
-(void)tableView:(UITableView *)tableView expandCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    // 展开
    NSDictionary *dic = dataSource[indexPath.row];
    NSArray *logList = [dic objectForKey:@"LogList"];
    NSString *strLogList = [NSString stringWithFormat:@"%@",logList];
    if (![strLogList isEqualToString:@"<null>"]) {
        [self addLabel:cell logList:logList];
        [UIView animateWithDuration:0.3 animations:^{
            UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
            imageView.frame = CGRectMake(imageView.frame.origin.x, 130 + 20 * logList.count ,imageView.frame.size.width, imageView.frame.size.height);
        }];
    }
}

-(void)addLabel:(UITableViewCell *)cell logList:(NSArray *)logList
{
    for (int n = 0; n < logList.count; n ++) {
        [[cell viewWithTag: n + 1001] removeFromSuperview];
        [[cell viewWithTag: n + 2002] removeFromSuperview];
        [[cell viewWithTag: n + 3003] removeFromSuperview];
        [[cell viewWithTag: n + 5005] removeFromSuperview];
        [[cell viewWithTag: n + 4004] removeFromSuperview];
        
        
        //        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10, 100 + 20 * n, 200, 16)];
        //        lable.tag = 100 + n;
        //        NSDictionary *dict = logList[n];
        //        lable.text = [NSString stringWithFormat:@"%@   %@",[dict objectForKey:@"F_BuildingKid"],[dict objectForKey:@"F_Remark"]];
        //        [cell addSubview:lable];
        
        float add_H=130;
        UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,add_H+20*n,12,12)];
        tagImg.tag = 1001+n;
        tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
        [cell addSubview:tagImg];
        
        UIImageView *process_lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3+add_H+20*n,7,20)];
        process_lineImg.tag=4004+n;
        process_lineImg.image = [UIImage imageNamed:@"process_line.png"];
        [cell addSubview:process_lineImg];
        
        UIImageView *process_pointImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3+add_H+20*n+65,7,7)];
        process_pointImg.image = [UIImage imageNamed:@"process_point.png"];
        process_pointImg.tag = 5005+n;
        [cell addSubview:process_pointImg];
        
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(45,-15+20*n+add_H,150,50)];
        tagLabel.tag = 2002+n;
        tagLabel.numberOfLines = 0;
        tagLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        tagLabel.textAlignment = NSTextAlignmentLeft;
        tagLabel.backgroundColor = [UIColor clearColor];
        tagLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [cell addSubview:tagLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(320-115,-3+20*n+add_H,100,20)];
        timeLabel.tag = 3003+n;
        timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        timeLabel.textAlignment = NSTextAlignmentRight;
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [cell addSubview:timeLabel];
        
        NSDictionary *dict = logList[n];
        tagLabel.text = [dict objectForKey:@"F_Remark"];
        timeLabel.text = [dict objectForKey:@"F_AddTime"];
        timeLabel.text = [timeLabel.text stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        timeLabel.text = [timeLabel.text substringToIndex:16];
        
        
    }
}

//perform your collapse stuff (may include animation) for cell here. It will be called when the user touches an expanded cell so it gets collapsed or the table is in the expandOnlyOneCell satate and the user touches another item, So the last expanded item has to collapse
-(void)tableView:(UITableView *)tableView collapseCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    //关闭
    [UIView animateWithDuration:0.3 animations:^{
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
        imageView.frame = CGRectMake(imageView.frame.origin.x,130 ,imageView.frame.size.width, imageView.frame.size.height);
        imageView.image = [UIImage imageNamed:@"line_show.png"];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    //you can define different heights for each cell. (then you probably have to calculate the height or e.g. read pre-calculated heights from an array
    if (isexpanded){
        NSDictionary *dic = dataSource[indexPath.row];
        NSMutableArray *logList = [dic objectForKey:@"LogList"];
        NSString *strLogList = [NSString stringWithFormat:@"%@",logList];
        if (![strLogList isEqualToString:@"<null>"]) {
            return 130+20*logList.count;
        }
    }
    return 140;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"cell%d",indexPath.row];//以indexPath来唯一确定cell
    pushBuildingCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[pushBuildingCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        cell.delegate = self;
        
     }
    
    NSDictionary *dict = dataSource[indexPath.row];
    cell.cell_index = indexPath.row;
    [cell setData:dict];
    if (dataSource.count > 0)
    {
        NSString *strLogList = [NSString stringWithFormat:@"%@",[dict objectForKey:@"LogList"]];
        
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
        
        NSArray *logList = [dict objectForKey:@"LogList"];
        
        
        if (![strLogList isEqualToString:@"<null>"])
        {
            
            for (int n = 0; n < logList.count; n ++) {
                [[cell viewWithTag: n + 1001] removeFromSuperview];
                [[cell viewWithTag: n + 2002] removeFromSuperview];
                [[cell viewWithTag: n + 3003] removeFromSuperview];
                [[cell viewWithTag: n + 5005] removeFromSuperview];
                [[cell viewWithTag: n + 4004] removeFromSuperview];
            }

            
            imageView.frame = CGRectMake( [cell.contentView viewWithTag:7].frame.origin.x,
                                         isExpanded ? (130 + 20*logList.count):130,
                                         [cell.contentView viewWithTag:7].frame.size.width,
                                         [cell.contentView viewWithTag:7].frame.size.height);
            imageView.image = [UIImage imageNamed:@"line_hide.png"];
            if (isExpanded)
            {
                [self addLabel:cell logList:logList];
            }
        }
        else
        {
            imageView.frame = CGRectMake( [cell.contentView viewWithTag:7].frame.origin.x,
                                         isExpanded ? (130 + 20*logList.count):130,
                                         [cell.contentView viewWithTag:7].frame.size.width,
                                         1);
            imageView.image = [UIImage imageNamed:@"mainview_line_long.png"];

        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)buttonClicked:(int)cell_index
{
    NSDictionary *dict = dataSource[cell_index];
    buildingDetailVC *vc = [[buildingDetailVC alloc] initWithNavigationBar:YES];
    vc.buildingKid_str = [dict objectForKey:@"Kid"];

    [self.navigationController pushViewController:vc animated:YES];

}


@end

