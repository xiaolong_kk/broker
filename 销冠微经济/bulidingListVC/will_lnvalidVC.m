//
//  will_lnvalidVC.m
//  销冠微经济
//
//  Created by zen huang on 14/10/10.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "will_lnvalidVC.h"
#import "QRCodeGenerator.h"

@implementation will_lnvalidVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createTableView];
}


- (void)createTableView
{
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHight1=44;
    self.navigationLabel.text = @"即将失效";
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,44+addHight,320,self.view.bounds.size.height-44-addHight) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [self getData];
    
}

-(void)getData
{
    
    
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.wxcustomerlistvalid&ctype=will&PageIndex=1&BrokerKid=%@",theApp.brokerKid];
    
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
        if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSArray *arr = [[dict objectForKey:@"data"] objectForKey:@"returnLsit"];
             
            dataSource = [NSMutableArray arrayWithArray:arr];
             [_tableView reloadData];
             
         }
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
    
}


-(void)encodeBtn:(UIButton *)btn
{
    
    
    sortView=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, theApp.window.bounds.size.height)];
    shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    [sortView addSubview:shadowView];
    sortView.hidden = YES;
    [[[UIApplication sharedApplication] keyWindow] addSubview:sortView];
    
    NSInteger i  = btn.tag-2222;
    NSDictionary *dict = dataSource[i];
    
    
    NSDictionary *qr_dict = [dict objectForKey:@"QRCode"];
    
    NSMutableString *jsonString = [[NSMutableString alloc] init];
    
    NSString *str1 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"F_BuildingKid"]];
    NSString *str2 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"F_WeiXinBrokerKid"]];
    NSString *str3 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"Kid"]];
    NSString *str4 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"Type"]];
    
    //        timeStr  = [timeStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
    //        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$€^•'@#$%^&*()_+'\""];
    //
    //        NSString *trimmedString = [timeStr stringByTrimmingCharactersInSet:set];
    //        trimmedString  = [trimmedString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *string  = [NSString stringWithFormat:
                         @"{\"F_BuildingKid\":\"%@\",\"F_WeiXinBrokerKid\":\"%@\",\"Kid\":\"%@\",\"Type\":\"%@\"}",str1,str2,str3,str4];
    
    [jsonString appendString:string];
    
    
    //    NSUInteger location = [jsonString length]-1;
    //
    //    NSRange range       = NSMakeRange(location, 1);
    
    // 4. 将末尾逗号换成结束的]}
    // [jsonString replaceCharactersInRange:range withString:@"]"];
    
    // NSLog(@"%@",jsonString);
    CGFloat qrSize = self.view.bounds.size.width - 10 * 2;
    
    UIView *codeBg =[[UIView alloc] initWithFrame:CGRectMake(10, (self.view.bounds.size.height - qrSize) / 2,
                                                             qrSize, qrSize)];
    codeBg.backgroundColor = [UIColor whiteColor];
    [sortView addSubview:codeBg];
    UIImage* image = [QRCodeGenerator qrImageForString:jsonString imageSize:300];
    
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0,0,qrSize, qrSize);
    [codeBg addSubview:imageView];
    [imageView layer].magnificationFilter = kCAFilterNearest;
    
    UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,44,320,self.view.bounds.size.height)];
    [bgBtn addTarget:self action:@selector(ViewHidden:) forControlEvents:UIControlEventTouchUpInside];
    [sortView addSubview:bgBtn];
    
    [AppSession fadeIn:sortView];
    
    
}

-(void)ViewHidden:(id)sender
{
    [AppSession fadeOut:[sender superview]];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
        // headView.backgroundColor= [UIColor whiteColor];
        [cell addSubview:headView];
        
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
        logoImg.tag = 101;
        //            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
        [headView addSubview:logoImg];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,240,20)];
        titleLabel.tag = 102;
        titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [headView addSubview:titleLabel];
        
        UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
        sexLabel.tag=103;
        sexLabel.textAlignment=NSTextAlignmentCenter;
        sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
        sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        sexLabel.backgroundColor=[UIColor clearColor];
        [headView addSubview:sexLabel];
        
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
        phoneLabel.tag = 104;
        phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.backgroundColor = [UIColor clearColor];
        phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
        [headView addSubview:phoneLabel];
        
        UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
        budildingLabel.tag = 105;
        budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
        budildingLabel.textAlignment = NSTextAlignmentLeft;
        budildingLabel.backgroundColor = [UIColor clearColor];
        budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [headView addSubview:budildingLabel];
        
        
        //            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(230,15,12,12)];
        //            tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
        //            [headView addSubview:tagImg];
        //
        //
        //            UILabel *tageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250,11,150,20)];
        //            tageLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        //            tageLabel.textAlignment = NSTextAlignmentLeft;
        //            tageLabel.text = @"无效客户";
        //            tageLabel.backgroundColor = [UIColor clearColor];
        //            tageLabel.textColor = [AppSession colorWithHexString:@"666666"];
        //            [headView addSubview:tageLabel];
        //
        
        UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(240,11,67,20)];
        clickBtn.tag = indexPath.row+2222;
        [clickBtn setBackgroundImage:[UIImage imageNamed:@"btn_code.png"] forState:UIControlStateNormal];
        [clickBtn addTarget:self action:@selector(encodeBtn:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:clickBtn];
        
        
        NSArray *arr = @[@"推荐有效",@"已到访",@"已认筹",@"已预定",@"已成交"];
        for(int i=0;i<5;i++)
        {
            UIView *lineBg = [[UIView alloc] initWithFrame:CGRectMake(10,60,300,60)];
            [headView addSubview:lineBg];
            
            UIImageView *pointlineImg = [[UIImageView alloc] initWithFrame:CGRectMake(i*60,0,60,10)];
            pointlineImg.tag = 222+i;
            pointlineImg.image = [UIImage imageNamed:@"pointline_gray.png"];
            [lineBg addSubview:pointlineImg];
            
            UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,15,60,20)];
            stateLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            stateLabel.textAlignment = NSTextAlignmentCenter;
            stateLabel.text =arr[i];
            stateLabel.tag = 333+i;
            stateLabel.backgroundColor = [UIColor clearColor];
            stateLabel.textColor = [AppSession colorWithHexString:@"000000"];
            [lineBg addSubview:stateLabel];
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,35,60,20)];
            timeLabel.tag = 555+i;
            timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            timeLabel.textAlignment = NSTextAlignmentCenter;
            timeLabel.backgroundColor = [UIColor clearColor];
            timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [lineBg addSubview:timeLabel];
            
        }
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,119,320,1);
        [cell addSubview:lineImg];
        
        
    }
    
    NSDictionary *dict = dataSource[indexPath.row];
    UIImageView *logoImg = (UIImageView *)[cell viewWithTag:101];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
    UILabel *phoneLabel = (UILabel *)[cell viewWithTag:104];
    UILabel *budildingLabel = (UILabel *)[cell viewWithTag:105];
    UILabel *stateLabel = (UILabel *)[cell viewWithTag:334];
    
    UILabel *timeLabel1 = (UILabel *)[cell viewWithTag:555];
    UILabel *timeLabel2 = (UILabel *)[cell viewWithTag:556];
    UILabel *timeLabel3 = (UILabel *)[cell viewWithTag:557];
    UILabel *timeLabel4 = (UILabel *)[cell viewWithTag:558];
    UILabel *timeLabel5 = (UILabel *)[cell viewWithTag:559];
    
    UIImageView *pointlineImg1 = (UIImageView *)[cell viewWithTag:222];
    UIImageView *pointlineImg2 = (UIImageView *)[cell viewWithTag:223];
    UIImageView *pointlineImg3 = (UIImageView *)[cell viewWithTag:224];
    UIImageView *pointlineImg4 = (UIImageView *)[cell viewWithTag:225];
    UIImageView *pointlineImg5 = (UIImageView *)[cell viewWithTag:226];
    
    NSDictionary *dict_valid = [dict objectForKey:@"Valid"];
    NSDictionary *dict_come = [dict objectForKey:@"Come"];
    NSDictionary *dict_ticket = [dict objectForKey:@"Ticket"];
    NSDictionary *dict_preordain = [dict objectForKey:@"Preordain"];
    NSDictionary *dict_business = [dict objectForKey:@"Business"];
    
    if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
        logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
    else
        logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
    
    
    if([[dict_valid objectForKey:@"values"] intValue]==1)
    {
        pointlineImg1.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel1.text = [dict_valid objectForKey:@"Times"];
    }
    if([[dict_come objectForKey:@"values"] intValue]==1)
    {
        pointlineImg2.image = [UIImage imageNamed:@"pointline_orange.png"];
        stateLabel.text = [dict_come objectForKey:@"Names"];
        timeLabel2.text = [dict_come objectForKey:@"Times"];
        
    }
    if([[dict_ticket objectForKey:@"values"] intValue]==1)
    {
        pointlineImg3.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel3.text = [dict_ticket objectForKey:@"Times"];
    }
    if([[dict_preordain objectForKey:@"values"] intValue]==1)
    {
        pointlineImg4.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel4.text = [dict_preordain objectForKey:@"Times"];
    }
    if([[dict_business objectForKey:@"values"] intValue]==1)
    {
        pointlineImg5.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel5.text = [dict_business objectForKey:@"Times"];
    }
    
    titleLabel.text = [dict objectForKey:@"F_Title"];
    sexLabel.text = [dict objectForKey:@"F_Sex"];
    phoneLabel.text = [dict objectForKey:@"F_Phone"];
    budildingLabel.text = [dict objectForKey:@"BuildingName"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)buttonClicked:(UIButton *)btn
{
    
    int i = btn.tag-5555;
    
    NSDictionary *dict = dataSource[i];
    
    // NSLog(@"%@",dict);
}

@end
