//
//  applyListVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/24.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCProgressBarView.h"

@interface applyListVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    UITableView *_tableView;
    UITableView *resultTableView;
    //UIButton *btn;
    UIView *topView;
   NSMutableArray *dataSource;
    UISearchBar *mySearchBar;

}

@property (nonatomic, retain) UISearchBar*mSearchBar;
@property (nonatomic, retain) NSMutableArray *resultDataArr;
//@property (nonatomic, retain) NSMutableArray *dataSource;
@property (nonatomic, retain) NSString *state_str;
@property(nonatomic,retain)NSString *title_str;
@property(nonatomic,strong) NSString *type_str;


@end
