//
//  pushBuildingCell.m
//  销冠微经济
//
//  Created by zen huang on 14/9/25.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "pushBuildingCell.h"

@implementation pushBuildingCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *buildingImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 81, 61)];
        buildingImg.tag = 101;
        buildingImg.backgroundColor = [UIColor grayColor];
        [self addSubview:buildingImg];
        
        UILabel *bulidingLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, 8,150,30)];
        bulidingLabel.tag=102;
        bulidingLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:16];
        bulidingLabel.backgroundColor = [UIColor clearColor];
        bulidingLabel.textColor = [UIColor blackColor];
        // bulidingLabel.text = @"卡考一号";
        [self addSubview:bulidingLabel];
        
        
        UIButton *nextBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        nextBtn.frame = CGRectMake(0,0,320,120);
        [nextBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:nextBtn];

        UIButton *pushBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        pushBtn.tag = 202;
        pushBtn.frame = CGRectMake(250,15,60,26);
        [pushBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
        //[pushBtn setTitle:@"推荐客户" forState:UIControlStateNormal];
        pushBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:12];
        [pushBtn addTarget:self action:@selector(pushCustom) forControlEvents:UIControlEventTouchUpInside];
        [pushBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
        [self addSubview:pushBtn];

        
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(105,42,80,20)];
        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:12];
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.textColor = [AppSession colorWithHexString:@"666666"];
        tempLabel.text = @"佣金";
        [self addSubview:tempLabel];
        
        
        UILabel *brokerageLabel = [[UILabel alloc] initWithFrame:CGRectMake(135,35,80,30)];
        brokerageLabel.tag = 103;
        brokerageLabel.font=[UIFont fontWithName:stringFontOfSize size:23];
        brokerageLabel.backgroundColor = [UIColor clearColor];
        brokerageLabel.textColor = [AppSession colorWithHexString:@"f00001"];
        //  brokerageLabel.text = @"0.03%";
        [self addSubview:brokerageLabel];
        
        UILabel *tempLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(213,42,80,20)];
        tempLabel2.font=[UIFont fontWithName:stringFontOfSize size:12];
        tempLabel2.backgroundColor = [UIColor clearColor];
        tempLabel2.textColor = [AppSession colorWithHexString:@"666666"];
        tempLabel2.text = @"客户界定";
        [self addSubview:tempLabel2];
        
        UILabel *getDefinedLabel = [[UILabel alloc] initWithFrame:CGRectMake(270,42,80,20)];
        getDefinedLabel.tag = 104;
        getDefinedLabel.font=[UIFont fontWithName:stringFontOfSize size:12];
        getDefinedLabel.backgroundColor = [UIColor clearColor];
        getDefinedLabel.textColor = [AppSession colorWithHexString:@"f00001"];
        //getDefinedLabel.text = @">5分钟";
        [self addSubview:getDefinedLabel];
        
        UILabel *CommissionEvaluateLabel = [[UILabel alloc] initWithFrame:CGRectMake(105,57,120,30)];
        CommissionEvaluateLabel.tag = 105;
        CommissionEvaluateLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
        CommissionEvaluateLabel.backgroundColor = [UIColor clearColor];
        CommissionEvaluateLabel.textColor = [AppSession colorWithHexString:@"666666"];
        CommissionEvaluateLabel.text = @"高于同楼盘";
        [self addSubview:CommissionEvaluateLabel];
        
        UIImageView *CommissionEvaluateImg = [[UIImageView alloc] initWithFrame:CGRectMake(170,68,13,8)];
        CommissionEvaluateImg.image = [UIImage imageNamed:@"icon_b12.png"];
        [self addSubview:CommissionEvaluateImg];
        
        
        UILabel *tempLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(213,57,80,30)];
        tempLabel3.font=[UIFont fontWithName:stringFontOfSize size:10];
        tempLabel3.backgroundColor = [UIColor clearColor];
        tempLabel3.textColor = [AppSession colorWithHexString:@"666666"];
        tempLabel3.text = @"结佣时间";
        [self addSubview:tempLabel3];
        
        UILabel *CommissionSpeedLabel = [[UILabel alloc] initWithFrame:CGRectMake(263,57,80,30)];
        CommissionSpeedLabel.tag = 106;
        CommissionSpeedLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
        CommissionSpeedLabel.backgroundColor = [UIColor clearColor];
        CommissionSpeedLabel.textColor = [AppSession colorWithHexString:@"e69609"];
        //CommissionSpeedLabel.text = @">5分钟";
        [self addSubview:CommissionSpeedLabel];
        
        NSArray *arr = @[@"带看",@"带看奖",@"认筹奖",@"超额奖"];
        for(int i=0;i<4;i++)
        {
            UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(10+(65*i),87,50,20)];
            tempLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
            tempLabel.backgroundColor = [UIColor clearColor];
            tempLabel.textAlignment = NSTextAlignmentCenter;
            tempLabel.textColor = [AppSession colorWithHexString:@"666666"];
            tempLabel.text = arr[i];
            [self addSubview:tempLabel];
            
            
            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,5,8,8)];
            tagImg.tag = 200+i;
            tagImg.image = [UIImage imageNamed:@"icon_b2on.png"];
            [tempLabel addSubview:tagImg];
            
        }
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,100,200,30)];
        timeLabel.tag = 107;
        timeLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [AppSession colorWithHexString:@"999999"];
        [self addSubview:timeLabel];
        
        
        UIImageView *exImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,130,320,9)];
        exImg.tag = 7;
        exImg.image = [UIImage imageNamed:@"line_show.png"];
        [self.contentView addSubview:exImg];
        
        
    }
    return self;
}

-(void)setData:(NSDictionary *)dict;
{
    UIImageView *buildingImg = (UIImageView *)[self viewWithTag:101];
    UILabel *bulidingLabel = (UILabel *)[self viewWithTag:102];
    UILabel *brokerageLabel = (UILabel *)[self viewWithTag:103];
    UILabel *getDefinedLabel = (UILabel *)[self viewWithTag:104];
    UILabel *CommissionEvaluateLabel = (UILabel *)[self viewWithTag:105];
    UILabel *CommissionSpeedLabel = (UILabel *)[self viewWithTag:106];
    UILabel *timeLabel = (UILabel *)[self viewWithTag:107];
    UIImageView *exImg = (UIImageView *)[self viewWithTag:108];
    UIButton *pushBtn = (UIButton *)[self viewWithTag:202];
    
   // NSLog(@"%@",dict);
    if(self.cell_index==self.cell_select)
    {
    if([self.hide_str isEqualToString:@"YES"])
        exImg.hidden = YES;
    else
        exImg.hidden = NO;
    }
    
    [buildingImg setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[dict objectForKey:@"F_Logo"]] placeholderImage:nil];
    bulidingLabel.text = [dict objectForKey:@"F_Title"];
    brokerageLabel.text = [dict objectForKey:@"Commission"];
    getDefinedLabel.text = [dict objectForKey:@"GetDefined"];
    
    CommissionEvaluateLabel.text = [dict objectForKey:@"F_WeiXinCommissionEvaluate"];
    
    NSString *startTime = [dict objectForKey:@"StartTime"];
    startTime = [startTime substringToIndex:10];
    
    NSString *endTime = [dict objectForKey:@"EndTime"];
    endTime = [endTime substringToIndex:10];
    
    CommissionSpeedLabel.text = [dict objectForKey:@"F_WeiXinCommissionSpeed"];
    timeLabel.text = [NSString stringWithFormat:@"有效期: %@ 至 %@",startTime,endTime];
    
    NSDictionary *pushDict = [dict objectForKey:@"PushBtnText"];
    
   // NSLog(@"%@",dict);
    
    if([[pushDict objectForKey:@"code"] intValue]==2)
    {
        [self timerFireMethod:[pushDict objectForKey:@"timeMin"]];
    }

    [pushBtn setTitle:[pushDict objectForKey:@"Text"] forState:UIControlStateNormal];
    
}

-(void)pushCustom
{
   // [self showAlertView];

    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.customerpushend&CustomerIDPush=%@&BrokerKid=%@&PushType=1&BuildingKid=9",theApp.brokerKid,self.cuostomid_str];
    
  //  NSLog(@"%@",str);
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:str]];
    request.timeOutSeconds=TIME_OUT_SECOND;
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSuccess:)];
    //[request setDidFailSelector:@selector(requestFail:)];
    [request startAsynchronous];

}

-(void)requestSuccess:(ASIFormDataRequest *)request
{
    
    UIButton *pushBtn = (UIButton *)[self viewWithTag:202];

    NSDictionary *dict = [request.responseString objectFromJSONString];
    
    //NSLog(@"%@",[dict objectForKey:@"msg"]);

    NSDictionary *dict2;
    if([[dict objectForKey:@"code"] intValue] ==0)
    {
        dict2 = [dict objectForKey:@"data"];
    }
    
    //NSLog(@"%@",dict2);
    NSDictionary *pushDict = [dict2 objectForKey:@"pushMsg"];
    NSLog(@"%@",pushDict);
    
    if([[pushDict objectForKey:@"code"] intValue]==2)
    {
        [self timerFireMethod:[pushDict objectForKey:@"timeMin"]];
    }
    if([[pushDict objectForKey:@"code"] intValue]==-15||[[pushDict objectForKey:@"code"] intValue]==-20)
    {
        [self showAlertView];
    }
    
}



-(void)pushCustomAgain:(UIButton *)btn
{
    
    NSLog(@"%d",btn.tag);
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.customerpushend&CustomerIDPush=%@&BrokerKid=%@&PushType=1&BuildingKid=9&strIsTimer=1&strIsCancel=%ld",theApp.brokerKid,self.cuostomid_str,(long)btn
                    .tag];
    
    NSLog(@"%@",str);
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:str]];
    request.timeOutSeconds=TIME_OUT_SECOND;
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSuccess2:)];
    //[request setDidFailSelector:@selector(requestFail:)];
    [request startAsynchronous];

}

-(void)requestSuccess2:(ASIFormDataRequest *)request
{
    
    UIButton *pushBtn = (UIButton *)[self viewWithTag:202];

    NSDictionary *dict = [request.responseString objectFromJSONString];
    
    //NSLog(@"%@",[dict objectForKey:@"msg"]);
   [AppSession fadeOut:sortView];

    NSDictionary *dict2;
    if([[dict objectForKey:@"code"] intValue] ==0)
    {
        dict2 = [dict objectForKey:@"data"];
    }
    
    //NSLog(@"%@",dict2);
    NSDictionary *pushDict = [dict2 objectForKey:@"pushMsg"];
    NSLog(@"%@",pushDict);
    [pushBtn setTitle:[pushDict objectForKey:@"msg"] forState:UIControlStateNormal];

    if([[pushDict objectForKey:@"code"] intValue]==-15||[[pushDict objectForKey:@"code"] intValue]==-20)
    {
    UIAlertView *baseAlert = [[UIAlertView alloc]
                              initWithTitle: nil message:[pushDict objectForKey:@"msg"]
                              delegate: self cancelButtonTitle:@"取消"
                              otherButtonTitles:nil,nil];
    [baseAlert show];
    }
    if([[pushDict objectForKey:@"code"] intValue]==2)
    {
        [self timerFireMethod:[pushDict objectForKey:@"timeMin"]];
    }

    

}


-(void)timerFireMethod:(NSString *)timeMin
{
//    NSLog(@"%@",timeMin);
//    int h = [[timeMin substringToIndex:0] intValue];
//    int m = [[timeMin substringWithRange:NSMakeRange(1,1)] intValue];
//    int s = [[timeMin substringWithRange:NSMakeRange(3,2)] intValue];
//
//    NSLog(@"%d",h);
//    
//    NSLog(@"%d",m);
//    NSLog(@"%d",s);

  __block  int timeout =60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    UIButton *pushBtn = (UIButton *)[self viewWithTag:202];

    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=-1)
        { //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            //dispatch_release(_timer);
            //dispatch_release(queue);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [self autoDelete];
                
            });
        }else{
            int hours = timeout/60/60;
            int minutes = (timeout/60)%60;
            int seconds = timeout%60;
            
            NSString *strTime = [NSString stringWithFormat:@"%.2d:%.2d",minutes,seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                
                [pushBtn setTitle:strTime forState:UIControlStateNormal];

            });
            timeout--;
            
        }
    });
    dispatch_resume(_timer);
}
-(void)showAlertView
{

    if(sortView==nil)
    {
        sortView=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
        UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, theApp.window.bounds.size.height)];
        shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        [sortView addSubview:shadowView];
        sortView.hidden = YES;
        [[[UIApplication sharedApplication] keyWindow] addSubview:sortView];
        
        UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,320,theApp.window.bounds.size.height)];
        [bgBtn addTarget:self action:@selector(ViewHidden:) forControlEvents:UIControlEventTouchUpInside];
        [sortView addSubview:bgBtn];
        
        UIImageView *menu_btn_bgImg = [[UIImageView alloc] init];
        menu_btn_bgImg.backgroundColor = [UIColor whiteColor];
        menu_btn_bgImg.userInteractionEnabled = YES;
        UIEdgeInsets capInsets = UIEdgeInsetsMake(10, 0,10,0);
        menu_btn_bgImg.image = [menu_btn_bgImg.image resizableImageWithCapInsets:capInsets];
        
        menu_btn_bgImg.frame = CGRectMake(10,150,300,270);
        
        UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame=CGRectMake(100,0,80,50);
        [btn setImage:[UIImage imageNamed:@"push_alert.png"] forState:UIControlStateNormal];
        btn.imageEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
        [btn setTitle:@"提示"forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont fontWithName:strinBoldFontOfSize size:17];
        // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
        btn.titleEdgeInsets = UIEdgeInsetsMake(0,20,0,0);
        [menu_btn_bgImg addSubview:btn];

        
        UILabel *titleLabel =[[UILabel alloc]initWithFrame:CGRectMake(0,40,300,20)];
        [titleLabel setBackgroundColor:[UIColor clearColor]];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.numberOfLines = 0;
        titleLabel.text = @"当前非楼盘工作时间(9:00-17:00)";
        titleLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:15];
        titleLabel.textColor=[AppSession colorWithHexString:@"333333"];
        [menu_btn_bgImg addSubview:titleLabel];

        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,70,300,1);
        [menu_btn_bgImg addSubview:lineImg];
        
        
        UILabel *subTitleLabel =[[UILabel alloc]initWithFrame:CGRectMake(20,80,250,50)];
        [subTitleLabel setBackgroundColor:[UIColor clearColor]];
        subTitleLabel.textAlignment = NSTextAlignmentLeft;
        subTitleLabel.numberOfLines = 0;
        subTitleLabel.text = @"您所推荐的客户将于下一个工作时间9:00自动推荐到楼盘";
        subTitleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        subTitleLabel.textColor=[AppSession colorWithHexString:@"333333"];
        [menu_btn_bgImg addSubview:subTitleLabel];
        
        
        UIButton *confirmBtn = [[UIButton alloc] initWithFrame:CGRectMake(20,150,260,40)];
        confirmBtn.tag=0;
        confirmBtn.userInteractionEnabled = YES;
        [confirmBtn setBackgroundImage:[UIImage imageNamed:@"push_btn.png"] forState:UIControlStateNormal];
        [confirmBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica"size:15]];
        [confirmBtn addTarget:self action:@selector(pushCustomAgain:) forControlEvents:UIControlEventTouchUpInside];
        [menu_btn_bgImg addSubview:confirmBtn];
        
        UIButton *cancelBtn = [[UIButton alloc] initWithFrame:CGRectMake(20,205,260,40)];
        cancelBtn.tag=1;
        cancelBtn.userInteractionEnabled = YES;
        [cancelBtn setBackgroundImage:[UIImage imageNamed:@"no_push_btn.png"] forState:UIControlStateNormal];
        [cancelBtn.titleLabel setFont:[UIFont fontWithName:@"Helvetica"size:15]];
        [cancelBtn addTarget:self action:@selector(pushCustomAgain:) forControlEvents:UIControlEventTouchUpInside];
        [menu_btn_bgImg addSubview:cancelBtn];
        
        [sortView addSubview:menu_btn_bgImg];
    }
    
    if(sortView.hidden)
    {
        sortView.hidden=NO;
        [AppSession fadeIn:sortView];
        
    }
    else
    {
        [AppSession fadeOut:sortView];
        
    }
    
}


-(void)ViewHidden:(id)sender
{
    [AppSession fadeOut:sortView];
}


- (void)buttonClicked:(int)cell_index
{
    cell_index= self.cell_index;
    
    [_delegate buttonClicked:cell_index];
}

@end
