//
//  CalloutSalonAnnotationView.h
//  cafekon
//
//  Created by 朱晓龙 on 13-4-16.
//  Copyright (c) 2013年 adam_yan. All rights reserved.
//

#import <MapKit/MapKit.h>

@protocol B_CalloutAnnotationViewDelegate;

@interface B_CalloutAnnotationView : MKAnnotationView

{
@private
    NSString *title_;
    //NSString *subTitle_;
    UILabel *titleLabel_;
    UIButton *button_;
    UILabel *subTitleLabel_;
    UILabel *shop_nameLabel;
}
@property (nonatomic, retain) NSString *title;
@property (nonatomic,retain) NSString *subTitle;
@property (nonatomic,retain)NSString *_kid;
@property (nonatomic,retain)NSString *img_str;
@property (nonatomic, assign) id<B_CalloutAnnotationViewDelegate> delegate;
@end

@protocol B_CalloutAnnotationViewDelegate
@required
- (void)buttonClicked:(NSString *)kid_str;
@end
