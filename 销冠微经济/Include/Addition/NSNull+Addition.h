//
//  NSNull+Addition.h
//  greentea
//
//  Created by xu shun wang on 13-6-25.
//
//

#import <Foundation/Foundation.h>

@interface NSNull (Addition)
- (id)objectForKey:(id)key;
- (id)objectAtIndex:(NSUInteger)index;
@end
