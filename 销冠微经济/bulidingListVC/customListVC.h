//
//  customListVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/19.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
#import "CustomSegmentControlBar.h"
@interface customListVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate,CustomSegmentControlBarDelegate>
{
    UITableView *_tableView;
     UITableView *_tableView2;
    UITableView *_tableView3;
    UITableView *_tableView4;
    UITableView *resultTableView;
    //UIButton *btn;
    UIView *topView;
    //NSMutableArray *dataSource;
    MJRefreshHeaderView *_header;
    UISearchBar *mySearchBar;
    CustomSegmentControlBar *bar;

    UIView *view_table1;
    UIView *view_table2;
    UIView *view_table3;
    UIView *view_table4;
    
    UIView *sortView;

}

@property (nonatomic, retain) UISearchBar*mSearchBar;
@property (nonatomic, retain) NSMutableArray *resultDataArr;
@property (nonatomic, retain) NSMutableArray *sortedArrForArrays;
@property (nonatomic, retain) NSMutableArray *sectionHeadsKeys;
@property (nonatomic, retain) NSMutableArray *dataSource;
@property (nonatomic, retain) NSMutableArray *dataSource2;
@property (nonatomic, retain) NSMutableArray *dataSource3;
@property (nonatomic, strong) NSMutableArray *dataArray;

@end
