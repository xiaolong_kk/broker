//
//  customListVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/19.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "customListVC.h"
#import "pinyin.h"
#import "ChineseString.h"
#import "customDetailVC.h"
#import "MCProgressBarView.h"
#import "addCell.h"
#import "groom_resultVC.h"
#import "simpleCustomPushBuildingVC.h"
#import <QuartzCore/QuartzCore.h>
//#import <QREncoder/QREncoder.h>
#import "QRCodeGenerator.h"
#import "addCustomVC.h"
#import "will_lnvalidVC.h"
#import "invalidVC.h"
#import "definition_invaildVC.h"
#import "definition_timoutVC.h"
@interface customListVC ()

@end

@implementation customListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createTableView];
}


- (void)createTableView
{
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHight1=44;
    self.navigationLeftTitleBtn.hidden = YES;
    self.navigationLabel.text = @"我的客户";
    topView = [[UIView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,36)];
    [self.view addSubview:topView];

    NSArray *arr = [[NSArray alloc]initWithObjects:@"我的客户",@"有效客户",@"正在界定",@"超级客户",nil];

    bar=[[CustomSegmentControlBar alloc] initWithDelegate:self totalCount:4 frame:CGRectMake(0,0,320,36) tag:1111];
    bar.titleArray = arr;
    bar.selectedIndex = 0;
    [topView addSubview:bar];
    
    [self init_firstView];
    [self init_secondView];
    [self init_thirdView];
}


-(void)init_firstView
{
    
    view_table1 = [[UIView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height, 320, self.view.frame.size.height-topView.frame.origin.y-topView.frame.size.height)];
    [self.view addSubview:view_table1];
    //初始化搜索条
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,44)];
    [mySearchBar setPlaceholder:@"请输入客户姓名或手机号"];
    mySearchBar.barTintColor=[AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"s_search_input.png"] forState:UIControlStateNormal];
    //mySearchBar.userInteractionEnabled = NO;
    mySearchBar.tintColor=[AppSession colorWithHexString:@"84c445"];
    mySearchBar.delegate = self;
    [view_table1 addSubview:mySearchBar];
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,5)];
    tempView.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView];
    
    UIView *tempView1 = [[UIView alloc]initWithFrame:CGRectMake(0,40, 320,5)];
    tempView1.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView1];
    
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,60)];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0,0,320,60);
    btn.tag = 114;
    [btn addTarget:self action:@selector(popToNextView:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:[UIImage imageNamed:@"icon_add.png"] forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0,-200,0,0);
    [btn setTitle:@"新增客户"forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
    btn.titleEdgeInsets = UIEdgeInsetsMake(0,-150,0,0);
    [headView addSubview:btn];
    
    UIImageView *arrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go2.png"]];
    arrowImg.frame = CGRectMake(285,25,10,19);
    [btn addSubview:arrowImg];
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,40,320,self.view.bounds.size.height-190) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = headView;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [view_table1 addSubview:_tableView];
    
    resultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,40,320,self.view.bounds.size.height-150) style:UITableViewStylePlain];
    resultTableView.dataSource = self;
    resultTableView.delegate = self;
    resultTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [view_table1 addSubview:resultTableView];
    resultTableView.hidden = YES;
    
    [self addHeader];
    

}


-(void)init_thirdView
{
    view_table3 = [[UIView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height, 320, self.view.frame.size.height)];
    view_table3.hidden = YES;
    [self.view addSubview:view_table3];
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,100)];
    // headView.backgroundColor = [UIColor whiteColor];
    
    for(int i=0;i<2;i++)
    {
        
        UIButton *tagBtn5=[UIButton buttonWithType:UIButtonTypeCustom];
        tagBtn5.tag = 3010+i;
        tagBtn5.frame=CGRectMake(0,50*i,320,50);
        [tagBtn5 addTarget:self action:@selector(nextView2:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:tagBtn5];
        
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,7,36,36)];
        [tagBtn5 addSubview:logoImg];
        
        UILabel *logoLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,15,100,20)];
        logoLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        logoLabel.textAlignment = NSTextAlignmentLeft;
        logoLabel.backgroundColor = [UIColor clearColor];
        logoLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [tagBtn5 addSubview:logoLabel];
        
        if(i==0)
        {
            logoImg.image = [UIImage imageNamed:@"icon_cb1.png"];
            logoLabel.text = @"界定超时";
            logoLabel.tag = 3020;
        }
        else
        {
            logoImg.image = [UIImage imageNamed:@"icon_cb2.png"];
            logoLabel.text = @"界定无效";
            logoLabel.tag = 3021;
        }
        
        UIImageView *arrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go2.png"]];
        arrowImg.frame = CGRectMake(285,15,10,19);
        [tagBtn5 addSubview:arrowImg];

    }
    
    UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg.frame = CGRectMake(0,50,320,1);
    [headView addSubview:lineImg];
    
    
    _tableView3 = [[UITableView alloc] initWithFrame:CGRectMake(0,0,320,self.view.bounds.size.height-150) style:UITableViewStylePlain];
    _tableView3.dataSource = self;
    _tableView3.delegate = self;
    _tableView3.backgroundColor = [UIColor clearColor];
    _tableView3.tableHeaderView = headView;
    _tableView3.separatorStyle=UITableViewCellSeparatorStyleNone;
    [view_table3 addSubview:_tableView3];

}

-(void)init_secondView
{
    
    view_table2 = [[UIView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height, 320, self.view.frame.size.height)];
    view_table2.hidden = YES;
    [self.view addSubview:view_table2];

    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,100)];
    // headView.backgroundColor = [UIColor whiteColor];
    
    
    for(int i=0;i<2;i++)
    {
    
        UIButton *tagBtn5=[UIButton buttonWithType:UIButtonTypeCustom];
        tagBtn5.tag = 2010+i;
        tagBtn5.frame=CGRectMake(0,50*i,320,50);
        [tagBtn5 addTarget:self action:@selector(nextView:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:tagBtn5];
        
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,7,36,36)];
        [tagBtn5 addSubview:logoImg];
       
        UILabel *logoLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,15,100,20)];
        logoLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        logoLabel.textAlignment = NSTextAlignmentLeft;
        logoLabel.backgroundColor = [UIColor clearColor];
        logoLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [tagBtn5 addSubview:logoLabel];
        
        if(i==0)
        {
            logoImg.image = [UIImage imageNamed:@"icon_cb1.png"];
            logoLabel.text = @"失效客户";
            logoLabel.tag = 2020;

        }
        
        else
        {
            logoImg.image = [UIImage imageNamed:@"icon_cb2.png"];
            logoLabel.text = @"即将失效";
            logoLabel.tag = 2021;
        }



        UIImageView *arrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go2.png"]];
        arrowImg.frame = CGRectMake(285,15,10,19);
        [tagBtn5 addSubview:arrowImg];

        //        UIButton *logoImg = [[UIButton alloc] initWithFrame:CGRectMake(25,i*36+15,36,37)];
        //        [logoImg setImage:[UIImage imageNamed:[NSString stringWithFormat:@"verify%d.jpg",i+1] forState:UIc];
        //        logoImg.image = [UIImage imageNamed:[NSString stringWithFormat:@"verify%d.jpg",i+1]];
        //        [headView addSubview:logoImg];
        
        //        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(55,45*i,100,45)];
        //        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
        //        tempLabel.textAlignment = NSTextAlignmentCenter;
        //        tempLabel.text = @"正在界定";
        //        tempLabel.backgroundColor = [UIColor clearColor];
        //        tempLabel.textColor = [AppSession colorWithHexString:@"000000"];
        //        [headView addSubview:tempLabel];
        
    }
    
    UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg.frame = CGRectMake(0,50,320,1);
    [headView addSubview:lineImg];
    
    
    _tableView2 = [[UITableView alloc] initWithFrame:CGRectMake(0,0,320,self.view.bounds.size.height-150) style:UITableViewStylePlain];
    _tableView2.dataSource = self;
    _tableView2.delegate = self;
    _tableView2.backgroundColor = [UIColor clearColor];
    _tableView2.tableHeaderView = headView;
    _tableView2.separatorStyle=UITableViewCellSeparatorStyleNone;
    [view_table2 addSubview:_tableView2];

    //[self getData2];
}

-(void)nextView:(UIButton *)btn
{
    if(btn.tag==2010)
    {
        invalidVC *vc = [[invalidVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    if(btn.tag==2011)
    {
        will_lnvalidVC *vc = [[will_lnvalidVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
}

-(void)nextView2:(UIButton *)btn
{
    if(btn.tag==3010)
    {
        definition_timoutVC *vc = [[definition_timoutVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    if(btn.tag==3011)
    {
        definition_invaildVC *vc = [[definition_invaildVC alloc] initWithNavigationBar:YES];
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
}

- (void)addHeader
{
    
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = _tableView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        [self performSelector:@selector(getData) withObject:refreshView afterDelay:0.0];
        
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:0.5];
        
        //NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        //NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                //NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
               // NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
               // NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    [header beginRefreshing];
    _header = header;
}
- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_tableView reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}


-(void)getData
{
    self.sortedArrForArrays = [[NSMutableArray alloc] init];
    self.sectionHeadsKeys = [[NSMutableArray alloc] init];
    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.mycustomer&BrokerKid=%@",theApp.brokerKid];
    [self startSynchronousRequest:str  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
        // NSLog(@"%@",request.responseString);
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
            // NSLog(@"%@",dict);
             NSArray *arr = [[dict objectForKey:@"data"] objectForKey:@"list_itme"];
             self.dataSource = [NSMutableArray arrayWithArray:arr];

         }
         
          //NSLog(@"%@",self.dataSource);
         
         if(self.dataSource.count>0)
         self.sortedArrForArrays = [self getChineseStringArr:self.dataSource];

         [_tableView reloadData];
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
     }];
}

-(void)getData2
{
    
    UILabel *logoLabel1 = (UILabel *)[self.view viewWithTag:2020];
    
    UILabel *logoLabel2 = (UILabel *)[self.view viewWithTag:2021];
    

    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.wxcustomerlistvalid&BrokerKid=%@&PageIndex=1",theApp.brokerKid];
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSDictionary *dict2= [dict objectForKey:@"data"];
             NSArray *arr = [dict2 objectForKey:@"returnLsit"];
             
             NSString *list_invalidCount = [NSString stringWithFormat:@"%@",[dict2 objectForKey:@"list_invalidCount"]];
             logoLabel1.text = [NSString stringWithFormat:@"失效客户 (%@)",list_invalidCount];
             
             
             NSString *CountWill = [NSString stringWithFormat:@"%@",[dict2 objectForKey:@"CountWill"]];
             logoLabel2.text = [NSString stringWithFormat:@"即将失效 (%@)",CountWill];
            
             self.dataSource2 = [NSMutableArray arrayWithArray:arr];
             
         }
         [_tableView2 reloadData];
   
     }
    didFailedRequest:^(ASIHTTPRequest *request){
                     
    }];
}

-(void)getData3
{
    
    UILabel *logoLabel1 = (UILabel *)[self.view viewWithTag:3020];
    
    UILabel *logoLabel2 = (UILabel *)[self.view viewWithTag:3021];
    
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.weixincustomer.wxcustomerlistpushendwait&BrokerKid=%@&PageIndex=1",theApp.brokerKid];
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSDictionary *dict2= [dict objectForKey:@"data"];
             
            // NSLog(@"%@",dict2);
             NSArray *arr = [dict2 objectForKey:@"UserList"];
             
             NSString *list_invalidCount = [NSString stringWithFormat:@"%@",[dict2 objectForKey:@"count_timeout"]];
             logoLabel1.text = [NSString stringWithFormat:@"界定超时 (%@)",list_invalidCount];
             
             
             NSString *CountWill = [NSString stringWithFormat:@"%@",[dict2 objectForKey:@"count_invalid"]];
             logoLabel2.text = [NSString stringWithFormat:@"界定无效 (%@)",CountWill];
             
             self.dataSource3 = [NSMutableArray arrayWithArray:arr];
             
         }
         [_tableView3 reloadData];
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
}


-(void)requestSuccess:(ASIFormDataRequest *)request
{
    NSDictionary *dict= [request.responseString objectFromJSONString];
    if([[dict objectForKey:@"code"] intValue] ==0)
    {
        NSDictionary *dict2= [dict objectForKey:@"data"];
        NSArray *arr = [dict2 objectForKey:@"returnLsit"];
        self.dataSource2 = [NSMutableArray arrayWithArray:arr];
        
    }
    [_tableView2 reloadData];
    
}


-(void)popToNextView:(UIButton *)sender
{
    if(sender.tag==114)
    {
        addCustomVC *vc = [[addCustomVC alloc] initWithNavigationBar:YES];
        [self.navigationController pushViewController:vc animated:YES];
    }
    if(sender.tag==113)
    {
//        groupListVC *vc = [[groupListVC alloc] init];
//        vc.back_name = @"通讯录";
//        [self.navigationController pushViewController:vc animated:YES];
    }
    if(sender.tag==112)
    {
//        newFriendsVC *vc = [[newFriendsVC alloc] init];
//        vc.back_name = @"通讯录";
//        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return  [[self.sortedArrForArrays objectAtIndex:section] count];
    }
    if(tableView==resultTableView)
    {
        return [self.resultDataArr count];
    }
    if(tableView==_tableView2)
    {
        return [self.dataSource2 count];
    }
    if(tableView==_tableView3)
    {
        return self.dataSource3.count;

    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==_tableView)
    {
        return [self.sortedArrForArrays count];
    }
    
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return [_sectionHeadsKeys objectAtIndex:section];
    }
    if(tableView==resultTableView)
    {
        return 0;
    }
    
    
    return 0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(tableView==_tableView)
    {
        return self.sectionHeadsKeys;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==_tableView||tableView==resultTableView)
    {
        return 55;
    }

    if(tableView==_tableView2)
    {
        return 120;
    }
    if(tableView==_tableView3)
    {
        return 65;
    }
    return 0;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
    NSString *tempStr = nil;
    if(tableView==_tableView)
    {
        if ([self.sortedArrForArrays count] > indexPath.section)
        {
            
            NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
            if ([arr count] > indexPath.row)
            {
                
                ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
                tempStr = str.string;
                
                for(NSDictionary *dict in _dataSource)
                {
                    if([[dict objectForKey:@"F_Title"] isEqualToString:str.string])
                    {
                        tempStr  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]];
                    }
                }
            }
        }
    }
    if(tableView==_tableView2)
    {
        tempStr  =[NSString stringWithFormat:@"%@",[[self.dataSource2 objectAtIndex:indexPath.row] objectForKey:@"Kid"]];

    }
    if(tableView==resultTableView)
    {
        tempStr  =[NSString stringWithFormat:@"%@",[[self.resultDataArr objectAtIndex:indexPath.row] objectForKey:@"Kid"]];
    }
    if(tableView==_tableView3)
    {
        
    }
    customDetailVC *vc = [[customDetailVC alloc] initWithNavigationBar:YES];
    vc.kid_str = tempStr;
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==_tableView)
    {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImageView *logImg = [[UIImageView alloc] init];
            logImg.tag = 100;
            logImg.frame = CGRectMake(15,7.5,40,40);
            [cell addSubview:logImg];
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,54,320,1);
            [cell addSubview:lineImg];
            
            UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,15,50,20)];
            titleLabel.tag=101;
            titleLabel.textAlignment=NSTextAlignmentLeft;
            titleLabel.textColor=[UIColor blackColor];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(120,15,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:sexLabel];

            UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(140,15,120,20)];
            phoneLabel.tag=102;
            phoneLabel.textAlignment=NSTextAlignmentCenter;
            phoneLabel.textColor=[AppSession colorWithHexString:@"75a7be"];
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            phoneLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:phoneLabel];
            
            UIButton *destroyBtn = [[UIButton alloc] init];
            [destroyBtn setImage:(UIImage *)[UIImage imageNamed:@"btn_1.png"] forState:(UIControlState)UIControlStateNormal];
            destroyBtn.tag = 5555;
            destroyBtn.frame = CGRectMake(255,15,43,25);
            [destroyBtn addTarget:self action:@selector(groomCustom:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:destroyBtn];
            
            
        }
        
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UIImageView *logoImg = (UIImageView *)[cell viewWithTag:100];
        UIButton *destroyBtn = (UIButton *)[cell viewWithTag:5555];
    
        if ([self.sortedArrForArrays count] > indexPath.section)
        {
            
            NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
            if ([arr count] > indexPath.row)
            {
                
                ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
                titleLabel.text = str.string;
                for(NSDictionary *dict in _dataSource)
                {
                    if([[dict objectForKey:@"F_Title"] isEqualToString:str.string])
                    {
                        titleLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Title"]];

                        phoneLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Phone"]];
                        sexLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Sex"]];

                        //NSLog(@"%@",[dict objectForKey:@"F_PicUrl"]);
                        if([self isBlankString:[dict objectForKey:@"F_PicUrl"]]==YES)
                        {
                        if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
                            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
                        else
                            logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
                        }
                        else
                        {
                        [logoImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];
                        }
                        
                        [destroyBtn setTitle:[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]] forState:UIControlStateNormal];
                        
                        
                    }
                }
            }
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    if(tableView==_tableView2)
    {
        
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
            // headView.backgroundColor= [UIColor whiteColor];
            [cell addSubview:headView];
            
            UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
            logoImg.tag = 101;
            //            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
            [headView addSubview:logoImg];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,50,20)];
            titleLabel.tag = 102;
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.textAlignment = NSTextAlignmentLeft;
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
            [headView addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [headView addSubview:sexLabel];
            
            UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
            phoneLabel.tag = 104;
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            phoneLabel.textAlignment = NSTextAlignmentLeft;
            phoneLabel.backgroundColor = [UIColor clearColor];
            phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
            [headView addSubview:phoneLabel];
            
            UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
            budildingLabel.tag = 105;
            budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            budildingLabel.textAlignment = NSTextAlignmentLeft;
            budildingLabel.backgroundColor = [UIColor clearColor];
            budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [headView addSubview:budildingLabel];
            
            
            UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(240,11,67,20)];
            clickBtn.tag = indexPath.row+2222;
            [clickBtn setBackgroundImage:[UIImage imageNamed:@"btn_code.png"] forState:UIControlStateNormal];
            [clickBtn addTarget:self action:@selector(encodeBtn:) forControlEvents:UIControlEventTouchUpInside];
            [headView addSubview:clickBtn];
            
            NSArray *arr = @[@"推荐有效",@"已到访",@"已认筹",@"已预定",@"已成交"];
            for(int i=0;i<5;i++)
            {
                UIView *lineBg = [[UIView alloc] initWithFrame:CGRectMake(10,60,300,60)];
                [headView addSubview:lineBg];
                
                UIImageView *pointlineImg = [[UIImageView alloc] initWithFrame:CGRectMake(i*60,0,60,10)];
                pointlineImg.tag = 222+i;
                pointlineImg.image = [UIImage imageNamed:@"pointline_gray.png"];
                [lineBg addSubview:pointlineImg];
                
                UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,15,60,20)];
                stateLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                stateLabel.textAlignment = NSTextAlignmentCenter;
                stateLabel.text =arr[i];
                stateLabel.tag = 333+i;
                stateLabel.backgroundColor = [UIColor clearColor];
                stateLabel.textColor = [AppSession colorWithHexString:@"000000"];
                [lineBg addSubview:stateLabel];
                
                UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,35,60,20)];
                timeLabel.tag = 555+i;
                timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                timeLabel.textAlignment = NSTextAlignmentCenter;
                timeLabel.backgroundColor = [UIColor clearColor];
                timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
                [lineBg addSubview:timeLabel];
                
            }
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,119,320,1);
            [cell addSubview:lineImg];
            
            
        }
        
        NSDictionary *dict = _dataSource2[indexPath.row];
        UIImageView *logoImg = (UIImageView *)[cell viewWithTag:101];
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:104];
        UILabel *budildingLabel = (UILabel *)[cell viewWithTag:105];
        UILabel *stateLabel = (UILabel *)[cell viewWithTag:334];
        
        UILabel *timeLabel1 = (UILabel *)[cell viewWithTag:555];
        UILabel *timeLabel2 = (UILabel *)[cell viewWithTag:556];
        UILabel *timeLabel3 = (UILabel *)[cell viewWithTag:557];
        UILabel *timeLabel4 = (UILabel *)[cell viewWithTag:558];
        UILabel *timeLabel5 = (UILabel *)[cell viewWithTag:559];
        
        UIImageView *pointlineImg1 = (UIImageView *)[cell viewWithTag:222];
        UIImageView *pointlineImg2 = (UIImageView *)[cell viewWithTag:223];
        UIImageView *pointlineImg3 = (UIImageView *)[cell viewWithTag:224];
        UIImageView *pointlineImg4 = (UIImageView *)[cell viewWithTag:225];
        UIImageView *pointlineImg5 = (UIImageView *)[cell viewWithTag:226];
        
        NSDictionary *dict_valid = [dict objectForKey:@"Valid"];
        NSDictionary *dict_come = [dict objectForKey:@"Come"];
        NSDictionary *dict_ticket = [dict objectForKey:@"Ticket"];
        NSDictionary *dict_preordain = [dict objectForKey:@"Preordain"];
        NSDictionary *dict_business = [dict objectForKey:@"Business"];
        
        if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
        else
            logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
        
        //[logoImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];

        if([[dict_valid objectForKey:@"values"] intValue]==1)
        {
            pointlineImg1.image = [UIImage imageNamed:@"pointline_orange.png"];
            timeLabel1.text = [dict_valid objectForKey:@"Times"];
        }
        if([[dict_come objectForKey:@"values"] intValue]==1)
        {
            pointlineImg2.image = [UIImage imageNamed:@"pointline_orange.png"];
            stateLabel.text = [dict_come objectForKey:@"Names"];
            timeLabel2.text = [dict_come objectForKey:@"Times"];
            
        }
        if([[dict_ticket objectForKey:@"values"] intValue]==1)
        {
            pointlineImg3.image = [UIImage imageNamed:@"pointline_orange.png"];
            timeLabel3.text = [dict_ticket objectForKey:@"Times"];
        }
        if([[dict_preordain objectForKey:@"values"] intValue]==1)
        {
            pointlineImg4.image = [UIImage imageNamed:@"pointline_orange.png"];
            timeLabel4.text = [dict_preordain objectForKey:@"Times"];
        }
        if([[dict_business objectForKey:@"values"] intValue]==1)
        {
            pointlineImg5.image = [UIImage imageNamed:@"pointline_orange.png"];
            timeLabel5.text = [dict_business objectForKey:@"Times"];
        }
        
        titleLabel.text = [dict objectForKey:@"F_Title"];
        sexLabel.text = [dict objectForKey:@"F_Sex"];
        phoneLabel.text = [dict objectForKey:@"F_Phone"];
        budildingLabel.text = [dict objectForKey:@"BuildingName"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        
        return cell;

    }
    
    if(tableView==_tableView3)
    {
        
            NSString *CellIdentifier = [NSString stringWithFormat:@"MainCell%d",indexPath.row];//以indexPath来唯一确定cell
            
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil)
            {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
                
                UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
                //headView.backgroundColor= [UIColor whiteColor];
                [cell addSubview:headView];
                
                UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
                logoImg.tag = 101;
                logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
                [headView addSubview:logoImg];
                
                UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,50,20)];
                titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
                titleLabel.textAlignment = NSTextAlignmentLeft;
                titleLabel.backgroundColor = [UIColor clearColor];
                titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
                [headView addSubview:titleLabel];
                
                UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
                sexLabel.tag=103;
                sexLabel.textAlignment=NSTextAlignmentCenter;
                sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
                sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                sexLabel.backgroundColor=[UIColor clearColor];
                [headView addSubview:sexLabel];
                
                UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
                phoneLabel.tag = 103;
                phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                phoneLabel.textAlignment = NSTextAlignmentLeft;
                phoneLabel.backgroundColor = [UIColor clearColor];
                phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
                [headView addSubview:phoneLabel];
                
                UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
                budildingLabel.tag = 103;
                budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
                budildingLabel.textAlignment = NSTextAlignmentLeft;
                budildingLabel.backgroundColor = [UIColor clearColor];
                budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
                [headView addSubview:budildingLabel];
                
                
                UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(230,15,12,12)];
                tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
                [headView addSubview:tagImg];
                
                
                UILabel *tageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250,11,150,20)];
                tageLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                tageLabel.textAlignment = NSTextAlignmentLeft;
                tageLabel.backgroundColor = [UIColor clearColor];
                tageLabel.textColor = [AppSession colorWithHexString:@"666666"];
                [headView addSubview:tageLabel];
                
                
                UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,320,50)];
                //extendedBtn.backgroundColor = [UIColor blackColor];
                //[extendedBtn setImage:[UIImage imageNamed:@"line_show.png"] forState:UIControlStateNormal];
                [clickBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
                [headView addSubview:clickBtn];
                
                UIImageView *exImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,65-9,320,9)];
                exImg.tag = 999;
                exImg.image = [UIImage imageNamed:@"line_show.png"];
                [cell addSubview:exImg];
                
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor clearColor];
            return cell;
            
        }
    
    if(tableView==resultTableView)
    {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImageView *logImg = [[UIImageView alloc] init];
            logImg.tag = 100;
            logImg.frame = CGRectMake(15,7.5,40,40);
            [cell addSubview:logImg];
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,54,320,1);
            [cell addSubview:lineImg];
            
            UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,15,50,20)];
            titleLabel.tag=101;
            titleLabel.textAlignment=NSTextAlignmentLeft;
            titleLabel.textColor=[UIColor blackColor];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(120,15,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:sexLabel];
            
            UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(140,15,120,20)];
            phoneLabel.tag=102;
            phoneLabel.textAlignment=NSTextAlignmentCenter;
            phoneLabel.textColor=[AppSession colorWithHexString:@"75a7be"];
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            phoneLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:phoneLabel];
            
            UIButton *destroyBtn = [[UIButton alloc] init];
            [destroyBtn setImage:(UIImage *)[UIImage imageNamed:@"btn_1.png"] forState:(UIControlState)UIControlStateNormal];
            destroyBtn.tag = 5555;
            destroyBtn.frame = CGRectMake(255,15,43,25);
            [destroyBtn addTarget:self action:@selector(groomCustom:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:destroyBtn];
            
            
        }
        
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UIImageView *logoImg = (UIImageView *)[cell viewWithTag:100];
        UIButton *destroyBtn = (UIButton *)[cell viewWithTag:5555];
        
        NSDictionary *dict = [self.resultDataArr objectAtIndex:indexPath.row];
        titleLabel.text = [dict objectForKey:@"F_Title"];
        
        titleLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Title"]];
        
        phoneLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Phone"]];
        sexLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Sex"]];
        
        if([self isBlankString:[dict objectForKey:@"F_PicUrl"]]==YES)
        {
            if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
                logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
            else
                logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
        }
        else
        {
            [logoImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];
        }
        
        [destroyBtn setTitle:[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]] forState:UIControlStateNormal];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    return nil;
}



-(void)encodeBtn:(UIButton *)btn
{
   
    
       sortView=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
        UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320, theApp.window.bounds.size.height)];
        shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        [sortView addSubview:shadowView];
        sortView.hidden = YES;
        [[[UIApplication sharedApplication] keyWindow] addSubview:sortView];
    
    NSInteger i  = btn.tag-2222;
    NSDictionary *dict = self.dataSource2[i];
    
    
    NSDictionary *qr_dict = [dict objectForKey:@"QRCode"];
    
    NSMutableString *jsonString = [[NSMutableString alloc] init];
    
    NSString *str1 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"F_BuildingKid"]];
    NSString *str2 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"F_WeiXinBrokerKid"]];
    NSString *str3 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"Kid"]];
    NSString *str4 = [NSString stringWithFormat:@"%@",[qr_dict objectForKey:@"Type"]];

//        timeStr  = [timeStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
//        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$€^•'@#$%^&*()_+'\""];
//        
//        NSString *trimmedString = [timeStr stringByTrimmingCharactersInSet:set];
//        trimmedString  = [trimmedString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
     NSString *string  = [NSString stringWithFormat:
                   @"{\"F_BuildingKid\":\"%@\",\"F_WeiXinBrokerKid\":\"%@\",\"Kid\":\"%@\",\"Type\":\"%@\"}",str1,str2,str3,str4];
        
        [jsonString appendString:string];
    
    
//    NSUInteger location = [jsonString length]-1;
//    
//    NSRange range       = NSMakeRange(location, 1);
    
    // 4. 将末尾逗号换成结束的]}
   // [jsonString replaceCharactersInRange:range withString:@"]"];

   // NSLog(@"%@",jsonString);
    CGFloat qrSize = self.view.bounds.size.width - 10 * 2;

    UIView *codeBg =[[UIView alloc] initWithFrame:CGRectMake(10, (self.view.bounds.size.height - qrSize) / 2,
                                                             qrSize, qrSize)];
    codeBg.backgroundColor = [UIColor whiteColor];
    [sortView addSubview:codeBg];
    UIImage* image = [QRCodeGenerator qrImageForString:jsonString imageSize:300];
        
    UIImageView* imageView = [[UIImageView alloc] initWithImage:image];
    imageView.frame = CGRectMake(0,0,qrSize, qrSize);
    [codeBg addSubview:imageView];
    [imageView layer].magnificationFilter = kCAFilterNearest;
    
    UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,44,320,self.view.bounds.size.height)];
    [bgBtn addTarget:self action:@selector(ViewHidden:) forControlEvents:UIControlEventTouchUpInside];
    [sortView addSubview:bgBtn];

    [AppSession fadeIn:sortView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [AppSession resignKeyBoardInView:self.view];
    }
}

-(void)ViewHidden:(id)sender
{
  [AppSession fadeOut:[sender superview]];
}

-(void)groomCustom:(UIButton *)btn
{
   // NSLog(@"%@",btn.titleLabel.text);
    simpleCustomPushBuildingVC *vc = [[simpleCustomPushBuildingVC alloc] initWithNavigationBar:YES];
    vc.CustomerID_str = btn.titleLabel.text;
    [self.navigationController pushViewController:vc animated:YES];
}
- (void)buttonClicked:(UIButton *)btn
{
    // NSLog(@"%ld",btn.tag);
    customDetailVC *vc = [[customDetailVC alloc] initWithNavigationBar:YES];
    [self.navigationController pushViewController:vc animated:YES];

    
}

- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort {
 
    NSMutableArray *chineseStringsArray = [NSMutableArray array];
    
    for(int i = 0; i < [arrToSort count]; i++) {
        ChineseString *chineseString=[[ChineseString alloc]init];
        chineseString.string=[NSString stringWithString:[[arrToSort objectAtIndex:i]  objectForKey:@"F_Title"]];
        if(chineseString.string==nil){
            chineseString.string=@"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            //join the pinYin
            NSString *pinYinResult = [NSString string];
            for(int j = 0;j < chineseString.string.length; j++) {
                NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                 pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            chineseString.pinYin = pinYinResult;
        } else {
            chineseString.pinYin = @"";
        }
        [chineseStringsArray addObject:chineseString];
        
    }
    
    //sort the ChineseStringArr by pinYin
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *arrayForArrays = [NSMutableArray array];
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [chineseStringsArray count]; index++)
    {
        ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
        NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
        NSString *sr= [strchar substringToIndex:1];
        //sr containing here the first character of each string
        if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
        {
            [_sectionHeadsKeys addObject:[sr uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
        {
            [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
            
            if(checkValueAtIndex == NO)
            {
                [arrayForArrays addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
                
            }
        }
    }
    return arrayForArrays;
}


#pragma mark - customSegmentBar回调
- (void)initSegmentButton:(CustomSegmentControlBar *)sender btn:(UIButton *)btn index:(int)index
{
    [btn setTitleColor:[AppSession colorWithHexString:@"999999"] forState:UIControlStateNormal];
    [btn setTitleColor:[AppSession colorWithHexString:@"333333"] forState:UIControlStateSelected];
    btn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:13];
    [btn setBackgroundImage:[UIImage imageNamed:@"tab_topbar.png"] forState:UIControlStateNormal];
    [btn setBackgroundImage:[UIImage imageNamed:@"tab_topbar_hl.png"] forState:UIControlStateSelected];
    btn.titleLabel.font=[UIFont systemFontOfSize:15];
    
}

- (void)selectButton:(CustomSegmentControlBar *)sender btn:(UIButton *)btn index:(int)index
{
    
    if(index==0)
    {
        self.selectInt=1;
        view_table1.hidden = NO;
        view_table2.hidden = YES;
        view_table3.hidden = YES;
        self.navigationLabel.text = @"我的客户";
        
    }
    if(index==1)
    {
        self.selectInt=2;
        view_table1.hidden = YES;
        view_table2.hidden = NO;
        view_table3.hidden = YES;
        [self getData2];
        self.navigationLabel.text = @"有效客户";
        
    }
    if(index==2)
    {
        view_table1.hidden = YES;
        view_table2.hidden = YES;
        view_table3.hidden = NO;
        [self getData3];
        self.navigationLabel.text = @"正在界定";
    }
    
}


#pragma mark searchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar;                      // return NO to not become first responder
{
    searchBar.showsCancelButton = YES;
    resultTableView.hidden = NO;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;                    // called when cancel button pressed
{
    [AppSession resignKeyBoardInView:self.view];
    searchBar.showsCancelButton = NO;
    resultTableView.hidden = YES;
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    [AppSession resignKeyBoardInView:self.view];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    self.resultDataArr = [[NSMutableArray alloc]init];
    //    if (mySearchBar.text.length>0&&![ChineseInclude isIncludeChineseInString:mySearchBar.text]) {
    //        for (int i=0; i<dataArray.count; i++) {
    //            if ([ChineseInclude isIncludeChineseInString:dataArray[i]]) {
    //                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:dataArray[i]];
    //                NSRange titleResult=[tempPinYinStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //                if (titleResult.length>0) {
    //                    for(NSDictionary *dict in dataSource)
    //                    {
    //                        if([[dict objectForKey:@"content"] isEqualToString:dataArray[i]])
    //                        {
    //                          [searchResults addObject:dataSource[i]];
    //
    //                        }
    //                    }
    //                }
    //                NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:dataArray[i]];
    //                NSRange titleHeadResult=[tempPinYinHeadStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //                if (titleHeadResult.length>0) {
    //                    for(NSDictionary *dict in dataSource)
    //                    {
    //                        if([[dict objectForKey:@"content"] isEqualToString:dataArray[i]])
    //                        {
    //                            [searchResults addObject:dataSource[i]];
    //
    //                        }
    //                    }
    //                }
    //            }
    //            else {
    //                NSRange titleResult=[dataArray[i] rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //                if (titleResult.length>0) {
    //                    for(NSDictionary *dict in dataSource)
    //                    {
    //                        if([[dict objectForKey:@"content"] isEqualToString:dataArray[i]])
    //                        {
    //                            [searchResults addObject:dataSource[i]];
    //
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    } else if (mySearchBar.text.length>0&&[ChineseInclude isIncludeChineseInString:mySearchBar.text]) {
    //        for (NSString *tempStr in dataArray) {
    //            NSRange titleResult=[tempStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //            if (titleResult.length>0) {
    //                [searchResults addObject:tempStr];
    //            }
    //        }
    //    }
    
    NSString *tempStr;
    NSString *regex = @"[0-9]";
    NSString *search_str;
    
    if(mySearchBar.text.length>1||mySearchBar.text.length==1)
        search_str = [mySearchBar.text substringToIndex:1];
    
   // NSLog(@"%@",_dataSource);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    for (NSDictionary *dict in _dataSource)
    {
        
        if ([predicate evaluateWithObject:search_str] == YES)
        {
            tempStr = [dict objectForKey:@"F_Phone"];
        }
        else
        {
            tempStr = [dict objectForKey:@"F_Title"];
            
        }
        
        
        NSRange titleResult=[tempStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
        if (titleResult.length>0)
        {
            [self.resultDataArr addObject:dict];
        }
        [resultTableView reloadData];
        //NSLog(@"%@",self.resultDataArr);
        
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
