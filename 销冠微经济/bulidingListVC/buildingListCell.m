//
//  buildingListCell.m
//  销冠微经济
//
//  Created by zen huang on 14/9/16.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "buildingListCell.h"

@implementation buildingListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        UIImageView *buildingImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,15, 81, 61)];
        buildingImg.tag = 101;
        buildingImg.backgroundColor = [UIColor grayColor];
        [self addSubview:buildingImg];
        
        UILabel *bulidingLabel = [[UILabel alloc] initWithFrame:CGRectMake(105, 8,150,30)];
        bulidingLabel.tag=102;
        bulidingLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:16];
        bulidingLabel.backgroundColor = [UIColor clearColor];
        bulidingLabel.textColor = [UIColor blackColor];
       // bulidingLabel.text = @"卡考一号";
        [self addSubview:bulidingLabel];
        
        
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(105,42,80,20)];
        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:12];
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.textColor = [AppSession colorWithHexString:@"666666"];
        tempLabel.text = @"佣金";
        [self addSubview:tempLabel];
        
        
        UILabel *brokerageLabel = [[UILabel alloc] initWithFrame:CGRectMake(135,35,80,30)];
        brokerageLabel.tag = 103;
        brokerageLabel.font=[UIFont fontWithName:stringFontOfSize size:23];
        brokerageLabel.backgroundColor = [UIColor clearColor];
        brokerageLabel.textColor = [AppSession colorWithHexString:@"f00001"];
      //  brokerageLabel.text = @"0.03%";
        [self addSubview:brokerageLabel];
        
        UILabel *tempLabel2 = [[UILabel alloc] initWithFrame:CGRectMake(213,42,80,20)];
        tempLabel2.font=[UIFont fontWithName:stringFontOfSize size:12];
        tempLabel2.backgroundColor = [UIColor clearColor];
        tempLabel2.textColor = [AppSession colorWithHexString:@"666666"];
        tempLabel2.text = @"客户界定";
        [self addSubview:tempLabel2];

        UILabel *getDefinedLabel = [[UILabel alloc] initWithFrame:CGRectMake(270,42,80,20)];
        getDefinedLabel.tag = 104;
        getDefinedLabel.font=[UIFont fontWithName:stringFontOfSize size:12];
        getDefinedLabel.backgroundColor = [UIColor clearColor];
        getDefinedLabel.textColor = [AppSession colorWithHexString:@"f00001"];
        //getDefinedLabel.text = @">5分钟";
        [self addSubview:getDefinedLabel];

        UILabel *CommissionEvaluateLabel = [[UILabel alloc] initWithFrame:CGRectMake(105,57,120,30)];
        CommissionEvaluateLabel.tag = 105;
        CommissionEvaluateLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
        CommissionEvaluateLabel.backgroundColor = [UIColor clearColor];
        CommissionEvaluateLabel.textColor = [AppSession colorWithHexString:@"666666"];
        CommissionEvaluateLabel.text = @"高于同楼盘";
        [self addSubview:CommissionEvaluateLabel];
        
        UIImageView *CommissionEvaluateImg = [[UIImageView alloc] initWithFrame:CGRectMake(170,68,13,8)];
        CommissionEvaluateImg.image = [UIImage imageNamed:@"icon_b12.png"];
        [self addSubview:CommissionEvaluateImg];
        
        
        UILabel *tempLabel3 = [[UILabel alloc] initWithFrame:CGRectMake(213,57,80,30)];
        tempLabel3.font=[UIFont fontWithName:stringFontOfSize size:10];
        tempLabel3.backgroundColor = [UIColor clearColor];
        tempLabel3.textColor = [AppSession colorWithHexString:@"666666"];
        tempLabel3.text = @"结佣时间";
        [self addSubview:tempLabel3];

        UILabel *CommissionSpeedLabel = [[UILabel alloc] initWithFrame:CGRectMake(263,57,80,30)];
        CommissionSpeedLabel.tag = 106;
        CommissionSpeedLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
        CommissionSpeedLabel.backgroundColor = [UIColor clearColor];
        CommissionSpeedLabel.textColor = [AppSession colorWithHexString:@"e69609"];
        //CommissionSpeedLabel.text = @">5分钟";
        [self addSubview:CommissionSpeedLabel];

        NSArray *arr = @[@"带看",@"带看奖",@"认筹奖",@"超额奖"];
        for(int i=0;i<4;i++)
        {
            UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(10+(65*i),87,50,20)];
            tempLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
            tempLabel.backgroundColor = [UIColor clearColor];
            tempLabel.textAlignment = NSTextAlignmentCenter;
            tempLabel.textColor = [AppSession colorWithHexString:@"666666"];
            tempLabel.text = arr[i];
            [self addSubview:tempLabel];
            
            
            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,5,8,8)];
            tagImg.tag = 200+i;
            tagImg.image = [UIImage imageNamed:@"icon_b2on.png"];
            [tempLabel addSubview:tagImg];

        }
        
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,100,200,30)];
        timeLabel.tag = 107;
        timeLabel.font=[UIFont fontWithName:stringFontOfSize size:10];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [AppSession colorWithHexString:@"999999"];
        [self addSubview:timeLabel];
        
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,130,320,1);
        [self addSubview:lineImg];
        
//        UIImageView *lineImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
//        lineImg2.frame = CGRectMake(0,130,320,1);
//        [self addSubview:lineImg2];

    }
    return self;
}

-(void)setData:(NSDictionary *)dict;

{
    
    UIImageView *buildingImg = (UIImageView *)[self viewWithTag:101];
    UILabel *bulidingLabel = (UILabel *)[self viewWithTag:102];
    UILabel *brokerageLabel = (UILabel *)[self viewWithTag:103];
    UILabel *getDefinedLabel = (UILabel *)[self viewWithTag:104];
    UILabel *CommissionEvaluateLabel = (UILabel *)[self viewWithTag:105];
    UILabel *CommissionSpeedLabel = (UILabel *)[self viewWithTag:106];
    UILabel *timeLabel = (UILabel *)[self viewWithTag:107];

    [buildingImg setWebImageWithFade:[dict objectForKey:@"F_Logo"] placeholderImage:nil];
    bulidingLabel.text = [dict objectForKey:@"F_Title"];
    brokerageLabel.text = [dict objectForKey:@"Commission"];
    getDefinedLabel.text = [dict objectForKey:@"GetDefined"];
   
    CommissionEvaluateLabel.text = [dict objectForKey:@"F_WeiXinCommissionEvaluate"];
    
    NSString *startTime = [dict objectForKey:@"StartTime"];
    startTime = [startTime substringToIndex:10];
    
    NSString *endTime = [dict objectForKey:@"EndTime"];
    endTime = [endTime substringToIndex:10];

    CommissionSpeedLabel.text = [dict objectForKey:@"F_WeiXinCommissionSpeed"];
    timeLabel.text = [NSString stringWithFormat:@"有效期: %@ 至 %@",startTime,endTime];

    //NSLog(@"%@",dict);
}
- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
