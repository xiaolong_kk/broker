//
//  definitioningVC.m
//  销冠微经济
//
//  Created by zen huang on 14/10/12.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "definitioningVC.h"

@interface definitioningVC ()

@end

@implementation definitioningVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationLeftTitleBtn.hidden = YES;
    self.navigationLabel.text = @"正在界定";
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;
    
    htTableView = [[HVTableView alloc] initWithFrame:CGRectMake(0,addHight+44,320,self.view.bounds.size.height-65) expandOnlyOneCell:YES enableAutoScroll:YES];
    htTableView.HVTableViewDelegate = self;
    htTableView.HVTableViewDataSource = self;
    htTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [htTableView reloadData];
    [self.view addSubview:htTableView];
    
    [self getData];
    
}

-(void)getData
{
    
    
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.weixincustomer.wxcustomerlistpushendwait&BrokerKid=%@&PageIndex=1",theApp.brokerKid];
    
    // NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         NSDictionary *dict2;
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             dict2 = [dict objectForKey:@"data"];
             
             dataSource = [NSMutableArray arrayWithArray:[dict2 objectForKey:@"UserList"]];
             [htTableView reloadData];
         }
         
     }
        didFailedRequest:^(ASIHTTPRequest *request){
    }];
}



#pragma mark - dataSource
//perform your expand stuff (may include animation) for cell here. It will be called when the user touches a cell
-(void)tableView:(UITableView *)tableView expandCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    // 展开
    NSDictionary *dic = dataSource[indexPath.row];
    NSArray *logList = [dic objectForKey:@"LogList"];
    NSString *strLogList = [NSString stringWithFormat:@"%@",logList];
    if (![strLogList isEqualToString:@"<null>"]) {
        [self addLabel:cell logList:logList];
        [UIView animateWithDuration:0.3 animations:^{
            UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
            imageView.frame = CGRectMake(imageView.frame.origin.x, 65 + 20 * logList.count ,imageView.frame.size.width, imageView.frame.size.height);
        }];
    }
}

-(void)addLabel:(UITableViewCell *)cell logList:(NSArray *)logList
{
    for (int n = 0; n < logList.count; n ++) {
        [[cell viewWithTag: n + 1001] removeFromSuperview];
        [[cell viewWithTag: n + 2002] removeFromSuperview];
        [[cell viewWithTag: n + 3003] removeFromSuperview];
        [[cell viewWithTag: n + 5005] removeFromSuperview];
        [[cell viewWithTag: n + 4004] removeFromSuperview];
        
        
        //        UILabel *lable = [[UILabel alloc]initWithFrame:CGRectMake(10, 100 + 20 * n, 200, 16)];
        //        lable.tag = 100 + n;
        //        NSDictionary *dict = logList[n];
        //        lable.text = [NSString stringWithFormat:@"%@   %@",[dict objectForKey:@"F_BuildingKid"],[dict objectForKey:@"F_Remark"]];
        //        [cell addSubview:lable];
        
        UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,65+20*n,12,12)];
        tagImg.tag = 1001+n;
        tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
        [cell addSubview:tagImg];
        
        UIImageView *process_lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3+20*n+65,7,20)];
        process_lineImg.tag=4004+n;
        process_lineImg.image = [UIImage imageNamed:@"process_line.png"];
        [cell addSubview:process_lineImg];
        
        UIImageView *process_pointImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3+20*n+65,7,7)];
        process_pointImg.image = [UIImage imageNamed:@"process_point.png"];
        process_pointImg.tag = 5005+n;
        [cell addSubview:process_pointImg];
        
        UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(45,-15+20*n+65,150,50)];
        tagLabel.tag = 2002+n;
        tagLabel.numberOfLines = 0;
        tagLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        tagLabel.textAlignment = NSTextAlignmentLeft;
        tagLabel.backgroundColor = [UIColor clearColor];
        tagLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [cell addSubview:tagLabel];
        
        UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(320-115,-3+20*n+65,100,20)];
        timeLabel.tag = 3003+n;
        timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        timeLabel.textAlignment = NSTextAlignmentRight;
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [cell addSubview:timeLabel];
        
        NSDictionary *dict = logList[n];
        tagLabel.text = [dict objectForKey:@"F_Remark"];
        timeLabel.text = [dict objectForKey:@"F_AddTime"];
        timeLabel.text = [timeLabel.text stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        timeLabel.text = [timeLabel.text substringToIndex:16];
        
        
    }
}

//perform your collapse stuff (may include animation) for cell here. It will be called when the user touches an expanded cell so it gets collapsed or the table is in the expandOnlyOneCell satate and the user touches another item, So the last expanded item has to collapse
-(void)tableView:(UITableView *)tableView collapseCell:(UITableViewCell *)cell withIndexPath:(NSIndexPath *)indexPath
{
    //关闭
    [UIView animateWithDuration:0.3 animations:^{
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
        imageView.frame = CGRectMake(imageView.frame.origin.x,56 ,imageView.frame.size.width, imageView.frame.size.height);
        imageView.image = [UIImage imageNamed:@"line_show.png"];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isexpanded
{
    //you can define different heights for each cell. (then you probably have to calculate the height or e.g. read pre-calculated heights from an array
    if (isexpanded){
        NSDictionary *dic = dataSource[indexPath.row];
        NSMutableArray *logList = [dic objectForKey:@"LogList"];
        NSString *strLogList = [NSString stringWithFormat:@"%@",logList];
        if (![strLogList isEqualToString:@"<null>"]) {
            return 70+20*logList.count;
        }
    }
    return 65;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath isExpanded:(BOOL)isExpanded
{
    //static NSString *CellIdentifier = @"CustomCellIdentifier";
    NSString *CellIdentifier = [NSString stringWithFormat:@"cell%d",indexPath.row];//以indexPath来唯一确定cell
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell==nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        
        UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
        // headView.backgroundColor= [UIColor whiteColor];
        [cell addSubview:headView];
        
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
        logoImg.tag = 101;
        //            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
        [headView addSubview:logoImg];
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,240,20)];
        titleLabel.tag = 102;
        titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [headView addSubview:titleLabel];
        
        UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
        sexLabel.tag=103;
        sexLabel.textAlignment=NSTextAlignmentCenter;
        sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
        sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        sexLabel.backgroundColor=[UIColor clearColor];
        [headView addSubview:sexLabel];
        
        UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
        phoneLabel.tag = 104;
        phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.backgroundColor = [UIColor clearColor];
        phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
        [headView addSubview:phoneLabel];
        
        UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
        budildingLabel.tag = 105;
        budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
        budildingLabel.textAlignment = NSTextAlignmentLeft;
        budildingLabel.backgroundColor = [UIColor clearColor];
        budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [headView addSubview:budildingLabel];
        
        
        //            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(230,15,12,12)];
        //            tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
        //            [headView addSubview:tagImg];
        //
        //
        //            UILabel *tageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250,11,150,20)];
        //            tageLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
        //            tageLabel.textAlignment = NSTextAlignmentLeft;
        //            tageLabel.text = @"无效客户";
        //            tageLabel.backgroundColor = [UIColor clearColor];
        //            tageLabel.textColor = [AppSession colorWithHexString:@"666666"];
        //            [headView addSubview:tageLabel];
        //
//        
//        UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(250,11,60,20)];
//        clickBtn.tag = indexPath.row+5555;
//        [clickBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
//        [clickBtn setTitle:@"继续推荐" forState:UIControlStateNormal];
//        clickBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:13];
//        [clickBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
//        [clickBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
//        [headView addSubview:clickBtn];
        
        UIImageView *exImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,50,320,9)];
        exImg.tag = 7;
        exImg.image = [UIImage imageNamed:@"line_show.png"];
        [cell.contentView addSubview:exImg];
    }
    
    NSDictionary *dict = dataSource[indexPath.row];
    UIImageView *logoImg = (UIImageView *)[cell viewWithTag:101];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
    UILabel *phoneLabel = (UILabel *)[cell viewWithTag:104];
    UILabel *budildingLabel = (UILabel *)[cell viewWithTag:105];
    
    
    if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
        logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
    else
        logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
    
    
    titleLabel.text = [dict objectForKey:@"F_Title"];
    sexLabel.text = [dict objectForKey:@"F_Sex"];
    phoneLabel.text = [dict objectForKey:@"F_Phone"];
    budildingLabel.text = [dict objectForKey:@"BuildingName"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    if (dataSource.count > 0)
    {
        NSString *strLogList = [NSString stringWithFormat:@"%@",[dict objectForKey:@"LogList"]];
        
        UIImageView *imageView = (UIImageView *)[cell.contentView viewWithTag:7];
        
        [cell.contentView viewWithTag:7].hidden = [strLogList isEqualToString:@"<null>"]?YES:NO;
        NSArray *logList = [dict objectForKey:@"LogList"];
        
        
        for (int n = 0; n < logList.count; n ++) {
            [[cell viewWithTag: n + 1001] removeFromSuperview];
            [[cell viewWithTag: n + 2002] removeFromSuperview];
            [[cell viewWithTag: n + 3003] removeFromSuperview];
            [[cell viewWithTag: n + 5005] removeFromSuperview];
            [[cell viewWithTag: n + 4004] removeFromSuperview];
        }
        
        
        if (![strLogList isEqualToString:@"<null>"])
        {
            
            imageView.frame = CGRectMake( [cell.contentView viewWithTag:7].frame.origin.x,
                                         isExpanded ? (56 + 20*logList.count):56,
                                         [cell.contentView viewWithTag:7].frame.size.width,
                                         [cell.contentView viewWithTag:7].frame.size.height);
            imageView.image = [UIImage imageNamed:@"line_hide.png"];
            if (isExpanded)
            {
                [self addLabel:cell logList:logList];
            }
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


- (void)buttonClicked:(UIButton *)btn
{
    // NSLog(@"%ld",btn.tag);
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
