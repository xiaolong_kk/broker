//
//  CalloutSalonAnnotationView.m
//  cafekon
//
//  Created by 朱晓龙 on 13-4-16.
//  Copyright (c) 2013年 adam_yan. All rights reserved.
//

#import "B_CalloutAnnotationView.h"
#import "B_CalloutAnnotation.h"

@implementation B_CalloutAnnotationView
@synthesize title=title_;
@synthesize delegate=delegate_;
@synthesize _kid;
@synthesize subTitle = subTitle_;
@synthesize img_str;

- (id)initWithAnnotation:(id<MKAnnotation>)annotation
         reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.frame = CGRectMake(0.0f, 5.0f, 240.0f, 65.0f);
        UIImageView *tempImg =[[UIImageView alloc] init];
        tempImg.frame = self.frame;
        tempImg.image = [UIImage imageNamed:@"map_infos.png"];
        self.centerOffset = CGPointMake(-25, -50);
        [self addSubview:tempImg];
        
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView *logoImg =[[UIImageView alloc] init];
        logoImg.tag = 111;
        logoImg.backgroundColor = [UIColor blackColor];
        logoImg.frame = CGRectMake(15,10,45,45);
        [tempImg addSubview:logoImg];
        

        titleLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(75,10,150,20)];
        titleLabel_.textColor = [UIColor blackColor];
        titleLabel_.textAlignment   = NSTextAlignmentLeft;
        titleLabel_.font = [UIFont fontWithName:@"Helvetica"size:15];
        titleLabel_.backgroundColor = [UIColor clearColor];
        [tempImg addSubview:titleLabel_];
        
        subTitleLabel_ = [[UILabel alloc] initWithFrame:CGRectMake(75,30,170,20)];
        subTitleLabel_.textColor = [UIColor blackColor];
        subTitleLabel_.font = [UIFont fontWithName:@"Helvetica"size:11];
        subTitleLabel_.backgroundColor = [UIColor clearColor];
        [tempImg addSubview:subTitleLabel_];
        

        
//        shop_nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(34, 10, 140, 60)];
//        shop_nameLabel.textColor = [UIColor colorWithRed:50.0/255 green:155.0/255 blue:145.0/255 alpha:1.0f];
//        shop_nameLabel.font = [UIFont fontWithName:@"Helvetica"size:15];
//        shop_nameLabel.backgroundColor = [UIColor clearColor];
//        shop_nameLabel.text = @"咖啡馆地址";
//        [tempImg addSubview:shop_nameLabel];
        
        
        button_ = [UIButton buttonWithType:UIButtonTypeCustom];
        button_.frame = CGRectMake(0, 0, 240, 65);
        [tempImg addSubview:button_];
        [button_ addTarget:self action:@selector(buttonClicked) forControlEvents:UIControlEventTouchDown];
        [self addSubview:button_];
    }
    
    return self;
}


-(void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    UIImageView *logoImg = (UIImageView *)[self viewWithTag:111];
    titleLabel_.text = self.title;
    [logoImg setWebImageWithFade:img_str placeholderImage:nil];
    subTitleLabel_.text = [NSString stringWithFormat:@"%@",[subTitle_ description]];
}

#pragma mark - button clicked
- (void)buttonClicked
{
    [delegate_ buttonClicked:_kid];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
