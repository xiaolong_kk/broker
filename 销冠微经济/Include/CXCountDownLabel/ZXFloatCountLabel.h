//
//  ZXFloatCountLabel.h
//  TopSales
//
//  Created by 朱晓龙 on 14-1-9.
//
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class ZXFloatCountLabel;

typedef void (^ZXFloatCountHandler)(ZXFloatCountLabel *label, float currentNumber, BOOL stopped);
@interface ZXFloatCountLabel : UILabel

@property (nonatomic, assign) float startNumber;
@property (nonatomic, assign) float endNumber;
@property (nonatomic, strong, readonly) CADisplayLink *countDownTimer;
@property (nonatomic, assign) float countInterval; // default is 0. when it is 0, use sqrtf(endNumber - startNumber) which is faster.
- (void)setStartNumber:(float)startNumber endNumber:(float)endNumber floatcountHandeler:(ZXFloatCountHandler)floatcountHandeler;

- (void)start;
- (void)pause;
- (void)resume;
@end
