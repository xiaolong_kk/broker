//
//  other_applyVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/27.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "other_applyVC.h"

@implementation other_applyVC

@synthesize calendar;
@synthesize calenderViewBg;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;
    
    self.navigationLabel.text = [NSString stringWithFormat:@"%@申请",self.type_str];
    
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,self.view.bounds.size.height)];
    _scrollView.delegate =self;
    _scrollView.showsVerticalScrollIndicator = NO;
    _scrollView.contentSize = CGSizeMake(320,570);
    [self.view addSubview:_scrollView];
    
    [self setUpMainView];
    [self setUpCalendarView];

}


-(void)setUpMainView

{
    UIImageView *textBg1 = [[UIImageView alloc] init];
    textBg1.userInteractionEnabled = YES;
    textBg1.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets1 = UIEdgeInsetsMake(10,0,10,0);
    textBg1.image = [textBg1.image resizableImageWithCapInsets:insets1];
    textBg1.frame = CGRectMake(15,17.5,294,108);
    [_scrollView addSubview:textBg1];
    
    NSArray *arr = [NSArray arrayWithObjects:@"客户姓名",@"联系电话",@"推荐楼盘", nil];
    for(int i =0;i<3;i++)
    {
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(10,36*i,150,36)];
        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.text= arr[i];
        tempLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [textBg1 addSubview:tempLabel];
        
        if(i<2)
        {
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,36*(i+1),290,1);
            [textBg1 addSubview:lineImg];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(130,36*i,150,36)];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textAlignment = NSTextAlignmentRight;
            titleLabel.text= arr[i];
            titleLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [textBg1 addSubview:titleLabel];
            
            
            if(i==0)
            {
                titleLabel.text= [self.user_dict objectForKey:@"F_Title"];
                
            }
            if(i==1)
            {
                titleLabel.text= [self.user_dict objectForKey:@"F_Phone"];
                
            }
            if(i==2)
            {
                titleLabel.text= [NSString stringWithFormat:@"%@",[self.user_dict objectForKey:@"BuildingName"]];
                
            }

        }
        
    }
    
    UIImageView *textBg2 = [[UIImageView alloc] init];
    textBg2.userInteractionEnabled = YES;
    textBg2.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets2 = UIEdgeInsetsMake(10,10,10,10);
    textBg2.image = [textBg2.image resizableImageWithCapInsets:insets2];
    textBg2.frame = CGRectMake(15,textBg1.frame.origin.y+textBg1.frame.size.height+10,294,36);
    [_scrollView addSubview:textBg2];
    
    UIButton *tagBtn2=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn2.frame=CGRectMake(0,0,320,36);
    [tagBtn2 setImage:[UIImage imageNamed:@"date_logo.png"] forState:UIControlStateNormal];
    tagBtn2.imageEdgeInsets = UIEdgeInsetsMake(0,-220,0,0);
    [tagBtn2 setTitle:@"到访时间"forState:UIControlStateNormal];
    [tagBtn2 setTitleColor:[AppSession colorWithHexString:@"999999"] forState:UIControlStateNormal];
    [tagBtn2 addTarget:self action:@selector(showCalendarView) forControlEvents:UIControlEventTouchUpInside];
    tagBtn2.titleLabel.font = [UIFont systemFontOfSize:14];
    tagBtn2.titleEdgeInsets = UIEdgeInsetsMake(0,-200,0,0);
    [textBg2 addSubview:tagBtn2];
    
    
    mid_view = [[UIView alloc] initWithFrame:CGRectMake(0,textBg2.frame.origin.y+textBg2.frame.size.height+10,320,85)];
    mid_view.userInteractionEnabled = YES;
    [_scrollView addSubview:mid_view];
    
    UIImageView *textBg3 = [[UIImageView alloc] init];
    textBg3.userInteractionEnabled = YES;
    textBg3.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets3 = UIEdgeInsetsMake(10,10,10,10);
    textBg3.image = [textBg3.image resizableImageWithCapInsets:insets3];
    textBg3.frame = CGRectMake(15,0,294,36);
    [mid_view addSubview:textBg3];
    
    UIImageView *money_textImg = [[UIImageView alloc] init];
    money_textImg.userInteractionEnabled = YES;
    money_textImg.image = [UIImage imageNamed:@"moeny.png"];
    money_textImg.frame = CGRectMake(15,10,11,13);
    [textBg3 addSubview:money_textImg];

    UITextField *money_textField = [[UITextField alloc] initWithFrame:CGRectMake(37,0,200,36)];
    money_textField.font=[UIFont fontWithName:stringFontOfSize size:14];
    money_textField.backgroundColor = [UIColor clearColor];
//    money_textField.text= @"是否在现场";
    money_textField.placeholder = [NSString stringWithFormat:@"%@金额",self.type_str];
    money_textField.textColor = [AppSession colorWithHexString:@"000000"];
    [textBg3 addSubview:money_textField];
    
    UIImageView *textBg4 = [[UIImageView alloc] init];
    textBg4.userInteractionEnabled = YES;
    textBg4.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets4 = UIEdgeInsetsMake(10,10,10,10);
    textBg4.image = [textBg4.image resizableImageWithCapInsets:insets4];
    textBg4.frame = CGRectMake(15,textBg3.frame.origin.y+textBg3.frame.size.height+10,294,36);
    [mid_view addSubview:textBg4];
    
    UIImageView *locationImg = [[UIImageView alloc] init];
    locationImg.userInteractionEnabled = YES;
    locationImg.image = [UIImage imageNamed:@"location.png"];
    locationImg.frame = CGRectMake(15,10,15,15);
    [textBg4 addSubview:locationImg];
   
    NSArray *arr1 =[NSArray arrayWithObjects:@"幢",@"单元",@"号", nil];
    for(int i=0;i<3;i++)
    {
        UIImageView *textBgImg = [[UIImageView alloc] initWithFrame:CGRectMake(i*80+40,6,39,23)];
        textBgImg.image = [UIImage imageNamed:@"moeny_text.png"];
        [textBg4 addSubview:textBgImg];
        
        UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*80+85,8,30,20)];
        tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
        tempLabel.backgroundColor = [UIColor clearColor];
        tempLabel.text= arr1[i];
        tempLabel.textColor = [AppSession colorWithHexString:@"999999"];
        [textBg4 addSubview:tempLabel];

    }
    
    mid_view2 = [[UIView alloc] initWithFrame:CGRectMake(0,mid_view.frame.origin.y+mid_view.frame.size.height+10,320,145)];
    mid_view2.userInteractionEnabled = YES;
    [_scrollView addSubview:mid_view2];
    
    UIImageView *textBg6 = [[UIImageView alloc] init];
    textBg6.userInteractionEnabled = YES;
    textBg6.image = [UIImage imageNamed:@"kuang.png"];
    UIEdgeInsets insets6 = UIEdgeInsetsMake(10,10,10,10);
    textBg6.image = [textBg6.image resizableImageWithCapInsets:insets6];
    textBg6.frame = CGRectMake(15,0,294,91);
    [mid_view2 addSubview:textBg6];
    
    _textView = [[UITextView alloc] initWithFrame:CGRectMake(0,0,290,91)];
    _textView.delegate = self;
    _textView.backgroundColor = [UIColor clearColor];
    [textBg6 addSubview:_textView];
    
    UIView *placeholderView = [[UIView alloc] initWithFrame:CGRectMake(0,5,320,20)];
    //placeholderView.userInteractionEnabled = YES;
    // placeholderView.backgroundColor = [UIColor blackColor];
    [_textView addSubview:placeholderView];
    
    UIImageView *tagImg3 = [[UIImageView alloc] initWithFrame:CGRectMake(10,0,15,16)];
    tagImg3.image = [UIImage imageNamed:@"custom_pen.png"];
    [placeholderView addSubview:tagImg3];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(35,0,250,20)];
    tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    tempLabel.backgroundColor = [UIColor clearColor];
    tempLabel.text= @"填写备注";
    tempLabel.textColor = [AppSession colorWithHexString:@"999999"];
    [placeholderView addSubview:tempLabel];
    
    UIButton *add_customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    add_customBtn.frame = CGRectMake(10,textBg6.frame.size.height+textBg6.frame.origin.y+15,300,40);
    [add_customBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [add_customBtn setTitle:@"确认申请" forState:UIControlStateNormal];
    add_customBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
    [add_customBtn addTarget:self action:@selector(subCustomInfo) forControlEvents:UIControlEventTouchUpInside];
    [add_customBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [mid_view2 addSubview:add_customBtn];
    
    
}


-(void)subCustomInfo
{
    NSLog(@"%@",self.user_dict);
    
    NSMutableDictionary *param=[[NSMutableDictionary alloc]initWithCapacity:10];
    [param setValue:theApp.brokerKid forKey:@"BrokerKid"];
    [param setValue:[self.user_dict objectForKey:@"Kid"] forKey:@"Kid"];
    [param setValue:[self.user_dict objectForKey:@"F_BuildingKid"] forKey:@"Bid"];
    [param setValue:self.dateString forKey:@"HandelTime"];
    [param setValue:self.state_str forKey:@"type"];
    [param setValue:_textView.text forKey:@"F_Remark"];
    
    [self startSynchronousPostRequest:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinagreement.xxweixinbrokerapply" postData:param target:self.view alertText:@"客户信息提交中..." didFinishRequest:^(ASIHTTPRequest *request)
     {
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
             [self.navigationController popViewControllerAnimated:YES];
         
         [self showMBAlertView:self.view message:[dict objectForKey:@"msg"] duration:1.5];
         
     } didFailedRequest:^(ASIHTTPRequest *request)
     {
         [self baseDidFailedRequest:request];
         
     }];
}

-(void)setUpCalendarView
{
    calenderViewBg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 568)];
    calenderViewBg.userInteractionEnabled = YES;
    calenderViewBg.hidden = YES;
    
    calenderViewBg.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
    UIView *calenderView = [[UIView alloc] initWithFrame:CGRectMake(0,100,320, 300)];
    calendar.userInteractionEnabled  =YES;
    calenderView.userInteractionEnabled  =YES;
    calendar = [[VRGCalendarView alloc] init];
    calendar.delegate=self;
    
    UIButton *selectBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    selectBtn.frame=CGRectMake(0,0,320,500);
    [selectBtn addTarget:self action:@selector(hiddenView) forControlEvents:UIControlEventTouchUpInside];
    [calenderViewBg addSubview:selectBtn];
    
    [calenderViewBg addSubview:calenderView];
    [calenderView addSubview:calendar];
    [theApp.window addSubview:calenderViewBg];
}


-(void)showCalendarView
{
    [AppSession fadeIn:calenderViewBg];
    
}

-(void)hiddenView
{
    [AppSession fadeOut:calenderViewBg];
    
}


#pragma mark-- VRGCalendarViewDelegate

-(void)calendarView:(VRGCalendarView *)calendarView switchedToMonth:(int)month targetHeight:(float)targetHeight animated:(BOOL)animated
{
    
    NSArray *dates = [NSArray arrayWithObjects:[NSNumber numberWithInt:1],[NSNumber numberWithInt:5], nil];
    [calendarView markDates:dates];
}

-(void)calendarView:(VRGCalendarView *)calendarView dateSelected:(NSDate *)date
{
    NSDateFormatter *formatter3 =[[NSDateFormatter alloc]init];
    [formatter3 setDateFormat:@"yyyy-MM-dd"];
    self.dateString = [formatter3 stringFromDate:date];
    [self hiddenView];
}


@end
