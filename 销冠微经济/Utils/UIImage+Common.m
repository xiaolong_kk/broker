//
//  Swizzling.m
//  TopSales
//
//  Created by zen huang on 14-4-29.
//
//

#import "UIImage+Common.h"
#import <objc/runtime.h>
#import <objc/message.h>


@implementation UIImage (Common)

// load 在初始化类时调用，每个类都有一个load 方法，
// 类的初始化先于对象
+(void)load
{
    //以下方法就告诉系统用后面的方法替换前面的
    method_exchangeImplementations(class_getClassMethod(self, @selector(imageNamed:)),
                                   class_getClassMethod(self, @selector(myImageNamed:)));
}

+(UIImage*)myImageNamed: (NSString*)imageName
{
    // 图片要支持jpg 和png
    NSString *path = [[NSBundle mainBundle] pathForResource:imageName ofType:@""];
    
    if (path == nil)
    {
        path = [[NSBundle mainBundle] pathForResource:imageName ofType:@""];
    }
    
    // 这里image 在回收后不会占用cash 内存
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    
    return image;
}


@end
