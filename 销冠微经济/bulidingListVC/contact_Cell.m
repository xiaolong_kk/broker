//
//  contact Cell.m
//  test_1
//
//  Created by liu on 14-10-12.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import "contact_Cell.h"

@implementation contact_Cell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        _selectImage = [[UIImageView alloc]initWithFrame:CGRectMake(10, 6, 30, 30)];
     
        _selectImage.image = [UIImage imageNamed:@"icon-checkbox-unselected-25x25"];
        [self addSubview:_selectImage];
        
        _nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 10, 100, 20)];
        _nameLabel.font = [UIFont boldSystemFontOfSize:16];
        [self addSubview:_nameLabel];
        
        _phoneLabel = [[UILabel alloc]initWithFrame:CGRectMake(160, 10, 160, 20)];
        _phoneLabel.font = [UIFont systemFontOfSize:15];
        _phoneLabel.textColor = [UIColor grayColor];
        [self addSubview:_phoneLabel];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
