//
//  bulidingListVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/15.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "bulidingListVC.h"
#import "DropDown.h"
#import "buildingListCell.h"
#import "buildingDetailVC.h"
#import "tableViewLsitVC.h"
@interface bulidingListVC ()

@end

@implementation bulidingListVC
@synthesize tableArray,xiaolei_arr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if([self.type_str isEqualToString:@"all"])
    self.navigationLeftTitleBtn.hidden = YES;
    else
    self.navigationLeftTitleBtn.hidden = NO;
   
    self.navigationLabel.text = @"杭州";
   
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;

    //右边按钮
    self.navigationRightTitleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    self.navigationRightTitleBtn.frame=CGRectMake(266,-6,60,60);
    [self.navigationRightTitleBtn setImage:[UIImage imageNamed:@"icon_1.png"] forState:UIControlStateNormal];
    [self.navigationRightTitleBtn addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBg addSubview:self.navigationRightTitleBtn];

    //[self setData];
    NSArray *arr = [[NSArray alloc]initWithObjects:@"排序",@"类型",@"区域", nil];
   
    headView = [[UIView alloc] initWithFrame:CGRectMake(0,44+addHight,320,100)];
    [self.view addSubview:headView];
    UIImageView *btnBgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,36)];
    btnBgImg.userInteractionEnabled = YES;
    btnBgImg.image = [UIImage imageNamed:@"tab_bg.png"];
    [headView addSubview:btnBgImg];
    
    bar=[[CustomSegmentControlBar alloc] initWithDelegate:self totalCount:3 frame:CGRectMake(0,0,320,36) tag:1111];
    bar.titleArray = arr;
    bar.selectedIndex = 4;
    [btnBgImg addSubview:bar];

    [self setData];
    
   // [self getData];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,80+addHight,320,0)];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.hidden = YES;
    _tableView.showsVerticalScrollIndicator = NO;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [[[UIApplication sharedApplication] keyWindow] addSubview:_tableView];
    
    
    _tableView1 = [[UITableView alloc] initWithFrame:CGRectMake(0,0,160,160)];
    _tableView1.delegate = self;
    _tableView1.dataSource = self;
    // _tableView1.backgroundColor = [UIColor blackColor];
    // _tableView1.hidden = YES;
    _tableView1.showsVerticalScrollIndicator = NO;
    _tableView1.separatorStyle=UITableViewCellSeparatorStyleNone;
//    [[[UIApplication sharedApplication] keyWindow] addSubview:_tableView1];
   
    
    _tableView2 = [[UITableView alloc] initWithFrame:CGRectMake(160,0,160,160)];
    _tableView2.delegate = self;
    _tableView2.dataSource = self;
    // _tableView2.hidden = YES;
    _tableView2.showsVerticalScrollIndicator = NO;
    _tableView2.separatorStyle=UITableViewCellSeparatorStyleNone;
//    [[[UIApplication sharedApplication] keyWindow] addSubview:_tableView2];

    table_view = [[UIView alloc] initWithFrame:_tableView.frame];
    [table_view addSubview:_tableView1];
    [table_view addSubview:_tableView2];
    table_view.hidden = YES;
    [[[UIApplication sharedApplication] keyWindow] addSubview:table_view];
    
    
    picker = [[HZAreaPickerView alloc] initWithStyle:HZAreaPickerWithStateAndCity delegate:self];
    picker.frame = _tableView.frame;
    picker.hidden = YES;
    [[[UIApplication sharedApplication] keyWindow] addSubview:picker];

    cell_index1 = cell_index2 = cell_index3= cell_index4= cell_index5= 999;
    
    //初始化搜索条
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,btnBgImg.frame.origin.y+btnBgImg.frame.size.height, 320, 44)];
    [mySearchBar setPlaceholder:@"请输入楼盘名称"];
    mySearchBar.barTintColor=[AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"s_search_input.png"] forState:UIControlStateNormal];
    //mySearchBar.userInteractionEnabled = NO;
    mySearchBar.tintColor=[AppSession colorWithHexString:@"84c445"];
    mySearchBar.delegate = self;
    [headView addSubview:mySearchBar];
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,40, 320, 5)];
    tempView.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView];

    float otherAdd = 50;
    if([self.type_str isEqualToString:@"all"])
        otherAdd = 100;
    
    budingListTableView=[[UITableView alloc] initWithFrame:CGRectMake(0,headView.frame.size.height+headView.frame.origin.y-26,320, 400+2*addHeight1-otherAdd)];
    budingListTableView.delegate = self;
    budingListTableView.dataSource = self;
    budingListTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    //tableViewController.table.rowHeight = 100;
    budingListTableView.backgroundColor = [AppSession colorWithHexString:@"f5f5f5"];
    
    [self.view addSubview:budingListTableView];
    [self addHeader];
    
    mapView = [[mapLsitVC alloc ]initWithNavigationBar:YES];

}

-(void)buttonPressed:(UIButton *)sneder
{
    mapView.dataSource = [NSMutableArray arrayWithArray:dataSource];
    [theApp.tabBarController.navigationController pushViewController:mapView animated:YES];
    

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSString *text_str =  [searchBar.text stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.building.getbiuldingbyareandtypelist&BrokerKid=%@&search=1&key=%@",theApp.brokerKid,text_str];
    
   // NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSArray *arr = [dict objectForKey:@"data"];
             dataSource = [NSMutableArray arrayWithArray:arr];
         }
         bar.selectedIndex=4;
         
         [budingListTableView reloadData];
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
    
    [AppSession resignKeyBoardInView:self.view];


}


-(void)setData
{
    self.xiaolei_arr = [[NSMutableArray alloc] init];
    self.jihe_arr = [[NSMutableArray alloc] init];
    
    self.Qu_arr = [[NSMutableArray alloc] init];
    self.Bankuai_arr = [[NSMutableArray alloc] init];

    self.tableArray = [self.userDefault objectForKey:@"sortData"];
        
   self.tableArray2 = [self.userDefault objectForKey:@"wuyeData"];
    self.jihe_arr = [self.tableArray2[0] objectForKey:@"jihe"];

    
    self.tableArray3 = [self.userDefault objectForKey:@"quyuListData"];
    self.Bankuai_arr = [self.tableArray3[0] objectForKey:@"Bankuai"];
    
  //  NSLog(@"%@",self.tableArray3);


}

- (void)addHeader
{
    
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = budingListTableView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        [self performSelector:@selector(getData) withObject:refreshView afterDelay:0.0];
        
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:0.5];
        
        //NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        //NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                //NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
                //NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
               // NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    [header beginRefreshing];
    _header = header;
}
- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [budingListTableView reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

-(void)searchData
{
   NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.building.getbiuldingbyareandtypelist&BrokerKid=%@&sort=%@&WuYeType=%@&AreaId=%@",theApp.brokerKid,sort,WuYeType,AreaId];
    
    NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
             NSDictionary *dict= [request.responseString objectFromJSONString];
         
             if([[dict objectForKey:@"code"] intValue] ==0)
             {
                 NSArray *arr = [dict objectForKey:@"data"];
                 dataSource = [NSMutableArray arrayWithArray:arr];
             }
                 bar.selectedIndex=4;

                [budingListTableView reloadData];

     }
            didFailedRequest:^(ASIHTTPRequest *request){
                     
    }];

}

-(void)getData
{
 
    if([self.type_str isEqualToString:@"all"])
    self.type_str = nil;
   
    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.building.getbiuldingbyareandtypelist&BrokerKid=%@&type=%@",theApp.brokerKid,self.type_str];
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:str]];
    request.timeOutSeconds=TIME_OUT_SECOND;
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSuccess:)];
    //[request setDidFailSelector:@selector(requestFail:)];
    [request startAsynchronous];
}

-(void)requestSuccess:(ASIFormDataRequest *)request
{
    NSDictionary *dict= [request.responseString objectFromJSONString];
    if([[dict objectForKey:@"code"] intValue] ==0)
    {
        NSArray *arr = [dict objectForKey:@"data"];
        dataSource = [NSMutableArray arrayWithArray:arr];
    }
    [budingListTableView reloadData];
    
}

-(void)dropdown2
{
    
    if(sortView1==nil)
    {
        sortView1=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
        UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0,100,320, theApp.window.bounds.size.height)];
        shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        [sortView1 addSubview:shadowView];
        sortView1.hidden = YES;
        [[[UIApplication sharedApplication] keyWindow] addSubview:sortView1];
        
        UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,44,320,self.view.bounds.size.height)];
        [bgBtn addTarget:self action:@selector(ViewHidden2:) forControlEvents:UIControlEventTouchUpInside];
        [sortView1 addSubview:bgBtn];
        [sortView1 addSubview:table_view];

    }
    table_view.hidden=NO;

    CGRect frame = table_view.frame;
    frame.size.height = 0;
    table_view.frame = frame;
    frame.size.height = 250;
    [UIView beginAnimations:@"pushAnimation" context:nil];
    [UIView setAnimationDuration: 0.3];
    table_view.frame = frame;
    [UIView commitAnimations];
    
//    [_tableView1 reloadData];
   // [_tableView2 reloadData];

    if(sortView1.hidden)
    {
        sortView1.hidden=NO;
        [self fadeIn:sortView1];
        
    }
    else
    {
        [self fadeOut:sortView1];
        
    }
    
    
    [_tableView1 reloadData];
    [_tableView2 reloadData];

    
    
    
}


-(void)dropdown
{
    float height = 0;
    height = self.tableArray.count*40;
  
    if(sortView==nil)
   {
        sortView=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
        UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0,100,320, theApp.window.bounds.size.height)];
        shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        [sortView addSubview:shadowView];
        sortView.hidden = YES;
        [[[UIApplication sharedApplication] keyWindow] addSubview:sortView];
        
        UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,44,320,self.view.bounds.size.height)];
        [bgBtn addTarget:self action:@selector(ViewHidden:) forControlEvents:UIControlEventTouchUpInside];
        [sortView addSubview:bgBtn];
    [sortView.superview bringSubviewToFront:_tableView];
}
    _tableView.hidden=NO;
    CGRect frame = _tableView.frame;
    frame.size.height = 0;
    _tableView.frame = frame;
    frame.size.height = height;
    [UIView beginAnimations:@"pushAnimation" context:nil];
    [UIView setAnimationDuration: 0.3];
    _tableView.frame = frame;
    [UIView commitAnimations];

    if(sortView.hidden)
    {
        sortView.hidden=NO;
        [self fadeIn:sortView];
        
    }
    else
    {
        [self fadeOut:sortView];
        
    }

    [_tableView reloadData];
   
}

- (void)fadeIn:(UIView *)view
{
    view.transform = CGAffineTransformMakeScale(1, 1);
    view.alpha = 0;
    [UIView animateWithDuration:.3 animations:^{
        view.alpha = 1;
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.hidden = NO;
    }];
    
}

-(void)sort_ViewHidden:(id)sender
{
    CGRect frame = picker.frame;
    frame.size.height = 200;
    picker.frame = frame;
    frame.size.height = 0;
    [UIView beginAnimations:@"pushAnimation" context:nil];
    [UIView setAnimationDuration: 0.3];
    picker.frame = frame;
    [UIView commitAnimations];
    [self fadeOut:sortView1];
    picker.hidden = YES;
    bar.selectedIndex=4;
    
    
}
-(void)ViewHidden:(id)sender
{
    CGRect frame = _tableView.frame;
    frame.size.height = tableArray.count*40;
    _tableView.frame = frame;
    frame.size.height = 0;
    [UIView beginAnimations:@"pushAnimation" context:nil];
    [UIView setAnimationDuration: 0.3];
    _tableView.frame = frame;
    [UIView commitAnimations];
    bar.selectedIndex=4;
    [self fadeOut:sortView];
}

-(void)ViewHidden2:(id)sender
{
    CGRect frame1 = table_view.frame;
    frame1.size.height = 250;
    table_view.frame = frame1;
    frame1.size.height = 0;
    [UIView beginAnimations:@"pushAnimation" context:nil];
    [UIView setAnimationDuration: 0.3];
    table_view.frame = frame1;
    [UIView commitAnimations];
    bar.selectedIndex=4;
    [self fadeOut:sortView1];

}
- (void)fadeOut:(UIView *)view
{
    [UIView animateWithDuration:.3 animations:^{
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.alpha = 0.0;
        // [view removeFromSuperview];
        
    } completion:^(BOOL finished) {
        if (finished) {
            view.hidden = YES;
        }
    }];
}

- (void) showPicker
{
    if(sortView1==nil)
    {
    sortView1=[[UIView alloc] initWithFrame:CGRectMake(0,0, 320,theApp.window.bounds.size.height)];
        sortView1.hidden = YES;
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 100,320, theApp.window.bounds.size.height)];
        shadowView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5f];
        [sortView1 addSubview:shadowView];
        [[[UIApplication sharedApplication] keyWindow] addSubview:sortView1];
        
        UIButton *bgBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,44,320,self.view.bounds.size.height)];
        [bgBtn addTarget:self action:@selector(sort_ViewHidden:) forControlEvents:UIControlEventTouchUpInside];
        [sortView1 addSubview:bgBtn];
        
    [sortView1.superview bringSubviewToFront:picker];
    }
    picker.hidden = NO;
    CGRect frame = picker.frame;
    frame.size.height = 0;
    picker.frame = frame;
    frame.size.height = 200;
    frame.origin.y=100;
    [UIView beginAnimations:@"pushAnimation" context:nil];
    [UIView setAnimationDuration: 0.3];
    picker.frame = frame;
    [UIView commitAnimations];
    
    if(sortView1.hidden)
    {
        sortView1.hidden=NO;
        [self fadeIn:sortView1];
        
    }
    else
    {
        [self fadeOut:sortView1];
        
    }
    
}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [AppSession resignKeyBoardInView:self.view];
    }
}

#pragma mark - customSegmentBar回调
- (void)initSegmentButton:(CustomSegmentControlBar *)sender btn:(UIButton *)btn index:(int)index
{
    [btn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [btn setTitleColor:[AppSession colorWithHexString:@"477a8f"] forState:UIControlStateSelected];
    btn.titleEdgeInsets = UIEdgeInsetsMake(0,-30,0,0);
    btn.imageEdgeInsets = UIEdgeInsetsMake(0,65,0,0);
    [btn setImage:[UIImage imageNamed:@"arrow_down.png"] forState:UIControlStateNormal];
    [btn setImage:[UIImage imageNamed:@"arrow_up.png"] forState:UIControlStateSelected];
    btn.titleLabel.font=[UIFont systemFontOfSize:15];
    
}

- (void)selectButton:(CustomSegmentControlBar *)sender btn:(UIButton *)btn index:(int)index
{
    
    if(index==0)
    {
        [self dropdown];

    }
    if(index==1)
    {
        self.selectInt=1;
        [self dropdown2];
//        tableViewLsitVC *vc  =[[tableViewLsitVC alloc] initWithFrame:CGRectMake(0,100,320,250)];
//        [theApp.window addSubview:vc.view];


    }
    if(index==2)
    {
        self.selectInt=2;
        [self dropdown2];
    }
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if(tableView==_tableView)
    {
        return  self.tableArray.count;
    }
    if(tableView==_tableView1)
    {
        if(self.selectInt==1)
        return  self.tableArray2.count;
        else
        return self.tableArray3.count;
    }
    if(tableView==_tableView2)
    {
        if(self.selectInt==1)
        return  self.jihe_arr.count;
        else
        return self.Bankuai_arr.count;

    }
    
    if(tableView==budingListTableView)
    {
        return dataSource.count;
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if(tableView==_tableView)
    {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,39,320,1);
        [cell addSubview:lineImg];
        
        UIImageView *checkImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_check.png"]];
        checkImg.hidden = YES;
        checkImg.tag = 111;
        checkImg.frame = CGRectMake(275,15,18,13);
        [cell addSubview:checkImg];

    }
        
        
        UIImageView *checkImg = (UIImageView *)[cell viewWithTag:111];
    NSDictionary *dict;
    NSInteger selected_cell = 0;

    
        selected_cell = cell_index1;
     dict = self.tableArray[indexPath.row];
        cell.textLabel.text = [dict objectForKey:@"SortName"];
    
      
    if(selected_cell==indexPath.row)
    {
        cell.textLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
        checkImg.hidden = NO;
        
    }
    else
    {
        cell.textLabel.textColor = [AppSession colorWithHexString:@"000000"];
        checkImg.hidden = YES;

    }
    
    cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    return cell;
    }
    
    if(tableView==budingListTableView)
    {
        static NSString *CellIdentifier = @"Cell";
        
        buildingListCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[buildingListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
        }
        
        NSDictionary *dict = dataSource[indexPath.row];
        [cell setData:dict];
    
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;

    }
    if(tableView==_tableView1)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,39,320,1);
            [cell addSubview:lineImg];
            
            
            UIImageView *checkImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_check.png"]];
            checkImg.hidden = YES;
            checkImg.tag = 111;
            checkImg.frame = CGRectMake(130,15,18,13);
            [cell addSubview:checkImg];
            
        }
        
        UIImageView *checkImg = (UIImageView *)[cell viewWithTag:111];
        NSDictionary *dict;
        NSInteger selected_cell = 0;

        if(self.selectInt==1)
        {
        dict = [self.tableArray2[indexPath.row] objectForKey:@"xiaolei"];
            selected_cell = cell_index2;
         
        }
        else
        {
        dict = [self.tableArray3[indexPath.row] objectForKey:@"Qu"];
            selected_cell = cell_index3;
        }

        if(selected_cell==indexPath.row)
        {
            cell.textLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
            checkImg.hidden = NO;
            
        }
        else
        {
            cell.textLabel.textColor = [AppSession colorWithHexString:@"000000"];
            checkImg.hidden = YES;
            
        }
        
        cell.textLabel.text = [dict objectForKey:@"ParameterValue"];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    
    if(tableView==_tableView2)
    {
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,39,320,1);
            [cell addSubview:lineImg];
            
            UIImageView *checkImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_check.png"]];
            checkImg.hidden = YES;
            checkImg.tag = 111;
            checkImg.frame = CGRectMake(130,15,18,13);
            [cell addSubview:checkImg];
            
        }
        
        UIImageView *checkImg = (UIImageView *)[cell viewWithTag:111];
        NSDictionary *dict;
        NSInteger selected_cell = 0;

        if(self.selectInt==1)
        {
        dict =self.jihe_arr[indexPath.row];
            selected_cell = cell_index4;

        }
        else
        {
        dict=self.Bankuai_arr[indexPath.row];
            selected_cell = cell_index5;
        }
        
        if(selected_cell==indexPath.row)
        {
            cell.textLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
            checkImg.hidden = NO;
            
        }
        else
        {
            cell.textLabel.textColor = [AppSession colorWithHexString:@"000000"];
            checkImg.hidden = YES;
            
        }

        cell.textLabel.text = [dict objectForKey:@"ParameterValue"];
        cell.textLabel.font = [UIFont systemFontOfSize:15.0f];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        
        return cell;
        
    }
    
    return nil;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(tableView==budingListTableView)
    {
        return 130;
    }
    else
    {
        return 40;
    }

    return 0;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_tableView)
    {
        NSDictionary *dict = self.tableArray[indexPath.row];
        cell_index1 = indexPath.row;
        sort=[dict objectForKey:@"SortValue"];
        [self searchData];
        [_tableView reloadData];
        
        CGRect frame = _tableView.frame;
        frame.size.height = tableArray.count*40;
        _tableView.frame = frame;
        frame.size.height = 0;
        [UIView beginAnimations:@"pushAnimation" context:nil];
        [UIView setAnimationDuration: 0.3];
        _tableView.frame = frame;
        [UIView commitAnimations];

        [self fadeOut:sortView];

        
    }
    
    
    if(tableView==_tableView1)
    {
        if(self.selectInt==1)
        {
        self.jihe_arr = [self.tableArray2[indexPath.row] objectForKey:@"jihe"];
            cell_index2 = indexPath.row;
        }
        else
        {
        self.Bankuai_arr = [self.tableArray3[indexPath.row] objectForKey:@"Bankuai"];
            cell_index3 = indexPath.row;

        }
        [_tableView1 reloadData];
        [_tableView2 reloadData];

    }
    
    if(tableView==_tableView2)
    {
        NSDictionary *dict;
        
        if(self.selectInt==1)
        {
            dict =self.jihe_arr[indexPath.row];
            
            WuYeType = [dict objectForKey:@"Kid"];
            cell_index4 = indexPath.row;

        }
        else
        {
            dict = self.Bankuai_arr[indexPath.row];
           // NSLog(@"%@",dict);
            AreaId = [dict objectForKey:@"Kid"];
            cell_index5 = indexPath.row;

        }
        
        [self searchData];
        
        [self fadeOut:sortView1];

    }
    
    if(tableView==budingListTableView)
    {
        NSDictionary *dict = dataSource[indexPath.row];
        buildingDetailVC *vc = [[buildingDetailVC alloc] initWithNavigationBar:YES];
        vc.buildingKid_str = [dict objectForKey:@"Kid"];
        
        [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
    }
    
}


//- (NSString *)refreshBehindStr:(int)currentPage currentDataArray:(NSArray *)currentDataArray
//{
//    //    NSLog(@"refresh behind %d",currentPage);
//    
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
