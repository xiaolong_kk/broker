//
//  addCell.m
//  销冠微经济
//
//  Created by zen huang on 14/9/26.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "addCell.h"

@implementation addCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        tableView_ = [[UITableView alloc]init];
        tableView_.separatorStyle=UITableViewCellSeparatorStyleNone;
        tableView_.delegate = self;
        tableView_.dataSource = self;
        tableView_.backgroundColor = [UIColor blackColor];
        //    tableView_.delegate_extend = self;
        
        [self addSubview:tableView_];
    }
    UIImageView *exImg = [[UIImageView alloc] init];
    exImg.tag = 108;
    exImg.image = [UIImage imageNamed:@"line_hide.png"];
    [self addSubview:exImg];

    return self;
}

-(void)setData:(NSMutableArray *)arr;
{
    
    UIImageView *exImg = (UIImageView *)[self viewWithTag:108];
    exImg.frame = CGRectMake(0,self.frame.size.height-9,320,9);
    self.temp_arr = [NSMutableArray arrayWithArray:arr];
    arr_count = arr.count;
    tableView_.frame = CGRectMake(0, 10,320, arr.count*20);
    [tableView_ reloadData];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arr_count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
        NSString *CellIdentifier = [NSString stringWithFormat:@"MainCell%ld",indexPath.row];//以indexPath来唯一确定cell
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(10,0,12,12)];
            tagImg.tag = 1001;
            tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
            [cell addSubview:tagImg];
            
        UIImageView *process_lineImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3,7,20)];
        process_lineImg.image = [UIImage imageNamed:@"process_line.png"];
        [cell addSubview:process_lineImg];
            
            UIImageView *process_pointImg = [[UIImageView alloc] initWithFrame:CGRectMake(30,3,7,7)];
            process_pointImg.image = [UIImage imageNamed:@"process_point.png"];
            [cell addSubview:process_pointImg];
            
            UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(45,-15,150,50)];
            tagLabel.tag = 1002;
            tagLabel.numberOfLines = 0;
            tagLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            tagLabel.textAlignment = NSTextAlignmentLeft;
            tagLabel.backgroundColor = [UIColor clearColor];
            tagLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [cell addSubview:tagLabel];
            
            UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(320-115,-3,100,20)];
            timeLabel.tag = 1003;
            timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            timeLabel.textAlignment = NSTextAlignmentRight;
            timeLabel.backgroundColor = [UIColor clearColor];
            timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [cell addSubview:timeLabel];

        }
    
    UIImageView *tagImg = (UIImageView *)[self viewWithTag:1001];
    UILabel *tagLabel = (UILabel *)[self viewWithTag:1002];
    UILabel *timeLabel = (UILabel *)[self viewWithTag:1003];

    NSLog(@"%@",self.temp_arr);

    NSDictionary *dict  = self.temp_arr[indexPath.row];
    tagLabel.text = [dict objectForKey:@"F_Remark"];
    timeLabel.text = [dict objectForKey:@"F_AddTime"];
    timeLabel.text = [timeLabel.text stringByReplacingOccurrencesOfString:@"T" withString:@" "];
    timeLabel.text = [timeLabel.text substringToIndex:16];
   
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    
}


@end
