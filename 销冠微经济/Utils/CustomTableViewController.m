//
//  CustomTableViewController.m
//  baseUtil
//
//  Created by Chai Xue Liang on 12-10-15.
//  Copyright (c) 2012年 xu shun wang. All rights reserved.
//

#import "CustomTableViewController.h"
#import "AppSession.h"
#import "JSONKit.h"
#import "AppDelegate.h"
@interface CustomTableViewController ()

@end

@implementation CustomTableViewController
{
    BOOL _needRefreshBehind;
    CGRect selfViewFrame;
}
@synthesize table;
@synthesize tableDataArray;
@synthesize refreshAllURLStr;
@synthesize noResultStr;
@synthesize customFirstCellHeight;
@synthesize needFirstCellCustom;
@synthesize currrentPage;
@synthesize customRowHeight;
@synthesize isRowHeightVariable;
@synthesize isOddList;
@synthesize delegate;
@synthesize target;
@synthesize isRefreshing;
@synthesize isTableDataCached;
@synthesize cachedTableDataName;
@synthesize requestInPost;
@synthesize refreshAllPostDic;
@synthesize refreshBehindURLStr;


- (id) initWithFrame:(CGRect)frame needRefreshBehind:(BOOL)needRefreshBehind
{
    self=[super init];
    if(self)
    {
        self.hiddenBaseNavigation=YES;
        self.table=[[PullingRefreshTableView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height) pullingDelegate:self];
        self.table.headerOnly=!needRefreshBehind;
        _needRefreshBehind=needRefreshBehind;
        self.table.dataSource=self;
        self.table.delegate=self;
        [self.table setBackgroundColor:[UIColor clearColor]];
        tableDataArray=[[NSMutableArray alloc] init];//不要使用self.tableDataArray,会调用setTableDataArray方法，会重置缓存数据。
//        self.tableDataArray=[NSMutableArray array];
        self.noResultStr=@"加载中...";
        self.customRowHeight=90;
        self.isRowHeightVariable=NO;
        self.isOddList=NO;
        selfViewFrame=frame;
        self.currrentPage=1;
    }
    return self;
}
- (id) initWithFrame:(CGRect)frame needRefreshBehind:(BOOL)needRefreshBehind needHeader:(BOOL) needHeader
{
    self=[super init];
    if(self)
    {
        self.hiddenBaseNavigation=YES;
        self.table=[[PullingRefreshTableView alloc] initWithFrame:CGRectMake(0,0,frame.size.width,frame.size.height) pullingDelegate:self];
        self.table.headerOnly=!needRefreshBehind;
        _needRefreshBehind=needRefreshBehind;
        self.table.needHeader=needHeader;
        self.table.dataSource=self;
        self.table.delegate=self;
        [self.table setBackgroundColor:[UIColor clearColor]];
        tableDataArray=[[NSMutableArray alloc] init];//不要使用self.tableDataArray,会调用setTableDataArray方法，会重置缓存数据
//        self.tableDataArray=[NSMutableArray array];
        self.noResultStr=@"加载中...";
        self.customRowHeight=90;
        self.isRowHeightVariable=NO;
        self.isOddList=NO;
        selfViewFrame=frame;
        self.currrentPage=1;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor= [UIColor clearColor];
    [self.view addSubview:self.table];
    self.view.frame=selfViewFrame;
    //给table加一个tableFooterview，用来增加一点高度，否则列表高度不够
    UIView *foot=[[UIView alloc] initWithFrame:CGRectMake(0,0,320,20)];

    self.table.tableFooterView=foot;
    
    if(self.isTableDataCached&&self.cachedTableDataName.length>0)
    {
        NSMutableArray *mutablArray=[self tableDataArrayFromCacheWithName:self.cachedTableDataName];
        if(mutablArray==nil&&self.table.needHeader)
        {
            [self refreshAll];
        }
        else
        {
            tableDataArray=mutablArray;//不要使用self.tableDataArray,会调用setTableDataArray方法，会重置缓存数据
            [self reloadTableData];
        }
    }
	// Do any additional setup after loading the view.
}


- (void)saveTableDataToLocal
{
    //如果缓存数据开启，则根据名字把数据存数缓存。
    if(self.isTableDataCached&&self.cachedTableDataName.length>0)
    {
        [self saveTableDataArrayToCache:self.cachedTableDataName array:self.tableDataArray];
    }
}
#pragma mark 列表回调
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0&&self.needFirstCellCustom)
    {
        UITableViewCell *cell=[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.frame.size.height;
//        return self.customFirstCellHeight;
    }
        
    if(self.isRowHeightVariable)
    {
        UITableViewCell *cell=[self tableView:tableView cellForRowAtIndexPath:indexPath];
        return cell.frame.size.height;
    }
    else
        return self.customRowHeight;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int i=0;
    if(self.needFirstCellCustom)
        i++;
    if(self.tableDataArray.count>0)
        return self.tableDataArray.count+i;
    else
        return i;//如果结果为0，则出现一行
}
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if([self.delegate respondsToSelector:@selector( customTableView:willShowCell:forRowAtIndexPath:)])
        [self.delegate customTableView:tableView willShowCell:cell forRowAtIndexPath:indexPath];
 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row==0&&self.needFirstCellCustom)
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"customView"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"customView"];
            UIView *customView=nil;
            if([self.delegate respondsToSelector:@selector(customFirstCell:)])
                customView=[self.delegate customFirstCell:tableView];
            [cell addSubview:customView];
            CGRect frame=cell.frame;
            cell.backgroundColor = [UIColor clearColor];
            frame.size.height=self.customFirstCellHeight;
            cell.frame=frame;
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
        }
        
        [cell removeFromSuperview];
        return cell;
    }
    if((indexPath.row==0&&!self.needFirstCellCustom&&self.tableDataArray.count==0)||(indexPath.row==1&&self.needFirstCellCustom&&self.tableDataArray.count==0))
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"noResult"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"noResult"];
            UILabel *noResultsLabel=[[UILabel alloc] initWithFrame:CGRectMake(0,0,self.table.frame.size.width,cell.frame.size.height)];
            noResultsLabel.text=self.noResultStr;
            noResultsLabel.backgroundColor=[UIColor clearColor];
            noResultsLabel.textColor=[AppSession colorWithHexString:@"bfbfbf"];
            noResultsLabel.textAlignment=NSTextAlignmentCenter;
            [cell addSubview:noResultsLabel];
            cell.backgroundColor = [UIColor clearColor];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            self.table.headerOnly=YES;
        }
        
        [cell removeFromSuperview];
        return cell;
    }
    int rowIndex=indexPath.row;
    if(self.needFirstCellCustom)
        rowIndex--;
    if(self.tableDataArray.count==0)
        rowIndex--;
    
    
    if(rowIndex<self.tableDataArray.count)//如果rowIndex比tableDataArray大，会调用出错，
    {
        //        //实际列表项
        //        UITableViewCell *cell=[self.delegate tableView:tableView rowNum:rowIndex dataDictionary:[self.tableDataArray objectAtIndex:rowIndex]];
        
        //实际列表项
        UITableViewCell *cell=[self.delegate tableView:tableView rowNum:rowIndex dataDictionary:[self.tableDataArray objectAtIndex:rowIndex]];
        
        return cell;
    }
    else
    {
        UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"emptyCell"];
        if(cell==nil)
        {
            cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"emptyCell"];
            cell.backgroundColor = [UIColor clearColor];
            
        }
        [cell removeFromSuperview];
        return cell;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //如果newsArray没有数据，会有一行无结果显示文字，这时候不要响应
    if(self.tableDataArray.count==0)
        return;
    if(indexPath.row==0&&self.needFirstCellCustom)
        return;
    
    int rowNum=indexPath.row;
    
    if(self.needFirstCellCustom)
        rowNum-=1;
    
    if([self.delegate respondsToSelector:@selector(didSelectRow:cell:rowNum:dataDictionary:)])
        [self.delegate didSelectRow:tableView cell:[tableView cellForRowAtIndexPath:indexPath] rowNum:rowNum dataDictionary:[self.tableDataArray objectAtIndex:rowNum]];
}
#pragma mark - PullingRefreshTableViewDelegate
//下拉刷新全部
- (void)pullingTableViewDidStartRefreshing:(PullingRefreshTableView *)tableView
{
    self.isRefreshing= YES;
    
    if([self.delegate respondsToSelector:@selector(didStartRefreshAll)])
        [self.delegate performSelector:@selector(didStartRefreshAll) withObject:nil afterDelay:1.f];
    
    if(self.requestInPost)
        [self startAsynchronousPostRequest:self.refreshAllURLStr postData:self.refreshAllPostDic delegate:self didFinishRequest:@selector(didSucceedRefreshAll:) didFailedRequest:@selector(didFailedRefreshAll:)];
    else
        [self startAsynchronousRequest:self.refreshAllURLStr delegate:self didFinishRequest:@selector(didSucceedRefreshAll:) didFailedRequest:@selector(didFailedRefreshAll:)];
}
//上拉获取后面的数据
- (void)pullingTableViewDidStartLoading:(PullingRefreshTableView *)tableView
{
    self.isRefreshing=YES;
    if([self.delegate respondsToSelector:@selector(didStartRefreshBehind)])
        [self.delegate performSelector:@selector(didStartRefreshBehind) withObject:nil afterDelay:1.f];
    
    
    if(self.requestInPost)
    {
        NSDictionary *dic=nil;
        if([self.delegate respondsToSelector:@selector(refreshBehindDictonary:currentDataArray:)])
            dic=[self.delegate refreshBehindDictonary:self.currrentPage currentDataArray:self.tableDataArray];
        [self startAsynchronousPostRequest:[self.refreshBehindURLStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] postData:dic delegate:self didFinishRequest:@selector(didSucceedRefreshBehind:) didFailedRequest:@selector(didFailedRefreshBehind:)];
//        [self startAsynchronousRequest:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] delegate:self didFinishRequest:@selector(didSucceedRefreshBehind:) didFailedRequest:@selector(didFailedRefreshBehind:)];
    }
    else
    {
        if([self.delegate respondsToSelector:@selector(refreshBehindStr:currentDataArray:)])
        {
            self.refreshBehindURLStr=[self.delegate refreshBehindStr:self.currrentPage currentDataArray:self.tableDataArray];
           // NSLog(@"%@",self.refreshBehindURLStr);
            [self startAsynchronousRequest:[self.refreshBehindURLStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] delegate:self didFinishRequest:@selector(didSucceedRefreshBehind:) didFailedRequest:@selector(didFailedRefreshBehind:)];
        }
        else
        {
            [self didFailedRefreshBehind:nil];
        }
    }
}

- (NSDate *)pullingTableViewRefreshingFinishedDate{
//    NSDateFormatter *df = [[NSDateFormatter alloc] init ];
//    df.dateFormat = @"yyyy-MM-dd HH:mm";
    NSDate *date = [NSDate date];
//    [df release];
    return date;
}

#pragma mark - Scroll
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.table tableViewDidScroll:scrollView];
    if([self.delegate respondsToSelector:@selector(tableDidScroll:)])
        [self.delegate tableDidScroll:scrollView];

}


- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
{
    if([self.delegate respondsToSelector:@selector(tableDidEndScrollingAnimation:)])
        [self.delegate tableDidEndScrollingAnimation:scrollView];

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self.table tableViewDidEndDragging:scrollView];
    if([self.delegate respondsToSelector:@selector(tableDidEndDragging:willDecelerate:)])
        [self.delegate tableDidEndDragging:scrollView willDecelerate:decelerate];
}
    
//-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
//{
//        
//}
#pragma mark 其他方法
- (void) refreshAll
{
    [self.table launchRefreshing];
}
- (void) finishRefresh
{
    self.isRefreshing=NO;
    [self.table tableViewDidFinishedLoading];
    self.table.reachedTheEnd  = NO;
    [self reloadTableData];
}
- (void) reloadTableData
{
    //因为在设置了needheader=NO后，在refreshBehind之后再调用refreshAll，视图在reloadData会有位移。所以在此设定。
    CGPoint pos=self.table.contentOffset;
    [self.table reloadData];
    self.table.contentOffset=pos;
    
}
- (void) reloadRowsAtIndexPaths:(NSArray *)indexPaths withRowAnimation:(UITableViewRowAnimation)animation
{
    //因为在设置了needheader=NO后，在refreshBehind之后再调用refreshAll，视图在reloadData会有位移。所以在此设定。
    CGPoint pos=self.table.contentOffset;
    [self.table reloadRowsAtIndexPaths:indexPaths withRowAnimation:animation];
    self.table.contentOffset=pos;
}
- (void) reloadTableDataFromCache
{
    if(self.isTableDataCached&&self.cachedTableDataName.length>0)
    {
        NSMutableArray *mutableArray=[self tableDataArrayFromCacheWithName:self.cachedTableDataName];
        if(mutableArray!=nil)
        {
            tableDataArray=mutableArray;//不要使用self.tableDataArray,会调用setTableDataArray方法，会重置缓存数据
//            NSLog(@"%@",[self.userDefault objectForKey:self.cachedTableDataName]);
            [self reloadTableData];
        }
    }
}
- (NSMutableArray *)tableDataArrayFromCacheWithName:(NSString *)name
{
    NSData *data=[self.userDefault objectForKey:name];
    if(data==nil)
    {
        return nil;
    }
    else
    {
        NSArray *cachedArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return [NSMutableArray arrayWithArray:cachedArray];
    }
}
- (void) saveTableDataArrayToCache:(NSString *)name array:(NSArray *)array//将数组存入缓存，根据名字
{
    NSData *desData = [NSKeyedArchiver archivedDataWithRootObject:array];
    [self.userDefault setObject:desData forKey:name];
}
//下拉刷新全部成功
- (void) didSucceedRefreshAll:(ASIHTTPRequest *)request
{
    NSArray *array=[request.responseString objectFromJSONString];
  
    if(array.count!=0)
    {
        self.tableDataArray=[NSMutableArray arrayWithArray:array];
        [self finishRefresh];
        self.currrentPage=1;
        if(self.tableDataArray.count>0&&_needRefreshBehind)
            self.table.headerOnly=NO;
        if([self.delegate respondsToSelector:@selector(didSucceedRefreshAll)])
            [self.delegate didSucceedRefreshAll];
    }
    else
    {
        [self finishRefresh];
       // [self showBDAlertView:theApp.window message:@"暂时没有信息!" image:[UIImage imageNamed:@"smile.png"] bgImage:[UIImage imageNamed:@"tishi.png"] duration:0.15f];
        
        if([self.delegate respondsToSelector:@selector(didFailedRefreshAll)])
            [self.delegate didFailedRefreshAll];
    }
}
//下拉刷新全部失败
- (void) didFailedRefreshAll:(ASIHTTPRequest *)request
{
    [self finishRefresh];
    
   // [self showBDAlertView:theApp.window message:@"哎呀，网络不给力，稍后再试试吧" image:[UIImage imageNamed:@"tanhao.png"] bgImage:[UIImage imageNamed:@"tishi_large.png"] duration:0.5f];
    
    if([self.delegate respondsToSelector:@selector(didFailedRefreshAll)])
        [self.delegate didFailedRefreshAll];
}

//上拉加载后面数据成功
- (void) didSucceedRefreshBehind:(ASIHTTPRequest *)request
{
    NSArray *array=[request.responseString objectFromJSONString];
    //NSLog(@"%@",array);
    if(array.count!=0)
    {
        [self.tableDataArray addObjectsFromArray:array];
        [self saveTableDataToLocal];
        [self finishRefresh];
       
        if(array.count>0)
            self.currrentPage+=1;
        if([self.delegate respondsToSelector:@selector(didSucceedRefreshBehind)])
            [self.delegate didSucceedRefreshBehind];
    }
    else
    {
        [self finishRefresh];
       // [self showMBAlertView:self.target message:@"加载数据失败" duration:1.5f];
        if([self.delegate respondsToSelector:@selector(didFailedRefreshBehind)])
            [self.delegate didFailedRefreshBehind];
    }
}
//上拉加载后面数据失败
- (void) didFailedRefreshBehind:(ASIHTTPRequest *)request
{
    [self finishRefresh];
   // [self showBDAlertView:self.view message:@"哎呀，网络不给力，稍后再试试吧" image:[UIImage imageNamed:@"icon_mind.png"] duration:1.5f];
    if([self.delegate respondsToSelector:@selector(didFailedRefreshBehind)])
        [self.delegate didFailedRefreshBehind];
}

- (void) setTableDataArray:(NSMutableArray *) _tableDataArray
{
    tableDataArray=_tableDataArray;
    
    if(self.tableDataArray.count>0&&_needRefreshBehind)
        self.table.headerOnly=NO;
    [self saveTableDataToLocal];
}

//-(void) showBDAlertView:(UIWindow *)window message:(NSString *) message image:(UIImage *) image duration:(float) duration
//{
//    BDKNotifyHUD* notify = [BDKNotifyHUD notifyHUDWithImage:image text:message];
//    [window addSubview:notify];
//    notify.center = CGPointMake(window.center.x, window.center.y);
//    [notify presentWithDuration:duration speed:0.3f inView:window completion:^{
//        [notify removeFromSuperview];
//    }];
//    
//}
@end
