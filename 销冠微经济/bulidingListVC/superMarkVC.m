//
//  superMarkVC.m
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-8.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import "superMarkVC.h"

@interface superMarkVC ()

@end

@implementation superMarkVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    float addHeight = 0;
    if(IOS_7)
        addHeight = 20;
    float addHeight2 = 0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight2 = 100;
    self.navigationLabel.text = @"超积金";
    
   NSArray *tempArr = [[NSArray alloc] initWithObjects:@"日期",@"积分描述",@"积分变化",@"总积分",nil];

    for(int i=0;i<4;i++)
    {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:14];
        titleLabel.textColor = [AppSession colorWithHexString:@"999999"];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.text =tempArr[i];
        titleLabel.backgroundColor = [AppSession colorWithHexString:@"e5e5e5"];
        [self.view addSubview:titleLabel];
        
        if(i==0)
            titleLabel.frame= CGRectMake(0,44+addHeight,80,40);
        if(i==1)
            titleLabel.frame= CGRectMake(80,44+addHeight,120,40);
        if(i==2)
            titleLabel.frame= CGRectMake(180,44+addHeight,80,40);
        if(i==3)
            titleLabel.frame= CGRectMake(250,44+addHeight,70,40);


    }
    
     superableView=[[UITableView alloc] initWithFrame:CGRectMake(0,84+addHeight,320,380+addHeight2) style:UITableViewStylePlain];
    superableView.dataSource=self;
    superableView.delegate=self;
    superableView.scrollEnabled = YES;
    superableView.backgroundColor = [UIColor clearColor];
    superableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    superableView.userInteractionEnabled=YES;
    [self.view addSubview:superableView];
    
    [self addHeader];
}


- (void)addHeader
{
    
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = superableView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        [self performSelector:@selector(getData) withObject:refreshView afterDelay:0.0];
        
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:0.5];
        
        //NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        //NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                //NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
                //NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
                // NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    [header beginRefreshing];
    _header = header;
}
- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [superableView reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}

-(void)getData
{
    
    NSString *str = nil;
    
    str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixinbroker.mypointdetail&BrokerKid=%@",theApp.brokerKid];
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:str]];
    request.timeOutSeconds=TIME_OUT_SECOND;
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(requestSuccess:)];
    //[request setDidFailSelector:@selector(requestFail:)];
    [request startAsynchronous];
}

-(void)requestSuccess:(ASIFormDataRequest *)request
{
    NSDictionary *dict= [request.responseString objectFromJSONString];
    if([[dict objectForKey:@"code"] intValue] ==0)
    {
        NSArray *arr = [dict objectForKey:@"data"];
        dataSource = [NSMutableArray arrayWithArray:arr];
    }
    [superableView reloadData];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell

    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        cell.layer.masksToBounds=YES;
        
        cell.backgroundColor = [UIColor clearColor];
        
        
        for(int i=0;i<4;i++)
        {
            UILabel *titleLabel = [[UILabel alloc] init];
            titleLabel.tag = 200+i;
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:12];
            titleLabel.textColor = [AppSession colorWithHexString:@"999999"];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            titleLabel.backgroundColor = [UIColor clearColor];
            [cell addSubview:titleLabel];
            
            if(i==0)
                titleLabel.frame= CGRectMake(0,0,80,40);
            if(i==1)
                titleLabel.frame= CGRectMake(80,0,120,40);
            if(i==2)
                titleLabel.frame= CGRectMake(200,0,50,40);
            if(i==3)
                titleLabel.frame= CGRectMake(250,0,70,40);

        }
        
        UIImageView *shuImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"green.png"]];
        shuImg.frame = CGRectMake(2,1,2,43);
        [cell addSubview:shuImg];

            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,44,320,1);
            [cell addSubview:lineImg];
        
    }
    
    if(dataSource.count>0)
    {
    NSDictionary *dict = dataSource[indexPath.row];
    
    UILabel *titleLabel1 = (UILabel *)[cell viewWithTag:200];
    UILabel *titleLabel2 = (UILabel *)[cell viewWithTag:201];
    UILabel *titleLabel3 = (UILabel *)[cell viewWithTag:202];
    UILabel *titleLabel4 = (UILabel *)[cell viewWithTag:203];
    
    NSString *time_str = [dict objectForKey:@"F_AddTime"];
    time_str = [time_str substringToIndex:10];
    titleLabel1.text = time_str;
    
    titleLabel2.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"F_PointAction"]];

    titleLabel3.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"AddPoint"]];

    titleLabel4.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"PointCount"]];

    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    }
    
    return cell;
    
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
