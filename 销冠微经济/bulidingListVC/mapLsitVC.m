//
//  mapLsitVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/20.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "mapLsitVC.h"
#import "show_g_annotaionView.h"
#import "buildingDetailVC.h"
@interface mapLsitVC ()

@end

@implementation mapLsitVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float  addHight = 0;
    
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;
   self.navigationLabel.text = @"楼盘地图";
   self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 44+addHight, 320, self.view.bounds.size.height)];
    self.mapView.delegate=self;
    self.mapView.userLocation.coordinate=CLLocationCoordinate2DMake(30.274089,120.155069);//设置默认坐标为杭州市
    [self.view addSubview:self.mapView];
    
    self.view.backgroundColor = [AppSession colorWithHexString:@"fbfbfb"];

}


-(void)refreshMapInfo
{
    
    
    CLLocationDegrees  currentLatitude;
    CLLocationDegrees  currentLongitude;
    
    CLLocationCoordinate2D coordinate;

    for (NSDictionary *dict in self.dataSource)
    {
       // NSLog(@"dict   %@",dict);
        
        currentLatitude = [[dict objectForKey:@"F_Latitude"] doubleValue];
        currentLongitude = [[dict objectForKey:@"F_Longitude"] doubleValue];
        coordinate.latitude = currentLatitude;
        coordinate.longitude = currentLongitude;
        _mapAnnotation = [[MapAnnotation alloc] init];
        _mapAnnotation.title  = [dict objectForKey:@"F_Title"];
        _mapAnnotation.sub_title = [dict objectForKey:@"F_WeiXinCommissionEvaluate"];
        _mapAnnotation.img_str = [dict objectForKey:@"F_Logo"];
        _mapAnnotation.coordinate = coordinate;
        _mapAnnotation._kid = [dict objectForKey:@"Kid"];
        // NSLog(@"asdasdad%@",theApp._UrlArray);
        [self.mapView addAnnotation:_mapAnnotation];
    }

}


- (void)mapViewWillStartLocatingUser:(MKMapView *)mapView
{
    MKCoordinateSpan theSpan;
    //地图的范围越小越精确（有效）
    theSpan.latitudeDelta=0.05;
    theSpan.longitudeDelta=0.05;
    MKCoordinateRegion theRegion;
    //获得已经得到的location经纬度信息
    theRegion.center= self.mapView.userLocation.coordinate;
    theRegion.span=theSpan;
    //设置地图显示的区域为当前地址为中心
    [self.mapView setRegion:theRegion];
}

//- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
//{
//    //虽然有时候会显示接受成功，但是实际上没有值，通过userlocation.location是否为nil判断
// 
//    //定位成功就到成功的地方
//    MKCoordinateSpan theSpan;
//    //地图的范围越小越精确（有效）
//    theSpan.latitudeDelta=0.05;
//    theSpan.longitudeDelta=0.05;
//    MKCoordinateRegion theRegion;
//    //获得已经得到的location经纬度信息
//    theRegion.center=userLocation.coordinate;
//    theRegion.span=theSpan;
//    //设置地图显示的区域为当前地址为中心
//    [self.mapView setRegion:theRegion];
//   
//    
//}
- (void)mapView:(MKMapView *)mapView didFailToLocateUserWithError:(NSError *)error
{
    //有可能会有多个定位失败返回
    NSLog(@"定位失败：%@",error);
    UIAlertView *alertView=[[UIAlertView alloc] initWithTitle:nil message:@"无法获取您的定位信息" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil];
    [alertView show];
    //    //没有定位信息，将当前的定位设置为自己的
    //    manager.location=CLLocationCoordinate2DMake(30.1970, 120.217);
    //    locationManager.location.coordinate=CLLocationCoordinate2DMake(,)
    //定位失败就定位到自己设定好的地方
    MKCoordinateSpan theSpan;
    //地图的范围越小越精确（有效）
    theSpan.latitudeDelta=0.05;
    theSpan.longitudeDelta=0.05;
    MKCoordinateRegion theRegion;
    //获得已经得到的location经纬度信息
    theRegion.center=CLLocationCoordinate2DMake(30.274089,120.155069);
    self.mapView.userLocation.coordinate=theRegion.center;
    theRegion.span=theSpan;
    //设置地图显示的区域为当前地址为中心
    [self.mapView setRegion:theRegion];
        self.mapView.showsUserLocation=NO;
    //    self.mapView.showsUserLocation=NO;//定位失败设置此值为no，mapview不会再重新尝试定位，否则会一直有定位数据返回
    //    [manager stopUpdatingLocation];
    //    if(self.searchAroundOnViewDidLoad)
    //        [self searchAround:theRegion.center needPoint:NO];
    
}


#pragma mark - #pragma mark MKMapViewDelegate

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
}


- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[MapAnnotation class]])
    {
        // Selected the pin annotation.
        B_CalloutAnnotation *calloutAnnotation = [[B_CalloutAnnotation alloc] init];
        
        _mapAnnotation = ((MapAnnotation *)view.annotation);
        calloutAnnotation.title      = _mapAnnotation.title;
        calloutAnnotation.shopKid   =_mapAnnotation.shopKid;
        calloutAnnotation.subTitle = _mapAnnotation.title;
        
        calloutAnnotation.coordinate = _mapAnnotation.coordinate;
        _mapAnnotation.calloutAnnotation = calloutAnnotation;
        
        [self.mapView addAnnotation:calloutAnnotation];
        [self.mapView setCenterCoordinate:calloutAnnotation.coordinate animated:YES];
    }
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view
{
    if ([view.annotation isKindOfClass:[MapAnnotation class]])
    {
        // Deselected the pin annotation.
        MapAnnotation *_shop = ((MapAnnotation *)view.annotation);
        
        [self.mapView removeAnnotation:_shop.calloutAnnotation];
        _mapAnnotation.calloutAnnotation = nil;
    }
}



- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    show_g_annotaionView *annotationView;
    
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    B_CalloutAnnotation *calloutAnnotation = (B_CalloutAnnotation *)annotation;
    NSString *identifier;
    
    if ([annotation isKindOfClass:[MapAnnotation class]])
    {
        annotationView =(show_g_annotaionView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:@"Annotation"];
        if (!annotationView)
        {
            annotationView = [[show_g_annotaionView alloc] initWithAnnotation:annotation
                                                             reuseIdentifier:@"Annotation"];
           // annotationView.canShowCallout = NO;
            annotationView.titleLabel.text = calloutAnnotation.title;
        
        }
        return annotationView;
    }
    else if([annotation isKindOfClass:[B_CalloutAnnotation class]])
    {
    
        identifier = @"Callout";
        buildingAnnotationView = (B_CalloutAnnotationView *)[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        buildingAnnotationView = [[B_CalloutAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
        
        ((B_CalloutAnnotationView *)buildingAnnotationView).title = calloutAnnotation.title;
        buildingAnnotationView.subTitle = _mapAnnotation.sub_title;
        buildingAnnotationView.img_str = _mapAnnotation.img_str;
        buildingAnnotationView._kid = _mapAnnotation._kid;
        ((B_CalloutAnnotationView *)buildingAnnotationView).delegate = self;
        //[annotationView setNeedsDisplay];
        
        return buildingAnnotationView;
        
 }

    
    return nil;
}
//    else if([annotation isKindOfClass:[buildingAnnotation class]])
//    {
//        
//        identifier = @"Callout";
//        buildingAnnotationView = (buildingAnnotationView *)[[self.mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
//        
//        
//        
//        buildingAnnotationView = [[buildingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
//        
//        
//        buildingAnnotation = (CalloutSalonAnnotation *)annotation;
//        
//        ((CalloutSalonAnnotationView *)salonAnnotationView).title = calloutAnnotation.title;
//        salonAnnotationView.subTitle = _salonMapAnnotation.subTitle;
//        salonAnnotationView.shop_name = _salonMapAnnotation.shop_name;
//        NSLog(@"aaaa%@",calloutAnnotation.title);
//        ((CalloutSalonAnnotationView *)salonAnnotationView).delegate = self;
//        [annotationView setNeedsDisplay];
//        
//        return salonAnnotationView;
//        
//    }
//    
//}

#pragma mark - B_CalloutAnnotationViewDelegate
- (void)buttonClicked:(NSString *)kid_str;
{
    buildingDetailVC *vc = [[buildingDetailVC alloc] initWithNavigationBar:YES];
    vc.buildingKid_str = kid_str;
    
    [self.navigationController pushViewController:vc animated:YES];
}
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if([CLLocationManager locationServicesEnabled])
        self.mapView.showsUserLocation=YES;
    
    [self refreshMapInfo];

    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
