//
//  selectTimeView.m
//  test_1
//
//  Created by liu on 14-10-11.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import "selectTimeView.h"

@implementation selectTimeView
{
    UILabel *titleLable;
    UIScrollView*  scrollView;
}

#define HIGHT 250.0f

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        selectColor = [AppSession colorWithHexString:@"fd710a"];
        color = [AppSession colorWithHexString:@"dadada"];
        _hoursArray = [[NSMutableArray alloc]init];
        [self viewCon];
    }
    return self;
}

- (void)viewCon
{
    selectView = [[UIView alloc]initWithFrame:CGRectMake(0, SCREEN_HEIGHT ,SCREEN_WIDTH, HIGHT)];
    selectView.backgroundColor = [UIColor whiteColor];
    shadeView = [[UIView alloc]initWithFrame:self.frame];
    shadeView.userInteractionEnabled = YES;
    [shadeView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(BtnClick)]];
    shadeView.backgroundColor = [UIColor blackColor];
    shadeView.alpha = 0.0f;

    NSEnumerator *frontToBackWindows = [[[UIApplication sharedApplication]windows]reverseObjectEnumerator];
    for (UIWindow *window in frontToBackWindows)
        if (window.windowLevel == UIWindowLevelNormal) {
            shadeView.frame = window.frame;
            [window addSubview:shadeView];
            [window addSubview:selectView];
                
            break;
        }
    
    titleLable = [[UILabel alloc]initWithFrame:CGRectMake(-1, 0, SCREEN_WIDTH + 2, 28)];
    titleLable.font = [UIFont systemFontOfSize:12];
    titleLable.backgroundColor = [AppSession colorWithHexString:@"f7f7f7"];
    titleLable.layer.borderWidth = 1.0f;
    titleLable.layer.borderColor = [AppSession colorWithHexString:@"dbdbdb"].CGColor;
    titleLable.textAlignment =  NSTextAlignmentCenter;
    [selectView addSubview:titleLable];
    
    scrollView = [[UIScrollView alloc]initWithFrame:
                  CGRectMake(0, 28,SCREEN_WIDTH , HIGHT - 62)];
    scrollView.backgroundColor = [UIColor whiteColor];
    scrollView.scrollEnabled = YES;
    scrollView.layer.shadowColor = [[UIColor blackColor]CGColor];
    scrollView.layer.shadowOffset = CGSizeMake(2, 2);
    scrollView.layer.shadowOpacity = 0.20;
    scrollView.layer.shadowRadius = 3.0;
    scrollView.layer.masksToBounds = YES;
    [selectView addSubview:scrollView];
    
    UIButton *liftButton =[[UIButton alloc]initWithFrame:CGRectMake(0, 216, SCREEN_WIDTH/2, 34)];
    [liftButton setTitle:@"确认" forState:UIControlStateNormal];
    liftButton.titleLabel.font = [UIFont systemFontOfSize:16];
    liftButton.titleLabel.textColor = [UIColor whiteColor];
    liftButton.backgroundColor =selectColor;
    [[liftButton layer] setBorderWidth:1];
    [[liftButton layer] setBorderColor:color.CGColor];
    [liftButton addTarget:self action:@selector(liftButton) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *rightButton = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2, 216, SCREEN_WIDTH/2, 34)];
    [rightButton setTitle:@"取消" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [[rightButton layer] setBorderWidth:1];
    [[rightButton layer] setBorderColor:color.CGColor];
    [rightButton setBackgroundColor:[UIColor whiteColor]];
    [rightButton addTarget:self action:@selector(rightButton) forControlEvents:UIControlEventTouchUpInside];
    
    [selectView addSubview:rightButton];
    [selectView addSubview:liftButton];
    
}

-(void)theCalendar:(NSArray *)Times
{
    _selectType = SelectTypeTime;
    titleLable.text = @"请选择时间";
    UILabel *weekLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 28, selectView.frame.size.width, 15)];
    weekLabel.text = @"    六         日         一         二          三        四         五";
    weekLabel.font = [UIFont boldSystemFontOfSize:13];
    [selectView addSubview:weekLabel];
    
    NSDate *  senddate=[NSDate date];
    NSDateFormatter  *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd"];
    _day = [[dateformatter stringFromDate:senddate] intValue];
    [dateformatter setDateFormat:@"HH"];
    _hours = [[dateformatter stringFromDate:senddate] intValue];
    
    for (int n = 0; n < 21; n++) {
        int  num_X = n % 7;
        int  num_Y = n / 7;
        UIButton *button= [[UIButton alloc]initWithFrame:CGRectMake(46*num_X, 44+44*num_Y, 46, 44)];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:14];
        [button setShowsTouchWhenHighlighted:YES];
        [button setUserInteractionEnabled:NO];
        //NSLog(@"%i,%i",num_X,num_Y);
        switch (num_Y) {
            case 0: [button setTitle:@"上午" forState:UIControlStateNormal]; break;
            case 1: [button setTitle:@"下午" forState:UIControlStateNormal]; break;
            case 2: [button setTitle:@"晚上" forState:UIControlStateNormal]; break;
            default: break;
        }
        button.tag = 100+n;
        button.backgroundColor = [UIColor whiteColor];
        [[button layer] setBorderWidth:1];
        [[button layer] setBorderColor:color.CGColor];
        [button addTarget:self action:@selector(buttonTouch:) forControlEvents:UIControlEventTouchUpInside];
        [selectView addSubview:button];
        
        for (NSDictionary *dic in [self times:Times]) {
            int comeHours = [[dic objectForKey:@"day"] intValue] - _day;
            if ([button.titleLabel.text isEqualToString:[dic objectForKey:@"hours"]]
                && comeHours == num_X) {
                [[button layer] setBorderColor:[UIColor greenColor].CGColor];
                [button setUserInteractionEnabled:YES];
            }
        }
    }
    UILabel *dayLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 180, SCREEN_WIDTH, 20)];
    dayLabel.font = [UIFont systemFontOfSize:11];
    dayLabel.textAlignment =  NSTextAlignmentCenter;
    NSString *days = nil;
    for (int n = 0; n<7; n++) {
        days = [NSString stringWithFormat:@"%@%i日       ", days?days:@"",_day+n];
    }
    dayLabel.text = days;
    [selectView addSubview:dayLabel];
    
    UILabel *promptLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 196, SCREEN_WIDTH, 20)];
    promptLabel.text = @"       请在客户希望带看的时间(绿色)内选择,最多可选两个时间点！";
    promptLabel.font =[UIFont systemFontOfSize:10];
    promptLabel.textColor =[UIColor grayColor];
    [selectView addSubview:promptLabel];
    
}

-(void)buttonTouch:(id)inSand
{
    UIButton *button = (UIButton *)inSand;
    if (button.layer.borderColor == selectColor.CGColor) {
        _selectNum --;
        for (int n = 0; n < _hoursArray.count; n++) {
            UIButton *seleButton = _hoursArray[n];
            if ([seleButton isEqual:button]) {
                [_hoursArray removeObjectAtIndex:n];
            }
        }
        [[button layer] setBorderColor:[UIColor greenColor].CGColor];
        [button setBackgroundColor:[UIColor clearColor]];
    } else if (!(_selectNum >= 2)) {
        _selectNum ++;
        [_hoursArray addObject:button];
        [[button layer] setBorderColor:selectColor.CGColor];
        [button setBackgroundColor:[UIColor greenColor]];
    }
    NSLog(@"%lu",(unsigned long)_selectNum);
}

-(void)classLabel:(NSArray *)labels
{
    _selectType = SelectTypeClassTag;
    titleLable.text = @"请选择个性标签";
    for(UIView *subv in [scrollView subviews]){
        [subv removeFromSuperview];
    }
    for (int n = 0; n < labels.count; n++) {
        int  num_X = n % 3;
        int  num_Y = n / 3;
        UIButton *button= [[UIButton alloc]initWithFrame:CGRectMake( 15 + 100*num_X, 10+44*num_Y, 90, 34)];
        [button setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:13];
        button.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
        button.titleLabel.numberOfLines = 0;
        button.titleLabel.textAlignment =  NSTextAlignmentCenter;
        [button setTitle:labels[n] forState:UIControlStateNormal];
        [[button layer] setBorderWidth:1];
        [[button layer] setBorderColor:color.CGColor];
        button.tag = 100+n;
        [button addTarget:self action:@selector(buttonTouchType:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:button];
       
    }
    int contentSize = (int)labels.count/3;
    scrollView.contentSize = CGSizeMake(SCREEN_WIDTH,contentSize * 44 + 50 );
}

-(void)buttonTouchType:(id)inSand
{
    UIButton *button = (UIButton *)inSand;
    if (button.layer.borderColor == selectColor.CGColor) {
        _selectNum --;
        for (int n = 0; n < _hoursArray.count; n++) {
            UIButton *seleButton = _hoursArray[n];
            if ([seleButton isEqual:button]) {
                [_hoursArray removeObjectAtIndex:n];
            }
        }
        [[button layer] setBorderColor:color.CGColor];
    } else if (!(_selectNum >= 5)) {
        _selectNum ++;
        [_hoursArray addObject:button];
        [[button layer] setBorderColor:selectColor.CGColor];
    }
    NSLog(@"%lu",(unsigned long)_selectNum);
}

-(void)liftButton
{
    if (self.delegate
        && [self.delegate conformsToProtocol:@protocol(selectTimeDelegate)]
        && _hoursArray.count > 0) {
        NSMutableArray *mtArray = [NSMutableArray new];
        for (UIButton *button in _hoursArray) {
            long tag = button.tag - 100;
            int comeDay = (int)tag % 7;
            comeDay = _day + comeDay;
            [mtArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:comeDay],@"day",button.titleLabel.text,@"hours", nil]];
        }
        [self.delegate selectDayAndHours:mtArray];
        [self disappear];
    }
}
-(void)rightButton
{
    [self disappear];
}
-(void)BtnClick
{
    [self disappear];
}

-(void)nowAppear
{
    [self nowAnimation:SCREEN_HEIGHT - HIGHT];
    [UIView animateWithDuration:0.3 animations:^{
        shadeView.alpha = 0.3;
    }];
}

-(void)disappear
{
    [self nowAnimation:SCREEN_HEIGHT];
    [UIView animateWithDuration:0.3 animations:^{
        shadeView.alpha = 0.0;
    }];
}

-(void)nowAnimation:(float)or_y
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:0.4];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    selectView.frame = CGRectMake(0, or_y ,self.bounds.size.width, HIGHT);
    [UIView commitAnimations];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(NSArray *)times:(NSArray *)Times
{
    NSMutableArray *arrayTime = [NSMutableArray new];
    for (NSString *time in Times) {
        NSString *comeDay = nil;
        int comeHors = 0;
        NSMutableDictionary *dict = [NSMutableDictionary new];
        NSArray *Array1 = [time componentsSeparatedByString:@" "];
        if (Array1.count == 2) {
            NSArray *array2 = [Array1[0] componentsSeparatedByString:@"/"];
            if (array2.count == 3) {
                comeDay = array2[2];
            }
            NSArray *array3 = [Array1[1] componentsSeparatedByString:@":"];
            if (array3.count == 3) {
                comeHors = [array3[0] intValue];
            }
        }
         //0 ~ 24 0~12 12~18 18~24
        NSString *comeH = nil;
        if (comeHors > 0 && comeHors <= 12) {
            comeH = @"上午";
        }
        if (comeHors > 12 && comeHors <= 18) {
            comeH = @"下午";
        }
        if (comeHors > 18 && comeHors <= 24) {
            comeH = @"晚上";
        }
        [dict setObject:comeH forKey:@"hours"];
        [dict setObject:comeDay forKey:@"day"];
        [dict setObject:[NSNumber numberWithInt:comeHors] forKey:@"numHours"];
        [arrayTime addObject:dict];
    }
    return arrayTime;
}

@end
