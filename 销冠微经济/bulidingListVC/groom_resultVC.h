//
//  groom_resultVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/27.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface groom_resultVC : BaseViewController<UITableViewDataSource,UITableViewDelegate>
{
    UITableView *tableView_;
    NSMutableArray *dataSource;
}

@property (nonatomic, strong) NSMutableArray *dataArray;
@end
