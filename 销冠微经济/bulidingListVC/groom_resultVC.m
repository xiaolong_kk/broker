//
//  groom_resultVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/27.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "groom_resultVC.h"
#import "addCell.h"
@implementation groom_resultVC


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationLeftTitleBtn.hidden = YES;
    self.navigationLabel.text = @"推荐结果";
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;
    
    
    tableView_ = [[UITableView alloc]initWithFrame:CGRectMake(0,addHight+44,320,self.view.bounds.size.height-65)];
    tableView_.separatorStyle=UITableViewCellSeparatorStyleNone;
    tableView_.delegate = self;
    tableView_.dataSource = self;
    //    tableView_.delegate_extend = self;
    
    [self.view addSubview:tableView_];
    NSDictionary *dic = @{@"Cell": @"MainCell",@"isAttached":@(NO)};
    NSArray * array = @[dic,dic,dic,dic,dic,dic];
    
    self.dataArray = [[NSMutableArray alloc]init];
    self.dataArray = [NSMutableArray arrayWithArray:array];
    
    //[self getData];
    
}

-(void)getData
{
    
    UIImageView *logoImg = (UIImageView *)[self.view viewWithTag:101];
    UILabel *priceLabel = (UILabel *)[self.view viewWithTag:102];
    UILabel *tagLabel=(UILabel *)[self.view viewWithTag:103];
    UILabel *titleLabel1 = (UILabel *)[self.view viewWithTag:222];
    UILabel *titleLabel2 = (UILabel *)[self.view viewWithTag:223];
    
    NSString *str = @"http://apiweixin.tops001.com/api.aspx?api=api.building.biuldingdetail&BuildingKid=18";
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict = [request.responseString objectFromJSONString];
         NSDictionary *dict2 = [dict objectForKey:@"building"];
         titleLabel1.text = [NSString stringWithFormat:@"电话 %@",[dict2 objectForKey:@"F_Phone"]];
         titleLabel2.text = [NSString stringWithFormat:@"地址 %@",[dict2 objectForKey:@"F_Address"]];
         priceLabel.text = [NSString stringWithFormat:@"均价 %@/㎡",[dict2 objectForKey:@"F_AvgPrice"]];
         tagLabel.text = [dict2 objectForKey:@"F_Activity"];
         [logoImg setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[dict2 objectForKey:@"F_Img1"]] placeholderImage:nil];
         //priceLabel.text = [dict2 objectForKey:];
         // NSLog(@"%@",dataSource);
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if ([[self.dataArray[indexPath.row] objectForKey:@"Cell"] isEqualToString:@"MainCell"])
    {
        
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"MainCell%ld",indexPath.row];//以indexPath来唯一确定cell
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
            headView.backgroundColor= [UIColor whiteColor];
            [cell addSubview:headView];
            
            UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
            logoImg.tag = 101;
            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
            [headView addSubview:logoImg];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,240,20)];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.textAlignment = NSTextAlignmentLeft;
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.text = @"周小雨";
            titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
            [headView addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
            sexLabel.tag=103;
            sexLabel.text = @"先生";
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [headView addSubview:sexLabel];
            
            UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
            phoneLabel.tag = 103;
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            phoneLabel.textAlignment = NSTextAlignmentLeft;
            phoneLabel.text = @"12345678989";
            phoneLabel.backgroundColor = [UIColor clearColor];
            phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
            [headView addSubview:phoneLabel];
            
            UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
            budildingLabel.tag = 103;
            budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            budildingLabel.textAlignment = NSTextAlignmentLeft;
            budildingLabel.text = @"楼盘名称  卡靠一号";
            budildingLabel.backgroundColor = [UIColor clearColor];
            budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [headView addSubview:budildingLabel];

            
            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(230,15,12,12)];
            tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
            [headView addSubview:tagImg];
            
            
            UILabel *tageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250,11,150,20)];
            tageLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            tageLabel.textAlignment = NSTextAlignmentLeft;
            tageLabel.text = @"无效客户";
            tageLabel.backgroundColor = [UIColor clearColor];
            tageLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [headView addSubview:tageLabel];

            
            UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(0,0,320,50)];
            //extendedBtn.backgroundColor = [UIColor blackColor];
            //[extendedBtn setImage:[UIImage imageNamed:@"line_show.png"] forState:UIControlStateNormal];
            [clickBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [headView addSubview:clickBtn];

            UIImageView *exImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,65-9,320,9)];
            exImg.image = [UIImage imageNamed:@"line_show.png"];
            [cell addSubview:exImg];
            
        }
        
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }else if([[self.dataArray[indexPath.row] objectForKey:@"Cell"] isEqualToString:@"addCell"])
    {
        
        NSString *CellIdentifier = [NSString stringWithFormat:@"addCell%d",indexPath.row];//以indexPath来唯一确定cell
        
        addCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[addCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            
        }
        NSDictionary *dict = dataSource[indexPath.row];
        
        //        exImg.frame = CGRectMake(0,70,320,20);
        //        exImg.image = [UIImage imageNamed:@"line_hide.png"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    return nil;
    
    
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
   // UITableViewCell *cell = [tableView_ cellForRowAtIndexPath:indexPath];
    NSIndexPath *path = nil;
    
    //    UIImageView *exImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,120,320,9)];
    //    exImg.image = [UIImage imageNamed:@"line_show.png"];
    
    if ([[self.dataArray[indexPath.row] objectForKey:@"Cell"] isEqualToString:@"MainCell"])
    {
        path = [NSIndexPath indexPathForItem:(indexPath.row+1) inSection:indexPath.section];
    }else
    {
        path = indexPath;
    }
    
    if ([[self.dataArray[indexPath.row] objectForKey:@"isAttached"] boolValue]) {
        // 关闭附加cell
        NSDictionary * dic = @{@"Cell": @"MainCell",@"isAttached":@(NO)};
        self.dataArray[(path.row-1)] = dic;
        [self.dataArray removeObjectAtIndex:path.row];
        [tableView_ beginUpdates];
        [tableView_ deleteRowsAtIndexPaths:@[path]  withRowAnimation:UITableViewRowAnimationFade];
        [tableView_ endUpdates];
        
    }else{
        // 打开附加cell
        NSDictionary * dic = @{@"Cell": @"MainCell",@"isAttached":@(YES)};
        self.dataArray[(path.row-1)] = dic;
        NSDictionary * addDic = @{@"Cell": @"addCell",@"isAttached":@(YES)};
        [self.dataArray insertObject:addDic atIndex:path.row];
        [tableView_ beginUpdates];
        [tableView_ insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationFade];
        [tableView_ endUpdates];
        if(indexPath.row+1==self.dataArray.count-1)
        {
            [tableView_ scrollToRowAtIndexPath:path atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
        
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
{
    UIImageView *exImg = (UIImageView *)[cell viewWithTag:indexPath.row+888];
    
    //NSLog(@"%d",exImg.tag);
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[self.dataArray[indexPath.row] objectForKey:@"Cell"] isEqualToString:@"MainCell"])
    {
        return 65;
    }else{
        return 80;
    }
}

- (void)buttonClicked:(UIButton *)btn
{
    // NSLog(@"%ld",btn.tag);
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
