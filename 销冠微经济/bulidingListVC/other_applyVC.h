//
//  other_applyVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/27.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VRGCalendarView.h"

@interface other_applyVC : BaseViewController<UITextFieldDelegate,UIScrollViewDelegate,UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextViewDelegate,VRGCalendarViewDelegate>
{
    
    UIScrollView *_scrollView;
    UITextView *_textView;
    UIButton *faceBtn;
    UITextField *name_text;
    UIImage *faceImg;
    
    UIView *mid_view;
    UIView *mid_view2;

}
@property(nonatomic,strong) VRGCalendarView *calendar;

@property(nonatomic,retain)NSString *type_str;
@property(nonatomic,strong)  UIView *calenderViewBg;
@property(nonatomic,strong)NSDictionary *user_dict;
@property (nonatomic, retain) NSString *state_str;
@property(nonatomic,strong)NSString *dateString;

@end
