//
//  AppSession.h
//  greentea
//
//  Created by Jian Wang on 12-9-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseViewController.h"

#import "CustomAvatarImageView.h"

//  !!IMPORTANT!!  You should only use sandbox server ( 19 or 55 ) for development.

//#define WEBAPP_URL @"http://testapi.tops001.com/s/api.aspx"
#define WEBAPP_URL @"http://api2.tops001.com/s/api.aspx"
#define WEBAPP_URL_V1 @"http://tops001.com/"

//#define WEBAPP_URL @"http://img06.tutugg.com/s/api.aspx"

#define MAIN_GET_LIST_URL @"api.indexpage.getlist"//主页面数据

#define  CHART_ANALYSIS_URL @"api.chart.statistics.analysis"//报表数据

#define CHARTTYPE_URL @"api.chart.statistics.analysis"//报表类型数据

//#define CHARTTYPE_URL @"api.chart.statistics.analysis"//报表类型数据
#define TICKETRATE_URL @"api.chart.consultanttop.ticketrate"//认筹率

#define COMPLETERATE_URL @"api.chart.consultanttop.completerate"//成交率

#define TELLBOOK_URL @"api.contacts.getlist"//电话数据

#define COMPETE_URL @"api.competebuilding.getlist"//竞盘信息

#define CONSULTANTTOP_URL @"api.chart.consultanttop.sales"//成交量

#define  CUSTOMCOMETOP_URL @"api.chart.customcometop" //来访客户

#define CONSULTANTBUSINESS_URL @"api.consultantbusiness.getlist"//置业顾问详情

#define TICKETTOP_URL @"api.chart.tickettop" //认筹量

#define  FEEDBACK_URL @"api.feedback.add"//意见反馈

#define LOGIN_URL  @"api.admin.login"//登陆

#define UPDATEPASSWORD_URL @"api.admin.updatepassword"//修改密码

#define  IS_STATUS_URL @"api.admin.status"//过期提醒

#define FACEIMG_URL @"@api.chart.consultanttop.business"//置业顾问头像

//#define BUSINESSRATE_URL @"api.chart.consultanttop.businessrate"//成交率数据

#define NEWS_LIST_URL @"api.news.getlist"//行业资讯
#define NEWS_GET_URL @"api.news.get"  //行业咨询详情

#define BUILDING_LIST_URL @"api.buildingstatus.get" //获取楼盘列表

#define UNALLOCATION_GETLIST_URL @"api.unallocation.intent.getlist"//获取所有未分配置业顾问的经纪人客户（公共池中）

#define ALLOCATION_INTENT_URL @"api.allocation.intent"//分配客户给置业顾问API传入楼盘ID，客户ID，置业顾问ID,服务端处理置业顾问关联，日期关联

#define CONSULTANT_GETLIST_URL @"api.consultant.getlist"// 获取所有置业顾问信息API,传入楼盘ID,返回置业顾问ID，置业顾问姓名

#define SMSPUSH_GETLIST_URL @"api.smspush.getlist" //推送相关api

#define SMSPUSH_READ_URL @"api.smspush.read"//推送相关api

#define CUSTOMERWAIT_CONFIRM_LIST @"api.weixin.customerwait.confirm.list"//微信客户带看相关api

#define CUSTOMERWAIT_OUTFLOW_LIST @"api.weixin.customerwait.outflow.list"//微信客户带看相关api

#define CUSTOMERWAIT_LIST @"api.weixin.customerwait.list"//微信客户带看相关api

#define CUSTOMERWAIT_CONFIRM @"api.weixin.customerwait.confirm"//微信客户带看相关api

//***********************************置业顾问相关api*************************

#define INTENT_QRCODE_URL @"api.intent.qrcode"//获取二维码扫描后的客户信息
#define ALLOCATION_INTENT_LOG2_URL @"api.allocation.intent.log2"//二维码扫描后客户再次来访跟进
#define ALLOCATION_INTENT_LOG_URL @"api.allocation.intent.log"//二维码扫描后客户首次来访添加
#define ALLOCATION_INTENT_BATCH_URL @"api.allocation.intent.batch"//客户转移
#define TICKET_INTENT_URL @"api.ticket.intent"//客户认筹

#define INTENT_VIEWNEWCUSTOM_URL @"api.intent.viewnewcustom"
#define CLIENT_LOGIN_URL @"api.intent.exist"
#define INTENT_UPDATE_CHANCE_URL @"api.intent.update.chance"//修改机会信息
#define INTENT_UPDATE_BASE_URL @"api.intent.update.base"//修改基本信息
//#define INTENT_ADD_URL @"api.intent.add"
#define INTENT_ADD_URL @"api.intentandlog.add"
#define INTENT_SEARCH_URL @"api.intent.search"
#define CUSTOMTYPEFORM_URL @"api.customtypeform.get"
//#define INTENTFORM_URL @"api.intent.form"
#define INTENTFORM_URL @"api.intent.comephone.form"
#define CUSTOMVISITORSLOG_URL @"api.customvisitorslog.getlist"
#define CUSTOMTYPE_CONSULTANT_URL @"api.customtype.consultant.get"
#define CUSTOMYPEFORM_URL @"api.customtypeform.get"
#define CHECKFUND_TICKET_URL @"api.checkfunds.ticket.get"
#define SALEROOM_INTENT_URL @"api.saleroom.intent.getlist"
#define SALEZHUANG_URL @"api.salezhuang.getlist"
//#define SALEROOM_URL @"api.saleroom.getlist"
#define SALEROOM_URL @"api.saleroom.getlist.morefloor"//销控楼盘
#define CUSTOMCLASS_URL @"api.customclass.getlist"
#define FOLLOW_INTENT_URL @"api.follow.intent.get"
#define INTENT_URL @"api.intent.get"
#define PREORDAIN_BUSINESS_URL @"api.preordain.business.get"
#define CUSTOMVISITORSLOGS_ADD_URL @"api.customvisitorslog.add"

//***********************************销售经理相关api*************************

#define CUSTOMER_WEIXIN_INDEXLIST_URL @"api.customer.weixin.indexlist"

//IOS版本
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
//设备分类
typedef enum CurrentDeviceType{
    CurrentDeviceTypeUnKnow=0,//未知设备
    CurrentDeviceTypeIPHONE=1,//iphone
    CurrentDeviceTypeIPAD=2,//ipad
    CurrentDeviceTypeIPOD=3//ipod
} CurrentDeviceType;

#define stringFontOfSize @"Helvetica"
#define strinBoldFontOfSize @"Helvetica-Bold"

#define IOS_7 (([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)? (YES):(NO))

#define IPHONE_5 [[UIScreen mainScreen] bounds].size.height==568
#define IPHONE_4 [[UIScreen mainScreen] bounds].size.height==480
#define DEGREE_TO_RADIANS(_ANGLE)((_ANGLE)/180.0*M_PI)
#import <Foundation/Foundation.h>


@interface AppSession :NSObject

+ (BOOL)isLogin;//是否登录
+ (void)logoff;//注销登录
+ (void)login:(NSDictionary *)user;//登录，保存为currentUser和userToken
+ (NSDictionary *)getCurrentUser;//获得当前登录的用户信息
+ (NSString *)getCurrentUserToken;//获得当前登录的用户token
+ (void)fadeInLayer:(CALayer *)layer;
// NSString值为Unicode格式的字符串编码(如\u7E8C)转换成中文
//unicode编码以\u开头
+ (NSString *)replaceUnicode:(NSString *)unicodeStr;
//获取默认的用户信息
+ (NSArray *)getDefaultUser;
//弹出提示
+ (void)ToastAlert:(NSString *)title message:(NSString*)message;
+ (NSString*) getConverTime:(NSString*)publishTime;
+ (NSData*)getDefaultUserAvatarData;
//获得头像数据流
+ (NSData*) getUserAvatarData:(NSString*)avatar_url;
+ (NSString *) getAbsoluteImageUrl:(NSString *) url;//通过相对路径获得远程图片的绝对地址
//将uiimage对象转换成base64编码后的字符串
+ (NSString*) getEncodeImageData:(UIImage*)image;
+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(float)alpha;
//处理ASIHttp的错误信息
+ (void) checkHttpError:(NSError *)error;

//处理业务上的错误信息
+ (void) checkMessageError:(NSDictionary *)error;
//颜色十六进制转UIColor
+ (UIColor *)colorWithHexString:(NSString *)hexString;
//背景可伸缩
//+ (UILabel *) getAutoResizeLabel:(NSString *) text lineBreakMode:(UILineBreakMode) lineBreakMode labelFrame:(CGRect) frame font:(UIFont *) font;
//+ (UILabel *) getAutoResizeLabel:(NSString *) text lineBreakMode:(UILineBreakMode) lineBreakMode labelFrame:(CGRect) frame font:(UIFont *) font maxHeight:(float)maxHeight;//可以设定文本最大高度，如果计算出的文本高度超出此高度，则使用maxHeight，否则，使用计算的高度
//通过路径从文件系统里取图片
+ (UIImage *) getImageFromContentFile:(NSString *) imageName;
+ (CustomAvatarImageView *) getRoundImageView:(CGPoint)position radius:(float)radius;//传入一张图片，返回圆形的图并且带边框，使用此方法获得圆形图片，在机子上运行会卡
+ (CurrentDeviceType) currentDeviceType;//返回当前设备类型
//拨打电话
//phoneNumber 电话号码
//name 对应的店家名称
//taget 所在controller，一般为self
//needSave 是否保存到应用程序中，将会在右侧最近拨打中展示出来
+ (void) makePhoneTo:(NSString *)phoneNumber name:(NSString*)name target:(BaseViewController *)target needSave:(BOOL)needSave;
//图像的翻转
+ (UIImage *)scaleAndRotateImage:(UIImage *)image;
//图片裁减
+(UIImage *)scaleImage:(UIImage *)image;
// 旋转图片
+(BOOL) isBlankString:(NSString *)string;

+ (void)resignKeyBoardInView:(UIView *)view;

+(void)fadeOut:(UIView *)view;

+(void)fadeIn:(UIView *)view;


@end
