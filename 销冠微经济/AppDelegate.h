//
//  AppDelegate.h
//  销冠微经济
//
//  Created by zen huang on 14/9/11.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MLNavigationController.h"
#import"MainTabBarViewController.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) MLNavigationController *MLNav;
@property (nonatomic, strong)  MainTabBarViewController *tabBarController;
@property(nonatomic,strong) NSString *brokerKid;
@property(nonatomic,strong)  NSUserDefaults *userDefault;

#define theApp ((AppDelegate *)[[UIApplication sharedApplication] delegate])

@end
