//
//  selectTimeView.h
//  test_1
//
//  Created by liu on 14-10-11.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum SelectType{
    SelectTypeTime = 0,
    SelectTypeClassTag,
    
} SelectType;

@protocol selectTimeDelegate <NSObject>

-(void)selectDayAndHours:(NSArray *)Hours;

@end

@interface selectTimeView : UIView
{
    UIView *selectView;
    UIView *shadeView;
    UIColor *selectColor;
    UIColor *color;
}
-(void)nowAppear;
-(void)disappear;
-(void)theCalendar:(NSArray *)Times;
-(void)classLabel:(NSArray *)labels;
@property(nonatomic,assign)NSUInteger selectNum;
@property(nonatomic,assign)int day;
@property(nonatomic,assign)int hours;
@property(nonatomic,retain)NSMutableArray *hoursArray;
@property(nonatomic,unsafe_unretained)id<selectTimeDelegate>delegate;
@property (nonatomic, assign) SelectType selectType;
@end
