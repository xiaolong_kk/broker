//
//  applyListVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/24.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "applyListVC.h"
#import "apply_comeVC.h"
#import "other_applyVC.h"
#import "verifyResultVC.h"

@interface applyListVC ()

@end

@implementation applyListVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createTableView];
}


- (void)createTableView
{
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHight1=44;
    self.navigationLabel.text = self.title_str;
    topView = [[UIView alloc] initWithFrame:CGRectMake(0,addHight+44,320,45)];
    topView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:topView];
    
    
    //初始化搜索条
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,44)];
    [mySearchBar setPlaceholder:@"请输入客户姓名或手机号"];
    mySearchBar.barTintColor=[AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"s_search_input.png"] forState:UIControlStateNormal];
    //mySearchBar.userInteractionEnabled = NO;
    mySearchBar.tintColor=[AppSession colorWithHexString:@"84c445"];
    mySearchBar.delegate = self;
    [topView addSubview:mySearchBar];
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,5)];
    tempView.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView];
    
    UIView *tempView1 = [[UIView alloc]initWithFrame:CGRectMake(0,40,320,5)];
    tempView1.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView1];
    
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,100)];
   // headView.backgroundColor = [UIColor whiteColor];
    
    for(int i=0;i<2;i++)
    {
        
        UIButton *tagBtn5=[UIButton buttonWithType:UIButtonTypeCustom];
        tagBtn5.tag = 2010+i;
        tagBtn5.frame=CGRectMake(0,50*i,320,50);
        [tagBtn5 addTarget:self action:@selector(verifyRestultVC:) forControlEvents:UIControlEventTouchUpInside];
        [headView addSubview:tagBtn5];
        
        UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,7,36,36)];
        [tagBtn5 addSubview:logoImg];
        
        UILabel *logoLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,15,100,20)];
        logoLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
        logoLabel.textAlignment = NSTextAlignmentLeft;
        logoLabel.backgroundColor = [UIColor clearColor];
        logoLabel.textColor = [AppSession colorWithHexString:@"000000"];
        [tagBtn5 addSubview:logoLabel];
        
        if(i==0)
        {
            logoImg.image = [UIImage imageNamed:@"virfey1.png"];
            logoLabel.text = @"正在审核";
            logoLabel.tag = 2020;
            
        }
        
        else
        {
            logoImg.image = [UIImage imageNamed:@"virfey2.png"];
            logoLabel.text = @"审核完成";
            logoLabel.tag = 2021;
        }
        
        
        
        UIImageView *arrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go2.png"]];
        arrowImg.frame = CGRectMake(285,15,10,19);
        [tagBtn5 addSubview:arrowImg];
        
        
    }

    
    UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg.frame = CGRectMake(0,50,320,1);
    [headView addSubview:lineImg];
    
  
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height,320,self.view.bounds.size.height-topView.frame.origin.y-topView.frame.size.height) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = headView;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    resultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,100,320,self.view.bounds.size.height-150) style:UITableViewStylePlain];
    resultTableView.dataSource = self;
    resultTableView.delegate = self;
    resultTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:resultTableView];
    resultTableView.hidden = YES;
    
    [self getData];
    
}

-(void)verifyRestultVC:(UIButton *)btn
{
    verifyResultVC *vc = [[verifyResultVC alloc] initWithNavigationBar:YES];
    vc.type_str = self.state_str;
    if(btn.tag==111)
    {
        vc.rtype_str = @"1";
    }
    if(btn.tag==112)
    {
        vc.rtype_str = @"2";

    }
    [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
}

-(void)getData
{
    
    UILabel *logoLabel1 = (UILabel *)[self.view viewWithTag:2020];
    
    UILabel *logoLabel2 = (UILabel *)[self.view viewWithTag:2021];

    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?appid=500&api=api.weixinbroker.applylist&BrokerKid=%@&type=%@",theApp.brokerKid,self.state_str];
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSDictionary *dict2 = [dict objectForKey:@"data"];
             
             logoLabel1.text = [NSString stringWithFormat:@"正在审核 (%@)",[dict2 objectForKey:@"procesoCheck_count"]];
             
            logoLabel2.text = [NSString stringWithFormat:@"审核完成 (%@)",[dict2 objectForKey:@"finishCheck_count"]];
             
            NSArray *arr = [dict2 objectForKey:@"list_itme"];
             dataSource = [NSMutableArray arrayWithArray:arr];
             [_tableView reloadData];
             
        }
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        static NSString *CellIdentifier = @"Cell";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,65)];
           // headView.backgroundColor= [UIColor whiteColor];
            [cell addSubview:headView];
            
            UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(15,10,36,36)];
            logoImg.tag = 101;
//            logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
            [headView addSubview:logoImg];
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,9,240,20)];
            titleLabel.tag = 102;
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.textAlignment = NSTextAlignmentLeft;
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.textColor = [AppSession colorWithHexString:@"000000"];
            [headView addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(110,10,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"666666"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [headView addSubview:sexLabel];
            
            UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(145,11,150,20)];
            phoneLabel.tag = 104;
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            phoneLabel.textAlignment = NSTextAlignmentLeft;
            phoneLabel.backgroundColor = [UIColor clearColor];
            phoneLabel.textColor = [AppSession colorWithHexString:@"75a7be"];
            [headView addSubview:phoneLabel];
            
            UILabel *budildingLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,31,275,20)];
            budildingLabel.tag = 105;
            budildingLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            budildingLabel.textAlignment = NSTextAlignmentLeft;
            budildingLabel.backgroundColor = [UIColor clearColor];
            budildingLabel.textColor = [AppSession colorWithHexString:@"666666"];
            [headView addSubview:budildingLabel];
            
            
//            UIImageView *tagImg = [[UIImageView alloc] initWithFrame:CGRectMake(230,15,12,12)];
//            tagImg.image = [UIImage imageNamed:@"icon_c2.png"];
//            [headView addSubview:tagImg];
//            
//
//            UILabel *tageLabel = [[UILabel alloc] initWithFrame:CGRectMake(250,11,150,20)];
//            tageLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
//            tageLabel.textAlignment = NSTextAlignmentLeft;
//            tageLabel.text = @"无效客户";
//            tageLabel.backgroundColor = [UIColor clearColor];
//            tageLabel.textColor = [AppSession colorWithHexString:@"666666"];
//            [headView addSubview:tageLabel];
//            
            
            UIButton *clickBtn = [[UIButton alloc] initWithFrame:CGRectMake(240,11,60,20)];
            clickBtn.tag = indexPath.row+5555;
            [clickBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
            [clickBtn setTitle:@"申 请" forState:UIControlStateNormal];
            clickBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
            [clickBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
            [clickBtn addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
            [headView addSubview:clickBtn];
            
            
            NSArray *arr = @[@"推荐有效",@"已到访",@"已认筹",@"已预定",@"已成交"];
            for(int i=0;i<5;i++)
            {
                UIView *lineBg = [[UIView alloc] initWithFrame:CGRectMake(10,60,300,60)];
                [headView addSubview:lineBg];
              
                UIImageView *pointlineImg = [[UIImageView alloc] initWithFrame:CGRectMake(i*60,0,60,10)];
                pointlineImg.tag = 222+i;
                pointlineImg.image = [UIImage imageNamed:@"pointline_gray.png"];
                [lineBg addSubview:pointlineImg];
                
                UILabel *stateLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,15,60,20)];
                stateLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                stateLabel.textAlignment = NSTextAlignmentCenter;
                stateLabel.text =arr[i];
                stateLabel.tag = 333+i;
                stateLabel.backgroundColor = [UIColor clearColor];
                stateLabel.textColor = [AppSession colorWithHexString:@"000000"];
                [lineBg addSubview:stateLabel];
                
                UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(i*60,35,60,20)];
                timeLabel.tag = 555+i;
                timeLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
                timeLabel.textAlignment = NSTextAlignmentCenter;
                timeLabel.backgroundColor = [UIColor clearColor];
                timeLabel.textColor = [AppSession colorWithHexString:@"666666"];
                [lineBg addSubview:timeLabel];

            }

            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,119,320,1);
            [cell addSubview:lineImg];
            
            
        }
    
    NSDictionary *dict = dataSource[indexPath.row];
    UIImageView *logoImg = (UIImageView *)[cell viewWithTag:101];
    UILabel *titleLabel = (UILabel *)[cell viewWithTag:102];
    UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
    UILabel *phoneLabel = (UILabel *)[cell viewWithTag:104];
    UILabel *budildingLabel = (UILabel *)[cell viewWithTag:105];
    UILabel *stateLabel = (UILabel *)[cell viewWithTag:334];
    
    UILabel *timeLabel1 = (UILabel *)[cell viewWithTag:555];
    UILabel *timeLabel2 = (UILabel *)[cell viewWithTag:556];
    UILabel *timeLabel3 = (UILabel *)[cell viewWithTag:557];
    UILabel *timeLabel4 = (UILabel *)[cell viewWithTag:558];
    UILabel *timeLabel5 = (UILabel *)[cell viewWithTag:559];

    UIImageView *pointlineImg1 = (UIImageView *)[cell viewWithTag:222];
    UIImageView *pointlineImg2 = (UIImageView *)[cell viewWithTag:223];
    UIImageView *pointlineImg3 = (UIImageView *)[cell viewWithTag:224];
    UIImageView *pointlineImg4 = (UIImageView *)[cell viewWithTag:225];
    UIImageView *pointlineImg5 = (UIImageView *)[cell viewWithTag:226];
    
    NSDictionary *dict_valid = [dict objectForKey:@"Valid"];
    NSDictionary *dict_come = [dict objectForKey:@"Come"];
    NSDictionary *dict_ticket = [dict objectForKey:@"Ticket"];
    NSDictionary *dict_preordain = [dict objectForKey:@"Preordain"];
    NSDictionary *dict_business = [dict objectForKey:@"Business"];

    if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
        logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
    else
        logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
   
    
    if([[dict_valid objectForKey:@"values"] intValue]==1)
    {
        pointlineImg1.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel1.text = [dict_valid objectForKey:@"Times"];
    }
    if([[dict_come objectForKey:@"values"] intValue]==1)
    {
        pointlineImg2.image = [UIImage imageNamed:@"pointline_orange.png"];
        stateLabel.text = [dict_come objectForKey:@"Names"];
        timeLabel2.text = [dict_come objectForKey:@"Times"];

    }
    if([[dict_ticket objectForKey:@"values"] intValue]==1)
    {
        pointlineImg3.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel3.text = [dict_ticket objectForKey:@"Times"];
    }
    if([[dict_preordain objectForKey:@"values"] intValue]==1)
    {
        pointlineImg4.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel4.text = [dict_preordain objectForKey:@"Times"];
    }
    if([[dict_business objectForKey:@"values"] intValue]==1)
    {
        pointlineImg5.image = [UIImage imageNamed:@"pointline_orange.png"];
        timeLabel5.text = [dict_business objectForKey:@"Times"];
    }
    
    titleLabel.text = [dict objectForKey:@"F_Title"];
    sexLabel.text = [dict objectForKey:@"F_Sex"];
    phoneLabel.text = [dict objectForKey:@"F_Phone"];
    budildingLabel.text = [dict objectForKey:@"BuildingName"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(void)buttonClicked:(UIButton *)btn
{
    
    int i = btn.tag-5555;
    
    NSDictionary *dict = dataSource[i];
    
   // NSLog(@"%@",dict);
    
    if([self.state_str isEqualToString:@"2"])
    {
        apply_comeVC *vc =[[apply_comeVC alloc] initWithNavigationBar:YES];
        vc.user_dict = dict;
        [self.navigationController pushViewController:vc animated:YES];
    }
    if([self.state_str isEqualToString:@"3"])
    {
        other_applyVC *vc =[[other_applyVC alloc] initWithNavigationBar:YES];
        vc.type_str = @"认筹";
        vc.user_dict = dict;
        vc.state_str =@"3";
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if([self.state_str isEqualToString:@"4"])
    {
        other_applyVC *vc =[[other_applyVC alloc] initWithNavigationBar:YES];
        vc.type_str = @"认购";
        vc.user_dict = dict;
        vc.state_str =@"4";
        [self.navigationController pushViewController:vc animated:YES];
        
    }
    if([self.state_str isEqualToString:@"5"])
    {
        other_applyVC *vc =[[other_applyVC alloc] initWithNavigationBar:YES];
        vc.type_str = @"成交";
        vc.user_dict = dict;
        vc.state_str =@"5";
        [self.navigationController pushViewController:vc animated:YES];
        
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
