//
//  AppSession.m
//  greentea
//
//  Created by Jian Wang on 12-9-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "AppSession.h"
#import "GTMBase64.h"
#import <QuartzCore/QuartzCore.h>

@implementation  AppSession
+ (BOOL)isLogin
{
    
    if([[[[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"] objectForKey:@"isguest"] isEqualToString:@"1"])
        return NO;
    else
        return YES;
}
+ (void)logoff
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"currentUser"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userToken"];
}
+ (void)login:(NSDictionary *)user
{
    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[user objectForKey:@"token"] forKey:@"userToken"];
    [userDefaults setObject:user forKey:@"currentUser"];
   // NSLog(@"%@",[user objectForKey:@"avatar_url"]);
   
   // NSDictionary *dict = [userDefaults objectForKey:@"currentUser"];
   // NSLog(@"%@",[userDefaults objectForKey:@"userToken"]);
   // NSLog(@"current user:\n%@",dict);
}
+ (NSDictionary *)getCurrentUser
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"currentUser"];
}
+ (NSString *)getCurrentUserToken
{
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"userToken"];
}
+ (NSString *)replaceUnicode:(NSString *)unicodeStr
{  
    
    NSString *tempStr1 = [unicodeStr stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"];  
    NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""];  
    NSString *tempStr3 = [[@"\""stringByAppendingString:tempStr2] stringByAppendingString:@"\""];  
    NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];  
    NSString* returnStr = [NSPropertyListSerialization propertyListFromData:tempData  
                                                           mutabilityOption:NSPropertyListImmutable  
                                                                     format:NULL 
                                                           errorDescription:NULL];  
    //    NSLog(@"%@",returnStr);
    return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"];  
}

+ (NSArray *)getDefaultUser{
    NSArray *array=[[NSArray alloc]initWithObjects:@"XX", nil];
    return array;
}

+ (void)ToastAlert:(NSString *)title message:(NSString*)message{
    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:title message:message delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

+ (NSString*) getConverTime:(NSString*)publishTime{
//    publishTime=@"2012-09-18 15:17:00";
    NSString *timeStr= [[NSString alloc]init];
    NSDateFormatter *formatter =[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *preTime= [formatter dateFromString:publishTime];
    NSTimeInterval interval=[preTime timeIntervalSinceNow];
    interval = fabs(interval);
    if(interval>24*60*60){//1天以上  
        timeStr = [timeStr stringByAppendingFormat:@"发布于%d天前",(int)(interval/(24*60*60))];
    }else if(interval>60*60){//1小时-24小时  
        timeStr = [timeStr stringByAppendingFormat:@"发布于%d小时前",(int)(interval/(60*60)) ];
    }else if(interval>60){//1分钟-59分钟  
        timeStr = [timeStr stringByAppendingFormat:@"发布于%d分钟前",(int)(interval/60)];
    }else{//1秒钟-59秒钟  
        timeStr = @"刚刚";  
    }  
    return timeStr;
}
+ (NSString *)forbiddenTime:(NSString *)stratTime and:(NSString *)endTime
{
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    NSDate *sDate = [formatter1 dateFromString:stratTime];
    
    NSDateFormatter *formatter2 = [[NSDateFormatter alloc] init];
    NSDate *eDate = [formatter2 dateFromString:endTime];
    
    NSTimeInterval interval = [sDate timeIntervalSinceDate:eDate];
    
    NSString *tempStr = [[NSString alloc]init];
    
    tempStr = [tempStr stringByAppendingFormat:@"亲，您暂时不能发言，%d分钟后再试试吧",(int)(interval/60)];
    
    return tempStr;
    
}
+ (NSData*)getDefaultUserAvatarData{
    return [self getUserAvatarData:@"avatar_default.jpg"];
}

+ (NSData*)getUserAvatarData:(NSString *)avatar_url{
    
    NSString *str = [WEBAPP_URL stringByAppendingFormat:@"%@", avatar_url];
   // NSLog(@"%@",str);
    return [NSData dataWithContentsOfURL:[NSURL URLWithString:str]];
}
+ (NSString *) getAbsoluteImageUrl:(NSString *) url
{
    if([url isEqual:[NSNull null]]||url.length==0)
        return nil;
    
    if([WEBAPP_URL_V1 hasSuffix:@"/"])
        return [[WEBAPP_URL_V1 substringToIndex:WEBAPP_URL_V1.length-1] stringByAppendingFormat:@"%@",url];
    else
        return [WEBAPP_URL_V1 stringByAppendingFormat:@"%@",url];
}
+ (NSString*) getEncodeImageData:(UIImage*)image{
//    NSData *data = UIImagePNGRepresentation(image);
    NSData *data = UIImageJPEGRepresentation(image, 0.6);
    NSString *str =[[NSString alloc]initWithData:[GTMBase64 encodeData:data] encoding:NSStringEncodingConversionAllowLossy];
    return str;
}

+ (void) checkHttpError:(NSError *)error{
    NSDictionary *dic=[error userInfo];
    [self ToastAlert:nil message:[dic objectForKey:@"NSLocalizedDescription"]];
}

+ (void) checkMessageError:(NSDictionary *)error{
    
    [self ToastAlert:nil message:[[error objectForKey:@"json"]objectForKey:@"message"]];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString
{
    unsigned int hex;
    [[NSScanner scannerWithString:hexString] scanHexInt:&hex];
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:1.0f];
}

+ (UIColor *)colorWithHexString:(NSString *)hexString alpha:(float)alpha
{
    unsigned int hex;
    [[NSScanner scannerWithString:hexString] scanHexInt:&hex];
    int r = (hex >> 16) & 0xFF;
    int g = (hex >> 8) & 0xFF;
    int b = (hex) & 0xFF;
    
    return [UIColor colorWithRed:r / 255.0f
                           green:g / 255.0f
                            blue:b / 255.0f
                           alpha:alpha];
}

//+ (UILabel *) getAutoResizeLabel:(NSString *) text lineBreakMode:(UILineBreakMode) lineBreakMode labelFrame:(CGRect) frame font:(UIFont *) font
//{
//    UILabel *label=[[UILabel alloc] init];
//    label.numberOfLines=0;
//    label.lineBreakMode=lineBreakMode;
//    label.text=text;
//    label.font=font;
//    //width要设置足够高，如果label中字符少，label会自动缩小，如果label字符超过高度，不会自动增高
//    CGSize size=[label.text sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width,9000) lineBreakMode:lineBreakMode];
//    label.frame=CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,size.height);
//    return label;
//}
//+ (UILabel *) getAutoResizeLabel:(NSString *) text lineBreakMode:(UILineBreakMode) lineBreakMode labelFrame:(CGRect) frame font:(UIFont *) font maxHeight:(float)maxHeight//可以设定文本最大高度，如果计算出的文本高度超出此高度，则使用maxHeight，否则，使用计算的高度
//{
//    UILabel *label=[[UILabel alloc] init];
//    label.numberOfLines=0;
//    label.lineBreakMode=lineBreakMode;
//    label.text=text;
//    label.font=font;
//    //width要设置足够高，如果label中字符少，label会自动缩小，如果label字符超过高度，不会自动增高
//    CGSize size=[label.text sizeWithFont:font constrainedToSize:CGSizeMake(frame.size.width,9000) lineBreakMode:lineBreakMode];
//    if(maxHeight<size.height)
//        label.frame=CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,maxHeight);
//    else
//        label.frame=CGRectMake(frame.origin.x,frame.origin.y,frame.size.width,size.height);
//    return label;
//}
+ (UIImage *) getImageFromContentFile:(NSString *) imageName
{
    UIImage *image=[UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
    return image;
}
+ (CustomAvatarImageView *) getRoundImageView:(CGPoint)position radius:(float)radius
{
//    UIImageView *imageView=[[[UIImageView alloc] init] autorelease];
//    imageView.frame=CGRectMake(position.x,position.y,2*radius,2*radius);
//    imageView.layer.cornerRadius=radius;
//    imageView.contentMode=UIViewContentModeScaleAspectFill;
////    imageView.layer.masksToBounds=YES;//使用此方法获得圆形图片，在机子上运行会卡
//    imageView.layer.borderWidth=1;
//    if(isMan==1)
//        imageView.layer.borderColor=[[AppSession colorWithHexString:@"00dbfc"] CGColor];
//    else
    

    CustomAvatarImageView *imageView=[[CustomAvatarImageView alloc] initWithPosition:position radius:radius];//[[[UIImageView alloc] initWithFrame:CGRectMake(position.x,position.y,2*radius,2*radius)] autorelease];//;

    
    
    return imageView;
}

+ (void) makePhoneTo:(NSString *)phoneNumber name:(NSString*)name target:(BaseViewController *)target needSave:(BOOL)needSave
{
    if(needSave){
    //要存储到recentPhone中
    //数组结构如下
    //name:XXX
    //phone_number:XXXXXXXXXXX

    NSMutableArray *array = [[NSUserDefaults standardUserDefaults]objectForKey:@"recentPhone"];
    NSMutableArray * arrayToStore = [NSMutableArray arrayWithArray:array];
    
    if (array!=nil) {
        int i=0;
        for (;i<[array count];i++) {
            
            if ([[[array objectAtIndex:i] objectForKey:@"phone_number"] isEqualToString:phoneNumber]){
                [arrayToStore removeObjectAtIndex:i];
                NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithCapacity:2];
                [dic setValue:phoneNumber forKey:@"phone_number"];
                [dic setValue:name forKey:@"name"];
                [arrayToStore insertObject:dic atIndex:0];
                break;
            }
        }
        if (i>=[array count]) {
            NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithCapacity:2];
            [dic setValue:phoneNumber forKey:@"phone_number"];
            [dic setValue:name forKey:@"name"];
            [arrayToStore insertObject:dic atIndex:0];
            
        }
        
    }else {
        NSMutableDictionary *dic =[[NSMutableDictionary alloc]initWithCapacity:2];
        [dic setValue:phoneNumber forKey:@"phone_number"];
        [dic setValue:name forKey:@"name"];
        arrayToStore = [NSMutableArray arrayWithObject:dic];
    }
    [[NSUserDefaults standardUserDefaults]setValue:arrayToStore forKey:@"recentPhone"];
}
    if([AppSession currentDeviceType]==CurrentDeviceTypeIPHONE)
    {
        [target showUIAlertView:[NSString stringWithFormat:@"拨打电话致%@吗",phoneNumber] message:nil cancelButtonTitle:@"拨打" otherButtonTitles:[NSArray arrayWithObject:@"取消"] onDismiss:^(int buttonIndex) {
        } onCancel:^{
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat: @"tel:/%@",phoneNumber]]];
        }];
    }
    else
    {
         [target showMBAlertCurrentWindow:@"该设备无法拨打电话" duration:1.5f];
//        [target showMBAlertView:target.view message:@"该设备无法拨打电话" duration:1.5f];
    }
}

+ (CurrentDeviceType) currentDeviceType//返回当前设备类型
{
    NSString *deviceType=[[UIDevice currentDevice].model substringToIndex:4];
    //    NSLog(@"现在设备：%@",[[UIDevice currentDevice] name]);
    if([deviceType isEqualToString:@"iPho"])
        return CurrentDeviceTypeIPHONE;
    else if([deviceType isEqualToString:@"iPad"])
        return CurrentDeviceTypeIPAD;
    else if([deviceType isEqualToString:@"iPod"])
        return CurrentDeviceTypeIPOD;
    return CurrentDeviceTypeUnKnow;
}
//判断字符串是否为空
+(BOOL) isBlankString:(NSString *)string
{
    if (string == nil || string == NULL)
    {
        return YES;
    }
    if ([string isKindOfClass:[NSNull class]])
    { return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0)
    { return YES;
    }
    if([string isEqualToString:@"<null>"])
    {
        return YES;
    }
    if([string isEqualToString:@"(null)"])
    {
        return YES;
    }
    
    return NO;
}




+(UIImage *)scaleImage:(UIImage *)image
{
    CGSize size;
    float newFloat,scaleFloat;
    if (image.size.height > image.size.width ) {
        newFloat = image.size.height / 1536;
        size = CGSizeMake(image.size.width / newFloat, 1536);
        scaleFloat = 1536 / image.size.height;
    }else{
        newFloat = image.size.width / 1536;
        size = CGSizeMake(1536, image.size.height / newFloat);
        scaleFloat = 1536 / image.size.width;
    }
    
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    transform = CGAffineTransformScale(transform, scaleFloat, scaleFloat);
    CGContextConcatCTM(context, transform);
    
    // Draw the image into the transformed context and return the image
    [image drawAtPoint:CGPointMake(0.0f, 0.0f)];
    UIImage *newimg = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newimg;
}
+ (UIImage *)scaleAndRotateImage:(UIImage *)image
{
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

+(void) rotateImageFromImageViewWithAnimation:(UIImageView *)imageView degree:(float)degree duration:(float)duration{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGAffineTransform transform=CGAffineTransformMakeRotation(DEGREE_TO_RADIANS(degree));
    imageView.transform=transform;
    [UIView commitAnimations];
}
+ (NSDate*) dateFromString:(NSString*)dateString format:(NSString*)format{
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:format];
    NSDate *date=[dateFormatter dateFromString:dateString];
    return date;
}
//+ (void)uploadUserDeviceToken:(BaseViewController *)target needUserToken:(BOOL)needUserToken//上传用户的deviceToken
//{
//    if ([target.userDefault objectForKey:@"device_token"] == nil) {
//        return;
//    }
//    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
//    [dic setValue:[target.userDefault objectForKey:@"device_token"] forKey:@"device_token"];
//    //[dic setValue:[NSString stringWithFormat:@"%d",NEIGHBORHOODID] forKey:@"neighborhood_id"];
//    if(needUserToken)
//        [dic setValue:[target.userDefault objectForKey:@"userToken"] forKey:@"token"];
//    
//    [target startAsynchronousPostRequest:UPLOAD_DEVICE_TOKEN postData:dic didFinishRequest:^(ASIHTTPRequest *request) {
//        
//    } didFailedRequest:^(ASIHTTPRequest *request) {
//        
//    }];
//
//}

+(void)fadeOut:(UIView *)view
{
    [UIView animateWithDuration:.3 animations:^{
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.alpha = 0.0;
        // [view removeFromSuperview];
        
    } completion:^(BOOL finished) {
        if (finished) {
            view.hidden = YES;
        }
    }];
}
+(void)fadeIn:(UIView *)view
{
    view.transform = CGAffineTransformMakeScale(1, 1);
    view.alpha = 0;
    [UIView animateWithDuration:.3 animations:^{
        view.alpha = 1;
        view.transform = CGAffineTransformMakeScale(1, 1);
        view.hidden = NO;
    }];
    
}

+ (void)resignKeyBoardInView:(UIView *)view
{
    for (UIView *v in view.subviews) {
        if ([v.subviews count] > 0) {
            [self resignKeyBoardInView:v];
        }
        
        if ([v isKindOfClass:[UITextView class]] || [v isKindOfClass:[UITextField class]]) {
            [v resignFirstResponder];
        }
    }
}


+(void)fadeInLayer:(CALayer *)layer
{
    CABasicAnimation *fadeInAnimate   = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeInAnimate.duration            = 0.65f;
    fadeInAnimate.repeatCount         = 1;
    fadeInAnimate.autoreverses        = NO;
    fadeInAnimate.fromValue           = [NSNumber numberWithFloat:0.0];
    fadeInAnimate.toValue             = [NSNumber numberWithFloat:1.0];
    fadeInAnimate.removedOnCompletion = YES;
    [layer addAnimation:fadeInAnimate forKey:@"animateOpacity"];
    return;
}

+(NSString *)setTodayDate:(NSDate *)date;//设置把当天日期转换为星期
{
    NSCalendar *_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents  *comps =[_calendar components:(NSWeekCalendarUnit | NSWeekdayCalendarUnit |NSWeekdayOrdinalCalendarUnit)
                               
                                           fromDate:date];
    
    NSInteger  NSIntegerweekday = [comps weekday]; // 星期几（注意，周日是“1”，周一是“2”。。。。）
    
    NSString *tempStr;
    if(NSIntegerweekday == 1)
    {
        tempStr =@"周日";
    }
    if(NSIntegerweekday == 2)
    {
        tempStr =@"周一";
        
    }
    if(NSIntegerweekday == 3)
    {
        tempStr =@"周二";
        
    }
    if(NSIntegerweekday == 4)
    {
        tempStr =@"周三";
        
    }
    if(NSIntegerweekday == 5)
    {
        tempStr =@"周四";
        
    }
    if(NSIntegerweekday == 6)
    {
        tempStr =@"周五";
        
    }
    if(NSIntegerweekday == 7)
    {
        tempStr =@"周六";
        
    }
    
    return tempStr;

}

@end
