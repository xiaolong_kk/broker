//
//  registerSuccessedVC.m
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-8.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import "registerSuccessedVC.h"
#import "bulidingListVC.h"
#import "MainVC.h"
#import "customListVC.h"
#import "settingVC.h"

@interface registerSuccessedVC ()

@end

@implementation registerSuccessedVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.bounds.size.height)];
    bgImg.image = [UIImage imageNamed:@"loginBg.png"];
    [self.view addSubview:bgImg];
    
    
    UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(102,105,113,120)];
    logoImg.image = [UIImage imageNamed:@"lg301.png"];
    [self.view addSubview:logoImg];

    
    UILabel *titleLabel1=[[UILabel alloc] initWithFrame:CGRectMake(15,logoImg.frame.origin.y+logoImg.frame.size.height+30,170,20)];
    titleLabel1.textAlignment=NSTextAlignmentRight;
    titleLabel1.textColor=[UIColor whiteColor];
    titleLabel1.text = @"恭喜你，成功注册";
    titleLabel1.font=[UIFont fontWithName:strinBoldFontOfSize size:18];
    titleLabel1.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel1];
    
    UILabel *titleLabel2=[[UILabel alloc] initWithFrame:CGRectMake(190,titleLabel1.frame.origin.y,130,20)];
    titleLabel2.textAlignment=NSTextAlignmentLeft;
    titleLabel2.textColor=[AppSession colorWithHexString:@"f98156"];
    titleLabel2.text = @"销冠经纪人";
    titleLabel1.font=[UIFont fontWithName:strinBoldFontOfSize size:18];
    titleLabel2.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel2];
    
    UILabel *titleLabel3=[[UILabel alloc] initWithFrame:CGRectMake(30,titleLabel1.frame.origin.y+titleLabel1.frame.size.height+10,200,20)];
    titleLabel3.textAlignment=NSTextAlignmentLeft;
    titleLabel3.textColor=[AppSession colorWithHexString:@"ffffff"];
    titleLabel3.text = @"接下来,我们将引导您如何使用";
    titleLabel3.font=[UIFont fontWithName:strinBoldFontOfSize size:13];
    titleLabel3.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel3];
    
    UILabel *titleLabel4=[[UILabel alloc] initWithFrame:CGRectMake(215,titleLabel1.frame.origin.y+titleLabel1.frame.size.height+10,110,20)];
    titleLabel4.textAlignment=NSTextAlignmentLeft;
    titleLabel4.textColor=[AppSession colorWithHexString:@"f98156"];
    titleLabel4.text = @"销冠经纪人";
    titleLabel4.font=[UIFont fontWithName:strinBoldFontOfSize size:13];
    titleLabel4.backgroundColor=[UIColor clearColor];
    [self.view addSubview:titleLabel4];


    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame =CGRectMake(35,titleLabel4.frame.size.height+titleLabel4.frame.origin.y+75,115,48);
    [backBtn setTitle:@"立即开始" forState:UIControlStateNormal];
    [backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    backBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"lg302.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(MainTabBarView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:backBtn];
    
    UIButton *registerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    registerBtn.frame =CGRectMake(170,titleLabel4.frame.size.height+titleLabel4.frame.origin.y+75,115,48);
    [registerBtn setTitle:@"我知道了" forState:UIControlStateNormal];
    [registerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    registerBtn.titleLabel.font = [UIFont systemFontOfSize:16];
    [registerBtn setBackgroundImage:[UIImage imageNamed:@"lg303.png"] forState:UIControlStateNormal];
    [registerBtn addTarget:self action:@selector(MainTabBarView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:registerBtn];
}


-(void)MainTabBarView
{
    theApp.tabBarController = [[MainTabBarViewController alloc] init];
    self.tabBarController.delegate = self;
    
    
    MainVC *vc1 = [[MainVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav1 = [[MLNavigationController alloc] initWithRootViewController:vc1];
    nav1.navigationBar.hidden= YES;
    //
    bulidingListVC *vc2 = [[bulidingListVC alloc] initWithNavigationBar:YES];
    vc2.type_str = @"all";
    MLNavigationController *nav2 = [[MLNavigationController alloc] initWithRootViewController:vc2];
    [nav2.navigationBar setHidden:YES];
    //
    customListVC *vc3 = [[customListVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav3 = [[MLNavigationController alloc] initWithRootViewController:vc3];
    nav3.navigationBar.hidden = YES;
    
    settingVC *vc4 = [[settingVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav4 = [[MLNavigationController alloc] initWithRootViewController:vc4];
    nav4.navigationBar.hidden = YES;
    
    NSArray *arrVC = [[NSArray alloc] initWithObjects:nav1,nav2,nav3,nav4,nil];
    [theApp.tabBarController setViewControllers:arrVC animated:YES];
    theApp.MLNav = [[MLNavigationController alloc] initWithRootViewController:theApp.tabBarController];
    theApp.MLNav.navigationBar.hidden = YES;
    if (IOS_7)
    {
        theApp.MLNav.interactivePopGestureRecognizer.enabled = NO;
    }
    [self presentViewController:theApp.MLNav animated:YES completion:nil];
    
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
