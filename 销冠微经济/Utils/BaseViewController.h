//
//  BaseViewController.h
//  baseUtil
//
//  Created by Chai Xue Liang on 12-9-25.
//  Copyright (c) 2012年 xu shun wang. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
#import "MBProgressHUD.h"
#import "BDKNotifyHUD.h"
#import "ASINetworkQueue.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "UIAlertView+MKBlockAdditions.h"
//#import "JSONKit.h"

#define TIME_OUT_SECOND 10


typedef void (^base_request_block)(ASIHTTPRequest *request);
typedef void (^dispatch_stage_block_t)(MBProgressHUD *hud);

typedef enum TimerActionType {
    TimerActionTypeDefault = 0,//默认timer类型，不做任何处理，
    TimerActionTypeStopWhenDisappear = 1,//当页面消失的时候，此定时器停止，当页面显示的时候，此定时器恢复执行。使用此属性，如果重写viewDidAppear或viewDidDisAppear，需要调用[super viewDidAppear]和[super viewDidDisAppear];
} TimerActionType;


@interface BaseViewController : UIViewController

@property (nonatomic,retain) ASINetworkQueue *requestQueue;//网络请求队列请求队列
@property (nonatomic,assign) NSUserDefaults *userDefault;
@property (nonatomic,assign) NSNotificationCenter *notificationCenter;
@property BOOL hiddenBaseNavigation;//提供一个默认的头样式，在此设置是否使用此样式，如果需要，则可以使用titleLabel、leftTitleBtn、rightTitleBtn这几个属性，否则这几个属性为空,默认no
@property (nonatomic,retain) NSMutableArray *timerArray;//存着定时器数组,每个数组对象是一个dictionary，key为timer、actionType分别对应timer对象和timer的动作类型

@property (nonatomic,retain) UIImageView *navigationBg;
@property (nonatomic,retain) UIImageView *logoImg;
@property (nonatomic,retain) UILabel *navigationLabel;
@property (nonatomic,retain) UILabel *navigationSubLabel;
@property (nonatomic,retain) UIButton *navigationLeftTitleBtn;
@property (nonatomic,retain) UIButton *navigationRightTitleBtn;
@property (nonatomic,retain) UIImageView *topImg;
@property(nonatomic,assign) int selectInt;

#pragma mark 内部方法

- (id) initWithNavigationBar:(BOOL)yesOrNo;
- (void) addSubview:(UIView *)subView;//加入的subview会在navigationBg下面。

#pragma mark 定时器
- (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)ti selector:(SEL)aSelector actionType:(TimerActionType)actionType userInfo:(id)userInfo repeats:(BOOL)yesOrNo;//当用此方法注册定时器的时候，需要调用backToUp和backToRoot进行回退
- (void) stopAllScheduledTimer;//不分种类停止所有此页面创建的定时器，当停止某一定时器的后，将其从self.timerArray中移除
- (void) stopScheduledTimerByType:(TimerActionType)actionType;//通过类型停止此页面创建的定时器

#pragma mark 基本的回调
- (void) backToUp:(BOOL) animated;//navigationcontroller中返回上一层
- (void) backToRoot:(BOOL) animated;//返回root层
- (void) onNavigationLeftBtnClick:(UIButton *)sender;
- (void) onNavigationRightBtnClick:(UIButton *)sender;
#pragma mark sdwebimage 方法简化
- (void) setWebImage:(UIImageView *) imageView imageUrl:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage;
- (void) setWebImage:(UIImageView *) imageView imageUrl:(NSString *) imageUrlStr;//给imageView添加网络图片
#pragma mark asi异步下载图片
- (void) setASIWebImage:(UIImageView *) imageView imageUrlStr:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage;
- (void) setASIWebImage:(UIImageView *) imageView imageUrlStr:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage didFinishMethod:(SEL)didFinishMethod didFailedMethod:(SEL)didFailedMethod;
- (void) setRoundImageTo:(UIImageView *)imageView imageUrlStr:(NSString *)imageUrlStr placeholderImage:(UIImage *)placeholderImage;//给指定的imageview设置圆形图片，圆形半径根据imageview的frame。
- (void) setRoundImageTo:(UIImageView *)imageView imageUrlStr:(NSString *)imageUrlStr placeholderImage:(UIImage *)placeholderImage isMan:(int) isMan;
//#pragma mark 通知
//- (void) addObserverToDefaultNotificationCenter:(id)observer selector:(SEL)aSelector name:(NSString *)aName object:(id)anObject;

#pragma mark 弹出框
//为了避免弹出框被键盘遮住的情况
- (void) showMBAlertCurrentWindow:(NSString *) message duration:(float) duration;
- (void) showMBAlertView:(UIView *) currentView message:(NSString *) message duration:(float) duration;//弹出MB对话框，过一段时间结束
- (void) showMBAlertView:(UIView *) currentView message:(NSString *) message duration:(float) duration completionBlock:(dispatch_block_t) completion;//弹出MB对话框，过一段时间结束,完成执行

- (void) showMBAlertView:(UIView *) currentView firstStage:(dispatch_stage_block_t) firstStage, ... NS_REQUIRES_NIL_TERMINATION;//弹出MB对话框，可以有多个阶段

- (void) showMBAlertView:(UIView *) currentView message:(NSString *) message whileExecutingBlock:(dispatch_block_t)block completionBlock:(dispatch_block_t) completion;//弹出MB对话框，在方法执行期间显示

- (UIAlertView*) showUIAlertView:(NSString*) title
                         message:(NSString*) message
               cancelButtonTitle:(NSString*) cancelButtonTitle
               otherButtonTitles:(NSArray*) otherButtons
                       onDismiss:(DismissBlock) dismissed
                        onCancel:(CancelBlock) cancelled;//弹出一个系统的对话框，一个页面只能显示一个系统对话框
#pragma mark 网络链接方法
- (void) stopAllRequest;//停止当前页面所有异步网络连接

- (BOOL) isRequestSucceed:(ASIHTTPRequest *)request;//判断http通信是否成功

- (BOOL) isRequestJsonSucceed:(id)sender;//判断http成功返回后，解析后的json数据status是否为600

- (void) baseDidFailedRequest:(ASIHTTPRequest *)request;//提供一个默认网络连接失败回调

- (BOOL) isBlankString:(NSString *)string;//判断字符串可以为空

/*-----------------------get请求----------------------------------*/
- (void) startAsynchronousRequest:(NSString *) urlStr delegate:(id) delegate  didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest;//开启普通的异步网络请求

- (void) startAsynchronousRequest:(NSString *) urlStr didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest;//开启普通的异步网络请求

- (void) startSynchronousRequest:(NSString *) urlStr delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest;//开启同步网络请求,没有弹出框

- (void) startSynchronousRequest:(NSString *) urlStr target:(UIView *)target alertText:(NSString *)alertText delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest ;//开启同步网络请求，有弹出框

- (void) startSynchronousRequest:(NSString *) urlStr didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest;//开启同步网络请求,没有弹出框,block形式

- (void) startSynchronousRequest:(NSString *) urlStr target:(UIView *)target alertText:(NSString *)alertText didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest; //开启同步网络请求，有弹出框，block形式

/*-----------------------post请求----------------------------------*/
- (void) startAsynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData delegate:(id) delegate  didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest;//开启post异步网络请求
- (void) startAsynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest;//开启post异步网络请求,block形式

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest;//开启post同步网络请求，没有弹出框

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData target:(UIView *)target alertText:(NSString *)alertText delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest ;//开启post同步网络请求，有弹出框

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest;//开启post同步网络请求，没有弹出框,block形式

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData target:(UIView *)target alertText:(NSString *)alertText  didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest ;//开启post同步网络请求，有弹出框，block形式

- (void) startSynchronousPostRequestWithData:(NSString *) urlStr postValue:(NSDictionary *) postValue postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest;//开启post同步网络请求，没有弹出框,block形式，附带数据

- (void) startSynchronousPostRequestWithData:(NSString *) urlStr postValue:(NSDictionary *) postValue postData:(NSDictionary *) postData target:(UIView *)target alertText:(NSString *)alertText  didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest ;//开启post同步网络请求，有弹出框，block形式，附带数据
//有图片的hud
-(void) showBDAlertView:(UIWindow *)window message:(NSString *) message image:(UIImage *) image bgImage:(UIImage *) bgImage duration:(float) duration;
@end
