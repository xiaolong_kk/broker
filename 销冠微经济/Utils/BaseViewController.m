//
//  BaseViewController.m
//  baseUtil
//
//  Created by Chai Xue Liang on 12-9-25.
//  Copyright (c) 2012年 xu shun wang. All rights reserved.
//

#import "BaseViewController.h"
#import "JSONKit.h"
#import "ASIDownloadCache.h"
#import "AppSession.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
@interface BaseViewController ()

@end

@implementation BaseViewController
{
    UIAlertView *baseAlertView;
    NSMutableArray *timerResumeArray;
//    NSMutableArray *notiArray;//存着使用addObserverToDefaultNotificationCenter的监听，此页面释放的时候，将此监听删除。
}
@synthesize requestQueue;
@synthesize userDefault;
@synthesize hiddenBaseNavigation;
@synthesize navigationLabel;
@synthesize navigationLeftTitleBtn;
@synthesize navigationRightTitleBtn;
@synthesize notificationCenter;
@synthesize navigationBg;
@synthesize logoImg;
- (void) dealloc
{
    [baseAlertView release];
    baseAlertView=nil;
    [timerResumeArray release];
    //销毁所有请求线程
    for(ASIHTTPRequest *req in self.requestQueue.operations)
    {
        req.delegate=nil;
        [req cancel];
    }
    
    self.requestQueue=nil;
    self.userDefault=nil;
    
    self.navigationLabel=nil;
    self.navigationLeftTitleBtn=nil;
    self.navigationRightTitleBtn=nil;
    self.notificationCenter=nil;
    self.navigationBg=nil;
    
    self.timerArray=nil;
    [super dealloc];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.requestQueue=[ASINetworkQueue queue];
        self.requestQueue.maxConcurrentOperationCount=1;//最大同时连接数为1
        self.requestQueue.shouldCancelAllRequestsOnFailure=NO;//设置当一个连接失败的时候，其他继续
        
        self.userDefault=[NSUserDefaults standardUserDefaults];
        self.notificationCenter=[NSNotificationCenter defaultCenter];
        
        self.timerArray=[NSMutableArray array];
        timerResumeArray=[[NSMutableArray alloc] init];
        
//        notiArray=[[NSMutableArray alloc] init];
        self.hiddenBaseNavigation=YES;
    }
    return self;
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait||interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}
- (void)viewDidAppear:(BOOL)animated
{
    [self resumeUnScheduledTimerInResumeArray];
    [super viewDidAppear:animated];
    
   }
- (void) viewDidDisappear:(BOOL)animated
{
    //界面消失的时候停止TimerActionTypeStopWhenDisappear属性的定时器
    [self stopScheduledTimerByType:TimerActionTypeStopWhenDisappear];
    [super viewDidDisappear:animated];
    
}
- (id) initWithNavigationBar:(BOOL)yesOrNo
{
    self=[self initWithNibName:nil bundle:nil];
    if(self)
    {
        self.hiddenBaseNavigation=!yesOrNo;
    }
    return self;
}
- (void) addSubview:(UIView *)subView
{
    [self.view addSubview:subView];
    [self.view bringSubviewToFront:self.navigationBg];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    

	// Do any additional setup after loading the view.
    if(!self.hiddenBaseNavigation)
    {
        [self initNavigation];
    }
  //  self.view.frame = CGRectMake(0, 0, 320, 548);
    self.view.backgroundColor=[AppSession colorWithHexString:@"f6f6f6"];
}

//判断字符串是否为空
- (BOOL) isBlankString:(NSString *)string
{
    if (string == nil || string == NULL)
   {
    return YES;
    }
    if ([string isKindOfClass:[NSNull class]])
    { return YES;
    }
    if ([[string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length]==0)
    { return YES;
    }
    if([string isEqualToString:@"<null>"])
    {
        return YES;
    }
    if([string isEqualToString:@"(null)"])
    {
        return YES;
    }
    
    return NO;
}
-(void) initNavigation
{
    
   self.topImg=[[[UIImageView alloc]initWithFrame:CGRectMake(0,0, 320,20)] autorelease];
    [self.topImg setImage:[UIImage imageNamed:@"mainview_topbar.png"]];
    self.topImg.hidden = YES;
    self.topImg.userInteractionEnabled=YES;
    [self.view addSubview:self.topImg];
    
    float  addHight = 0;

    if(IOS_7)
    {
       addHight = 20;
        self.topImg.hidden = NO;
    }

    //navigation背景图
    self.navigationBg=[[[UIImageView alloc]initWithFrame:CGRectMake(0,0+addHight, 320,44)] autorelease];
    [self.navigationBg setImage:[UIImage imageNamed:@"mainview_topbar.png"]];
    self.navigationBg.userInteractionEnabled=YES;
    [self.view addSubview:self.navigationBg];
    
//    self.logoImg  = [[[UIImageView alloc] initWithFrame:CGRectMake(126.5, 10, 67, 31)] autorelease];
//    self.logoImg.image = [UIImage imageNamed:@"mainview_top_logo.png"];
//    [self.navigationBg addSubview:self.logoImg];

    //左边按钮
    self.navigationLeftTitleBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    self.navigationLeftTitleBtn.frame=CGRectMake(0,0,32,44);
    [self.navigationLeftTitleBtn setImage:[UIImage imageNamed:@"arrow_back.png"] forState:UIControlStateNormal];
    [self.navigationLeftTitleBtn addTarget:self action:@selector(onNavigationLeftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBg addSubview:self.navigationLeftTitleBtn];
    
    //右边按钮
    self.navigationRightTitleBtn =[UIButton buttonWithType:UIButtonTypeCustom];
    self.navigationRightTitleBtn.frame=CGRectMake(276+5,0,32,44);
//    [self.navigationRightTitleBtn setBackgroundImage:[UIImage imageNamed:@"tp_btn_menu.png"] forState:UIControlStateNormal];
    [self.navigationRightTitleBtn addTarget:self action:@selector(onNavigationRightBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.navigationBg addSubview:self.navigationRightTitleBtn];
    
    //文字
    self.navigationLabel=[[[UILabel alloc] initWithFrame:CGRectMake(44,0,232,44)] autorelease];
    self.navigationLabel.textAlignment=NSTextAlignmentCenter;
    self.navigationLabel.font=[UIFont fontWithName:@"Helvetica-Bold"size:21];
    self.navigationLabel.textColor=[AppSession colorWithHexString:@"ffffff"];
     self.navigationLabel.backgroundColor=[UIColor clearColor];
    [self.navigationBg addSubview:self.navigationLabel];
    

    self.navigationSubLabel=[[[UILabel alloc] initWithFrame:CGRectMake(44,12,232,44)] autorelease];
    self.navigationSubLabel.textAlignment=NSTextAlignmentCenter;
    self.navigationSubLabel.font=[UIFont fontWithName:@"Helvetica-Bold"size:13];
    self.navigationSubLabel.textColor=[AppSession colorWithHexString:@"a24336"];
    self.navigationSubLabel.backgroundColor=[UIColor clearColor];
    [self.navigationBg addSubview:self.navigationSubLabel];
    
    //右划手势
//    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(backToUp:)];
//    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
//    [swipe setEnabled:YES];
//    [self.view addGestureRecognizer:swipe];
//    [swipe release];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

//- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
//    {
//        [self.view endEditing:YES];
//    }
#pragma mark 定时器
- (NSTimer *) scheduledTimerWithTimeInterval:(NSTimeInterval)ti selector:(SEL)aSelector actionType:(TimerActionType)actionType userInfo:(id)userInfo repeats:(BOOL)yesOrNo
{
    NSTimer *timer=[NSTimer scheduledTimerWithTimeInterval:ti target:self selector:aSelector userInfo:userInfo repeats:yesOrNo];
    
    //记录此timer对象属性，用于重建及其他用途，由于NSTimer对象不提供下面的一些属性，需要自己额外记录
    NSMutableDictionary *dic=[NSMutableDictionary dictionaryWithCapacity:6];
    [dic setObject:timer forKey:@"timer"];
    [dic setObject:[NSNumber numberWithDouble:ti] forKey:@"interval"];
    [dic setObject:[NSValue valueWithPointer:aSelector] forKey:@"selector"];
    [dic setObject:[NSNumber numberWithInt:actionType] forKey:@"actionType"];
    [dic setValue:userInfo forKey:@"userInfo"];
    [dic setObject:[NSNumber numberWithBool:yesOrNo] forKey:@"repeat"];
    
    [self.timerArray addObject:dic];
    
    return timer;
}

- (void) stopAllScheduledTimer
{
    for(int i=0;i<self.timerArray.count;i++)
    {
        NSDictionary *dic=[self.timerArray objectAtIndex:i];
        NSTimer *timer=[dic objectForKey:@"timer"];
        [timer invalidate];
        [self.timerArray removeObjectAtIndex:i];
    }
}
- (void) stopScheduledTimerByType:(TimerActionType)actionType
{
    for(int i=0;i<self.timerArray.count;i++)
    {
        NSDictionary *dic=[self.timerArray objectAtIndex:i];
        NSTimer *timer=[dic objectForKey:@"timer"];
        NSNumber *type=[dic objectForKey:@"actionType"];
        if(type.intValue==actionType)
        {
            [timer invalidate];
            //将此timer存入恢复数组用于重建
            [timerResumeArray addObject:dic];
            
            //在当前timer数组中删除此timer
            [self.timerArray removeObjectAtIndex:i];
        }
    }
}
//内部方法，将此页面被停止的，保存在恢复数组里面的timer恢复
- (void) resumeUnScheduledTimerInResumeArray
{
    for(int i=0;i<timerResumeArray.count;i++)
    {
        //        [dic setObject:[NSNumber numberWithDouble:ti] forKey:@"interval"];
        //        [dic setObject:[NSValue valueWithPointer:aSelector] forKey:@"selector"];
        //        [dic setObject:[NSNumber numberWithInt:actionType] forKey:@"actionType"];
        //        [dic setValue:userInfo forKey:@"userInfo"];
        //        [dic setObject:[NSNumber numberWithBool:yesOrNo] forKey:@"repeat"];
        //
        NSDictionary *dic=[timerResumeArray objectAtIndex:i];
        
        double interval=[[dic objectForKey:@"interval"] doubleValue];
        SEL selector=[[dic objectForKey:@"selector"] pointerValue];
        TimerActionType actionType=[[dic objectForKey:@"actionType"] intValue];
        id userInfo=[dic objectForKey:@"userInfo"];
        BOOL repeat=[[dic objectForKey:@"repeat"] boolValue];
        
        [self scheduledTimerWithTimeInterval:interval selector:selector actionType:actionType userInfo:userInfo repeats:repeat];
        
        [timerResumeArray removeObjectAtIndex:i];
    }
}

#pragma mark 基本回调
//navigationcontroller中返回上一层
- (void) backToUp:(BOOL) animated
{
    [self stopAllScheduledTimer];//在退回界面的时候停止此界面所有定时器
    [self.navigationController popViewControllerAnimated:animated];
}
//返回root层
- (void) backToRoot:(BOOL) animated
{
    [self stopAllScheduledTimer];//在退回界面的时候停止此界面所有定时器
    [self.navigationController popToRootViewControllerAnimated:animated];
}
- (void) onNavigationLeftBtnClick:(UIButton *)sender
{
    UINavigationController *nav = super.self.navigationController;

    [nav popViewControllerAnimated:YES];
}
- (void) onNavigationRightBtnClick:(UIButton *)sender
{
    [self backToRoot:YES];
}
#pragma mark sdwebimage 方法简化
- (void) setWebImage:(UIImageView *) imageView imageUrl:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage
{
    [imageView setImageWithURL:[NSURL URLWithString:imageUrlStr] placeholderImage:placeholderImage];
}
- (void) setWebImage:(UIImageView *) imageView imageUrl:(NSString *) imageUrlStr
{
    [imageView setImageWithURL:[NSURL URLWithString:imageUrlStr]];
}
#pragma mark asi异步下载图片
//asi异步加载图片基本方法
- (void) setASIWebImage:(UIImageView *) imageView imageUrlStr:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage didFinishMethod:(SEL)didFinishMethod didFailedMethod:(SEL)didFailedMethod
{
    ASIDownloadCache *cache=[ASIDownloadCache sharedCache];
    [cache setDefaultCachePolicy:ASIOnlyLoadIfNotCachedCachePolicy];//不管缓存过不过期，都拿缓存数据
    
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:imageUrlStr]];
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    [dic setValue:imageView forKey:@"imageView"];
    [dic setValue:placeholderImage forKey:@"holderImage"];
    
    [request setUserInfo:dic];
    [request setTimeOutSeconds:TIME_OUT_SECOND];
    [request setCacheStoragePolicy:ASICachePermanentlyCacheStoragePolicy];//设置数据永久缓存在本地
    [request setDownloadCache:cache];
    [request setDelegate:self];
    [request setDidFinishSelector:didFinishMethod];
    [request setDidFailSelector:didFailedMethod];
    [requestQueue addOperation:request];
    
    [requestQueue go];
}
//asi基本加载图片方法
- (void) setASIWebImage:(UIImageView *) imageView imageUrlStr:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage
{
    [self setASIWebImage:imageView imageUrlStr:imageUrlStr placeholderImage:placeholderImage didFinishMethod:@selector(didRequestASIImageFinish:) didFailedMethod:@selector(didRequestASIImageFailed:)];
}
//基本图片加载回调
- (void) didRequestASIImageFinish:(ASIHTTPRequest *)request
{
    NSData *data=request.responseData;
    UIImage *image=[UIImage imageWithData:data];
    NSDictionary *dic=request.userInfo;
    UIImageView *imageView=[dic objectForKey:@"imageView"];
    imageView.image=image;
}
- (void) didRequestASIImageFailed:(ASIHTTPRequest *)request
{
    NSDictionary *dic=request.userInfo;
    UIImageView *imageView=[dic objectForKey:@"imageView"];
    UIImage *holderImage=[dic objectForKey:@"holderImage"];
    imageView.image=holderImage;
}

//获得图片，并将之剪切为圆形。
- (void) setRoundImageTo:(UIImageView *)imageView imageUrlStr:(NSString *) imageUrlStr placeholderImage:(UIImage *)placeholderImage
{
    imageView.layer.cornerRadius=imageView.frame.size.width/2;
    imageView.layer.masksToBounds=YES;//使用此方法获得圆形图片，在机子上运行会卡
    imageView.layer.borderWidth=1;
    
    [self setASIWebImage:imageView imageUrlStr:imageUrlStr placeholderImage:placeholderImage didFinishMethod:@selector(didFinishRequestRoundImage:) didFailedMethod:@selector(didFailedRequestRoundImage:)];
}
- (void) setRoundImageTo:(UIImageView *)imageView imageUrlStr:(NSString *)imageUrlStr placeholderImage:(UIImage *)placeholderImage isMan:(int) isMan
{
    imageView.layer.cornerRadius=imageView.frame.size.width/2;
    imageView.layer.masksToBounds=YES;//使用此方法获得圆形图片，在机子上运行会卡
    imageView.layer.borderWidth=1;
    if(isMan==1)
        imageView.layer.borderColor=[[AppSession colorWithHexString:@"00dbfc"] CGColor];
    else
        imageView.layer.borderColor=[[AppSession colorWithHexString:@"fc0088"] CGColor];
    
    [self setASIWebImage:imageView imageUrlStr:imageUrlStr placeholderImage:placeholderImage didFinishMethod:@selector(didFinishRequestRoundImage:) didFailedMethod:@selector(didFailedRequestRoundImage:)];
}
- (void)didFinishRequestRoundImage:(ASIHTTPRequest *)request
{
    NSData *data=request.responseData;
    UIImage *image=[UIImage imageWithData:data];
    NSDictionary *dic=request.userInfo;
    UIImageView *imageView=[dic objectForKey:@"imageView"];
    imageView.image=[AppSession createRoundedRectImage:image size:imageView.frame.size];
}
- (void)didFailedRequestRoundImage:(ASIHTTPRequest *)request
{
    NSDictionary *dic=request.userInfo;
    UIImageView *imageView=[dic objectForKey:@"imageView"];
    UIImage *holderImage=[dic objectForKey:@"holderImage"];
    imageView.image=[AppSession createRoundedRectImage:holderImage size:imageView.frame.size];
}
#pragma mark 弹出框
- (void) showMBAlertView:(UIView *) currentView message:(NSString *) message duration:(float) duration
{
    MBProgressHUD *hud=[[MBProgressHUD alloc] initWithView:currentView];
    [currentView addSubview:hud];
    [hud setMode:MBProgressHUDModeText];
    hud.labelText=message;
    hud.margin=10.f;
    hud.yOffset=10.f;
    hud.removeFromSuperViewOnHide=YES;
    [hud show:YES];
    [hud hide:YES afterDelay:duration];
    [hud release];
}
-(void) showBDAlertView:(UIWindow *)window message:(NSString *) message image:(UIImage *) image bgImage:(UIImage *) bgImage duration:(float) duration
{
    
    BDKNotifyHUD* notify = [BDKNotifyHUD notifyHUDWithImage:image bgImage:bgImage text:message];
    window.userInteractionEnabled = NO;
    [window addSubview:notify];
    notify.center = CGPointMake(0,0);
    [notify presentWithDuration:duration speed:.5f inView:window completion:^{
        [notify removeFromSuperview];
        window.userInteractionEnabled = YES;
    }];
        

}

- (void) showMBAlertCurrentWindow:(NSString *) message duration:(float) duration
{
    UIWindow *currentWindow=[[[UIApplication sharedApplication]windows]lastObject];
    MBProgressHUD *hud=[[MBProgressHUD alloc] initWithWindow:currentWindow];
    [currentWindow addSubview:hud];
    [hud setMode:MBProgressHUDModeText];
    hud.labelText=message;
    hud.margin=10.f;
    hud.yOffset=10.f;
    hud.removeFromSuperViewOnHide=YES;
    [hud show:YES];
    [hud hide:YES afterDelay:duration];
    [hud release];
}
- (void) showMBAlertView:(UIView *) currentView message:(NSString *) message duration:(float) duration completionBlock:(dispatch_block_t) completion//弹出MB对话框，过一段时间结束,完成执行
{    
    MBProgressHUD *hud=[[MBProgressHUD alloc] initWithView:currentView];
    [currentView addSubview:hud];
    [hud setMode:MBProgressHUDModeText];
    hud.labelText=message;
    hud.margin=10.f;
    hud.yOffset=10.f;
    [hud showAnimated:NO whileExecutingBlock:^{
        sleep(duration);
    } completionBlock:^{
        [hud removeFromSuperview];
        [hud release];
        completion();
    }];
}
//MBHUD 多步骤
- (void) showMBAlertView:(UIView *) currentView firstStage:(dispatch_stage_block_t) firstStage, ... NS_REQUIRES_NIL_TERMINATION
{
    va_list args;
    va_start(args, firstStage); // scan for arguments after firstObject.
    NSMutableArray *array=[NSMutableArray array];
    for (dispatch_stage_block_t stage = firstStage; stage != nil; stage = va_arg(args,dispatch_stage_block_t))
    {
        [array addObject:stage];
    }
    va_end(args);
    
    MBProgressHUD *hud=[[MBProgressHUD alloc] initWithView:currentView];
    [currentView addSubview:hud];
    [hud setMode:MBProgressHUDModeText];
    hud.margin=10.f;
    hud.yOffset=10.f;
    hud.removeFromSuperViewOnHide=YES;
    
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    [dic setObject:array forKey:@"stages"];
    [dic setObject:hud forKey:@"hud"];
    
    [hud showWhileExecuting:@selector(mbMixTask:) onTarget:self withObject:dic animated:YES];
    [hud release];
}
- (void) mbMixTask:(NSDictionary *) dic
{
    NSArray *stages=[dic objectForKey:@"stages"];
    MBProgressHUD *hud=[dic objectForKey:@"hud"];
    for(dispatch_stage_block_t stage in stages)
    {
        stage(hud);
    }
}

- (void) showMBAlertView:(UIView *) currentView message:(NSString *) message whileExecutingBlock:(dispatch_block_t)block completionBlock:(dispatch_block_t) completion
{
    MBProgressHUD *hud=[[MBProgressHUD alloc] initWithView:currentView];
    hud.labelText=message;
    [currentView addSubview:hud];
    [hud showAnimated:YES whileExecutingBlock:block completionBlock:^{
        [hud removeFromSuperview];
        [hud release];
        
        completion();
    }];
}

- (UIAlertView*) showUIAlertView:(NSString*) title
                            message:(NSString*) message
                  cancelButtonTitle:(NSString*) cancelButtonTitle
                  otherButtonTitles:(NSArray*) otherButtons
                          onDismiss:(DismissBlock) dismissed
                           onCancel:(CancelBlock) cancelled
{
    //可能还会有泄露，需要再查查
    //屏蔽多个弹出框的情况
    if(baseAlertView==nil)
    {
        baseAlertView=[UIAlertView alertViewWithTitle:title message:message cancelButtonTitle:cancelButtonTitle otherButtonTitles:otherButtons onDismiss:^(int buttonIndex) {
            dismissed(buttonIndex);
            baseAlertView=nil;
        } onCancel:^{
            cancelled();
            baseAlertView=nil;
        }];
    }
    return baseAlertView;
}
//#pragma mark 通知
//- (void) addObserverToDefaultNotificationCenter:(id)observer selector:(SEL)aSelector name:(NSString *)aName object:(id)anObject
//{
//    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
//    [dic setObject:aName forKey:@"name"];
//    [dic setObject:observer forKey:@"observer"];
//    //在dealloc中释放oberver和aName，但是存在字典以后，observer释放不了，执行不到dealloc;
//    [self.notificationCenter addObserver:observer selector:aSelector name:aName object:anObject];
//    
//    
//    [self.notificationCenter removeObserver:<#(id)#> name:<#(NSString *)#> object:<#(id)#>]
//}
#pragma mark 网络链接方法
- (void) stopAllRequest
{
    for(ASIHTTPRequest *req in self.requestQueue.operations)
    {
        req.delegate=nil;
        [req cancel];
    }
    //由于不知道如何将requestQueue中的取消的request删除，所以只能重新创建了
    self.requestQueue=[ASINetworkQueue queue];
    self.requestQueue.maxConcurrentOperationCount=1;//最大同时连接数为1
    self.requestQueue.shouldCancelAllRequestsOnFailure=NO;//设置当一个连接失败的时候，其他继续
}
//判断http通信是否成功
- (BOOL) isRequestSucceed:(ASIHTTPRequest *)request
{
   // NSLog(@"%@",request.responseString);
    if(request.responseStatusCode/100==2)
        return YES;
    else
        return NO;
}
- (BOOL) isRequestJsonSucceed:(id)sender//判断http成功返回后，解析后的json数据status是否为600
{
    if([sender isKindOfClass:[NSDictionary class]])
    {
        id status=[sender objectForKey:@"status"];
        if(status==nil)
            return YES;
        else if([status intValue]==600)
            return YES;
    }
    if([sender isKindOfClass:[NSArray class]])//是NSArray对象，说明不会有status，肯定是成功返回
    {
            return YES;
    }
    return NO;
}
- (void) baseDidFailedRequest:(ASIHTTPRequest *)request//默认网络连接失败回调
{
    
   // NSLog(@"%@",request.responseString);
    // [self showBDAlertView:theApp.window message:@"哎呀，网络不给力，稍后再试试吧" image:[UIImage imageNamed:@"icon_mind.png"] duration:0.0f];
    
  //  [self showBDAlertView:theApp.window message:@"哎呀，网络不给力，稍后再试试吧" image:[UIImage imageNamed:@"tanhao.png"] bgImage:[UIImage imageNamed:@"tishi_large.png"] duration:0.25f];

//    NSLog(@"status code:%d\n respString:%@",request.responseStatusCode,request.responseString);
//    NSString *str=@"网络连接请求失败";
    //id dic=[request.responseString objectFromJSONString];
    
   // NSLog(@"%@",dic);

//    if(request.responseStatusCode==401)
//    {
//        id dic=[request.responseString objectFromJSONString];
//        NSLog(@"%@",[dic objectForKey:@"error"]);
//        if([[dic objectForKey:@"error"] isEqualToString:@"401 认证失败!"])
//        {
//            [self.userDefault removeObjectForKey:@"userToken"];
//            [self.userDefault removeObjectForKey:@"currentUser"];
//            
//            [self showMBAlertView:self.view message:@"请重新登录" duration:2 completionBlock:^{
//                if([self respondsToSelector:@selector(menuController)])
//                {
//                    //UIViewController *controller=[self menuController];
//                    [self.navigationController popToRootViewControllerAnimated:YES];
//                }
//                else
//                {
//                    [self backToRoot:YES];
//                }
////                NSLog(@"%@",self.menuController);
////                [self backToRoot:YES];
//            }];
//        }
//    
//    }
//    else
//        [self showBDAlertView:self.view message:@"哎呀，网络不给力，稍后再试试吧" image:[UIImage imageNamed:@"icon_mind.png"] duration:1.5f];
}
//供asyn request使用的回调，内部使用
- (void) asynRequestDidFinished:(ASIHTTPRequest *)request
{
    base_request_block block= [request.userInfo objectForKey:@"finishedMethod"];
   // block(request);
}
- (void) asynRequestDidFailed:(ASIHTTPRequest *)request
{
    base_request_block block= [request.userInfo objectForKey:@"failedMethod"];
    block(request);
}
/*-----------------------get请求----------------------------------*/
//开启普通的异步网络请求
- (void) startAsynchronousRequest:(NSString *) urlStr delegate:(id) delegate  didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest
{
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setDelegate:delegate];
    [request setDidFinishSelector:didFinishRequest];
    [request setDidFailSelector:didFailedRequest];
    request.timeOutSeconds=TIME_OUT_SECOND;
    [self.requestQueue addOperation:request];
    [self.requestQueue go];
    //NSLog(@"request:%@",request);
}
- (void) startAsynchronousRequest:(NSString *) urlStr didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest//开启普通的异步网络请求,block形式
{
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    [dic setObject:didFinishRequest forKey:@"finishedMethod"];
    [dic setObject:didFailedRequest forKey:@"failedMethod"];
    
    ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(asynRequestDidFinished:)];
    [request setDidFailSelector:@selector(asynRequestDidFailed:)];
    request.userInfo=dic;
    request.timeOutSeconds=TIME_OUT_SECOND;
    [self.requestQueue addOperation:request];
    [self.requestQueue go];
}
//开启普通的同步网络请求
- (void) startSynchronousRequest:(NSString *) urlStr delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest showAlertView:(BOOL) showAlertView target:(UIView *)target alertText:(NSString *)alertText
{
    
    if(!showAlertView)
    {
        ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.timeOutSeconds=TIME_OUT_SECOND;
        [request startSynchronous];
        if([self isRequestSucceed:request])
            [delegate performSelector:didFinishRequest withObject:request];
        else
            [delegate performSelector:didFailedRequest withObject:request];
    }
    else
    {
        __block ASIHTTPRequest *request;
        [self showMBAlertView:target message:alertText whileExecutingBlock:^{
            request=[[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlStr]] retain];
            request.timeOutSeconds=TIME_OUT_SECOND;
            [request startSynchronous];
        } completionBlock:^{
            if([self isRequestSucceed:request])
                [delegate performSelector:didFinishRequest withObject:request];
            else
                [delegate performSelector:didFailedRequest withObject:request];
            [request release];
        }];
    }
}
- (void) startSynchronousRequest:(NSString *) urlStr delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest//开启同步网络请求,没有弹出框
{
    [self startSynchronousRequest:urlStr delegate:delegate didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest showAlertView:NO target:nil alertText:nil];
}

- (void) startSynchronousRequest:(NSString *) urlStr target:(UIView *)target alertText:(NSString *)alertText delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest//开启同步网络请求，有弹出框
{
    [self startSynchronousRequest:urlStr delegate:delegate didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest showAlertView:YES target:target alertText:alertText];
}

//开启普通的同步网络请求,block形式
- (void) startSynchronousRequest:(NSString *) urlStr showAlertView:(BOOL) showAlertView target:(UIView *)target alertText:(NSString *)alertText didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest 
{
    
    if(!showAlertView)
    {
        ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlStr]];
        request.timeOutSeconds=TIME_OUT_SECOND;
        [request startSynchronous];
        if([self isRequestSucceed:request])
            didFinishRequest(request);
        else
            didFailedRequest(request);
    }
    else
    {
        
        __block ASIHTTPRequest *request;
        [self showMBAlertView:target message:alertText whileExecutingBlock:^{
            request=[[ASIHTTPRequest requestWithURL:[NSURL URLWithString:urlStr]] retain];
            request.timeOutSeconds=TIME_OUT_SECOND;
            [request startSynchronous];
           // NSLog(@"%@",request.responseString);
        } completionBlock:^{
            if([self isRequestSucceed:request])
                didFinishRequest(request);
            else
                didFailedRequest(request);
            [request release];
        }];
    }
}
- (void) startSynchronousRequest:(NSString *) urlStr didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest//开启同步网络请求,没有弹出框,block形式
{
    [self startSynchronousRequest:urlStr showAlertView:NO target:nil alertText:nil didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest];
}

- (void) startSynchronousRequest:(NSString *) urlStr target:(UIView *)target alertText:(NSString *)alertText didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest //开启同步网络请求，有弹出框，block形式
{
    [self startSynchronousRequest:urlStr showAlertView:YES target:target alertText:alertText didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest];
}

/*-----------------------post请求----------------------------------*/
//开启post异步网络请求
- (void) startAsynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData delegate:(id) delegate  didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest;//开启post异步网络请求
{
    ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    for(NSString *key in postData.keyEnumerator)
    {
        [request setPostValue:[postData objectForKey:key] forKey:key];
    }
    [request setDelegate:delegate];
    [request setDidFinishSelector:didFinishRequest];
    [request setDidFailSelector:didFailedRequest];
    request.timeOutSeconds=TIME_OUT_SECOND;
    [self.requestQueue addOperation:request];
    [self.requestQueue go];
}
- (void) startAsynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest//开启post异步网络请求,block形式
{
    NSMutableDictionary *dic=[NSMutableDictionary dictionary];
    [dic setObject:didFinishRequest forKey:@"finishedMethod"];
    [dic setObject:didFailedRequest forKey:@"failedMethod"];
    
    ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    for(NSString *key in postData.keyEnumerator)
    {
        [request setPostValue:[postData objectForKey:key] forKey:key];
    }
    [request setDelegate:self];
    [request setDidFinishSelector:@selector(asynRequestDidFinished:)];
    [request setDidFailSelector:@selector(asynRequestDidFailed:)];
    request.timeOutSeconds=TIME_OUT_SECOND;
    request.userInfo=dic;
    [self.requestQueue addOperation:request];
    [self.requestQueue go];
}
//开启post同步网络请求
- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest showAlertView:(BOOL) showAlertView target:(UIView *)target alertText:(NSString *)alertText
{
    if(!showAlertView)
    {
        ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
        for(NSString *key in postData.keyEnumerator)
        {
            [request setPostValue:[postData objectForKey:key] forKey:key];
        }
        request.timeOutSeconds=TIME_OUT_SECOND;
        [request startSynchronous];
        if([self isRequestSucceed:request])
            [delegate performSelector:didFinishRequest withObject:request];
        else
            [delegate performSelector:didFailedRequest withObject:request];
    }
    else
    {
        __block ASIFormDataRequest *request;
        [self showMBAlertView:target message:alertText whileExecutingBlock:^{
            request=[[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]] retain];
            for(NSString *key in postData.keyEnumerator)
            {
                [request setPostValue:[postData objectForKey:key] forKey:key];
            }
            request.timeOutSeconds=TIME_OUT_SECOND;
            [request startSynchronous];
        } completionBlock:^{
            if([self isRequestSucceed:request])
                [delegate performSelector:didFinishRequest withObject:request];
            else
                [delegate performSelector:didFailedRequest withObject:request];
            [request release];
        }];
    }
}
- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest//开启post同步网络请求，没有弹出框
{
    [self startSynchronousPostRequest:urlStr postData:postData delegate:delegate didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest showAlertView:NO target:nil alertText:nil];
}

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData target:(UIView *)target alertText:(NSString *)alertText delegate:(id) delegate didFinishRequest:(SEL)didFinishRequest  didFailedRequest:(SEL)didFailedRequest//开启post同步网络请求，有弹出框
{
    [self startSynchronousPostRequest:urlStr postData:postData delegate:delegate didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest showAlertView:YES target:target alertText:alertText];
}

//开启post同步网络请求,block形式
- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest showAlertView:(BOOL) showAlertView target:(UIView *)target alertText:(NSString *)alertText
{
   // NSLog(@"%@",urlStr);
    if(!showAlertView)
    {
        ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
        NSArray *keys = [postData allKeys];
        
        for (id key in keys) {
            id value = [postData objectForKey:key];
            if ([value isKindOfClass:[UIImage class]])
            {
                [request setData:UIImageJPEGRepresentation(value, 0.75f) withFileName:[key stringByAppendingString:@".jpg"] andContentType:@"image/jpeg;charset=utf-8" forKey:key];
            }
            else
            {
                [request setPostValue:value forKey:key];
            }
        }
        
//        for(NSString *key in postData.keyEnumerator)
//        {
//            [request setPostValue:[postData objectForKey:key] forKey:key];
//        }
        

        
        request.timeOutSeconds=TIME_OUT_SECOND;
        [request startSynchronous];
        if([self isRequestSucceed:request])
            didFinishRequest(request);
        else
            didFailedRequest(request);
    }
    else
    {
        __block ASIFormDataRequest *request;
        [self showMBAlertView:target message:alertText whileExecutingBlock:^{
            request=[[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]] retain];
            NSArray *keys = [postData allKeys];
            
            for (id key in keys) {
                id value = [postData objectForKey:key];
                if ([value isKindOfClass:[UIImage class]])
                {
                    [request setData:UIImageJPEGRepresentation(value, 0.75f) withFileName:[key stringByAppendingString:@".jpg"] andContentType:@"image/jpeg;charset=utf-8" forKey:key];
                }
                else
                {
                    [request setPostValue:value forKey:key];
                }
            }
            
//            for(NSString *key in postData.keyEnumerator)
//            {
//                [request setPostValue:[postData objectForKey:key] forKey:key];
//            }

            request.timeOutSeconds=TIME_OUT_SECOND;
            [request startSynchronous];

        } completionBlock:^{
            if([self isRequestSucceed:request])
                didFinishRequest(request);
            else
                didFailedRequest(request);
            [request release];
        }];
    }
}

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest//开启post同步网络请求，没有弹出框,block形式
{
    [self startSynchronousPostRequest:urlStr postData:postData didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest showAlertView:NO target:nil alertText:nil];
}

- (void) startSynchronousPostRequest:(NSString *) urlStr postData:(NSDictionary *) postData target:(UIView *)target alertText:(NSString *)alertText  didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest //开启post同步网络请求，有弹出框，block形式
{
    [self startSynchronousPostRequest:urlStr postData:postData didFinishRequest:didFinishRequest didFailedRequest:didFailedRequest showAlertView:YES target:target alertText:alertText];
}

//post，block形式 附带数据
- (void) startSynchronousPostRequestWithData:(NSString *) urlStr postValue:(NSDictionary *) postValue postData:(NSDictionary *) postData didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest//开启post同步网络请求，没有弹出框,block形式，附带数据
{
    ASIFormDataRequest *request=[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]];
    for(NSString *key in postValue.keyEnumerator)
    {
        [request setPostValue:[postValue objectForKey:key] forKey:key];
    }
    for(NSString *key in postData.keyEnumerator)
    {
        [request addData:[postData objectForKey:key] forKey:key];
    }
    request.timeOutSeconds=TIME_OUT_SECOND;
    [request startSynchronous];
    if([self isRequestSucceed:request])
        didFinishRequest(request);
    else
        didFailedRequest(request);
}

- (void) startSynchronousPostRequestWithData:(NSString *) urlStr postValue:(NSDictionary *) postValue postData:(NSDictionary *) postData target:(UIView *)target alertText:(NSString *)alertText  didFinishRequest:(base_request_block)didFinishRequest  didFailedRequest:(base_request_block)didFailedRequest//开启post同步网络请求，有弹出框，block形式，附带数据
{
    __block ASIFormDataRequest *request;
    [self showMBAlertView:target message:alertText whileExecutingBlock:^{
    request=[[ASIFormDataRequest requestWithURL:[NSURL URLWithString:urlStr]] retain];
        for(NSString *key in postValue.keyEnumerator)
        {
            [request setPostValue:[postValue objectForKey:key] forKey:key];
        }
        for(NSString *key in postData.keyEnumerator)
        {
            [request addData:[postData objectForKey:key] forKey:key];
        }
        request.timeOutSeconds=TIME_OUT_SECOND;
        [request startSynchronous];
    } completionBlock:^{
        if([self isRequestSucceed:request])
            didFinishRequest(request);
        else
            didFailedRequest(request);
        [request release];
    }];
}
@end
