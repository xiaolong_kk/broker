//
//  groomCustomVC.h
//  销冠微经济
//
//  Created by zen huang on 14/9/25.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"
#import "QCheckBox.h"
@interface groomCustomVC : BaseViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    UITableView *_tableView;
    UITableView *resultTableView;
    //UIButton *btn;
    UIView *topView;
    //NSMutableArray *dataSource;
    MJRefreshHeaderView *_header;
    UISearchBar *mySearchBar;
        
}

@property (nonatomic, retain) UISearchBar*mSearchBar;
@property (nonatomic, retain) NSMutableArray *resultDataArr;
@property (nonatomic, retain) NSMutableArray *sortedArrForArrays;
@property (nonatomic, retain) NSMutableArray *sectionHeadsKeys;
@property (nonatomic, retain) NSMutableArray *dataSource;


@end
