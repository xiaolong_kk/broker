//
//  Verification.h
//  topsales
//
//  Created by adam_yan on 13-11-02.
//  Copyright (c) 2013年 topsales. All rights reserved.
//

#import "Verification.h"
#import "MyMD5.h"
#import "UIDevice+IdentifierAddition.h"


//#import <Util/Util.h>
//#import <Util/ASIFormDataRequest.h>
//#import <Util/SBJsonParser.h>

#define appid                       @"500"
#define appkey                      @"34aee295a97053a348644263217e2e5b"
@implementation Verification


+ (NSString *)getCurrentTime{
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps;
    
    comps = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:date];
    
    NSString *currentYear;
    currentYear = [NSString stringWithFormat:@"%ld",(long)comps.year];
    
    NSString *currentmMonth;
    if (comps.month < 10) {
        currentmMonth = [NSString stringWithFormat:@"%@%ld",@"0",(long)comps.month];
    }else {
        currentmMonth = [NSString stringWithFormat:@"%ld",(long)comps.month];
    }
    
    
    NSString *currentmDay;
    if (comps.day < 10) {
        currentmDay = [NSString stringWithFormat:@"%@%ld",@"0",(long)comps.day];
    }else {
        currentmDay = [NSString stringWithFormat:@"%ld",(long)comps.day];
    }
    
    
    NSString *currentHour;
    if (comps.hour < 10) {
        currentHour = [NSString stringWithFormat:@"%@%ld",@"0",(long)comps.hour];
    }else{
        currentHour = [NSString stringWithFormat:@"%ld",(long)comps.hour];
    }
    
    NSString *currentMinute;
    if (comps.minute < 10) {
        currentMinute = [NSString stringWithFormat:@"%@%ld",@"0",(long)comps.minute];
    }else{
        currentMinute = [NSString stringWithFormat:@"%ld",(long)comps.minute];
    }
    
    NSString *currentSecond;
    if (comps.second < 10) {
        currentSecond = [NSString stringWithFormat:@"%@%ld",@"0",(long)comps.second];
    }else{
        currentSecond = [NSString stringWithFormat:@"%ld",(long)comps.second];
    }
    
    NSString *currentTime = [NSString stringWithFormat:@"%@%@%@%@%@%@",currentYear,currentmMonth,currentmDay,currentHour,currentMinute,currentSecond];
    
    //    currentTime = @"201209112153000";
    return currentTime;
}

+ (NSString *)getDeviceId
{
    
    return [[UIDevice currentDevice]uniqueDeviceIdentifier];
    //    return @"119";    
}
//b897105ea9fbb664ff1a2a872bb8b6fb iphone

+ (NSString *)toSort:(NSString *)post {
    
    NSMutableArray *stringsToSort = [NSMutableArray arrayWithCapacity:99];
    
//    NSLog(@"%d",[post length]);
    
    if (![[post substringWithRange:NSMakeRange([post length] - 1, 1)] isEqualToString:@"&"]) {
        post = [post stringByAppendingString:@"&"];
    }
    
//    NSLog(@"%@",post);
    
    NSRange range = [post rangeOfString:@"&"];
    
    while (range.location > 0 && range.length > 0) {
        NSString *item = [post substringToIndex:range.location];
        
        if ([item length] + 1 < [post length]) {
            post = [post substringFromIndex:range.location + 1];
        }else {
            post = @"";
        }
        
//        NSLog(@"%@",post);
        
        range = [post rangeOfString:@"&"];
        
        NSMutableDictionary *tempDic = [[NSMutableDictionary alloc] initWithCapacity:1] ;
        [tempDic setObject:item forKey:@"item"];
        [stringsToSort addObject:tempDic];
    }
    
    NSSortDescriptor *sortDescriptor;
    
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"item" ascending:YES];
    
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    [stringsToSort sortUsingDescriptors:sortDescriptors];
    
    
    NSString *items=[[NSString alloc] init];
    
    for (NSDictionary *everyItem in stringsToSort) {
        items = [NSString stringWithFormat:@"%@%@%@",items,[everyItem objectForKey:@"item"],@"&"];
    }
    
    
    if ([[items substringWithRange:NSMakeRange([items length] - 1, 1)] isEqualToString:@"&"]) {
        items = [items substringToIndex:[items length] - 1];
    }
    
//    NSLog(@"%@",items);
    
    return items;
}


+ (NSURL *)auth {
    
    NSString *post = [NSString stringWithFormat:@"%@%@%@%@%@%@",@"appid=",appid,@"&auth_time=",[self getCurrentTime],@"&device_id=",[self getDeviceId]];
    
    NSString *post_temp = [NSString stringWithFormat:@"%@%@%@%@%@",appkey,@"&",post,@"&",appkey];
    ASIFormDataRequest *formDataRequest = [ASIFormDataRequest requestWithURL:nil];
    NSString *encoded = [formDataRequest encodeURL:post_temp];
    
    NSString *sign = [[MyMD5 md5:encoded] uppercaseString];
    
    NSURL *url = [NSURL URLWithString:[WEBAPP_URL stringByAppendingFormat:@"%@%@%@%@",@"/auth/?",post,@"&sign=",sign]];
    
    return url;
    
}


+ (NSURL *)post:(NSString *)authCode apiCode:(NSString *)apiCode apiParams:(NSString *)apiParams requestTime:(NSString *)requestTime {

//    NSDictionary* infoDict =[[NSBundle mainBundle] infoDictionary];
//    NSString* versionNum =[infoDict objectForKey:@"CFBundleShortVersionString"];
//    NSUserDefaults *userDefault=[NSUserDefaults standardUserDefaults];
//
//    if(theApp.login_int!=2)
//    {
//    NSString *addStr = [NSString stringWithFormat:@"&AdminKid=%@&v=%@",[userDefault objectForKey:@"userKid"],versionNum];
//    apiParams = [apiParams stringByAppendingString:addStr];
//    }
//    
//    theApp.login_int = 0;
    NSString *post;
    if (apiParams != nil)
    {
        post = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",@"appid=",appid,@"&api=",apiCode,@"&",apiParams,@"&device_id=",[self getDeviceId],@"&request_time=",requestTime];
    }else{
        post = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@",@"appid=",appid,@"&api=",apiCode,@"&device_id=",[self getDeviceId],@"&request_time=",requestTime];
    }
    
    //    NSString *post = [NSString stringWithFormat:@"%@%@%@%@%@%@%@%@%@%@",@"appid=",appid,@"&api=",apiCode,@"&",apiParams,@"&device_id=",[self getDeviceId],@"&request_time=",requestTime];
   // NSLog(@"%@",post);
    post = [post lowercaseString];
    NSString *post_sort = [self toSort:post];
   // NSLog(@"%@",post_sort);
    NSString *post_temp = [NSString stringWithFormat:@"%@%@%@%@%@",appkey,@"&",post_sort,@"&",appkey];
    ASIFormDataRequest *formDataRequest = [ASIFormDataRequest requestWithURL:nil];
    NSString *encoded = [formDataRequest encodeURL:post_temp];
    
   // NSLog(@"%@",encoded);
    
    NSString *sign = [[MyMD5 md5:encoded] uppercaseString];
    
    NSURL *url = [NSURL URLWithString:[WEBAPP_URL stringByAppendingFormat:@"%@%@%@%@",@"?",[post stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding],@"&sign=",sign]];
    
    return url;
}


@end
