//
//  ALTabBarView.m
//  ALCommon
//
//  Created by Andrew Little on 10-08-17.
//  Copyright (c) 2010 Little Apps - www.myroles.ca. All rights reserved.
//

#import "TabBarView.h"


@implementation TabBarView

@synthesize delegate;
@synthesize selectedButton;
@synthesize btn1;
- (void)dealloc {
    
    [selectedButton release];
   // [btn1 release];
    delegate = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame]))
    {
        btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn1.tag =0;
        btn1.frame = CGRectMake(0,0,80,50);
        [btn1 addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [btn1 setBackgroundImage:[UIImage imageNamed:@"tabbar_1HL.png"] forState:UIControlStateNormal];
        [self addSubview:btn1];
        
        UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn2.tag = 1;
        btn2.frame = CGRectMake(btn1.frame.size.width,0,80,50);
        [btn2 addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [btn2 setBackgroundImage:[UIImage imageNamed:@"tabbar_2.png"] forState:UIControlStateNormal];
        [self addSubview:btn2];
      //  [btn2 release];
        
        UIButton *btn3 = [UIButton buttonWithType:UIButtonTypeCustom];
        btn3.tag = 2;
        btn3.frame = CGRectMake(btn2.frame.size.width+btn2.frame.origin.x,0,80,50);
        [btn3 addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [btn3 setBackgroundImage:[UIImage imageNamed:@"tabbar_3.png"] forState:UIControlStateNormal];
        [self addSubview:btn3];
        //[btn3 release];
        
        UIButton *btn4 = [UIButton buttonWithType:UIButtonTypeCustom];
       // [btn4 release];
        btn4.tag = 3;
        btn4.frame = CGRectMake(btn3.frame.size.width+btn3.frame.origin.x,0,80,50);
        [btn4 addTarget:self action:@selector(touchButton:) forControlEvents:UIControlEventTouchUpInside];
        
        [btn4 setBackgroundImage:[UIImage imageNamed:@"tabbar_4.png"] forState:UIControlStateNormal];
        [self addSubview:btn4];
    
    }
    return self;
}

//Let the delegate know that a tab has been touched
-(void) touchButton:(id)sender {

    
  NSArray *BtnImg = [NSArray arrayWithObjects:@"tabbar_1.png",@"tabbar_2.png",@"tabbar_3.png",@"tabbar_4.png",nil];
    
    
  NSArray *BtnImg_hl = [NSArray arrayWithObjects:@"tabbar_1HL.png",@"tabbar_2HL.png",@"tabbar_3HL.png",@"tabbar_4HL.png",nil];

  
    if( delegate != nil && [delegate respondsToSelector:@selector(tabWasSelected:)]) {
        
        if (selectedButton)
        {
          //  NSLog(@"%d",selectedButton.tag);
            [selectedButton setBackgroundImage:[UIImage imageNamed:[BtnImg objectAtIndex:selectedButton.tag]] forState:UIControlStateNormal];
            [selectedButton release];
        
        }
        selectedButton = [((UIButton *)sender) retain];
        [btn1 setBackgroundImage:[UIImage imageNamed:@"tabbar_1.png"] forState:UIControlStateNormal];

       // NSLog(@"%d",selectedButton.tag);
        [selectedButton setBackgroundImage:[UIImage imageNamed:[BtnImg_hl objectAtIndex:selectedButton.tag]] forState:UIControlStateNormal];
        [delegate tabWasSelected:selectedButton.tag];
    }
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {


}

@end
