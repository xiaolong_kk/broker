//
//  tagLableView.h
//  test_1
//
//  Created by liu on 14-10-10.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface tagLableView : NSObject

+(CGSize)lableSize:(UILabel *)label;
-(float)tagLableHight:(NSArray *)tagArray;
-(float)tagLabel:(NSArray *)tagArray withView:(UIView *)view;
@end
