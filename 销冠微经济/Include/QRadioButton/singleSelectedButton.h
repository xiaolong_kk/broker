//
//  singleSelectedButton.h
//  TopSales
//
//  Created by zen huang on 14/6/23.
//
//

#import <UIKit/UIKit.h>

@protocol singleSelectedButtonDelegate;

@interface singleSelectedButton : UIButton {
    NSString                        *_groupId;
    BOOL                            _checked;
    __unsafe_unretained id<singleSelectedButtonDelegate>       _delegate;
}

@property(nonatomic, assign)id<singleSelectedButtonDelegate>   delegate;
@property(nonatomic, copy, readonly)NSString            *groupId;
@property(nonatomic, assign)BOOL checked;

@property(nonatomic, retain)NSString *titleStr;
@property(nonatomic, retain)NSString *tellStr;
@property(nonatomic, retain)UIImage *_imageName;
@property(nonatomic, retain)UIImage *_selectedImageName;
@property(nonatomic) int Index_row;

-(void)getTitleStr :(NSString *)titleStr;
-(void)getTellStr :(NSString *)tellStr;
-(void)getIndex_row:(int)index_row;


- (id)initWithDelegate:(id)delegate groupId:(NSString*)groupId;


@end

@protocol singleSelectedButtonDelegate <NSObject>

@optional

- (void)didSelectedButton:(singleSelectedButton *)radio groupId:(NSString *)groupId;

@end
