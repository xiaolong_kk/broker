//
//  AppDelegate.m
//  销冠微经济
//
//  Created by zen huang on 14/9/11.
//  Copyright (c) 2014年 ___FULLUSERNAME___. All rights reserved.
//

#import "AppDelegate.h"
#import "bulidingListVC.h"
#import "MainVC.h"
#import "customListVC.h"
#import "settingVC.h"
#import "loginVC.h"

//#import "applyListVC.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    self.userDefault = [[NSUserDefaults alloc] init];

    theApp.brokerKid = [theApp.userDefault objectForKey:@"brokerKid"];
    if([[theApp.userDefault objectForKey:@"FirstUse"] length] <= 0)
    {
        [self loginView];
    }
    else
    {
        [self MainTabBarView];
        
    }
    
    [self.window makeKeyAndVisible];
    //fgskdjhfgasdjhfgahjds
    [self getData];
    return YES;
}

//-(void)createMainView
//{
//   // [self MainTabBarView];
//   // else
//    [self loginView];
//}

-(void)loginView
{
    loginVC *vc = [[loginVC alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    self.window.rootViewController = nav;
}


-(void)MainTabBarView
{
    self.tabBarController = [[MainTabBarViewController alloc] init];
    self.tabBarController.delegate = self;
    
    
    MainVC *vc1 = [[MainVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav1 = [[MLNavigationController alloc] initWithRootViewController:vc1];
    nav1.navigationBar.hidden= YES;
//
    bulidingListVC *vc2 = [[bulidingListVC alloc] initWithNavigationBar:YES];
    vc2.type_str = @"all";
    MLNavigationController *nav2 = [[MLNavigationController alloc] initWithRootViewController:vc2];
    [nav2.navigationBar setHidden:YES];
//
    customListVC *vc3 = [[customListVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav3 = [[MLNavigationController alloc] initWithRootViewController:vc3];
    nav3.navigationBar.hidden = YES;
    
    settingVC *vc4 = [[settingVC alloc] initWithNavigationBar:YES];
    MLNavigationController *nav4 = [[MLNavigationController alloc] initWithRootViewController:vc4];
    nav4.navigationBar.hidden = YES;
//
//    settingViewController *vc5 = [[settingViewController alloc] initWithNavigationBar:YES];
//    MLNavigationController *nav5 = [[MLNavigationController alloc] initWithRootViewController:vc5];
//    nav5.navigationBar.hidden = YES;
//    
    NSArray *arrVC = [[NSArray alloc] initWithObjects:vc1,vc2,vc3,vc4,nil];

    [self.tabBarController setViewControllers:arrVC animated:YES];
    
    MLNavigationController *nav = [[MLNavigationController alloc] initWithRootViewController:self.tabBarController];
    nav.navigationBar.hidden = YES;
    if (IOS_7) {
        nav.interactivePopGestureRecognizer.enabled = NO;
    }
    
    //applyListVC *vc = [[applyListVC alloc] initWithNavigationBar:YES];
    
    self.window.rootViewController = nav;
    //[self.window addSubview:self.tabBarController.view];
    nav.navigationBar.hidden = YES;
    
    
}

-(void)getData
{
        NSString *str = @"http://apiweixin.tops001.com/api.aspx?api=api.building.selectparamarray&brokerId=10";
        ASIHTTPRequest *request=[ASIHTTPRequest requestWithURL:[NSURL URLWithString:str]];
        request.timeOutSeconds=TIME_OUT_SECOND;
        [request setDelegate:self];
        [request setDidFinishSelector:@selector(requestSuccess:)];
        //[request setDidFailSelector:@selector(requestFail:)];
        [request startAsynchronous];
}

-(void)requestSuccess:(ASIFormDataRequest *)request
{
    NSDictionary *dict = [request.responseString objectFromJSONString];
    NSDictionary *dict2;
    if([[dict objectForKey:@"code"] intValue] ==0)
    {
        dict2 = [dict objectForKey:@"data"];
    }
    //NSLog(@"%@",dict2);
    NSArray *arr = [dict2 objectForKey:@"Sort"];
    NSArray *arr2 = [dict2 objectForKey:@"list_Wuye"];
    NSArray *arr3 = [dict2 objectForKey:@"quyuList"];

    NSUserDefaults *userDefaults=[NSUserDefaults standardUserDefaults];
    [userDefaults setObject:arr forKey:@"sortData"];
    [userDefaults setObject:arr2 forKey:@"wuyeData"];
    [userDefaults setObject:arr3 forKey:@"quyuListData"];

}





- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
