//
//  buildingDetailVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/21.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "buildingDetailVC.h"
#import "groomCustomVC.h"
#import "SMPageControl.h"
#import "AgreementVC.h"
#import "MapViewController.h"
@interface buildingDetailVC ()

@end

@implementation buildingDetailVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationLeftTitleBtn.hidden = YES;
    self.navigationLabel.text = @"楼盘详情";
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;

    UIView *headNiewBg = [[UIView alloc] initWithFrame:CGRectMake(0,addHight+44,320,370)];
    headNiewBg.backgroundColor = [AppSession colorWithHexString:@"e5e5e5"];
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,355)];
    headView.backgroundColor= [UIColor whiteColor];
    [headNiewBg addSubview:headView];
    
    UIImageView *logoImg = [[UIImageView alloc] initWithFrame:CGRectMake(0,0,320,150)];
    logoImg.tag = 101;
    logoImg.backgroundColor = [UIColor grayColor];
    [headView addSubview:logoImg];
    
    UILabel *priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(20,165,220,20)];
    priceLabel.tag = 102;
    priceLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    priceLabel.textAlignment = NSTextAlignmentLeft;
    priceLabel.backgroundColor = [UIColor clearColor];
    priceLabel.textColor = [AppSession colorWithHexString:@"f98156"];
    [headView addSubview:priceLabel];
  
    UIButton *xiaokongBtn=[UIButton buttonWithType:UIButtonTypeCustom];
    xiaokongBtn.frame=CGRectMake(230,150,90,45);
   // [xiaokongBtn addTarget:self action:@selector(popToNextView:) forControlEvents:UIControlEventTouchUpInside];
    [xiaokongBtn setImage:[UIImage imageNamed:@"icon_bd1.png"] forState:UIControlStateNormal];
    xiaokongBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-10,0,0);
    xiaokongBtn.backgroundColor = [AppSession colorWithHexString:@"f98156"];
    [xiaokongBtn setTitle:@"房源销控"forState:UIControlStateNormal];
    [xiaokongBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    xiaokongBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
    xiaokongBtn.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    [headView addSubview:xiaokongBtn];

    UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg.frame = CGRectMake(0,195,320,1);
    [headView addSubview:lineImg];
    
    UIImageView *tagImg=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_bd2.png"]];
    tagImg.frame=CGRectMake(20,205,15,18);
    [headView addSubview:tagImg];
    
    UILabel *tagLabel = [[UILabel alloc] initWithFrame:CGRectMake(45,205,275,20)];
    tagLabel.tag = 103;
    tagLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
    tagLabel.textAlignment = NSTextAlignmentLeft;
    tagLabel.backgroundColor = [UIColor clearColor];
    tagLabel.textColor = [AppSession colorWithHexString:@"f98156"];
    [headView addSubview:tagLabel];


    UIImageView *lineImg2 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg2.frame = CGRectMake(0,235,320,1);
    [headView addSubview:lineImg2];
    
    UIView *threebtnView = [[UIView alloc] initWithFrame:CGRectMake(0,235,320,120)];
    [headView addSubview:threebtnView];
    
    for(int i =0;i<3;i++)
    {
        UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(20,i*40,295,40)];
        logoBtn.tag = i;
        [logoBtn addTarget:self action:@selector(agreement:) forControlEvents:UIControlEventTouchUpInside];
        [threebtnView addSubview:logoBtn];
        
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,10,240,20)];
        titleLabel.tag = 222+i;
        titleLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
        titleLabel.textAlignment = NSTextAlignmentLeft;
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [AppSession colorWithHexString:@"666666"];
        [logoBtn addSubview:titleLabel];

        if(i==2)
        {
            UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(65,10,120,20)];
            tempLabel.tag = 1024;
            tempLabel.text = @"已签";
            tempLabel.hidden = YES;
            tempLabel.font=[UIFont fontWithName:stringFontOfSize size:14];
            tempLabel.textAlignment = NSTextAlignmentLeft;
            tempLabel.backgroundColor = [UIColor clearColor];
            tempLabel.textColor = [AppSession colorWithHexString:@"f98156"];
            [logoBtn addSubview:tempLabel];

            titleLabel.text = @"分销协议";

        }
        UIImageView *tagImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_bd%d.png",i+3]]];
        tagImg.frame = CGRectMake(235,10,14,17);
        [logoBtn addSubview:tagImg];

        UIImageView *arrowImg =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go4.png"]];
        arrowImg.frame = CGRectMake(265,14,8,13);
        [logoBtn addSubview:arrowImg];

        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(0,40*(i+1),320,1);
        [threebtnView addSubview:lineImg];
        
    }
    
    UIView *footView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,200)];
    footView.backgroundColor= [UIColor whiteColor];
    UIButton *tagBtn2=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn2.frame=CGRectMake(10,0,120,40);
    [tagBtn2 setImage:[UIImage imageNamed:@"icon_bd8.png"] forState:UIControlStateNormal];
    tagBtn2.imageEdgeInsets = UIEdgeInsetsMake(0,-20,0,0);
    [tagBtn2 setTitle:@"户型图"forState:UIControlStateNormal];
    [tagBtn2 setTitleColor:[AppSession colorWithHexString:@"f98156"] forState:UIControlStateNormal];
    tagBtn2.titleLabel.font = [UIFont systemFontOfSize:14];
    tagBtn2.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    [footView addSubview:tagBtn2];
    
    UIImageView *lineImg3 = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
    lineImg3.frame = CGRectMake(15,40,290,1);
    [footView addSubview:lineImg3];
    
    UIImageView *logoImg2 = [[UIImageView alloc] initWithFrame:CGRectMake(0,55,320,150)];
    logoImg2.backgroundColor = [UIColor grayColor];
    [footView addSubview:logoImg2];
    
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,50, 320, 150)];
    [scrollView setContentSize:CGSizeMake(320*3,150)];
    [scrollView setPagingEnabled:YES];
    scrollView.bounces = NO;
    //scrollView.backgroundColor  =[UIColor brownColor];
    scrollView.tag = 10001;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    for(int i=0;i<3;i++)
    {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(320*i, 0, 320,150)];
        imageView.tag = 38+i;
        [scrollView addSubview:imageView];
    }
    scrollView.delegate = self;
    
    [footView addSubview:scrollView];
    
    SMPageControl *tempPageControl =[[SMPageControl alloc]initWithFrame:CGRectMake(100,self.view.frame.size.height-120,120,36)];
    tempPageControl.tag=10011;
    tempPageControl.backgroundColor = [UIColor clearColor];
    tempPageControl.numberOfPages =3;
    [tempPageControl setCurrentPage:0];
    [tempPageControl setPageIndicatorImage:[UIImage imageNamed:@"dian.png"]];
    [tempPageControl setCurrentPageIndicatorImage:[UIImage imageNamed:@"dian_hl.png"]];
    [footView addSubview:tempPageControl];
    
    
    myTable = [[UITableView alloc] initWithFrame:CGRectMake(0,addHight+44,320,self.view.bounds.size.height-130)];
    myTable.tableHeaderView = headNiewBg;
    myTable.tableFooterView = footView;
    myTable.delegate = self;
    myTable.dataSource = self;
    myTable.separatorStyle=UITableViewCellSeparatorStyleNone;
    myTable.backgroundColor  = [AppSession colorWithHexString:@"e5e5e5"];
    [myTable reloadData];
    [self.view addSubview:myTable];
    
    UIButton *add_customBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    add_customBtn.frame = CGRectMake(10,self.view.bounds.size.height-50,300,40);
    [add_customBtn setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateSelected];
    [add_customBtn setTitle:@"推荐客户" forState:UIControlStateNormal];
    add_customBtn.titleLabel.font=[UIFont fontWithName: stringFontOfSize size:15];
    [add_customBtn addTarget:self action:@selector(popToGroomCustomView) forControlEvents:UIControlEventTouchUpInside];
    [add_customBtn setBackgroundImage:[UIImage imageNamed:@"add_customBtn.png"] forState:UIControlStateNormal];
    [self.view addSubview:add_customBtn];

    [self getData];

}


-(void)agreement:(UIButton *)btn
{
    if(btn.tag==0)
    {
        
    }
    if(btn.tag==1)
    {
     
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                    self.address,@"address",
                                    @"google",@"from_map_type",
                                    google_lat,@"google_lat",
                                    google_lng,@"google_lng", nil];
        
        MapViewController *mv = [[MapViewController alloc] initWithNavigationBar:YES];
        mv.navDic = dic;
        mv.mapType = RegionNavi;
        [self.navigationController pushViewController:mv animated:YES];
    }
    if(btn.tag==2)
    {
        NSString *boolStr = [NSString stringWithFormat:@"%@",self.Agreement?@"YES":@"NO"];
        if([boolStr isEqualToString:@"YES"])
        {
            [self popToGroomCustomView];
            return;
        }
        
        AgreementVC *vc = [[AgreementVC alloc] initWithNavigationBar:YES];
        vc.AgreementKid_str = self.AgreementKid_str;
        vc.Agreement = self.Agreement;
        vc.buildingKid_str = self.buildingKid_str;
        [self.navigationController pushViewController:vc animated:YES];
    }
}
-(void)popToGroomCustomView
{
    groomCustomVC *vc = [[groomCustomVC alloc] initWithNavigationBar:YES];
    [self.navigationController pushViewController:vc animated:YES];
}


-(void)getData
{
    
    UIImageView *logoImg = (UIImageView *)[self.view viewWithTag:101];
    UILabel *priceLabel = (UILabel *)[self.view viewWithTag:102];
    UILabel *tagLabel=(UILabel *)[self.view viewWithTag:103];
    UILabel *titleLabel1 = (UILabel *)[self.view viewWithTag:222];
    UILabel *titleLabel2 = (UILabel *)[self.view viewWithTag:223];
    UILabel *tempLabel = (UILabel *)[self.view viewWithTag:1024];
    UIImageView *imageView1 = (UIImageView *)[self.view viewWithTag:38];
    UIImageView *imageView2 = (UIImageView *)[self.view viewWithTag:39];
    UIImageView *imageView3 = (UIImageView *)[self.view viewWithTag:40];

    NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.building.biuldingdetail&BuildingKid=%@&BrokerKid=%@",self.buildingKid_str,theApp.brokerKid];
    
   // NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
    
         NSDictionary *dict= [request.responseString objectFromJSONString];
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
            // NSLog(@"%@",dict);
             self.AgreementKid_str = [[dict objectForKey:@"data"] objectForKey:@"AgreementKid"];
             self.Agreement = [[[dict objectForKey:@"data"] objectForKey:@"Agreement"] boolValue];
            
             NSDictionary *dict2 = [[dict objectForKey:@"data"]objectForKey:@"building"];
             
         titleLabel1.text = [NSString stringWithFormat:@"电话 %@",[dict2 objectForKey:@"F_Phone"]];
         titleLabel2.text = [NSString stringWithFormat:@"地址 %@",[dict2 objectForKey:@"F_Address"]];
         priceLabel.text = [NSString stringWithFormat:@"%@",[dict2 objectForKey:@"F_WeiXinSummary"]];
         tagLabel.text = [dict2 objectForKey:@"F_Activity"];
         [logoImg setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[dict2 objectForKey:@"F_Img1"]] placeholderImage:nil];
             
             google_lat = [dict2 objectForKey:@"F_Latitude"];
            
             google_lng = [dict2 objectForKey:@"F_Longitude"];
             
             self.address = titleLabel2.text;
             
              NSString *boolStr = [NSString stringWithFormat:@"%@",self.Agreement?@"YES":@"NO"];
               if([boolStr isEqualToString:@"YES"])
                tempLabel.hidden = NO;
             else
                 tempLabel.hidden = YES;

             NSArray *arr = [[dict objectForKey:@"data"] objectForKey:@"imageList"];
             
             if(arr.count>2)
             {
                [imageView1 setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[arr[0] objectForKey:@"F_FileUrl"]]placeholderImage:nil];
             [imageView2 setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[arr[1] objectForKey:@"F_FileUrl"]]placeholderImage:nil];
             [imageView3 setWebImageWithFade:[NSString stringWithFormat:@"http://www.tops001.com%@",[arr[2] objectForKey:@"F_FileUrl"]]placeholderImage:nil];

             }
         }
         //priceLabel.text = [dict2 objectForKey:];
         // NSLog(@"%@",dataSource);
     }
        didFailedRequest:^(ASIHTTPRequest *request){
                     
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
		return 400;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"aCell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if (!cell)
	{
		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
		cell.selectionStyle = UITableViewCellSelectionStyleNone;
		
        UIView *cellView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,120)];
        cellView.backgroundColor = [UIColor whiteColor];
        [cell addSubview:cellView];
        
        UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
        lineImg.frame = CGRectMake(15,40,290,1);
        [cellView addSubview:lineImg];

        UIButton *tagBtn=[UIButton buttonWithType:UIButtonTypeCustom];
        tagBtn.frame=CGRectMake(10,0,120,40);
        [tagBtn setImage:[UIImage imageNamed:@"icon_bd6.png"] forState:UIControlStateNormal];
        tagBtn.imageEdgeInsets = UIEdgeInsetsMake(0,-20,0,0);
        [tagBtn setTitle:@"楼盘卖点"forState:UIControlStateNormal];
        [tagBtn setTitleColor:[AppSession colorWithHexString:@"f98156"] forState:UIControlStateNormal];
        tagBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
        tagBtn.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
        [cellView addSubview:tagBtn];

        webView = [[UIWebView alloc] initWithFrame:CGRectMake(0,40,320,400)];
        webView.delegate = self;
        webView.scalesPageToFit = YES;
        NSString *str =[NSString stringWithFormat:@"http://apiweixin.tops001.com/BuildingInfoForclient.aspx?BuildingKid=%@",self.buildingKid_str];
        NSURL *url = [NSURL URLWithString:str];
        NSURLRequest *request =[NSURLRequest requestWithURL:url];
        [webView loadRequest:request];
        [cell addSubview:webView];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
		
		return cell;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
