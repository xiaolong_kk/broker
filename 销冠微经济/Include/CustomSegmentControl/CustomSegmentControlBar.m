//
//  CustomSegmentControlBar.m
//  BaseLibrary
//
//  Created by Chai Xue Liang on 12-12-24.
//  Copyright (c) 2012年 xu shun wang. All rights reserved.
//

#import "CustomSegmentControlBar.h"

@implementation CustomSegmentControlBar
@synthesize selectedIndex;
@synthesize totalCount;
@synthesize delegate;
@synthesize segmentButtons;
@synthesize imageArray;
@synthesize highlightImageArray;
@synthesize highlightTitleArray;
@synthesize titleArray;
#pragma mark - 生命周期
-(void)dealloc
{
    self.segmentButtons=nil;
    self.delegate=nil;
    self.imageArray=nil;
    self.highlightImageArray=nil;
    self.titleArray=nil;
    [super dealloc];
}
- (id) initWithDelegate:(id)_delegate totalCount:(int)_totalCount frame:(CGRect)frame tag:(int)tag
{
    self=[super initWithFrame:frame];
    if(self)
    {
        self.delegate=_delegate;
        self.totalCount=_totalCount;
        self.segmentButtons=[NSMutableArray arrayWithCapacity:_totalCount];
        self.tag=tag;
        
        float btnWidth=self.frame.size.width/self.totalCount;
        float btnHeight=self.frame.size.height;
        
        for(int i=0;i<self.totalCount;i++)
        {
            UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame=CGRectMake(i*btnWidth, 0, btnWidth, btnHeight);
            [btn setAdjustsImageWhenHighlighted:NO];//去除按下按钮的阴影
            if(i==0)
                btn.selected=YES;
            
            [btn addTarget:self action:@selector(onBtnClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.segmentButtons addObject:btn];
            [self addSubview:btn];
            if([self.delegate respondsToSelector:@selector(initSegmentButton:btn:index:)])
                [self.delegate initSegmentButton:self btn:btn index:i];
        }
        
    }
    return self;
}
- (void)setImageArray:(NSArray *)_imageArray
{
    [imageArray release];
    imageArray=[_imageArray retain];
    for(int i=0;i<_imageArray.count;i++)
    {
        UIButton *btn=(UIButton *)[self.segmentButtons objectAtIndex:i];
        UIImage *image=[UIImage imageNamed:[_imageArray objectAtIndex:i]];
        [btn setBackgroundImage:image forState:UIControlStateNormal];
    }
}
- (void)setHighlightImageArray:(NSArray *)_highlightImageArray
{
    [highlightImageArray release];
    highlightImageArray=[_highlightImageArray retain];
    for(int i=0;i<_highlightImageArray.count;i++)
    {
        UIButton *btn=(UIButton *)[self.segmentButtons objectAtIndex:i];
        UIImage *image=[UIImage imageNamed:[_highlightImageArray objectAtIndex:i]];
        [btn setBackgroundImage:image forState:UIControlStateSelected];
    }
}
- (void)setTitleArray:(NSArray *)_titleArray
{
    [titleArray release];
    titleArray=[_titleArray retain];
    for(int i=0;i<_titleArray.count;i++)
    {
        UIButton *btn=(UIButton *)[self.segmentButtons objectAtIndex:i];
        [btn setTitle:[_titleArray objectAtIndex:i] forState:UIControlStateNormal];
    }
}

- (void)setHighlightTitleArray:(NSArray *)_HighlightTitleArray
{
    [highlightTitleArray release];
    highlightTitleArray=[_HighlightTitleArray retain];
    for(int i=0;i<_HighlightTitleArray.count;i++)
    {
        UIButton *btn=(UIButton *)[self.segmentButtons objectAtIndex:i];
        [btn setTitle:[_HighlightTitleArray objectAtIndex:i] forState:UIControlStateNormal];
    }
}
//切换选中的index，并作一些替换图片之类的事
- (void)setSelectedIndex:(int)_selectedIndex
{
    //如果选择的已经选中了，就不响应
//    if(selectedIndex==_selectedIndex)
//        return;
    selectedIndex=_selectedIndex;
    for(int i=0;i<self.totalCount;i++)
    {
        
        UIButton *btn=[self.segmentButtons objectAtIndex:i];
        if(i==_selectedIndex)
        {
            btn.selected=YES;
            if([self.delegate respondsToSelector:@selector(selectButton:btn:index:)])
               [self.delegate selectButton:self btn:btn index:i];
        }
        else
        {
            btn.selected=NO;
        }
    }
    
}
#pragma mark - 回调
- (void) onBtnClick:(UIButton *)sender
{
    for(UIButton *btn in self.segmentButtons)
    {
        if([btn isEqual:sender])
        {
            self.selectedIndex=[self.segmentButtons indexOfObject:btn];
            break;
        }
    }
}
@end
