//
//  contact Cell.h
//  test_1
//
//  Created by liu on 14-10-12.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface contact_Cell : UITableViewCell

@property(nonatomic,strong)UIImageView *selectImage;
@property(nonatomic,strong)UILabel *nameLabel;
@property(nonatomic,strong)UILabel *phoneLabel;

@end
