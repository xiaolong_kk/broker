//
//  myCustomVC.m
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-11.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import "myCustomVC.h"
#import "pinyin.h"
#import "ChineseString.h"
#import "simpleCustomPushBuildingVC.h"
#import "customDetailVC.h"
#import "addCustomVC.h"
@interface myCustomVC ()

@end

@implementation myCustomVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createTableView];
}


- (void)createTableView
{
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHight1=44;
    self.navigationLabel.text = @"我的客户";
    topView = [[UIView alloc] initWithFrame:CGRectMake(0,addHight+44, 320,45)];
    [self.view addSubview:topView];
    
    //初始化搜索条
    mySearchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0,0,320,44)];
    [mySearchBar setPlaceholder:@"请输入客户姓名或手机号"];
    mySearchBar.barTintColor=[AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar setSearchFieldBackgroundImage:[UIImage imageNamed:@"s_search_input.png"] forState:UIControlStateNormal];
    //mySearchBar.userInteractionEnabled = NO;
    mySearchBar.tintColor=[AppSession colorWithHexString:@"84c445"];
    mySearchBar.delegate = self;
    [topView addSubview:mySearchBar];
    
    UIView *tempView = [[UIView alloc]initWithFrame:CGRectMake(0,0,320,5)];
    tempView.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView];
    
    UIView *tempView1 = [[UIView alloc]initWithFrame:CGRectMake(0,40, 320,5)];
    tempView1.backgroundColor = [AppSession colorWithHexString:@"f6f6f6"];
    [mySearchBar addSubview:tempView1];
    
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0,0,320,60)];
    
    UIButton *btn=[UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame=CGRectMake(0,0,320,60);
    btn.tag = 114;
    [btn addTarget:self action:@selector(popToNextView:) forControlEvents:UIControlEventTouchUpInside];
    [btn setImage:[UIImage imageNamed:@"icon_add.png"] forState:UIControlStateNormal];
    btn.imageEdgeInsets = UIEdgeInsetsMake(0,-200,0,0);
    [btn setTitle:@"新增客户"forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:15];
    // btn1.titleLabel.textAlignment = NSTextAlignmentCenter;//设置title的字体居中
    btn.titleEdgeInsets = UIEdgeInsetsMake(0,-150,0,0);
    [headView addSubview:btn];
    
    UIImageView *arrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go2.png"]];
    arrowImg.frame = CGRectMake(285,25,10,19);
    [btn addSubview:arrowImg];
    
    
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height+5,320,self.view.bounds.size.height-100) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.tableHeaderView = headView;
    _tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    resultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0,topView.frame.origin.y+topView.frame.size.height+5,320,self.view.bounds.size.height-100) style:UITableViewStylePlain];
    resultTableView.dataSource = self;
    resultTableView.delegate = self;
    resultTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:resultTableView];
    resultTableView.hidden = YES;
    
    [self addHeader];
    
    
}

-(void)popToNextView
{
//    groom_resultVC *vc = [[groom_resultVC alloc ]initWithNavigationBar:YES];
//    [self.navigationController pushViewController:vc animated:YES];
}

- (void)addHeader
{
    
    MJRefreshHeaderView *header = [MJRefreshHeaderView header];
    header.scrollView = _tableView;
    header.beginRefreshingBlock = ^(MJRefreshBaseView *refreshView) {
        // 进入刷新状态就会回调这个Block
        [self performSelector:@selector(getData) withObject:refreshView afterDelay:0.0];
        
        [self performSelector:@selector(doneWithView:) withObject:refreshView afterDelay:0.5];
        
        //NSLog(@"%@----开始进入刷新状态", refreshView.class);
    };
    header.endStateChangeBlock = ^(MJRefreshBaseView *refreshView) {
        // 刷新完毕就会回调这个Block
        //NSLog(@"%@----刷新完毕", refreshView.class);
    };
    header.refreshStateChangeBlock = ^(MJRefreshBaseView *refreshView, MJRefreshState state) {
        // 控件的刷新状态切换了就会调用这个block
        switch (state) {
            case MJRefreshStateNormal:
                //NSLog(@"%@----切换到：普通状态", refreshView.class);
                break;
                
            case MJRefreshStatePulling:
                // NSLog(@"%@----切换到：松开即可刷新的状态", refreshView.class);
                break;
                
            case MJRefreshStateRefreshing:
                // NSLog(@"%@----切换到：正在刷新状态", refreshView.class);
                break;
            default:
                break;
        }
    };
    [header beginRefreshing];
    _header = header;
}
- (void)doneWithView:(MJRefreshBaseView *)refreshView
{
    // 刷新表格
    [_tableView reloadData];
    // (最好在刷新表格后调用)调用endRefreshing可以结束刷新状态
    [refreshView endRefreshing];
}


-(void)getData
{
    self.sortedArrForArrays = [[NSMutableArray alloc] init];
    self.sectionHeadsKeys = [[NSMutableArray alloc] init];
    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.mycustomer&BrokerKid=%@",theApp.brokerKid];
    [self startSynchronousRequest:str  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             NSDictionary *dict2= [dict objectForKey:@"data"];
             
             NSArray *arr = [dict2 objectForKey:@"list_itme"];

             self.dataSource = [NSMutableArray arrayWithArray:arr];
             
         }
         
         if(self.dataSource.count>0)
             self.sortedArrForArrays = [self getChineseStringArr:self.dataSource];
         
         [_tableView reloadData];
         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];
}

-(void)popToNextView:(UIButton *)sender
{
    if(sender.tag==114)
    {
        addCustomVC *vc = [[addCustomVC alloc] initWithNavigationBar:YES];
        [self.navigationController pushViewController:vc animated:YES];
    
    }
    if(sender.tag==113)
    {
        //        groupListVC *vc = [[groupListVC alloc] init];
        //        vc.back_name = @"通讯录";
        //        [self.navigationController pushViewController:vc animated:YES];
    }
    if(sender.tag==112)
    {
        //        newFriendsVC *vc = [[newFriendsVC alloc] init];
        //        vc.back_name = @"通讯录";
        //        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return  [[self.sortedArrForArrays objectAtIndex:section] count];
    }
    if(tableView==resultTableView)
    {
        return [self.resultDataArr count];
    }
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(tableView==_tableView)
    {
        return [self.sortedArrForArrays count];
    }
    if(tableView==resultTableView)
    {
        return [self.resultDataArr count];
        
    }
    
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if(tableView==_tableView)
    {
        return [_sectionHeadsKeys objectAtIndex:section];
    }
    if(tableView==resultTableView)
    {
        return 0;
    }
    
    return 0;
}

- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if(tableView==_tableView)
    {
        return self.sectionHeadsKeys;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 55;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *tempStr = nil;
    if(tableView==_tableView)
    {
        if ([self.sortedArrForArrays count] > indexPath.section)
        {
            
            NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
            if ([arr count] > indexPath.row)
            {
                
                ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
                tempStr = str.string;
                
                for(NSDictionary *dict in _dataSource)
                {
                    if([[dict objectForKey:@"F_Title"] isEqualToString:str.string])
                    {
                        tempStr  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]];
                    }
                }
            }
        }
    }
    if(tableView==resultTableView)
    {
        tempStr  =[NSString stringWithFormat:@"%@",[[self.resultDataArr objectAtIndex:indexPath.row] objectForKey:@"Kid"]];
        
    }
    
    customDetailVC *vc = [[customDetailVC alloc] initWithNavigationBar:YES];
    vc.kid_str = tempStr;
    [self.navigationController pushViewController:vc animated:YES];

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView==_tableView)
    {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImageView *logImg = [[UIImageView alloc] init];
            logImg.tag = 100;
            logImg.frame = CGRectMake(15,7.5,40,40);
            [cell addSubview:logImg];
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,54,320,1);
            [cell addSubview:lineImg];
            
            UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,15,50,20)];
            titleLabel.tag=101;
            titleLabel.textAlignment=NSTextAlignmentLeft;
            titleLabel.textColor=[UIColor blackColor];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(120,15,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:sexLabel];
            
            UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(140,15,120,20)];
            phoneLabel.tag=102;
            phoneLabel.textAlignment=NSTextAlignmentCenter;
            phoneLabel.textColor=[AppSession colorWithHexString:@"75a7be"];
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            phoneLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:phoneLabel];
            
            UIButton *destroyBtn = [[UIButton alloc] init];
            [destroyBtn setImage:(UIImage *)[UIImage imageNamed:@"btn_1.png"] forState:(UIControlState)UIControlStateNormal];
            destroyBtn.tag = 5555;
            destroyBtn.frame = CGRectMake(255,15,43,25);
            [destroyBtn addTarget:self action:@selector(groomCustom:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:destroyBtn];
            
            
        }
        
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UIImageView *logoImg = (UIImageView *)[cell viewWithTag:100];
        UIButton *destroyBtn = (UIButton *)[cell viewWithTag:5555];
        
        if ([self.sortedArrForArrays count] > indexPath.section)
        {
            
            NSArray *arr = [self.sortedArrForArrays objectAtIndex:indexPath.section];
            if ([arr count] > indexPath.row)
            {
                
                ChineseString *str = (ChineseString *) [arr objectAtIndex:indexPath.row];
                titleLabel.text = str.string;
                for(NSDictionary *dict in _dataSource)
                {
                    if([[dict objectForKey:@"F_Title"] isEqualToString:str.string])
                    {
                        titleLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Title"]];
                        
                        phoneLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Phone"]];
                        sexLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Sex"]];
                        
                        //NSLog(@"%@",[dict objectForKey:@"F_PicUrl"]);
                        if([self isBlankString:[dict objectForKey:@"F_PicUrl"]]==YES)
                        {
                            if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
                                logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
                            else
                                logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
                        }
                        else
                        {
                            [logoImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];
                        }
                        
                        [destroyBtn setTitle:[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]] forState:UIControlStateNormal];
                        
                        
                    }
                }
            }
        }
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
        return cell;
    }
    
    if(tableView==resultTableView)
    {
        NSString *CellIdentifier = [NSString stringWithFormat:@"Cell%ld",(long)indexPath.row];//以indexPath来唯一确定cell
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            
            UIImageView *logImg = [[UIImageView alloc] init];
            logImg.tag = 100;
            logImg.frame = CGRectMake(15,7.5,40,40);
            [cell addSubview:logImg];
            
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,54,320,1);
            [cell addSubview:lineImg];
            
            UILabel *titleLabel=[[UILabel alloc] initWithFrame:CGRectMake(70,15,50,20)];
            titleLabel.tag=101;
            titleLabel.textAlignment=NSTextAlignmentLeft;
            titleLabel.textColor=[UIColor blackColor];
            titleLabel.font=[UIFont fontWithName:stringFontOfSize size:15];
            titleLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:titleLabel];
            
            UILabel *sexLabel=[[UILabel alloc] initWithFrame:CGRectMake(120,15,30,20)];
            sexLabel.tag=103;
            sexLabel.textAlignment=NSTextAlignmentCenter;
            sexLabel.textColor=[AppSession colorWithHexString:@"999999"];
            sexLabel.font=[UIFont fontWithName:stringFontOfSize size:11];
            sexLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:sexLabel];
            
            UILabel *phoneLabel=[[UILabel alloc] initWithFrame:CGRectMake(140,15,120,20)];
            phoneLabel.tag=102;
            phoneLabel.textAlignment=NSTextAlignmentCenter;
            phoneLabel.textColor=[AppSession colorWithHexString:@"75a7be"];
            phoneLabel.font=[UIFont fontWithName:stringFontOfSize size:13];
            phoneLabel.backgroundColor=[UIColor clearColor];
            [cell addSubview:phoneLabel];
            
            UIButton *destroyBtn = [[UIButton alloc] init];
            [destroyBtn setImage:(UIImage *)[UIImage imageNamed:@"btn_1.png"] forState:(UIControlState)UIControlStateNormal];
            destroyBtn.tag = 5555;
            destroyBtn.frame = CGRectMake(255,15,43,25);
            [destroyBtn addTarget:self action:@selector(groomCustom:) forControlEvents:UIControlEventTouchUpInside];
            [cell addSubview:destroyBtn];
            
            
        }
        
        
        UILabel *titleLabel = (UILabel *)[cell viewWithTag:101];
        UILabel *phoneLabel = (UILabel *)[cell viewWithTag:102];
        UILabel *sexLabel = (UILabel *)[cell viewWithTag:103];
        UIImageView *logoImg = (UIImageView *)[cell viewWithTag:100];
        UIButton *destroyBtn = (UIButton *)[cell viewWithTag:5555];
        
        NSDictionary *dict = [self.resultDataArr objectAtIndex:indexPath.row];
        titleLabel.text = [dict objectForKey:@"F_Title"];
        
        titleLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Title"]];
        
        phoneLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Phone"]];
        sexLabel.text  =[NSString stringWithFormat:@"%@",[dict objectForKey:@"F_Sex"]];
        
        if([self isBlankString:[dict objectForKey:@"F_PicUrl"]]==YES)
        {
            if([[dict objectForKey:@"F_Sex"] isEqualToString:@"女士"])
                logoImg.image = [UIImage imageNamed:@"user_0.jpg"];
            else
                logoImg.image = [UIImage imageNamed:@"user_1.jpg"];
        }
        else
        {
            [logoImg setWebImageWithFade:[dict objectForKey:@"F_PicUrl"] placeholderImage:nil];
        }
        
        [destroyBtn setTitle:[NSString stringWithFormat:@"%@",[dict objectForKey:@"Kid"]] forState:UIControlStateNormal];
        
        cell.selectionStyle=UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    
    return nil;
}

-(void)groomCustom:(UIButton *)btn
{
    // NSLog(@"%@",btn.titleLabel.text);
    simpleCustomPushBuildingVC *vc = [[simpleCustomPushBuildingVC alloc] initWithNavigationBar:YES];
    vc.CustomerID_str = btn.titleLabel.text;
    [self.navigationController pushViewController:vc animated:YES];
}

- (NSMutableArray *)getChineseStringArr:(NSMutableArray *)arrToSort {
    
    NSMutableArray *chineseStringsArray = [NSMutableArray array];
    
    for(int i = 0; i < [arrToSort count]; i++) {
        ChineseString *chineseString=[[ChineseString alloc]init];
        chineseString.string=[NSString stringWithString:[[arrToSort objectAtIndex:i]  objectForKey:@"F_Title"]];
        if(chineseString.string==nil){
            chineseString.string=@"";
        }
        
        if(![chineseString.string isEqualToString:@""]){
            //join the pinYin
            NSString *pinYinResult = [NSString string];
            for(int j = 0;j < chineseString.string.length; j++) {
                NSString *singlePinyinLetter = [[NSString stringWithFormat:@"%c",
                                                 pinyinFirstLetter([chineseString.string characterAtIndex:j])]uppercaseString];
                
                pinYinResult = [pinYinResult stringByAppendingString:singlePinyinLetter];
            }
            chineseString.pinYin = pinYinResult;
        } else {
            chineseString.pinYin = @"";
        }
        [chineseStringsArray addObject:chineseString];
        
    }
    
    //sort the ChineseStringArr by pinYin
    NSArray *sortDescriptors = [NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"pinYin" ascending:YES]];
    [chineseStringsArray sortUsingDescriptors:sortDescriptors];
    
    
    NSMutableArray *arrayForArrays = [NSMutableArray array];
    BOOL checkValueAtIndex= NO;  //flag to check
    NSMutableArray *TempArrForGrouping = nil;
    
    for(int index = 0; index < [chineseStringsArray count]; index++)
    {
        ChineseString *chineseStr = (ChineseString *)[chineseStringsArray objectAtIndex:index];
        NSMutableString *strchar= [NSMutableString stringWithString:chineseStr.pinYin];
        NSString *sr= [strchar substringToIndex:1];
        //sr containing here the first character of each string
        if(![_sectionHeadsKeys containsObject:[sr uppercaseString]])//here I'm checking whether the character already in the selection header keys or not
        {
            [_sectionHeadsKeys addObject:[sr uppercaseString]];
            TempArrForGrouping = [[NSMutableArray alloc] initWithObjects:nil];
            checkValueAtIndex = NO;
        }
        if([_sectionHeadsKeys containsObject:[sr uppercaseString]])
        {
            [TempArrForGrouping addObject:[chineseStringsArray objectAtIndex:index]];
            
            if(checkValueAtIndex == NO)
            {
                [arrayForArrays addObject:TempArrForGrouping];
                checkValueAtIndex = YES;
                
            }
        }
    }
    return arrayForArrays;
}

#pragma mark searchBarDelegate

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar;                      // return NO to not become first responder
{
    searchBar.showsCancelButton = YES;
    resultTableView.hidden = NO;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar;                    // called when cancel button pressed
{
    [AppSession resignKeyBoardInView:self.view];
    searchBar.showsCancelButton = NO;
    resultTableView.hidden = YES;
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    [AppSession resignKeyBoardInView:self.view];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    
    self.resultDataArr = [[NSMutableArray alloc]init];
    //    if (mySearchBar.text.length>0&&![ChineseInclude isIncludeChineseInString:mySearchBar.text]) {
    //        for (int i=0; i<dataArray.count; i++) {
    //            if ([ChineseInclude isIncludeChineseInString:dataArray[i]]) {
    //                NSString *tempPinYinStr = [PinYinForObjc chineseConvertToPinYin:dataArray[i]];
    //                NSRange titleResult=[tempPinYinStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //                if (titleResult.length>0) {
    //                    for(NSDictionary *dict in dataSource)
    //                    {
    //                        if([[dict objectForKey:@"content"] isEqualToString:dataArray[i]])
    //                        {
    //                          [searchResults addObject:dataSource[i]];
    //
    //                        }
    //                    }
    //                }
    //                NSString *tempPinYinHeadStr = [PinYinForObjc chineseConvertToPinYinHead:dataArray[i]];
    //                NSRange titleHeadResult=[tempPinYinHeadStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //                if (titleHeadResult.length>0) {
    //                    for(NSDictionary *dict in dataSource)
    //                    {
    //                        if([[dict objectForKey:@"content"] isEqualToString:dataArray[i]])
    //                        {
    //                            [searchResults addObject:dataSource[i]];
    //
    //                        }
    //                    }
    //                }
    //            }
    //            else {
    //                NSRange titleResult=[dataArray[i] rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //                if (titleResult.length>0) {
    //                    for(NSDictionary *dict in dataSource)
    //                    {
    //                        if([[dict objectForKey:@"content"] isEqualToString:dataArray[i]])
    //                        {
    //                            [searchResults addObject:dataSource[i]];
    //
    //                        }
    //                    }
    //                }
    //            }
    //        }
    //    } else if (mySearchBar.text.length>0&&[ChineseInclude isIncludeChineseInString:mySearchBar.text]) {
    //        for (NSString *tempStr in dataArray) {
    //            NSRange titleResult=[tempStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
    //            if (titleResult.length>0) {
    //                [searchResults addObject:tempStr];
    //            }
    //        }
    //    }
    
    NSString *tempStr;
    NSString *regex = @"[0-9]";
    NSString *search_str;
    
    if(mySearchBar.text.length>1||mySearchBar.text.length==1)
        search_str = [mySearchBar.text substringToIndex:1];
    
    //NSLog(@"%@",_dataSource);
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    for (NSDictionary *dict in _dataSource)
    {
        
        if ([predicate evaluateWithObject:search_str] == YES)
        {
            tempStr = [dict objectForKey:@"F_Phone"];
        }
        else
        {
            tempStr = [dict objectForKey:@"F_Title"];
            
        }
        
        
        NSRange titleResult=[tempStr rangeOfString:mySearchBar.text options:NSCaseInsensitiveSearch];
        if (titleResult.length>0)
        {
            [self.resultDataArr addObject:dict];
        }
        [resultTableView reloadData];
        //NSLog(@"%@",self.resultDataArr);
        
    }
    
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if(decelerate)
    {
        
        [AppSession resignKeyBoardInView:self.view];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
