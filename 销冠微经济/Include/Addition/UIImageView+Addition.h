//
//  UIImageView+Addition.h
//  vanke
//
//  Created by xu shun wang on 13-5-31.
//
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"
@interface UIImageView (Addition)
- (void)setWebImage:(NSString *)url;

- (void)setWebImage:(NSString *)url placeholderImage:(UIImage *)placeholder;

- (void)setWebImageWithFade:(NSString *)url;

- (void)setWebImageWithFade:(NSString *)url placeholderImage:(UIImage *)placeholder;

@end
