//
//  AgreementVC.h
//  销冠微经济
//
//  Created by 朱晓龙 on 14-10-6.
//  Copyright (c) 2014年 朱晓龙. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AgreementVC : BaseViewController<UIWebViewDelegate>
{
    UIWebView *webView;
    
    BOOL isYseOrNo;
    
    int agree_int;

}

@property(nonatomic,strong)NSString *buildingKid_str;
@property(nonatomic,strong)NSString *AgreementKid_str;
@property(nonatomic)BOOL Agreement;
@end
