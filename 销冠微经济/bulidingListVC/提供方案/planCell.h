//
//  planCell.h
//  HRVTableView
//
//  Created by liu on 14-10-9.
//  Copyright (c) 2014年 Hamidreza Vakilian. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "starView.h"

@interface planCell : UITableViewCell
{
    UIEdgeInsets insets;
}
@property(nonatomic,retain)UILabel *labelTitle;
@property(nonatomic,retain)UILabel *labelContent;;
@property(nonatomic,retain)UILabel *labelDetial;
@property(nonatomic,retain)UIImageView *expandGlyph;
@property(nonatomic,retain)UIImageView *bgdImage;
@property(nonatomic,retain)starView *starView;
@property(nonatomic,retain)UIImageView *lineView;

-(void)content:(NSDictionary *)dataDic cellForRowAtIndexPath:(NSIndexPath *)indexPath;
@end
