//
//  pickerViewController.m
//  test_1
//
//  Created by liu on 14-10-12.
//  Copyright (c) 2014年 liu. All rights reserved.
//

#import "pickerViewController.h"
#import "BATableView.h"
#import <AddressBook/AddressBook.h>
#import "THContact.h"
#import "contact_Cell.h"

@interface pickerViewController ()<BATableViewDelegate>
{
    NSMutableArray *searchArray;
    NSMutableArray *selectArray;
}
@property (nonatomic, strong) BATableView *contactTableView;
@property (nonatomic, strong) NSMutableDictionary *dicArray;
@property (nonatomic, strong) NSArray * dataSource;
@property(strong,nonatomic)UISearchBar *searchBar;
@property(strong,nonatomic)UISearchDisplayController *searchDisPlayCon;
@end

@implementation pickerViewController

// 创建tableView
- (void) createTableView
{
   self.navigationLabel.text = @"本地导入";
    
    float  addHight = 0;
    if(IOS_7)
        addHight = 20;
    
    float  addHeight1=0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight1=44;

    UIButton *tagBtn2=[UIButton buttonWithType:UIButtonTypeCustom];
    tagBtn2.frame=CGRectMake(245,3,80,44);
    [tagBtn2 setTitle:@"提交"forState:UIControlStateNormal];
    [tagBtn2 setTitleColor:[AppSession colorWithHexString:@"ffffff"] forState:UIControlStateNormal];
    [tagBtn2 addTarget:self action:@selector(ddd) forControlEvents:UIControlEventTouchUpInside];
    tagBtn2.titleLabel.font = [UIFont systemFontOfSize:15];
    tagBtn2.titleEdgeInsets = UIEdgeInsetsMake(0,0,0,0);
    [self.navigationBg addSubview:tagBtn2];

    self.contactTableView = [[BATableView alloc] initWithFrame:CGRectMake(0,44+addHight,320,self.view.bounds.size.height-44-addHight)];
    self.contactTableView.delegate = self;
    self.contactTableView.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.contactTableView];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        //self.title = @"本地导入";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self getContactsFromAddressBook];
    [self createTableView];

  //  [self getPersonInfo];
    _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 44)];
    _searchBar.delegate = self;
    _searchBar.placeholder = @"搜索联系人";
   // self.contactTableView.tableView.tableHeaderView = _searchBar;
    _searchDisPlayCon = [[UISearchDisplayController alloc]initWithSearchBar:_searchBar contentsController:self];
    _searchDisPlayCon.delegate = self;
    [self setSearchDisPlayCon:_searchDisPlayCon];
    _searchDisPlayCon.searchResultsDataSource = self;
    _searchDisPlayCon.searchResultsDelegate = self;
    searchArray = [[NSMutableArray alloc]init];
    selectArray = [[NSMutableArray alloc]init];
    
    UIButton* leftButton= [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 60, 22)];
    [leftButton setTitle:@"全选" forState:UIControlStateNormal];
    [leftButton setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
    leftButton.titleLabel.font  =[UIFont systemFontOfSize:16];
    [leftButton setSelected:YES];
    leftButton.adjustsImageWhenHighlighted = FALSE;
    [leftButton addTarget:self action:@selector(selectAll:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc]initWithCustomView:leftButton];
    self.navigationItem.rightBarButtonItem = leftItem;
}

-(void)selectAll:(id)sender
{
    UIButton *button = (UIButton *)sender;
    if ([button.titleLabel.text isEqualToString:@"全选"]) {
        for (NSArray *array in self.dicArray.allValues) {
            for (THContact *thCon in array) {
                thCon.isSelect = YES;
            }
        }
        [button setTitle:@"取消" forState:UIControlStateNormal];
    } else {
        for (NSArray *array in self.dicArray.allValues) {
            for (THContact *thCon in array) {
                thCon.isSelect = NO;
            }
        }
        [button setTitle:@"全选" forState:UIControlStateNormal];
    }
    
    [self.contactTableView.tableView reloadData];
}


#pragma mark - 取得联系人数据
-(void)getContactsFromAddressBook
{
    self.dicArray = [[NSMutableDictionary alloc]init];
    
    //取得本地通信录名柄
    ABAddressBookRef addressBook ;
    
    ABAddressBookGetAuthorizationStatus();
    
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)    {
        addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        //等待同意后向下执行
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error)                                                 {                                                     dispatch_semaphore_signal(sema);                                                 });
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }else{
        addressBook = ABAddressBookCreate();
    }
    
    
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        
        NSArray *allContacts = (__bridge_transfer NSArray*)ABAddressBookCopyArrayOfAllPeople(addressBook);
        
       // NSLog(@"%@",allContacts);
        NSMutableArray *mutableContacts = [NSMutableArray arrayWithCapacity:allContacts.count];
        
        NSUInteger i = 0;
        for (i = 0; i<[allContacts count]; i++)
        {
            THContact *contact = [[THContact alloc] init];
            ABRecordRef contactPerson = (__bridge ABRecordRef)allContacts[i];
            contact.recordId = ABRecordGetRecordID(contactPerson);
            
            // Get first and last names
            NSString *firstName = (__bridge_transfer NSString*)ABRecordCopyValue(contactPerson, kABPersonFirstNameProperty);
            NSString *lastName = (__bridge_transfer NSString*)ABRecordCopyValue(contactPerson, kABPersonLastNameProperty);
            
            // Set Contact properties
            contact.firstName = firstName;
            contact.lastName = lastName;
            
            // Get mobile number
            ABMultiValueRef phonesRef = ABRecordCopyValue(contactPerson, kABPersonPhoneProperty);
            contact.phone = [self getMobilePhoneProperty:phonesRef];
            if(phonesRef) {
                CFRelease(phonesRef);
            }
            
            // Get image if it exists
            NSData  *imgData = (__bridge_transfer NSData *)ABPersonCopyImageData(contactPerson);
            contact.image = [UIImage imageWithData:imgData];
            if (!contact.image) {
                contact.image = [UIImage imageNamed:@"icon-avatar-60x60"];
            }
            [mutableContacts addObject:contact];
            
            NSString *nameStr = [NSString stringWithFormat:@"%@%@",lastName?lastName:@"",firstName?firstName:@""];
            NSString *phonetic = [self phonetic:nameStr];
            if (phonetic.length  > 1 ) {
                phonetic = [phonetic substringToIndex:1];
                //NSLog(@"%@",phonetic);
            }
            NSArray *phArray = [self.dicArray objectForKey:phonetic];
            NSMutableArray *lastArray =[[NSMutableArray alloc]initWithArray:phArray];
            [lastArray addObject:contact];
            [self.dicArray setObject:lastArray forKey:phonetic];
        }
        [self.contactTableView reloadData];
        if(addressBook) {
            CFRelease(addressBook);
        }
    }
    else{
        NSLog(@"Error");
    }
}

- (NSString *)getMobilePhoneProperty:(ABMultiValueRef)phonesRef
{
    for (int i=0; i < ABMultiValueGetCount(phonesRef); i++) {
        CFStringRef currentPhoneLabel = ABMultiValueCopyLabelAtIndex(phonesRef, i);
        CFStringRef currentPhoneValue = ABMultiValueCopyValueAtIndex(phonesRef, i);
        
        if(currentPhoneLabel) {
            if (CFStringCompare(currentPhoneLabel, kABPersonPhoneMobileLabel, 0) == kCFCompareEqualTo) {
                return (__bridge NSString *)currentPhoneValue;
            }
            
            if (CFStringCompare(currentPhoneLabel, kABHomeLabel, 0) == kCFCompareEqualTo) {
                return (__bridge NSString *)currentPhoneValue;
            }
        }
        if(currentPhoneLabel) {
            CFRelease(currentPhoneLabel);
        }
        if(currentPhoneValue) {
            CFRelease(currentPhoneValue);
        }
    }
    return nil;
}

#pragma mark -汉字转拼音
- (NSString *) phonetic:(NSString*)sourceString {
    NSMutableString *source = [sourceString mutableCopy];
    CFStringTransform((__bridge CFMutableStringRef)source, NULL, kCFStringTransformMandarinLatin, NO);
    CFStringTransform((__bridge CFMutableStringRef)source, NULL, kCFStringTransformStripDiacritics, NO);
    return source;
}


#pragma mark -
#pragma mark - UISearchDisplayController delegate methods

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller
shouldReloadTableForSearchString:(NSString *)searchString
{
    // 输入框变化
    // Return YES to cause the search result table view to be reloaded.
    [self filterContentForSearchText:searchString scope:[self.searchDisplayController.searchBar                                       selectedScopeButtonIndex]];
    return YES;
}

- (BOOL)searchDisplayController:(UISearchDisplayController *)controller  shouldReloadTableForSearchScope:(NSInteger)searchOption
{
    // 切换搜索方式
    [self filterContentForSearchText:[self.searchDisplayController.searchBar text] scope:searchOption];
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSInteger )scopeOption
{
    [searchArray removeAllObjects];
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[cd] %@",searchText];
    for (NSArray *array in self.dicArray.allValues) {
        for (THContact *thCon in array) {
            NSString *name = [NSString stringWithFormat:@"%@%@",thCon.lastName?thCon.lastName:@"",thCon.firstName?thCon.firstName:@""];
             if ([resultPredicate evaluateWithObject:name]) {
                 [searchArray addObject:thCon];
             }
            if ([resultPredicate evaluateWithObject:thCon.phone]) {
                [searchArray addObject:thCon];
            }
        }
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.contactTableView hideFlotage];
}

#pragma mark - UITableViewDataSource
- (NSArray *) sectionIndexTitlesForABELTableView:(BATableView *)tableView {

    return self.dicArray.allKeys;
}

- (NSString *)titleString:(NSInteger)section
{
    return self.dicArray.allKeys[section];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    return self.dicArray.allKeys[section];
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    if ([tableView isEqual:_searchDisPlayCon.searchResultsTableView]) {
        return 1;
    }
    return self.dicArray.count;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([tableView isEqual:_searchDisPlayCon.searchResultsTableView]) {
        return searchArray.count;
    }
    NSArray *array = [self.dicArray objectForKey:self.dicArray.allKeys[section]];
    return array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * cellName = [NSString stringWithFormat:@"UITableViewCell%li",(long)indexPath.row];

    contact_Cell * cell = [tableView dequeueReusableCellWithIdentifier:cellName];
    if (cell == nil) {
        cell = [[contact_Cell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellName];
    }
    
   // NSLog(@"%@",self.dicArray);
    THContact *thCon = [THContact new];
    if ([tableView isEqual:_searchDisPlayCon.searchResultsTableView]) {
        thCon = [searchArray objectAtIndex:indexPath.row];
    } else {
        NSArray *array = [self.dicArray objectForKey:self.dicArray.allKeys[indexPath.section]];
        thCon = [array objectAtIndex:indexPath.row];
    }
   
    cell.nameLabel.text = [NSString stringWithFormat:@"%@%@",thCon.lastName?thCon.lastName:@"",thCon.firstName?thCon.firstName:@""];
    cell.phoneLabel.text = thCon.phone;
    
    UIImage *imageSE = [UIImage imageNamed:@"icon-checkbox-selected-green-25x25@2x.png"];
    UIImage *image = [UIImage imageNamed:@"icon-checkbox-unselected-25x25@2x.png"];
    cell.selectImage.image = thCon.isSelect?imageSE:image;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"%i,%i",indexPath.section,indexPath.row);
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    THContact *thCon = [THContact new];
    NSMutableArray *array = [[NSMutableArray alloc]init];
    if ([tableView isEqual:_searchDisPlayCon.searchResultsTableView]) {
        thCon = [searchArray objectAtIndex:indexPath.row];
    } else {
        array = [self.dicArray objectForKey:self.dicArray.allKeys[indexPath.section]];
        thCon = [array objectAtIndex:indexPath.row];
    }
    if (thCon.isSelect) {
        thCon.isSelect = NO;
        for (int n = 0; n < selectArray.count ; n ++ ) {
            THContact *thcont = selectArray[n];
            if ([thCon.phone isEqualToString:thcont.phone]) {
                [selectArray removeObjectAtIndex:n];
            }
        }
    } else {
        thCon.isSelect = YES;
        [selectArray addObject:thCon];
    }
     if ([tableView isEqual:_searchDisPlayCon.searchResultsTableView]) {
        [searchArray setObject:thCon atIndexedSubscript:indexPath.row];
     } else {
         [array setObject:thCon atIndexedSubscript:indexPath.row];
         [self.dicArray setObject:array forKey:self.dicArray.allKeys[indexPath.section]];
     }
    
    
    
    [self.contactTableView.tableView reloadData];
}

-(void)ddd
{
    
    NSString *broker_str = [NSString stringWithFormat:@"\"BrokerKid\":\"%@\"",theApp.brokerKid];

    NSMutableString *jsonString = [[NSMutableString alloc] initWithString:[NSString stringWithFormat:@"{%@,\"Item\":[",broker_str]];
    THContact *thCon = [THContact new];

    for(thCon in selectArray)
    {
        NSString *string;
        
        NSString *nameStr = [NSString stringWithFormat:@"%@%@",thCon.lastName,thCon.firstName];
        
        NSString *timeStr = [NSString stringWithFormat:@"%@",thCon.phone];
        
        timeStr  = [timeStr stringByReplacingOccurrencesOfString:@"-" withString:@""];
        NSCharacterSet *set = [NSCharacterSet characterSetWithCharactersInString:@"@／：；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$€^•'@#$%^&*()_+'\""];
        
        NSString *trimmedString = [timeStr stringByTrimmingCharactersInSet:set];
        trimmedString  = [trimmedString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        string  = [NSString stringWithFormat:
                   @"{\"Name\":\"%@\",\"Phone\":\"%@\"},",nameStr,trimmedString];
        
        [jsonString appendString:string];
    }
    
    
    NSUInteger location = [jsonString length]-1;
    
    NSRange range       = NSMakeRange(location, 1);
    
    // 4. 将末尾逗号换成结束的]}
    

    [jsonString replaceCharactersInRange:range withString:@"]}"];
    
   // NSLog(@"jsonString = %@",jsonString);

     [self uploadrecommend:jsonString];

}

-(void)uploadrecommend:(NSString *)jsonString
{
    NSString *str = [NSString stringWithFormat:@"http://apiweixin.tops001.com/api.aspx?api=api.weixincustomer.batchimport&CustomerList=%@",jsonString];
    str =  [str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

   // NSLog(@"%@",str);
    [self startSynchronousRequest:str target:self.view alertText:@"数据加载中....."  didFinishRequest:^(ASIHTTPRequest *request)
     {
         
         NSDictionary *dict= [request.responseString objectFromJSONString];
         
         if([[dict objectForKey:@"code"] intValue] ==0)
         {
             //[self.navigationController popViewControllerAnimated:YES];
        }
         [self showMBAlertView:self.view message:[dict objectForKey:@"msg"] duration:1.5];

         
     }
                 didFailedRequest:^(ASIHTTPRequest *request){
                     
                 }];


}
@end
