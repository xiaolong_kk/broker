//
//  MainTabBarViewController.m
//  TopSales
//
//  Created by 朱晓龙 on 13-9-20.
//
//

#import "MainTabBarViewController.h"
#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
@interface MainTabBarViewController ()

@end

@implementation MainTabBarViewController

@synthesize customTabBarView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.view.backgroundColor = [AppSession colorWithHexString:@"e5e2db"];
    
    [self hideExistingTabBar];
    
    self.delegate =self;
    
    self.customTabBarView = [[TabBarView alloc] initWithFrame:CGRectMake(0,self.view.frame.size.height-50, 320,50)];
    self.customTabBarView.backgroundColor = [UIColor whiteColor];
    self.customTabBarView.delegate = self;
    [self.view addSubview:self.customTabBarView];
    
}

- (void)hideExistingTabBar
{
	for(UIView *view in self.view.subviews)
	{
		if([view isKindOfClass:[UITabBar class]])
		{
			view.hidden = YES;
			break;
		}
	}
}



#pragma mark ALTabBarDelegate

-(void)tabWasSelected:(NSInteger)index
{
    
    //[AppSession playSound:1002];
    self.selectedIndex = index;
    
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
