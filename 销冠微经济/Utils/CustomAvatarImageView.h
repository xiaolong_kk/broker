//
//  CustomAvatarImageView.h
//  greentea
//
//  Created by xu shun wang on 13-6-17.
//
//

#import <UIKit/UIKit.h>
#import "UIImageView+WebCache.h"

@interface CustomAvatarImageView : UIView<SDWebImageManagerDelegate>
@property (nonatomic,retain) UIImage *image;
- (id)initWithPosition:(CGPoint)position radius:(float)radius;

- (void) setImageWithURL:(NSString *)url;
- (void)setWebImage:(NSString *)url placeholderImage:(UIImage *)placeholder;

@end
