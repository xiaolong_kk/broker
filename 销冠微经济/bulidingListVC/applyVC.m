//
//  applyVC.m
//  销冠微经济
//
//  Created by zen huang on 14/9/21.
//  Copyright (c) 2014年 zen huang. All rights reserved.
//

#import "applyVC.h"
#import "applyListVC.h"
@interface applyVC ()

@end

@implementation applyVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    float addHeight = 0;
    if(IOS_7)
        addHeight = 20;
    float addHeight2 = 0;
    if([[UIScreen mainScreen] bounds].size.height==568)
        addHeight2 = 100;
     self.navigationLabel.text = @"我的申请";
   
    
    UITableView *setingTableView=[[UITableView alloc] initWithFrame:CGRectMake(0,45+addHeight,320,380+addHeight2) style:UITableViewStylePlain];
    setingTableView.dataSource=self;
    setingTableView.delegate=self;
    setingTableView.scrollEnabled = YES;
    setingTableView.backgroundColor = [UIColor clearColor];
    setingTableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    setingTableView.userInteractionEnabled=YES;
    [self.view addSubview:setingTableView];

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell==nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
        cell.layer.masksToBounds=YES;
        
        cell.backgroundColor = [UIColor clearColor];
        
        
        UIImageView *bgImg = [[UIImageView alloc] init];
        bgImg.backgroundColor = [UIColor whiteColor];
        bgImg.frame = CGRectMake(0,0, 320,45);
        [cell addSubview:bgImg];
        
        
        UIButton *logoBtn = [[UIButton alloc] initWithFrame:CGRectMake(15,0,45,45)];
        [logoBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"icon_a%d.png",indexPath.row+1]] forState:UIControlStateNormal];
        //logoBtn.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_p%d.png",i+1]];
        [cell addSubview:logoBtn];

        NSArray *tempArr = [[NSArray alloc] init];
        
        
        tempArr = [[NSArray alloc] initWithObjects:@"到访申请",@"认筹申请",@"认购申请",@"成交申请",nil];
    
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60,0,100,45)];
        titleLabel.font=[UIFont fontWithName:strinBoldFontOfSize size:14];;
        titleLabel.textColor = [AppSession colorWithHexString:@"333333"];
        titleLabel.text = [tempArr objectAtIndex:indexPath.row];
        titleLabel.backgroundColor = [UIColor clearColor];
        [cell addSubview:titleLabel];
        
        UIImageView *arrowImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"arrow_go4.png"]];
        arrowImg.frame = CGRectMake(290,15,8,13);
        [cell addSubview:arrowImg];
        
        
        if(indexPath.row ==0)
        {
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,0,320,1);
            [cell addSubview:lineImg];
        }
        if(indexPath.row ==4)
        {
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(0,44,320,1);
            [cell addSubview:lineImg];
        }
        else
        {
            UIImageView *lineImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mainview_line_long.png"]];
            lineImg.frame = CGRectMake(15,44,290,1);
            [cell addSubview:lineImg];
        }
    }
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    
    return cell;
    
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    applyListVC *vc = [[applyListVC alloc]initWithNavigationBar:YES];
    if(indexPath.row==0)
    {
        vc.state_str = @"2";
        vc.title_str = @"到访客户";
    }
    if(indexPath.row==1)
    {
        vc.state_str = @"3";
        vc.title_str = @"认筹客户";
    }
    if(indexPath.row==2)
    {
        vc.state_str = @"4";
        vc.title_str = @"认购客户";
    }
    if(indexPath.row==3)
    {
        vc.state_str = @"5";
        vc.title_str = @"成交客户";
    }
//    if(indexPath.row==4)
//    {
//        vc.state_str = @"5";
//        vc.title_str = @"超级客户";
//    }


    [theApp.tabBarController.navigationController pushViewController:vc animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
