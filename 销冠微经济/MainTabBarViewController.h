//
//  MainTabBarViewController.h
//  TopSales
//
//  Created by 朱晓龙 on 13-9-20.
//
//

#import <UIKit/UIKit.h>
#import "TabBarView.h"

@interface MainTabBarViewController : UITabBarController<ALTabBarDelegate,UITabBarControllerDelegate>
{
    
    BOOL isNOSelected;
}

@property (nonatomic, retain) TabBarView *customTabBarView;

@end
